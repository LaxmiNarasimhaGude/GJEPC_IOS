//
//  StatisticsImportAndExportViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 15/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
class StatisticsImportAndExportViewController: UIViewController,UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var headingLabel: UILabel!
    
     var pdfUrl1 : String = ""
     var activityLoader : MBProgressHUD? = nil
     var heading : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.delegate  = self
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0
        webView.backgroundColor = UIColor.white
        if heading == "IMPORT"{
            let url1 : String =  statisticImportPdfUrl
            let url2 :  String = pdfUrl1
            let url3 = url1 + url2
            let replacedString = url3.replacingOccurrences(of: " ", with: "%20")
            let url = NSURL(string: replacedString)
            webView.loadRequest(URLRequest(url: (url as URL?)!) as URLRequest)
            loadActivityIndicator()
            webView.scalesPageToFit = true
        }
        else if(heading == "EXPORT"){
                   let url1 : String =  statisticExportPdfUrl
                   let url2 :  String = pdfUrl1
                   let url3 = url1 + url2
                   let replacedString = url3.replacingOccurrences(of: " ", with: "%20")
                   let url = NSURL(string: replacedString)
                   print(url as Any)
                   webView.loadRequest(URLRequest(url: url! as URL) as URLRequest)
            loadActivityIndicator()
                   webView.scalesPageToFit = true
        }
        else if(heading == "ARCHIVE EXPORT"){
            let url1 : String =  archivePdfurl
                              let url2 :  String = pdfUrl1
                              let url3 = url1 + url2
                              let replacedString = url3.replacingOccurrences(of: " ", with: "%20")
                              let url = NSURL(string: replacedString)
                              print(url as Any)
                              webView.loadRequest(URLRequest(url: url! as URL) as URLRequest)
            loadActivityIndicator()
                              webView.scalesPageToFit = true
        }
        else if(heading == "ARCHIVE IMPORT"){
                              let url1 : String =  archiveImportPdfurl
                              let url2 :  String = pdfUrl1
                              let url3 = url1 + url2
                              let replacedString = url3.replacingOccurrences(of: " ", with: "%20")
                              let url = NSURL(string: replacedString)
                              print(url as Any)
                              webView.loadRequest(URLRequest(url: url! as URL) as URLRequest)
                              loadActivityIndicator()
                              webView.scalesPageToFit = true
        }
        else{
            
        

                              let url1 : String =  statisticsTradePdfurl
                              let url2 :  String = pdfUrl1
                              let url3 = url1 + url2
                              let replacedString = url3.replacingOccurrences(of: " ", with: "%20")
                              let url = NSURL(string: replacedString)
                              print(url as Any)
                              webView.loadRequest(URLRequest(url: url! as URL) as URLRequest)
                              loadActivityIndicator()
                              webView.scalesPageToFit = true
//        }
        }
    }
    
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
   func loadActivityIndicator()
        {
            activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
            activityLoader?.label.text = "Loading";
            activityLoader?.detailsLabel.text = "Please Wait";
            activityLoader?.isUserInteractionEnabled = false;
        }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        activityLoader?.hide(animated: true)
    }
}
