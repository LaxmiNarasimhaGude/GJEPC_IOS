//
//  AnalyticsTableViewCell.swift
//  GJEPC
//
//  Created by Kwebmaker on 20/04/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit

class AnalyticsTableViewCell: UITableViewCell {

    @IBOutlet weak var bagroundview: UIView!
    @IBOutlet weak var analyticLabel: UILabel!
    @IBOutlet weak var analyticImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
