//
//  AnalyticsViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 23/03/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
class AnalyticsViewController: UIViewController,dataReceivedFromServerDelegate,UITableViewDelegate,UITableViewDataSource {
   
    
   
    

    @IBOutlet weak var analyticsTableview: UITableView!
    
    
    var requestToServer = RequestToServer()
    var activityLoader : MBProgressHUD? = nil
    var tableViewArrayForCircularmember : NSMutableArray = []
    var type = ""
    var replacedImgstring : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
print(type)
//        bagroungView.layer.cornerRadius = 10.0
//  bagroungView.layer.shadowColor = UIColor.darkGray.cgColor
//  bagroungView.layer.shadowOffset = CGSize(width: 5.0, height: 5.0)
//  bagroungView.layer.shadowRadius = 25.0
//  bagroungView.layer.shadowOpacity = 0.3
//
//  analyticsImage.layer.cornerRadius = 10.0
//  analyticsImage.clipsToBounds = true
        
        requestToServer.delegate = self
        analyticsTableview.delegate = self
        analyticsTableview.dataSource = self
        checkforInternet()
        
//        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
//           analyticsImage.isUserInteractionEnabled = true
//           analyticsImage.addGestureRecognizer(tapGestureRecognizer)
    }
    
//    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
//    {
//
//
////        self.clipsToBounds = true
////        self.layer.cornerRadius = 10
//    }
    func checkforInternet(){
        if CheckInternet.isConnectedToNetwork() == true{
            DispatchQueue.main.async {
                self.loadActivityIndicator()
            }
            if (type == "TRADEINFORMATION"){
               requestToServer.getDataFromServer(myGetUrl: tradeInfo!)
            }
            else{
               requestToServer.getDataFromServer(myGetUrl: analyticsUrl!)
            }
        }
    }
    func dataReceivedFromServer(data: NSDictionary, url: URL) {
                       if ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "true" {
                   let temporaryArray = ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "Result") as! NSArray)
                   if temporaryArray.count > 0 {
                    tableViewArrayForCircularmember = (temporaryArray.mutableCopy() as! NSMutableArray)

                    DispatchQueue.main.async {
                       
                        self.analyticsTableview.reloadData()
                        self.activityLoader?.hide(animated: true)
                    }
                   
   

               }
               }
           }
   
   func loadActivityIndicator()
   {
       activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
       activityLoader?.label.text = "Loading";
       activityLoader?.detailsLabel.text = "Please Wait";
       activityLoader?.isUserInteractionEnabled = false;
   }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewArrayForCircularmember.count
       }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         let statisticImport = self.storyboard?.instantiateViewController(withIdentifier: "StatisticsImportAndExportViewController") as! StatisticsImportAndExportViewController
        let pdfUrl1 = ( (self.tableViewArrayForCircularmember.object(at: indexPath.row) as! NSDictionary).object(forKey: "pdf") as! String)
               
               statisticImport.pdfUrl1 = pdfUrl1
        
        if(type == "TRADEINFORMATION"){
            statisticImport.heading = "TRADEINFORMATION"
        }
        else{
            statisticImport.heading = "ANALYTICS"
        }
              self.navigationController?.pushViewController(statisticImport, animated: true)

    }
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AnalyticsTableViewCell", for: indexPath) as! AnalyticsTableViewCell
        cell.analyticLabel.text = (((self.tableViewArrayForCircularmember.object(at: indexPath.row) as! NSDictionary)["name"]) as!
        String)
        
        DispatchQueue.main.async {
             cell.bagroundview.layer.cornerRadius = 2.0
             cell.bagroundview.layer.borderWidth = 1.0
             cell.bagroundview.layer.borderColor = UIColor.clear.cgColor
             cell.bagroundview.layer.masksToBounds = true
            
 cell.bagroundview.layer.masksToBounds = false
            cell.bagroundview.layer.shadowColor = UIColor.black.cgColor
            cell.bagroundview.layer.shadowOpacity = 0.3
 cell.bagroundview.layer.shadowOffset = CGSize(width: -1, height: 1)
 cell.bagroundview.layer.shadowRadius = 1



        }
        
        var replacedImgstring = (((self.tableViewArrayForCircularmember.object(at: indexPath.row) as! NSDictionary)["image"]) as!
                            String)
        print(replacedImgstring)
        print(type)
        
//        if(type == "TRADEINFORMATION"){
            let imgfilterUrl = statisticsImagrUrl + replacedImgstring
              print(imgfilterUrl)
            self.replacedImgstring = imgfilterUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!

            
            cell.analyticImage.sd_setImage(with: URL(string: self.replacedImgstring))

//        }
//        else{
//    URLSession.shared.dataTask(with: NSURL(string: replacedImgstring)! as URL, completionHandler: { (data, response, error) -> Void in
//
//            if error != nil {
//                return
//                        }
//            else {
//            DispatchQueue.main.async(execute: { () -> Void in
//            let image = UIImage(data: data!)
//            print(data as Any)
//            if (image != nil) {
//cell.analyticImage.image = image
////                                                            print(image as Any)
//self.activityLoader?.hide(animated: true)
//                                                      }
//                                                  })
//                                              }
//                                          }).resume()
////            URLSession.shared.dataTask(with: URL(string: replacedImgstring)!) { data, response, error in
////                guard
////                    let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
////                    let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
////                    let data = data, error == nil,
////                    let image = UIImage(data: data)
////                    else { return }
////                DispatchQueue.main.async() { [weak self] in
////                    cell.analyticImage.image = image
////                }
////            }.resume()
//        }
        return cell
       }
    
}
//extension UIImageView {
//    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
//        contentMode = mode
//        URLSession.shared.dataTask(with: url) { data, response, error in
//            guard
//                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
//                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
//                let data = data, error == nil,
//                let image = UIImage(data: data)
//                else { return }
//            DispatchQueue.main.async() { [weak self] in
//                self?.image = image
//            }
//        }.resume()
//    }
//    func downloaded(from link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
//        guard let url = URL(string: link) else { return }
//        downloaded(from: url, contentMode: mode)
//    }
//}
