//
//  ArchiveViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 19/03/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
class ArchiveViewController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource,dataReceivedFromServerDelegate,UITableViewDataSource,UITableViewDelegate {
    
    
   
    

    @IBOutlet weak var BagroundView: UIView!
    @IBOutlet weak var selectYear: UITextField!
     
    @IBOutlet weak var archiveTableView: UITableView!
    
     var yearArray : NSMutableArray = []
     var activityLoader : MBProgressHUD? = nil
     var pickerView = UIPickerView()
     var custom = Custom_ObjectsViewController()
     var requestToServer = RequestToServer()
    var tableViewArrayForCircularmember : NSMutableArray = []
    override func viewDidLoad() {
        super.viewDidLoad()

        archiveTableView.dataSource = self
        archiveTableView.delegate = self
          requestToServer.delegate = self
        yearArray = ["Select Statistics","Statistics Import","Statistics Export"]
        
        pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        manageToolBar()
    }
    
    func manageToolBar() {
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        //toolBar.isTranslucent = true
        toolBar.tintColor = custom.hexStringToUIColor(hex: "#1862AF")
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(ArchiveViewController.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(CircularMemberViewController.cancelPicker))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        selectYear.inputView = pickerView
        selectYear.inputAccessoryView = toolBar
    }
        func numberOfComponents(in pickerView: UIPickerView) -> Int {
               return 1
           }
        func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    //        searchBar.isHidden = true
    //        cancelButton.isHidden = true
    //        searchButton.isHidden = false
    //        headingLabel.isHidden = false
            return yearArray.count
        }
        
        func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
               return yearArray[row] as? String
           }
        func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            selectYear.text = yearArray[row] as? String
        }
        
        override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
              self.view.endEditing(true)
          }

   @objc func donePicker()
      {
        if (selectYear.text == "Statistics Export"){
            if CheckInternet.isConnectedToNetwork() == true{
                loadActivityIndicator()
                archiveTableView.isHidden = false
                requestToServer.getDataFromServer(myGetUrl: exportArchive!)
            }
            
            
        }
        else if (selectYear.text == "Statistics Import"){
            if CheckInternet.isConnectedToNetwork() == true{
                loadActivityIndicator()
                           archiveTableView.isHidden = false
                           requestToServer.getDataFromServer(myGetUrl: importArchive!)
                       }
                       
        }
        selectYear.resignFirstResponder()
        archiveTableView.isScrollEnabled = true
      }

    func dataReceivedFromServer(data: NSDictionary, url: URL) {
                   if ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "true" {
               let temporaryArray = ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "Result") as! NSArray)
               if temporaryArray.count > 0 {
                tableViewArrayForCircularmember = (temporaryArray.mutableCopy() as! NSMutableArray)
//                DispatchQueue.main.async {
//
//                    self.loadActivityIndicator()
//
//                }
                
                DispatchQueue.main.async {
                    
                    self.archiveTableView.reloadData()
                    self.activityLoader?.hide(animated: true)
                }
               
           }
           }

       }
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return   tableViewArrayForCircularmember.count
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = archiveTableView.dequeueReusableCell(withIdentifier: "ArchiveTableViewCell") as! ArchiveTableViewCell
        
        DispatchQueue.main.async {
                                         //
                           cell.BagroundView.backgroundColor = UIColor.white
               
                           cell.BagroundView.layer.cornerRadius = 3.0
                           cell.BagroundView.layer.masksToBounds = false
                                         
                                         
                           cell.BagroundView.layer.shadowOffset = CGSize(width: 0, height: 0)
                           cell.BagroundView.layer.shadowOpacity = 0.3

                                     }
        cell.layer.borderWidth = 0.5
                 cell.layer.borderColor = UIColor.lightGray.cgColor
                 cell.selectionStyle = .none
        
        
        cell.title.text = (((self.tableViewArrayForCircularmember.object(at: indexPath.row) as! NSDictionary)["name"]) as!
            String)
        cell.title.labelSpacing(label:  cell.title, font: "Bold")
        
//        cell.title.setCharacterSpacing(1.1)
//               cell.title.setLineSpacing(lineSpacing: 5,lineHeightMultiple: 0)
        return cell
       }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         let statisticImport = self.storyboard?.instantiateViewController(withIdentifier: "StatisticsImportAndExportViewController") as! StatisticsImportAndExportViewController
        
        if(selectYear.text == "Statistics Export"){
            let pdfUrl1 = ( (self.tableViewArrayForCircularmember.object(at: indexPath.row) as! NSDictionary).object(forKey: "upload_statistics_export") as! String)
                               statisticImport.heading = "ARCHIVE EXPORT"
                               statisticImport.pdfUrl1 = pdfUrl1
        }
        else if(selectYear.text == "Statistics Import"){
            let pdfUrl1 = ( (self.tableViewArrayForCircularmember.object(at: indexPath.row) as! NSDictionary).object(forKey: "upload_statistics_import") as! String)
            statisticImport.heading = "ARCHIVE IMPORT"
            statisticImport.pdfUrl1 = pdfUrl1
        }
                   
        
        self.navigationController?.pushViewController(statisticImport, animated: true)
              
    }
    func loadActivityIndicator()
    {
        activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
        activityLoader?.label.text = "Loading";
        activityLoader?.detailsLabel.text = "Please Wait";
        activityLoader?.isUserInteractionEnabled = false;
    }
}
