//
//  Search_ChildViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 21/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
class Search_ChildViewController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource, UITextFieldDelegate {

    

   
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var resetBtn: UIButton!
    @IBOutlet weak var commodityNo: UITextField!
    @IBOutlet weak var selectQtr2: UITextField!
    @IBOutlet weak var selectQtr1: UITextField!
    @IBOutlet weak var selectYear: UITextField!
    @IBOutlet weak var selectQtr: UITextField!
    @IBOutlet weak var selectMonth: UITextField!
    @IBOutlet weak var quarterDropdownImg: UIImageView!
    @IBOutlet weak var monthDropdownImg: UIImageView!
    
    var months : NSArray = ["Select Months To Display","January","February","March","April","May","June","July","August","September","October","November","December"]
    var quarter : NSArray = ["Select Quarter To Display","Quarter I","Quarter II","Quarter III","Quarter IV"]
    var yearArray : NSArray = ["Select Year To Display","2019","2018","2017","2016","2015","2014","2013","2012","2011","2010","2009","2008","2007","2006","2005","2004","2003","2002","2001","2000"]
    var commodityNameArray : NSArray = ["Enter Gold Bar","Colored Gem", "Costume Fashion Jew", "Cut & Polish Diamond", "Gold Bar", "Gold Findings", "Gold Jewellery", "Gold Medallions,Coin", "Raw Pearls", "Rough Colored Gemstone ", "Rough Diamond", "Rough Synthetic Stones", "Non Gold Jewellery", "Pearls", "Platinum", "Silver Bar", "Synthetic Stones"]
    var tradeTypeArray : NSArray = ["Select Trade Type","Export", "Re-Export", "Import", "Re-Import"]
    
       var limitlength = 8
       var obj = Custom_ObjectsViewController()
       var commodity : NSDictionary = [:]
       var pickerViewQtr : UIPickerView!
       var pickerViewMonth : UIPickerView!
       var pickerViewYear : UIPickerView!
       var pickerViewCommodityName : UIPickerView!
       var pickerViewTradeType : UIPickerView!
       var requestToServer = RequestToServer()
       var callUrls : URL!
       var StatsArray : NSMutableArray = []
       var activity : UIActivityIndicatorView? = nil
       var activityLoader : MBProgressHUD? = nil
       var indexvalue = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.endEditing(true)
        commodityNo.delegate = self
        submitBtn.layer.cornerRadius = 10
        submitBtn.layer.borderWidth = 1
//        submitBtn.layer.borderColor = obj.hexStringToUIColor(hex: "#d1d1d1").cgColor
        resetBtn.layer.cornerRadius = 10
        resetBtn.layer.borderWidth = 1
//        resetBtn.layer.borderColor = obj.hexStringToUIColor(hex: "#d1d1d1").cgColor
        
//        selectYear.layer.cornerRadius = 10
//        selectYear.layer.borderWidth = 1
//        selectYear.layer.borderColor = obj.hexStringToUIColor(hex: "#d1d1d1").cgColor
        
        pickerViewMonth = UIPickerView()
        pickerViewMonth.delegate = self
        pickerViewMonth.dataSource = self
        
        pickerViewQtr = UIPickerView()
        pickerViewQtr.delegate = self
        pickerViewQtr.dataSource = self
        
        pickerViewYear = UIPickerView()
        pickerViewYear.delegate = self
        pickerViewYear.dataSource = self
        
        pickerViewCommodityName = UIPickerView()
        print(pickerViewCommodityName)
        pickerViewCommodityName.delegate = self
        pickerViewCommodityName.dataSource = self
        
        pickerViewTradeType = UIPickerView()
        pickerViewTradeType.delegate = self
        pickerViewTradeType.dataSource = self
        
        manageToolBar()
        setview()
    }
    
    @IBAction func search(_ sender: Any) {
        activityLoader?.hide(animated: true)
        obj.checkForInternet()
        if (selectYear.text?.isEmpty)!  {
           selectYear.placeholder = "Select Year"
           selectYear.attributedPlaceholder = NSAttributedString(string: selectYear.placeholder!, attributes: [NSAttributedString.Key.foregroundColor : UIColor.red])
            
           
        }
        else  if(commodityNo.text != ""){
            if((commodityNo.text?.count)!<8){
               let alert = UIAlertController.init(title: "", message: "Enter valid commodity number", preferredStyle: UIAlertController.Style.alert)
                              self.present(alert, animated: true, completion: nil)
                              let when = DispatchTime.now() + 2
                              DispatchQueue.main.asyncAfter(deadline: when){
                                // your code with delay
                                alert.dismiss(animated: true, completion: nil)
                              }

            }

        }
          

        else if (commodityNo.text?.isEmpty == true)||(selectYear.text != ""){
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "loadStatsResults") as! loadStatsResults
            vc.year = selectYear.text!
            vc.hs_code = commodityNo.text!
            
            
            if selectQtr.text! == "Quarter I"{
                
               vc.quarter = "1"
            }
            else if selectQtr.text! == "Quarter II" {
                vc.quarter = "2"
            }
            else if selectQtr.text! == "Quarter III"{
                vc.quarter = "3"
             
            }
            else if selectQtr.text! == "Quarter IV"{
                vc.quarter = "4"
             
            }
            
            
            if selectQtr1.text! == "Colored Gem"{
                 vc.commodity_name = "COL GEM"
                
            } else if selectQtr1.text! == "Costume Fashion Jew"{
                vc.commodity_name = "COSTUME FAS JEW"
            }else if selectQtr1.text! == "Cut & Polish Diamond"{
                vc.commodity_name = "CUT & POL"
            }else if selectQtr1.text! == "Gold Jewellery"{
            vc.commodity_name =  "GOLD JEW"
            }else if selectQtr1.text! == "Rough Colored Gemstone"{
                vc.commodity_name =  "ROUGH COL GEMSTONES"
            }else if selectQtr1.text! == "Non Gold Jewellery"{
                vc.commodity_name = "NON GOLD JEW"
            }else if selectQtr1.text! == "Rough Synthetic Stones"{
                vc.commodity_name = "RGH SYNTHETIC STONES"
            }else{
                
                vc.commodity_name = selectQtr1.text!
            }
            
            
            if selectQtr2.text! == "Export"{
                vc.trade_type = "1"
                
            }else if selectQtr2.text! == "Re-Export"{
                vc.trade_type = "3"
                
            }else if selectQtr2.text! == "Import"{
                vc.trade_type = "2"
            }else if selectQtr2.text! == "Re-Import"{
                
                vc.trade_type = "4"
            }
            vc.month = indexvalue
          //  vc.commodity_name = selectQtr1.text!
            //vc.trade_type = selectQtr2.text!
            self.navigationController?.pushViewController(vc, animated: true)
        }
            
            
     else if selectYear.text != "" {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "loadStatsResults") as! loadStatsResults
            vc.year = selectYear.text!
            vc.hs_code = commodityNo.text!
            
            
            if selectQtr.text! == "Quarter I"{
                
               vc.quarter = "1"
            }
            else if selectQtr.text! == "Quarter II" {
                vc.quarter = "2"
            }
            else if selectQtr.text! == "Quarter III"{
                vc.quarter = "3"
             
            }
            else if selectQtr.text! == "Quarter IV"{
                vc.quarter = "4"
             
            }
            
            
            if selectQtr1.text! == "Colored Gem"{
                 vc.commodity_name = "COL GEM"
                
            } else if selectQtr1.text! == "Costume Fashion Jew"{
                vc.commodity_name = "COSTUME FAS JEW"
            }else if selectQtr1.text! == "Cut & Polish Diamond"{
                vc.commodity_name = "CUT & POL"
            }else if selectQtr1.text! == "Gold Jewellery"{
            vc.commodity_name =  "GOLD JEW"
            }else if selectQtr1.text! == "Rough Colored Gemstone"{
                vc.commodity_name =  "ROUGH COL GEMSTONES"
            }else if selectQtr1.text! == "Non Gold Jewellery"{
                vc.commodity_name = "NON GOLD JEW"
            }else if selectQtr1.text! == "Rough Synthetic Stones"{
                vc.commodity_name = "RGH SYNTHETIC STONES"
            }else{
                
                vc.commodity_name = selectQtr1.text!
            }
            
            
            if selectQtr2.text! == "Export"{
                vc.trade_type = "1"
                
            }else if selectQtr2.text! == "Re-Export"{
                vc.trade_type = "3"
                
            }else if selectQtr2.text! == "Import"{
                vc.trade_type = "2"
            }else if selectQtr2.text! == "Re-Import"{
                
                vc.trade_type = "4"
            }
            vc.month = indexvalue
          //  vc.commodity_name = selectQtr1.text!
            //vc.trade_type = selectQtr2.text!
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func reset(_ sender: Any) {
        selectQtr.text  = ""
        selectQtr.placeholder = "Select Quarter"
        selectQtr1.text = ""
        selectQtr2.text = ""
        selectMonth.placeholder = "Select Month"
        selectMonth.text = ""
        commodityNo.text = ""
        selectYear.text = ""
        selectYear.placeholder = "Select Year"
        selectMonth.isUserInteractionEnabled = true
        selectQtr.isUserInteractionEnabled = true
    }
    func manageToolBar() {
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.tintColor = obj.hexStringToUIColor(hex: "#1862AF")
        
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(Search_ChildViewController.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(Search_ChildViewController.cancelPicker))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        selectYear.inputView = pickerViewYear
        selectYear.inputAccessoryView = toolBar
        
        selectQtr.inputView = pickerViewQtr
        selectQtr.inputAccessoryView = toolBar
        
        print(pickerViewCommodityName)
        selectQtr1.inputView = pickerViewCommodityName           // commodity name
        selectQtr1.inputAccessoryView = toolBar
        
        selectQtr2.inputView = pickerViewTradeType            // trade type
        selectQtr2.inputAccessoryView = toolBar
        
        selectMonth.inputView = pickerViewMonth
        selectMonth.inputAccessoryView = toolBar
        
    }
    
    @objc func cancelPicker(_ pickerView: UIPickerView) {
           self.view.endEditing(true)
           if pickerView == pickerViewMonth {
               selectMonth.text = ""
           } else if pickerView == pickerViewYear {
               selectYear.text = ""
           } else if pickerView == pickerViewCommodityName {
               selectQtr1.text = ""
           } else if pickerView == pickerViewTradeType {
               selectQtr2.text = ""
           } else if pickerView == pickerViewQtr {
               selectQtr.text = ""
           }
           
       }
       
       @objc func donePicker()
       {
           selectQtr.resignFirstResponder()
           selectQtr1.resignFirstResponder()
           selectQtr2.resignFirstResponder()
           selectYear.resignFirstResponder()
           selectMonth.resignFirstResponder()
       }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
           if (textField == commodityNo) {
               let text = commodityNo.text!
               let newLength = text.count + string.count - range.length
               return newLength <= limitlength
           }
            return true;
       }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == pickerViewMonth
        {
            return months.count
        }
        else if pickerView == pickerViewQtr
        {
            return quarter.count
        } else if pickerView == pickerViewYear
        {
            return yearArray.count
        } else if pickerView == pickerViewCommodityName {
            return commodityNameArray.count
            
        } else if pickerView == pickerViewTradeType {
            return tradeTypeArray.count
        }
        return months.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == pickerViewMonth
        {
            return months[row]  as? String
        } else if pickerView == pickerViewQtr
        {
            return quarter[row] as? String
        } else if pickerView == pickerViewYear
        {
            selectYear.text = yearArray[row] as? String
            return yearArray[row] as? String
        } else if pickerView == pickerViewCommodityName {
            return commodityNameArray[row] as? String
        } else if pickerView == pickerViewTradeType {
            return tradeTypeArray[row] as? String
        }
        else
        {
            return months[row]  as? String
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == pickerViewYear
        {
            selectYear.text = yearArray[row] as? String
        }
        else if pickerView == pickerViewQtr
        {
            selectQtr.text = quarter[row] as? String
            selectMonth.isUserInteractionEnabled = false
            selectMonth.placeholder = "Not Applicable"
            selectMonth.attributedPlaceholder = NSAttributedString(string: selectMonth.placeholder!, attributes: [NSAttributedString.Key.foregroundColor : UIColor.red])
            monthDropdownImg.isHidden = true
        }
        else if pickerView == pickerViewMonth {
            if (selectQtr.text?.isEmpty)! {
                selectMonth.isUserInteractionEnabled = true
                monthDropdownImg.isHidden = false
                selectMonth.text = months[row] as? String
                let  selectmonthindex = months.object(at: row)
                
                indexvalue = String(months.index(of: selectmonthindex) + 1)
                print(",,,,,,,,,,,,,,..........>>>\(indexvalue)")
                selectQtr.isUserInteractionEnabled = false
                quarterDropdownImg.isHidden = true
                selectQtr.placeholder = "Not Applicable"
                selectQtr.attributedPlaceholder = NSAttributedString(string: selectQtr.placeholder!, attributes: [NSAttributedString.Key.foregroundColor : UIColor.red])
            }
        }
        if pickerView == pickerViewYear
        {
            selectYear.text = yearArray[row] as? String
        }
        if pickerView == pickerViewCommodityName {
            selectQtr1.text = commodityNameArray[row] as? String
        }
        if pickerView == pickerViewTradeType {
            selectQtr2.text = tradeTypeArray[row] as? String
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func setview(){
                DispatchQueue.main.async {
                                 
    self.selectYear.layer.backgroundColor = UIColor.white.cgColor
    self.selectYear.layer.cornerRadius = 5.0
    self.selectYear.layer.masksToBounds = false
    self.selectYear.layer.shadowOffset = CGSize(width: 0, height: 0)
    self.selectYear.layer.shadowOpacity = 0.3
                    
                    self.selectQtr1.layer.backgroundColor = UIColor.white.cgColor
                    self.selectQtr1.layer.cornerRadius = 5.0
                    self.selectQtr1.layer.masksToBounds = false
                    self.selectQtr1.layer.shadowOffset = CGSize(width: 0, height: 0)
                    self.selectQtr1.layer.shadowOpacity = 0.3
                    
                    self.selectQtr2.layer.backgroundColor = UIColor.white.cgColor
                    self.selectQtr2.layer.cornerRadius = 5.0
                    self.selectQtr2.layer.masksToBounds = false
                    self.selectQtr2.layer.shadowOffset = CGSize(width: 0, height: 0)
                    self.selectQtr2.layer.shadowOpacity = 0.3
                    
                    
                   self.selectQtr.layer.backgroundColor = UIColor.white.cgColor
                      self.selectQtr.layer.cornerRadius = 5.0
                      self.selectQtr.layer.masksToBounds = false
                      self.selectQtr.layer.shadowOffset = CGSize(width: 0, height: 0)
                      self.selectQtr.layer.shadowOpacity = 0.3
                    
                    self.selectMonth.layer.backgroundColor = UIColor.white.cgColor
                    self.selectMonth.layer.cornerRadius = 5.0
                    self.selectMonth.layer.masksToBounds = false
                    self.selectMonth.layer.shadowOffset = CGSize(width: 0, height: 0)
                    self.selectMonth.layer.shadowOpacity = 0.3
                    
                    self.commodityNo.layer.backgroundColor = UIColor.white.cgColor
                       self.commodityNo.layer.cornerRadius = 5.0
                       self.commodityNo.layer.masksToBounds = false
                       self.commodityNo.layer.shadowOffset = CGSize(width: 0, height: 0)
                       self.commodityNo.layer.shadowOpacity = 0.3
                    
                              }

    }
}
