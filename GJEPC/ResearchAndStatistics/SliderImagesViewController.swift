//
//  SliderImagesViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 25/03/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
class SliderImagesViewController: UIViewController,dataReceivedFromServerDelegate,UICollectionViewDelegateFlowLayout {
   
    
    
    @IBOutlet weak var leftSliderCollectionView: UICollectionView!
    
    @IBOutlet weak var rightSliderCollectionView: UICollectionView!
    
    
    var requestToServer = RequestToServer()
    var imgArr : NSMutableArray = []
    var rightImgArr : NSMutableArray = []
    var activityLoader : MBProgressHUD? = nil
    var replacedImgstring  = ""
    var timer = Timer()
    var counter = 0
    var rightCounter = 0
    override func viewDidLoad() {
        super.viewDidLoad()
           requestToServer.delegate = self
       
             requestToServer.getDataFromServer(myGetUrl: leftSliderImages!)
             getdata(myGetUrl: RightSliderImages!)
        
  DispatchQueue.main.async {
    self.timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.changeImage), userInfo: nil, repeats: true)
    

  }
self.timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.changeRightImage), userInfo: nil, repeats: true)
  
    }
    
    @objc func changeRightImage(){
              if rightCounter < rightImgArr.count{
                  let index = IndexPath.init(item: rightCounter, section: 0)
                  self.rightSliderCollectionView.scrollToItem(at: index, at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)
        //          pageView.currentPage = counter
                  
                  rightCounter += 1
              }
              else {
                  rightCounter = 0
                  let index = IndexPath.init(item: rightCounter, section: 0)
                      self.rightSliderCollectionView.scrollToItem(at: index, at: UICollectionView.ScrollPosition.centeredHorizontally, animated: false)
        //          pageView.currentPage = counter
                   rightCounter = 1

              }

    }
    
    @objc func bottomChangeLeftImage(){
            rightCounter = rightCounter-2
            print(rightCounter)
              if counter < rightImgArr.count{
                if rightCounter == -1{
                    rightCounter = rightImgArr.count-1
                    let index = IndexPath.init(item: rightCounter, section: 0)
                    self.rightSliderCollectionView.scrollToItem(at: index, at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)
                }
                else{
                    let index = IndexPath.init(item: rightCounter, section: 0)
                    self.rightSliderCollectionView.scrollToItem(at: index, at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)
                }
                  
        //          pageView.currentPage = counter
                  
    //              counter += 1
              }
    //          else {
    //              counter = 0
    //              let index = IndexPath.init(item: counter, section: 0)
    //                  self.leftSliderCollectionView.scrollToItem(at: index, at: UICollectionView.ScrollPosition.centeredHorizontally, animated: false)
    //    //          pageView.currentPage = counter
    //               counter = 1
    //
    //          }
              }
    
    
    @objc func changeImageLeft(){
        counter = counter-2
        print(counter)
          if counter < imgArr.count{
            if counter == -1{
                counter = imgArr.count-1
                let index = IndexPath.init(item: counter, section: 0)
                self.leftSliderCollectionView.scrollToItem(at: index, at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)
            }
            else{
                let index = IndexPath.init(item: counter, section: 0)
                self.leftSliderCollectionView.scrollToItem(at: index, at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)
            }
              
    //          pageView.currentPage = counter
              
//              counter += 1
          }
//          else {
//              counter = 0
//              let index = IndexPath.init(item: counter, section: 0)
//                  self.leftSliderCollectionView.scrollToItem(at: index, at: UICollectionView.ScrollPosition.centeredHorizontally, animated: false)
//    //          pageView.currentPage = counter
//               counter = 1
//
//          }
          }
    
    @objc func changeImage(){
      if counter < imgArr.count{
          let index = IndexPath.init(item: counter, section: 0)
          self.leftSliderCollectionView.scrollToItem(at: index, at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)
//          pageView.currentPage = counter
          
          counter += 1
      }
      else {
          counter = 0
          let index = IndexPath.init(item: counter, section: 0)
              self.leftSliderCollectionView.scrollToItem(at: index, at: UICollectionView.ScrollPosition.centeredHorizontally, animated: false)
//          pageView.currentPage = counter
           counter = 1

      }
      }

    @IBAction func bottomcollectionViewLeft(_ sender: Any) {
        
        bottomChangeLeftImage()
        
    }
    
    
    func dataReceivedFromServer(data: NSDictionary, url: URL) {
                         if ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "true" {
                     let temporaryArray = ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "Result") as! NSArray)
                     if temporaryArray.count > 0 {
                      imgArr = (temporaryArray.mutableCopy() as! NSMutableArray)
                      DispatchQueue.main.async {
                       self.loadActivityIndicator()
                       self.leftSliderCollectionView.delegate = self
                       self.leftSliderCollectionView.dataSource = self
//
                      }
                    
                 }
                 }

             }
    func loadActivityIndicator()
    {
        activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
        activityLoader?.label.text = "Loading";
        activityLoader?.detailsLabel.text = "Please Wait";
        activityLoader?.isUserInteractionEnabled = false;
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = leftSliderCollectionView.frame.size
        return CGSize(width: size.width, height: size.height)
    }
    
    func getdata(myGetUrl : URL)  {
        let url = myGetUrl
        
        //create the session object
        let session = URLSession.shared
        
        //now create the URLRequest object using the url object
        let request = URLRequest(url: url )
        
        //create dataTask using the session object to send data to the server
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
//                    print(json)
                    if json.count > 0 {
            if ((json.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "true" {
    let temporaryArray = ((json.value(forKey: "Response") as! NSDictionary).value(forKey: "Result") as! NSArray)
    if temporaryArray.count > 0 {
        self.rightImgArr = (temporaryArray.mutableCopy() as! NSMutableArray)
     DispatchQueue.main.async {
//       self.loadActivityIndicator()
        self.rightSliderCollectionView.delegate = self
        self.rightSliderCollectionView.dataSource = self
       
     }
   
}
//                print(self.rightImgArr)
}

                    }
//                    print("Here: \(self.responsedata)")
//                    self.delegate.dataReceivedFromServer(data: self.responsedata, url : myGetUrl)
                }
            } catch let error {
                print(error.localizedDescription)
            }
        })
        task.resume()
    }
    
    @IBAction func right(_ sender: Any) {
        changeImage()
    }
    
    @IBAction func left(_ sender: Any) {
        changeImageLeft()
    }
    @IBAction func bottomCollectionViewRightBtn(_ sender: Any) {
        changeRightImage()
    }
    
}
extension SliderImagesViewController: UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
        if collectionView == leftSliderCollectionView{
            return imgArr.count
        }
        else{
            return rightImgArr.count
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
         let cell = leftSliderCollectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! LeftCollectionViewCell
        
        
        if collectionView == leftSliderCollectionView{
           
        cell.leftsliderImage.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleBottomMargin, .flexibleRightMargin, .flexibleLeftMargin, .flexibleTopMargin]
          cell.leftsliderImage.contentMode = .scaleAspectFit // OR .scaleAspectFill
        cell.leftsliderImage.clipsToBounds = true
        
        let imgdata = ((self.imgArr.object(at: indexPath.row) as AnyObject).object(forKey: "uploads")as! String)
    
//            print(imgArr)
            print(imgdata)
        DispatchQueue.main.async {
          let imgfilterUrl = sliderImagePath + imgdata
            self.replacedImgstring = imgfilterUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!
            URLSession.shared.dataTask(with: NSURL(string: imgfilterUrl)! as URL, completionHandler: { (data, response, error) -> Void in
                           if error != nil {
                               return
                           }
                           else {

                               DispatchQueue.main.async {
                                   let image = UIImage(data: data!)
//print(image)
                                   if (image != nil) {

                                           cell.leftsliderImage.image = image
                                           cell.leftsliderImage.contentMode = .scaleAspectFit

                                      // cell1.menuImg.contentMode = UIViewContentMode.scaleToFill
                                   }
                                   else{
//                                        cell.laboratoriesImage.image = UIImage(named: "logo-1.png")
                                   }
                               }
                           }
                       }).resume()
            self.activityLoader?.hide(animated: true)
                     }
                   
                   return cell
//        }
//        else{
//            let cell = horizintalSlider.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CellCollectionViewCell
//            cell.image.image = imgArr[indexPath.row]
//
//
//            return cell
//        }
        }
        else {
            
            cell.leftsliderImage.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleBottomMargin, .flexibleRightMargin, .flexibleLeftMargin, .flexibleTopMargin]
              cell.leftsliderImage.contentMode = .scaleAspectFit // OR .scaleAspectFill
            cell.leftsliderImage.clipsToBounds = true
            
            let imgdata = ((self.rightImgArr.object(at: indexPath.row) as AnyObject).object(forKey: "uploads")as! String)
            
                    DispatchQueue.main.async {
                      let imgfilterUrl = rightSliderImagePath + imgdata
                        self.replacedImgstring = imgfilterUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!
                        URLSession.shared.dataTask(with: NSURL(string: imgfilterUrl)! as URL, completionHandler: { (data, response, error) -> Void in
                                       if error != nil {
                                           return
                                       }
                                       else {
                                           
                                           DispatchQueue.main.async {
                                               let image = UIImage(data: data!)
            //print(image)
                                               if (image != nil) {
                                                 
                                                       cell.leftsliderImage.image = image
                                                       cell.leftsliderImage.contentMode = .scaleAspectFit

                                                  // cell1.menuImg.contentMode = UIViewContentMode.scaleToFill
                                               }
                                               else{
            //                                        cell.laboratoriesImage.image = UIImage(named: "logo-1.png")
                                               }
//                                             self.activityLoader?.hide(animated: true)
                                           }
                                        
                                       }
                                   }).resume()
                       
                                 }


            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpVC") as? PopUpVC
        if collectionView == leftSliderCollectionView{
            popOverVC!.imageString = ((self.imgArr.object(at: indexPath.row) as AnyObject).object(forKey: "uploads")as! String)
            popOverVC!.collectionview = "left"
            popOverVC!.imgArray = self.imgArr
            
        }
        else{
            popOverVC!.imageString = ((self.rightImgArr.object(at: indexPath.row) as AnyObject).object(forKey: "uploads")as! String)
            popOverVC!.collectionview = "right"
            popOverVC!.imgArray = self.rightImgArr
        }
        popOverVC?.indexpath = indexPath.row
        self.addChild(popOverVC!)
//        popOverVC?.view.frame = self.view.frame
        
        self.view.addSubview(popOverVC!.view)
        popOverVC?.didMove(toParent: self)
    }
  
       
}

//extension SliderImagesViewController: UICollectionViewDelegateFlowLayout{
////    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
////        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
////    }
//
////    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
////        return 0.0
////    }
////    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
////        return 0.0
////    }
//}
