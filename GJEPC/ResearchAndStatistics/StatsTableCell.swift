//
//  StatsTableCell.swift
//  GJEPC
//
//  Created by Kwebmaker on 21/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit

class StatsTableCell: UITableViewCell {

    @IBOutlet weak var us: UILabel!
    @IBOutlet weak var rupees: UILabel!
    @IBOutlet weak var countryName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
