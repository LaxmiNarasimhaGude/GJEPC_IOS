//
//  loadStatsResults.swift
//  GJEPC
//
//  Created by Kwebmaker on 21/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
class loadStatsResults: UIViewController,UITableViewDataSource,UITableViewDelegate,
dataReceivedFromServerDelegate {
   
    

    @IBOutlet weak var usdOverall: UILabel!
    @IBOutlet weak var inrOverall: UILabel!
    @IBOutlet weak var inrTotal: UILabel!
    @IBOutlet weak var usdTotal: UILabel!
    @IBOutlet weak var yearLbl: UILabel!
    @IBOutlet weak var statsTableView: UITableView!
    
      var dataArray : NSMutableArray = []
       var year  = ""
       var quarter = ""
       var month = String()
       var commodity_name = ""
       var hs_code = ""
       var trade_type = ""
       var requestToServer = RequestToServer()
       var callUrls : URL!
       var obj = Custom_ObjectsViewController()
       var newArray : NSArray = []
       var activityLoader : MBProgressHUD? = nil
    var tradetype = ""
     
    override func viewDidLoad() {
        super.viewDidLoad()

        self.checkForInternet()
    }
    
    func loadActivityIndicator()
    {
        activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
        activityLoader?.label.text = "Loading";
        activityLoader?.detailsLabel.text = "Please Wait!!!";
        activityLoader?.isUserInteractionEnabled = false;
    }

    func numberOfSections(in tableView: UITableView) -> Int {
          return 1
      }
      
      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return dataArray.count
      }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "StatsTableCell") as! StatsTableCell
        if indexPath.row%2 == 0{
            cell.backgroundColor = UIColor(red: 246/256, green: 246/256, blue: 246/256, alpha: 1)
        }
        else{
            cell.backgroundColor = UIColor.white
        }
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
      cell.layer.borderColor = UIColor.lightGray.cgColor
      cell.layer.borderWidth = 0.5
        DispatchQueue.main.async {
            
                        cell.countryName.text = (self.dataArray.object(at: indexPath.row) as AnyObject).value(forKey: "country_name") as? String
       
            
            if   let totalinr  =  (self.dataArray.object(at: indexPath.row) as AnyObject).value(forKey: "total_inr") as? String {
               
                 let test  =  (self.dataArray.object(at: indexPath.row) as AnyObject).value(forKey: "total_inr") as? String
                cell.rupees.text = test
              
            }
            else
            {
                
                  let x  =  (self.dataArray.object(at: indexPath.row) as AnyObject).value(forKey: "total_inr") as? Int
                
                let stringValue =  "\(x ?? 0)"
                
                cell.rupees.text = stringValue
                
                
            }
            
            if   let totalinr  =  (self.dataArray.object(at: indexPath.row) as AnyObject).value(forKey: "total_usd") as? String {
                
                let test  =  (self.dataArray.object(at: indexPath.row) as AnyObject).value(forKey: "total_usd") as? String
                cell.us.text = test
                
            }
            else
            {
                
                let x  =  (self.dataArray.object(at: indexPath.row) as AnyObject).value(forKey: "total_usd") as? Int
                
                let stringValue =  "\(x ?? 0)"
                
                cell.us.text = stringValue
                
                
            }
            
            
           
            
        }
        return cell
    }
      
   
      func dataReceivedFromServer(data: NSDictionary, url: URL) {
         
          
          let temporaryArray = (data.value(forKey: "Response") as! NSArray)
          if temporaryArray.count == 0  {
            let alert = UIAlertController(title: "Error", message: "No Data Found", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
              DispatchQueue.main.async {
                  self.dismiss(animated: true, completion: nil)
              }
            }))
              DispatchQueue.main.async {
                   self.activityLoader?.hide(animated: true)
                    self.present(alert, animated: true, completion: nil)
              }
          }
          
           else  if  temporaryArray != [] {
              dataArray = (temporaryArray.mutableCopy() as! NSMutableArray)
                  print(dataArray)
             
              DispatchQueue.main.async {
              self.inrOverall.text = data.value(forKey: "Total INR")  as? String
              
              self.usdOverall.text = data.value(forKey: "Total USD")  as? String
              
              self.inrTotal.text = data.value(forKey: "Total INR")  as? String
              self.usdTotal.text = data.value(forKey: "Total USD")  as? String
              }
              DispatchQueue.main.async {
                  self.activityLoader?.hide(animated: true)
                  self.statsTableView.reloadData()
              }
          }
      }
    func checkForInternet() {
        if CheckInternet.isConnectedToNetwork() == true
        {
            print("internet available")
            self.statsTableView.isHidden = false
            DispatchQueue.main.async {
                self.loadActivityIndicator()
            }
            onFirstTimeLoad()
        }
        else
        {
            print("no internet")
            statsTableView.isHidden = true
            activityLoader?.hide(animated: true)
        }
    }
        func onFirstTimeLoad(){
            if trade_type == "1"{
                tradetype = "Export"
            }
            else if(trade_type == "2"){
                tradetype = "Import"
            }
            else if(trade_type == "3"){
                tradetype = "Re-Export"
            }
            else if(trade_type == "4"){
                tradetype = "Re-Import"
            }
        requestToServer.delegate = self
        
            
            if year != "" && quarter == "" && month == "" && commodity_name == "" && hs_code == "" && tradetype == ""{
                yearLbl.text = year
            }
           if year != "" && quarter != "" && month == "" && commodity_name == "" && hs_code == "" && tradetype == ""{
            yearLbl.text = "\(year)"+" | Quarter"+"\(quarter)"
                      }
            if year != "" && quarter != "" && month == "" && commodity_name != "" && hs_code == "" && tradetype == ""{
                yearLbl.text = "\(year)"+" | Quarter"+"\(quarter)"+" |"+"\(commodity_name)"
            }
//            print(hs_code)
            if year != "" && quarter != "" && month == "" && commodity_name != "" && hs_code != "" && tradetype == ""{
                            yearLbl.text = "\(year)"+" | Quarter"+"\(quarter)"+" | "+"\(commodity_name)"+" | "+"\(hs_code)"
                       }
            if year != "" && quarter != "" && month == "" && commodity_name != "" && hs_code != "" && tradetype != ""{
                 yearLbl.text = "\(year)"+" | Quarter"+"\(quarter)"+" | "+"\(commodity_name)"+" | "+"\(hs_code)"+" | "+"\(tradetype)"
            }
            if year != "" && quarter != "" && month == "" && commodity_name != "" && hs_code == "" && tradetype != ""{
                 yearLbl.text = "\(year)"+" | Quarter"+"\(quarter)"+" | "+"\(commodity_name)"+" | "+"\(hs_code)"+" | "+"\(tradetype)"
            }
            
         print(year)
         print(quarter)
         print(month)
         print(commodity_name)
         print(hs_code)
         print(trade_type)
            
        callUrls = statsSearch
        let params = [
        "year": year,
        "quarter": quarter,
        "month": month,
        "commodity_name": commodity_name,
        "hs_code":hs_code,
        "trade_type":trade_type]
        print("this parameters...\(params)")
        requestToServer.connectToServer(myUrl: callUrls as URL, params: params as AnyObject)
        
        
        }

    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}
