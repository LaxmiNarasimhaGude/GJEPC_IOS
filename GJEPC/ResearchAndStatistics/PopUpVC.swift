//
//  PopUpVC.swift
//  GJEPC
//
//  Created by Kwebmaker on 26/03/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit

class PopUpVC: UIViewController,UIScrollViewDelegate {

    @IBOutlet weak var sliderSelectedImage: UIImageView!
    
//    @IBOutlet weak var leftBtn: UIButton!
//    @IBOutlet weak var rightBtn: UIButton!
    var imageString = ""
    var replacedImgstring = ""
    var collectionview = ""
    var imagepath = ""
    var imgArray : NSMutableArray = []
    var indexpath:Int!
    var getClickImage = UIImageView()
    var zoomscrollV = UIScrollView()
    var button = UIButton()
    var leftButton = UIButton()
    var rightButton = UIButton()
//    var locationButton = UIButton()
    
    override func viewDidAppear(_ animated: Bool) {
//        zoomscrollV.isHidden = true
        zoomscrollV.frame = CGRect(x:0, y:0, width:self.view.frame.width, height:self.view.frame.height - 50)
        zoomscrollV.minimumZoomScale=1
        zoomscrollV.maximumZoomScale=10
        zoomscrollV.bounces=false

        self.view.addSubview(zoomscrollV)

        getClickImage=UIImageView()

        getClickImage.frame = CGRect(x:0, y:70, width:zoomscrollV.frame.width, height:250)
        getClickImage.backgroundColor = .white
        getClickImage.contentMode = .scaleAspectFit
        zoomscrollV.addSubview(getClickImage)
        
       

    }
//    @objc func myImageTapped(_ sender: UITapGestureRecognizer) {
//
//        zoomscrollV.isHidden = false
//
//        // RESPECTIVE IMAGE
//        getClickImage.image = BG_CARD_IMGS[(sender.view?.tag)!]
//
//

//
//        // CLOSE BUTTON
//
//
//    }

    @objc func closeZoom() {
        self.view.removeFromSuperview()
//        print("hello")
//        zoomscrollV.isHidden = true
//        zoomscrollV.setZoomScale(1.0, animated: false)
//        sender.removeFromSuperview()
    }

    //SCROLLVIEW DELEGATE
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {

        return getClickImage

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        zoomscrollV.delegate = self
       
       

//        newView.translatesAutoresizingMaskIntoConstraints = false
//        let horizontalConstraint = newView.centerXAnchor.constraint(equalTo: view.centerXAnchor)
////        let horizontalConstraint = newView.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 1.5)
//        let verticalConstraint = newView.centerYAnchor.constraint(equalTo: view.centerYAnchor)
//        let widthConstraint = newView.widthAnchor.constraint(equalToConstant: 30)
//        let heightConstraint = newView.heightAnchor.constraint(equalToConstant: 30)
//        view.addConstraints([horizontalConstraint, verticalConstraint, widthConstraint, heightConstraint])
        
        
        
        
        if collectionview == "left" {
            imagepath = sliderImagePath
        }
            else{
                imagepath = rightSliderImagePath
        }
                           DispatchQueue.main.async {
                            let imgfilterUrl = self.imagepath + self.imageString
                               self.replacedImgstring = imgfilterUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!
                               URLSession.shared.dataTask(with: NSURL(string: imgfilterUrl)! as URL, completionHandler: { (data, response, error) -> Void in
                                              if error != nil {
                                                  return
                                              }
                                              else {
                                                  
            DispatchQueue.main.async {
                let image = UIImage(data: data!)
                    if (image != nil) {
                     
                         self.getClickImage.image = image
                                                        
                       
                        self.button = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
                        self.button.setBackgroundImage(UIImage(named: "img_close"), for: .normal)
//                                self.button.setTitle("Test Button", for: .normal)
                        self.button.addTarget(self, action: #selector(self.closeZoom), for: .touchUpInside)

                        self.view.addSubview(self.button)
                        
                        
                        
                        
                        
                        self.rightButton = UIButton(frame: CGRect(x: self.zoomscrollV.frame.width-30 , y: self.getClickImage.bounds.midY+50, width: 30, height: 30))
                         self.rightButton.setBackgroundImage(UIImage(named: "icons8-greater-than-30"), for: .normal)
                        //                                self.button.setTitle("Test Button", for: .normal)
                        self.rightButton.addTarget(self, action: #selector(self.right), for: .touchUpInside)

                        self.view.addSubview(self.rightButton)
                        
                        
                        
                        
//                        self.leftButton = UIButton(frame: CGRect(x: self.zoomscrollV.frame.width-30 , y: self.getClickImage.bounds.midY+50, width: 30, height: 30))
//                         self.leftButton.setBackgroundImage(UIImage(named: "img_close"), for: .normal)
//                        //                                self.button.setTitle("Test Button", for: .normal)
//                        self.leftButton.addTarget(self, action: #selector(self.left), for: .touchUpInside)
//
//                        self.view.addSubview(self.leftButton)

                        
                        self.leftButton = UIButton(frame: CGRect(x: 0, y: self.getClickImage.bounds.midY+50, width: 30, height: 30))
                                                self.leftButton.setBackgroundImage(UIImage(named: "icons8-less-than-30"), for: .normal)
                                               //                                self.button.setTitle("Test Button", for: .normal)
                                               self.leftButton.addTarget(self, action: #selector(self.left), for: .touchUpInside)

                                               self.view.addSubview(self.leftButton)
                        
                        

                        
                        
                        
                        
                        
                                                      }
                        else{
                  
                                                      }
                                                  }
                                               
                                              }
                                          }).resume()
                              
                                        }

    }
    

   
    @IBAction func cancel(_ sender: Any) {
         self.view.removeFromSuperview()
    }
    @objc func left() {
        rightButton.isHidden = false
        print(indexpath)
        indexpath -= 1
         if (indexpath == 0) {
             leftButton.isHidden = true
                    DispatchQueue.main.async {
                        let imgfilterUrl = self.imagepath + ((self.imgArray.object(at: self.indexpath) as AnyObject).object(forKey: "uploads")as! String)
                                 self.replacedImgstring = imgfilterUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!
                                 URLSession.shared.dataTask(with: NSURL(string: imgfilterUrl)! as URL, completionHandler: { (data, response, error) -> Void in
                                                if error != nil {
                                                    return
                                                }
                                                else {

                                                    DispatchQueue.main.async {
                                                        let image = UIImage(data: data!)
                                                        if (image != nil) {

                                                          self.getClickImage.image = image
            //                                                self.indexpath += 1
                                                        }
                                                        else{

                                                        }
                                                    }

                                                }
                                            }).resume()

                                          }

         }
         else{
                     DispatchQueue.main.async {
                         let imgfilterUrl = self.imagepath + ((self.imgArray.object(at: self.indexpath) as AnyObject).object(forKey: "uploads")as! String)
                                  self.replacedImgstring = imgfilterUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!
                                  URLSession.shared.dataTask(with: NSURL(string: imgfilterUrl)! as URL, completionHandler: { (data, response, error) -> Void in
                                                 if error != nil {
                                                     return
                                                 }
                                                 else {

                                                     DispatchQueue.main.async {
                                                         let image = UIImage(data: data!)
                                                         if (image != nil) {

                                                           self.getClickImage.image = image
             //                                                self.indexpath += 1
                                                         }
                                                         else{

                                                         }
                                                     }

                                                 }
                                             }).resume()

                                           }

         }

        
    }
    
    @objc func right() {
        leftButton.isHidden = false
       indexpath += 1
        if (indexpath == imgArray.count-1) {
            rightButton.isHidden = true
                    DispatchQueue.main.async {
                        let imgfilterUrl = self.imagepath + ((self.imgArray.object(at: self.indexpath) as AnyObject).object(forKey: "uploads")as! String)
                                 self.replacedImgstring = imgfilterUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!
                                 URLSession.shared.dataTask(with: NSURL(string: imgfilterUrl)! as URL, completionHandler: { (data, response, error) -> Void in
                                                if error != nil {
                                                    return
                                                }
                                                else {

                                                    DispatchQueue.main.async {
                                                        let image = UIImage(data: data!)
                                                        if (image != nil) {

                                                          self.getClickImage.image = image
            //                                                self.indexpath += 1
                                                        }
                                                        else{

                                                        }
                                                    }

                                                }
                                            }).resume()

                                          }

        }
        else{
                    DispatchQueue.main.async {
                        let imgfilterUrl = self.imagepath + ((self.imgArray.object(at: self.indexpath) as AnyObject).object(forKey: "uploads")as! String)
                                 self.replacedImgstring = imgfilterUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!
                                 URLSession.shared.dataTask(with: NSURL(string: imgfilterUrl)! as URL, completionHandler: { (data, response, error) -> Void in
                                                if error != nil {
                                                    return
                                                }
                                                else {

                                                    DispatchQueue.main.async {
                                                        let image = UIImage(data: data!)
                                                        if (image != nil) {

                                                          self.getClickImage.image = image
            //                                                self.indexpath += 1
                                                        }
                                                        else{

                                                        }
                                                    }

                                                }
                                            }).resume()

                                          }

        }
       

        
    }
}
