//
//  StatisticsViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 14/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
class StatisticsViewController: UIViewController,dataReceivedFromServerDelegate,UITableViewDataSource,UITableViewDelegate {
  
    
   
    
    @IBOutlet weak var statisticsScrollView: UIScrollView!
    @IBOutlet weak var archiveImage: UIImageView!
    @IBOutlet weak var tradeInformationImage: UIImageView!
    @IBOutlet weak var analyticsReportsImage: UIImageView!
    @IBOutlet weak var searchImage: UIImageView!
    @IBOutlet weak var exportImage: UIImageView!
    @IBOutlet weak var importImage: UIImageView!
    @IBOutlet weak var gjepcanalysisImage: UIImageView!
    @IBOutlet weak var tradeInformationLabel: UILabel!
    @IBOutlet weak var archiveLabel: UILabel!
    @IBOutlet weak var searchLabel: UILabel!
    @IBOutlet weak var exportLabel: UILabel!
    @IBOutlet weak var importLabel: UILabel!
    @IBOutlet weak var statisticsTableView: UITableView!
    @IBOutlet weak var analyticsLabel: UILabel!
    @IBOutlet weak var lastLabel: UILabel!
    
       var tabIndex = 0
       var requestFromServer = RequestToServer()
       var custom = Custom_ObjectsViewController()
//       var searchVC = Search_ChildViewController()
       var tableViewArrayForImport : NSMutableArray = []
       var tableViewArrayForExport : NSMutableArray = []
       var customArray : NSMutableArray = []
       var activity : UIActivityIndicatorView? = nil
       var activityLoader : MBProgressHUD? = nil
       var whichIsSelected = "Import"
    override func viewDidLoad() {
        super.viewDidLoad()
//        exportLabel.backgroundColor = UIColor.white
//        importLabel.backgroundColor = UIColor.black
//        searchLabel.backgroundColor = UIColor.white
//        archiveLabel.backgroundColor = UIColor.white
//        analyticsLabel.backgroundColor = UIColor.white
//        lastLabel.backgroundColor = UIColor.white
//        tradeInformationLabel.backgroundColor = UIColor.white
        
        gjepcanalysisImage.isHidden = false
        importImage.isHidden = true
        exportImage.isHidden = true
        searchImage.isHidden = true
        archiveImage.isHidden = true
        analyticsReportsImage.isHidden = true
        tradeInformationImage.isHidden = true
        
        


        if self.children.count >= 0{
                          let viewControllers:[UIViewController] = self.children
                          for viewContoller in viewControllers{
                              viewContoller.willMove(toParent: nil)
                              viewContoller.view.removeFromSuperview()
                              viewContoller.removeFromParent()
                          }
                      }
                      
                      let controller:SliderImagesViewController =
                              self.storyboard!.instantiateViewController(withIdentifier: "SliderImagesViewController") as!
                              SliderImagesViewController

                              let statusbarHeight = UIApplication.shared.statusBarFrame.height
                      //        let navigationBarHeight = uiappl
                              
                              let tabBarHeight = self.tabBarController!.tabBar.frame.size.height+100;

                              
                              
                              controller.view.frame = CGRect(x: 0, y: statusbarHeight+100, width: UIScreen.main.bounds.size.width, height: (UIScreen.main.bounds.size.height-tabBarHeight))
                              controller.willMove(toParent: self)
                              self.view.addSubview(controller.view)
                              self.addChild(controller)
                              controller.didMove(toParent: self)
        


        custom.whenNoInternet()
//        checkForInternetAndLoadData()
        statisticsTableView.dataSource = self
        statisticsTableView.delegate = self
               
        requestFromServer.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
       
    }
    func checkForInternetAndLoadData()
       {
           if CheckInternet.isConnectedToNetwork() == true
           {
               statisticsTableView.isHidden = false
               DispatchQueue.main.async {
                   self.loadActivityIndicator()
                self.statisticsTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
                 self.statisticsTableView.separatorColor = UIColor.white

               }
               if tabIndex == 0
               {
                   let params = [""]
                 self.statisticsTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
                 self.statisticsTableView.separatorColor = UIColor.white
                   requestFromServer.connectToServer(myUrl: statisticsImportUrl! , params: params as AnyObject)
               }
               else if tabIndex == 1
               {
                 self.statisticsTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
                 self.statisticsTableView.separatorColor = UIColor.white
                   let params = [""]
                   requestFromServer.connectToServer(myUrl: statisticsExportUrl! , params: params as AnyObject)
               }
           }
           else
           {
               statisticsTableView.isHidden = true
           }
       }

   func loadActivityIndicator()
      {
          activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
          activityLoader?.label.text = "Loading";
          activityLoader?.detailsLabel.text = "Please Wait";
          activityLoader?.isUserInteractionEnabled = false;
      }
    func dataReceivedFromServer(data: NSDictionary, url: URL) {
        
        if ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "true" {
            let temporaryArray = ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "Result") as! NSArray)
            tableViewArrayForImport = (temporaryArray.mutableCopy() as! NSMutableArray)
            print("arr : \(tableViewArrayForImport)")
            customArray = [];

            
            DispatchQueue.main.async {
                self.activityLoader?.hide(animated: true)
//                self.statisticsTableView.delegate = self
//                self.statisticsTableView.dataSource =  self
                 self.statisticsTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
                self.statisticsTableView.separatorColor = UIColor.white
                self.statisticsTableView.reloadData()
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return ((customArray.object(at: section) as! NSDictionary).object(forKey: "Result") as! NSArray).count
        return tableViewArrayForImport.count
    }
      
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.none

          
          let cell = tableView.dequeueReusableCell(withIdentifier: "StatisticsTableViewCell") as! StatisticsTableViewCell
        cell.separatorInset = .zero
        
        DispatchQueue.main.async {
                                  //
                    cell.BagroundView.backgroundColor = UIColor.white
        
                    cell.BagroundView.layer.cornerRadius = 3.0
                    cell.BagroundView.layer.masksToBounds = false
                                  
                                  
                    cell.BagroundView.layer.shadowOffset = CGSize(width: 0, height: 0)
                    cell.BagroundView.layer.shadowOpacity = 0.3

                              }
        
        
        

          cell.layer.borderWidth = 0.5
          cell.layer.borderColor = UIColor.lightGray.cgColor
          cell.selectionStyle = .none
       

        cell.statisticPdfName_label.text = (((self.tableViewArrayForImport.object(at: indexPath.row) as! NSDictionary)["name"]) as! String)
        cell.statisticPdfName_label.labelSpacing(label: cell.statisticPdfName_label, font: "Bold")
//        cell.statisticPdfName_label.setCharacterSpacing(1.1)
//        cell.statisticPdfName_label.setLineSpacing(lineSpacing: 5,lineHeightMultiple: 0)
        
          
          return cell
      }
    
    @IBAction func exports(_ sender: Any) {
        
        
        if self.children.count >= 0{
                           let viewControllers:[UIViewController] = self.children
                           for viewContoller in viewControllers{
                               viewContoller.willMove(toParent: nil)
                               viewContoller.view.removeFromSuperview()
                               viewContoller.removeFromParent()
                           }
                       }
gjepcanalysisImage.isHidden = true
importImage.isHidden = true
exportImage.isHidden = false
searchImage.isHidden = true
archiveImage.isHidden = true
analyticsReportsImage.isHidden = true
tradeInformationImage.isHidden = true

        whichIsSelected = "Export"
        statisticsTableView.isHidden = true
        self.tabIndex = 1
        checkForInternetAndLoadData()
//        let newContentOffsetX = ((statisticsScrollView.contentSize.width - statisticsScrollView.frame.size.width) / 2)+220;
               statisticsScrollView.contentOffset = CGPoint(x: 0, y: 0);
    }
    
    @IBAction func imports(_ sender: Any) {
        
        if self.children.count >= 0{
                           let viewControllers:[UIViewController] = self.children
                           for viewContoller in viewControllers{
                               viewContoller.willMove(toParent: nil)
                               viewContoller.view.removeFromSuperview()
                               viewContoller.removeFromParent()
                           }
                       }
        
gjepcanalysisImage.isHidden = true
importImage.isHidden = false
exportImage.isHidden = true
searchImage.isHidden = true
archiveImage.isHidden = true
analyticsReportsImage.isHidden = true
tradeInformationImage.isHidden = true
 
        
        whichIsSelected = "Import"
         statisticsTableView.isHidden = true
        self.tabIndex = 0
        checkForInternetAndLoadData()
        statisticsScrollView.contentOffset = CGPoint(x: 0, y: 0);
    }
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let statisticImport = self.storyboard?.instantiateViewController(withIdentifier: "StatisticsImportAndExportViewController") as! StatisticsImportAndExportViewController
        
        if(whichIsSelected == "Import"){
             let pdfUrl1 = ( (self.tableViewArrayForImport.object(at: indexPath.row) as! NSDictionary).object(forKey: "upload_statistics_import") as! String)
            statisticImport.heading = "IMPORT"
                    statisticImport.pdfUrl1 = pdfUrl1
        }
        else{
            let pdfUrl1 = ( (self.tableViewArrayForImport.object(at: indexPath.row) as! NSDictionary).object(forKey: "upload_statistics_export") as! String)
            statisticImport.heading = "EXPORT"

                        statisticImport.pdfUrl1 = pdfUrl1
        }
        
         
        
        
        self.navigationController?.pushViewController(statisticImport, animated: true)
    }
    @IBAction func search(_ sender: Any) {
        
gjepcanalysisImage.isHidden = true
importImage.isHidden = true
exportImage.isHidden = true
searchImage.isHidden = false
archiveImage.isHidden = true
analyticsReportsImage.isHidden = true
tradeInformationImage.isHidden = true

        if self.children.count >= 0{
                                  let viewControllers:[UIViewController] = self.children
                                  for viewContoller in viewControllers{
                                      viewContoller.willMove(toParent: nil)
                                      viewContoller.view.removeFromSuperview()
                                      viewContoller.removeFromParent()
                                  }
                              }
     
        
        
    let controller:Search_ChildViewController =
       self.storyboard!.instantiateViewController(withIdentifier: "Search_ChildViewController") as!
       Search_ChildViewController

                       let statusbarHeight = UIApplication.shared.statusBarFrame.height
       //        let navigationBarHeight = uiappl
               
               let tabBarHeight = self.tabBarController!.tabBar.frame.size.height+100;

               
               
               controller.view.frame = CGRect(x: 0, y: statusbarHeight+100, width: UIScreen.main.bounds.size.width, height: (UIScreen.main.bounds.size.height-tabBarHeight))
               controller.willMove(toParent: self)
               self.view.addSubview(controller.view)
               self.addChild(controller)
               controller.didMove(toParent: self)
        
let newContentOffsetX = ((statisticsScrollView.contentSize.width - statisticsScrollView.frame.size.width) / 2)-100;
        statisticsScrollView.contentOffset = CGPoint(x: newContentOffsetX, y: 0);

    }
    
    
    @IBAction func archive(_ sender: Any) {
gjepcanalysisImage.isHidden = true
importImage.isHidden = true
exportImage.isHidden = true
searchImage.isHidden = true
archiveImage.isHidden = false
analyticsReportsImage.isHidden = true
tradeInformationImage.isHidden = true
               if self.children.count >= 0{
                         let viewControllers:[UIViewController] = self.children
                         for viewContoller in viewControllers{
                             viewContoller.willMove(toParent: nil)
                             viewContoller.view.removeFromSuperview()
                             viewContoller.removeFromParent()
                         }
                     }
                
                
                let controller:ArchiveViewController =
                self.storyboard!.instantiateViewController(withIdentifier: "ArchiveViewController") as!
                ArchiveViewController

                let statusbarHeight = UIApplication.shared.statusBarFrame.height
        //        let navigationBarHeight = uiappl
                
                let tabBarHeight = self.tabBarController!.tabBar.frame.size.height+100;

                
                
                controller.view.frame = CGRect(x: 0, y: statusbarHeight+100, width: UIScreen.main.bounds.size.width, height: (UIScreen.main.bounds.size.height-tabBarHeight))
                controller.willMove(toParent: self)
                self.view.addSubview(controller.view)
                self.addChild(controller)
                controller.didMove(toParent: self)
        
        let newContentOffsetX = ((statisticsScrollView.contentSize.width - statisticsScrollView.frame.size.width) / 2)+5;
        statisticsScrollView.contentOffset = CGPoint(x: newContentOffsetX, y: 0);

        
    }
    
    @IBAction func analytics(_ sender: Any) {
        
gjepcanalysisImage.isHidden = true
importImage.isHidden = true
exportImage.isHidden = true
searchImage.isHidden = true
archiveImage.isHidden = true
analyticsReportsImage.isHidden = false
tradeInformationImage.isHidden = true
        
        if self.children.count >= 0{
            let viewControllers:[UIViewController] = self.children
            for viewContoller in viewControllers{
                viewContoller.willMove(toParent: nil)
                viewContoller.view.removeFromSuperview()
                viewContoller.removeFromParent()
            }
        }
        
        let controller:AnalyticsViewController =
                self.storyboard!.instantiateViewController(withIdentifier: "AnalyticsViewController") as!
                AnalyticsViewController

                let statusbarHeight = UIApplication.shared.statusBarFrame.height
        //        let navigationBarHeight = uiappl
                
                let tabBarHeight = self.tabBarController!.tabBar.frame.size.height+100;

                
                
                controller.view.frame = CGRect(x: 0, y: statusbarHeight+100, width: UIScreen.main.bounds.size.width, height: (UIScreen.main.bounds.size.height-tabBarHeight))
                controller.willMove(toParent: self)
                self.view.addSubview(controller.view)
                self.addChild(controller)
                controller.didMove(toParent: self)
        
        let newContentOffsetX = ((statisticsScrollView.contentSize.width - statisticsScrollView.frame.size.width) / 2)+110;
        statisticsScrollView.contentOffset = CGPoint(x: newContentOffsetX, y: 0);
        
    }
    
    @IBAction func sliderImages(_ sender: Any) {
        
gjepcanalysisImage.isHidden = false
importImage.isHidden = true
exportImage.isHidden = true
searchImage.isHidden = true
archiveImage.isHidden = true
analyticsReportsImage.isHidden = true
tradeInformationImage.isHidden = true
        
        if self.children.count >= 0{
                   let viewControllers:[UIViewController] = self.children
                   for viewContoller in viewControllers{
                       viewContoller.willMove(toParent: nil)
                       viewContoller.view.removeFromSuperview()
                       viewContoller.removeFromParent()
                   }
               }
               
               let controller:SliderImagesViewController =
                       self.storyboard!.instantiateViewController(withIdentifier: "SliderImagesViewController") as!
                       SliderImagesViewController

                       let statusbarHeight = UIApplication.shared.statusBarFrame.height
               //        let navigationBarHeight = uiappl
                       
                       let tabBarHeight = self.tabBarController!.tabBar.frame.size.height+100;

                       
                       
                       controller.view.frame = CGRect(x: 0, y: statusbarHeight+100, width: UIScreen.main.bounds.size.width, height: (UIScreen.main.bounds.size.height-tabBarHeight))
                       controller.willMove(toParent: self)
                       self.view.addSubview(controller.view)
                       self.addChild(controller)
                       controller.didMove(toParent: self)
        statisticsScrollView.contentOffset = CGPoint(x: 0, y: 0);
        
    }
    
    @IBAction func tradeInformation(_ sender: Any) {
        
gjepcanalysisImage.isHidden = true
importImage.isHidden = true
exportImage.isHidden = true
searchImage.isHidden = true
archiveImage.isHidden = true
analyticsReportsImage.isHidden = true
tradeInformationImage.isHidden = false
        
        if self.children.count >= 0{
            let viewControllers:[UIViewController] = self.children
            for viewContoller in viewControllers{
                viewContoller.willMove(toParent: nil)
                viewContoller.view.removeFromSuperview()
                viewContoller.removeFromParent()
            }
        }
        
        let controller:AnalyticsViewController =
                self.storyboard!.instantiateViewController(withIdentifier: "AnalyticsViewController") as!
                AnalyticsViewController
        controller.type = "TRADEINFORMATION"

                let statusbarHeight = UIApplication.shared.statusBarFrame.height
        //        let navigationBarHeight = uiappl
                
                let tabBarHeight = self.tabBarController!.tabBar.frame.size.height+100;

                
                
                controller.view.frame = CGRect(x: 0, y: statusbarHeight+90, width: UIScreen.main.bounds.size.width, height: (UIScreen.main.bounds.size.height-tabBarHeight))
                controller.willMove(toParent: self)
                self.view.addSubview(controller.view)
                self.addChild(controller)
                controller.didMove(toParent: self)
        let newContentOffsetX = ((statisticsScrollView.contentSize.width - statisticsScrollView.frame.size.width) / 2)+220;
        statisticsScrollView.contentOffset = CGPoint(x: newContentOffsetX, y: 0);
        
    }
}
