//
//  Customs_ObjectsViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 14/01/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
class Custom_ObjectsViewController: UIViewController {

    var refreshTable : UIRefreshControl!
    var activity : UIActivityIndicatorView? = nil
    var activityLoader : MBProgressHUD? = nil
    var isInternet : Bool!
    
    func displayAlertMessage() {
        let alert = UIAlertController(title: "", message: "No Internet Connection", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Retry", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func checkForInternet() {
        if CheckInternet.isConnectedToNetwork() == true
        {
            print("internet available")
            DispatchQueue.main.async {
                self.loadActivityIndicator()
            }
        }
        else
        {
            print("no internet")
            activityLoader?.hide(animated: true)
        }
    }

    
    func loadActivityIndicator()
    {
        activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
        activityLoader?.label.text = "Loading";
        activityLoader?.detailsLabel.text = "Please Wait!!!";
        activityLoader?.isUserInteractionEnabled = false;
    }
    
       func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func whenNoInternet() {
        //declare this property where it won't go out of scope relative to your listener
        let reachability = Reachability()!
        
        reachability.whenReachable = { reachability in
            // this is called on a background thread, but UI updates must
            // be on the main thread, like this:
            DispatchQueue.main.async() {
                if reachability.isReachableViaWiFi {
                    print("Reachable via WiFi")
                } else {
                    print("Reachable via Cellular")
                }
            }
        }
        
        reachability.whenUnreachable = { reachability in
            // this is called on a background thread, but UI updates must
            // be on the main thread, like this:
            DispatchQueue.main.async() {
                print("Not reachable / no internet")
                self.isInternet = false
                
            }
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.displayAlertMessage()
        self.whenNoInternet()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
