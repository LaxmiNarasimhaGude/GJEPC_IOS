//
//  ShowUpdateContent.swift
//  GJEPC
//
//  Created by Kwebmaker on 19/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit

class ShowUpdateContent: UIViewController {

    @IBOutlet weak var content: UITextView!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    
       var titleUpdate = ""
       var contentUpdate = ""
       var dateString = ""
       var timeString = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.content.attributedText = self.contentUpdate.convertHtml()
//        content.font = UIFont(name: content.font!.fontName, size: 15)
        self.headingLabel.text = self.titleUpdate
        self.dateLabel.text = self.dateString
        self.timeLabel.text = self.timeString
        
    }
  
    @IBAction func back(_ sender: Any) {
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
        }
    }
}
extension String{
    func convertHtml() -> NSAttributedString{
        
guard let data = data(using: .utf8) else { return NSAttributedString() }
do{
            return try NSAttributedString(data: data,
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        }catch{
            return NSAttributedString()
        }
        
        
       
        
        
    }
}
