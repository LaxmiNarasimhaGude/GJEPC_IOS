//
//  VenueService.swift
//  GJEPC
//
//  Created by Kwebmaker on 19/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
class VenueService: UIViewController,dataReceivedFromServerDelegate,UITableViewDataSource,UITableViewDelegate {
   
    
  
    

    @IBOutlet weak var viewTitle: UILabel!
    @IBOutlet weak var venuServiceTableView: UITableView!
    
    
    var tableViewArray : NSArray = []
    var requestToServer = RequestToServer()
    var callUrls : URL!
    var selectedFileName = ""
    var fileName = ""
    var eventName : String = ""
    var year : String = ""
    var customObj = Custom_ObjectsViewController()
    var requestName = ""
    var params : NSDictionary = [:]
    var activityLoader : MBProgressHUD? = nil
    override func viewDidLoad() {
          super.viewDidLoad()
      //    print(eventName)
    //      print(requestName)
          requestToServer.delegate = self
          getRequest()
          venuServiceTableView.tableFooterView = UIView(frame: CGRect.zero)
          checkForInternet()
//          retryBtnOutlet.layer.cornerRadius = 5
      }
    func loadActivityIndicator()
       {
           activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
           activityLoader?.label.text = "Loading";
           activityLoader?.detailsLabel.text = "Please Wait";
           activityLoader?.isUserInteractionEnabled = false;
       }
    func dataReceivedFromServer(data: NSDictionary, url: URL) {
        
        if ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "true" {
            
            
            
            
            
            let temporaryArray = ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "Result") as! NSArray)
            tableViewArray = (temporaryArray.mutableCopy() as! NSMutableArray)
            print(tableViewArray)
            DispatchQueue.main.async {
                self.activityLoader?.hide(animated: true)
                self.venuServiceTableView.reloadData()
            }
        }
    }
   
    func getRequest()
       {
           if requestName == "venue"
           {
               callUrls = venueServiceURL
               self.viewTitle.text = "Service At Venue"
           }
           else if requestName == "showinfo"
           {
               callUrls = showInfoUrl
               self.viewTitle.text = "Show Info"
               
           } else if requestName == "reach"
           {
               callUrls = howtoreach
               self.viewTitle.text = "How To Reach"

           } else if requestName == "operation"
           {
               callUrls = op
               self.viewTitle.text = "Operation Manual"
           }
       }
    
    func checkForInternet()
    {
        if CheckInternet.isConnectedToNetwork() == true
        {
            self.venuServiceTableView.isHidden = false
//            self.noInternetLbl.isHidden = true
//            self.retryBtnOutlet.isHidden = true
            
            ////shraddha
            
            DispatchQueue.main.async {
                self.loadActivityIndicator()
            }
            
            if requestName == "operation" {
                 params = [ "event_name" : "signature", "year": "2021"]
            } else {
                 params = [ "event_name" : eventName, "year": "2021"]
            }
            
           
            
            print(params)
            print(callUrls)
           
            
            requestToServer.connectToServer(myUrl: callUrls as URL, params: params as AnyObject)
           
        }
        else
        {
//            self.noInternetLbl.isHidden = false
//            self.retryBtnOutlet.isHidden = false
            self.venuServiceTableView.isHidden = true
            
        }
    }
     func numberOfSections(in tableView: UITableView) -> Int {
           return 1
       }
       
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return tableViewArray.count
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           
           let cell = tableView.dequeueReusableCell(withIdentifier: "VenueCellTableViewCell") as! VenueCellTableViewCell
           
           cell.selectionStyle = .none
           
           cell.title.text = ((self.tableViewArray.object(at: indexPath.row) as AnyObject).object(forKey: "title") as! String)
           
           DispatchQueue.main.async {
                      cell.BagroundView1.backgroundColor = UIColor.white
                     cell.BagroundView1.layer.cornerRadius = 3.0
                     cell.BagroundView1.layer.masksToBounds = false
                                

                     cell.BagroundView1.layer.shadowOffset = CGSize(width: 0, height: 0)
                     cell.BagroundView1.layer.shadowOpacity = 0.3
                     

                 }
        
           return cell
       }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return 90.0
       }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedFileName =  ((self.tableViewArray.object(at: indexPath.row) as AnyObject).object(forKey: "title") as! String)
        fileName = ((self.tableViewArray.object(at: indexPath.row) as AnyObject).object(forKey: "html_files") as! String)
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ServiceVenueWebVIew") as! ServiceVenueWebVIew
        vc.fileTitle = selectedFileName
        vc.htmlFileName = fileName
        vc.requestName = requestName
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }

    @IBAction func back(_ sender: Any) {
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
        }
    }
}
