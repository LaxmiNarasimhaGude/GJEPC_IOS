//
//  ShowUpdate.swift
//  GJEPC
//
//  Created by Kwebmaker on 19/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
class ShowUpdate: UIViewController,dataReceivedFromServerDelegate,UITableViewDelegate,UITableViewDataSource {
   
    
    
    @IBOutlet weak var showupdatetableview: UITableView!
    
       var requestToServer = RequestToServer()
       var callUrls : URL!
       var getUpdatesArray : NSArray = []
       var eventName :  String = ""
       var activityLoader : MBProgressHUD? = nil
       var customObj = Custom_ObjectsViewController()
       var activity : MBProgressHUD!
       
       var sectionArray : NSMutableArray = []
       var sectionListData : NSMutableDictionary = [:]
    var year:String!

    override func viewDidLoad() {
        super.viewDidLoad()
        showupdatetableview.sizeToFit()
//        Retybtton.layer.cornerRadius = 5
        checkForInternetAndLoadData()
        showupdatetableview.tableFooterView = UIView(frame: CGRect.zero)
    }
    func loadActivityIndicator()
       {
           activity = MBProgressHUD.showAdded(to: self.view, animated: true);
           activity?.label.text = "Loading";
           activity?.detailsLabel.text = "Please Wait";
           activity?.isUserInteractionEnabled = false;
       }
    func checkForInternetAndLoadData()
       {
           if CheckInternet.isConnectedToNetwork() == true{
               showupdatetableview.isHidden = false
               
               requestToServer.delegate = self
               
               callUrls = updateList
               
               let params = [
                   "year":year,
                   "event_name":eventName ]
               
               
               print(callUrls)
               print(params)
               
               requestToServer.connectToServer(myUrl: callUrls as URL, params:params as AnyObject)
               
               DispatchQueue.main.async {
                   self.loadActivityIndicator()
               }
           }
               
           else
           {
               showupdatetableview.isHidden = true
           }
       }
    
    func dataReceivedFromServer(data: NSDictionary, url: URL) {
        
        
        
        if ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "true" {
            
            DispatchQueue.main.async {
               MBProgressHUD.hide(for: self.view, animated: true)
            }
            
            
            let temporaryArray = ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "Result") as! NSArray)
            getUpdatesArray = (temporaryArray.mutableCopy() as! NSMutableArray)
            
            if getUpdatesArray.count > 0 {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.sortData()
                }
            }
            else {
                
                
                let alert = UIAlertController(title: "Alert", message: "No Record Found", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
                DispatchQueue.main.async {
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
        func sortData() {
            
            for i in 0..<getUpdatesArray.count {
                let dateTime = (getUpdatesArray.object(at: i) as! NSDictionary).value(forKey: "post_date") as! String
                let date = dateTime.components(separatedBy: " ")
                let dateStr = date[0]
                if !sectionArray.contains(dateStr) {
                    sectionArray.add(dateStr)
                }
            }
            
            var tempDict : NSMutableArray = []
            
            for i in 0..<sectionArray.count {
                tempDict = []
                let sectiondate  = self.sectionArray[i] as! String
                for j in 0..<self.getUpdatesArray.count {
                    let dateTime = (getUpdatesArray.object(at: j) as! NSDictionary).value(forKey: "post_date") as! String
                    let date = dateTime.components(separatedBy: " ")
                    let dateStr = date[0]
                    if sectiondate == dateStr {
                        tempDict.add((getUpdatesArray.object(at: j) as! NSDictionary))
                    }
                }
                sectionListData.setObject(tempDict, forKey: sectiondate as NSCopying)
            }
            
            DispatchQueue.main.async {
    //            self.activityLoader?.hide(animated: true)
                MBProgressHUD.hide(for: self.view, animated: true)
                
                self.showupdatetableview.reloadData()
            }
        }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (sectionListData.object(forKey: self.sectionArray[section]) as! NSArray).count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionListData.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        print(sectionArray[section])
        let dateFormat:String = (sectionArray[section] as? String)!
       let trimmed =  dateFormat.components(separatedBy: "-")
        print(trimmed)
    
        let trimmedText = "\(trimmed[2])"+"-"+"\(trimmed[1])"+"-"+"\(trimmed[0])"
        print(trimmedText)
        return trimmedText as? String
    }
    
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ShowUpdateTablecell") as! ShowUpdateTablecell
            
            cell.selectionStyle = .none
            
            let trimText = ((self.sectionListData.object(forKey: self.sectionArray[indexPath.section]) as! NSArray).object(at: indexPath.row) as! NSDictionary).value(forKey: "post_date") as! String?

            let resultString = trimText?.components(separatedBy: " ")

    //        cell.timelbl.text = ""
            cell.desclbl.text = ((self.sectionListData.object(forKey: self.sectionArray[indexPath.section]) as! NSArray).object(at: indexPath.row) as! NSDictionary).value(forKey: "title") as! String?
            return cell
        }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShowUpdateContent") as! ShowUpdateContent
            vc.titleUpdate = (((self.sectionListData.object(forKey: self.sectionArray[indexPath.section]) as! NSArray).object(at: indexPath.row) as! NSDictionary).value(forKey: "title") as! String?)!
            vc.contentUpdate = (((self.sectionListData.object(forKey: self.sectionArray[indexPath.section]) as! NSArray).object(at: indexPath.row) as! NSDictionary).value(forKey: "description") as! String?)!
            let trimText = ((self.sectionListData.object(forKey: self.sectionArray[indexPath.section]) as! NSArray).object(at: indexPath.row) as! NSDictionary).value(forKey: "post_date") as! String?
            print(trimText!)
            let resultString = trimText?.components(separatedBy: " ")
    //        print(resultString)
            
            let trimmedDate = resultString![0]
             let trimmed =  trimmedDate.components(separatedBy: "-")
            print(trimmed)
            
            let trimmedDateText = "\(trimmed[2])"+"-"+"\(trimmed[1])"+"-"+"\(trimmed[0])"
            
    //        vc.timeString = (resultString?[0])!
            vc.dateString = (trimmedDateText)
            DispatchQueue.main.async {
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
////       headerImage = UIImageView(frame: CGRect(5, 8, 40, 40));
//        let headerImage: UIImage = UIImage(named: "507257")!
//        let headerView = UIImageView()
//        headerView.frame = CGRect(x: 5, y: 8, width: 40, height: 40)
//        headerView.image = headerImage
//        return headerView
//    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//
//        var myCustomView = UIImageView()
//        var myImage: UIImage = UIImage(named: "507257")!
//        myCustomView.image = myImage
//
//        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
//        header.addSubview(myCustomView)
//        return header
//    }
}
