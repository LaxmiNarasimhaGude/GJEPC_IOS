//
//  ServiceVenueWebVIew.swift
//  GJEPC
//
//  Created by Kwebmaker on 19/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
class ServiceVenueWebVIew: UIViewController,UIWebViewDelegate {
    
    @IBOutlet weak var webview: UIWebView!
    @IBOutlet weak var headerLbl: UILabel!
    var fileTitle : String = ""
       var htmlFileName = ""
       var requestName = ""
       var fileurl : URL!
       var isDir: ObjCBool = false
       var request : NSMutableURLRequest?
       var fm = FileManager()
       var comingFromExhiProfile = false
       var exhibitorId = ""
      var activityLoader : MBProgressHUD? = nil


    override func viewDidLoad() {
        super.viewDidLoad()
        
        webview.delegate = self
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0
     webview.delegate = self
    
        webview.backgroundColor = UIColor.white
        var docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last as NSURL?
        
        print(requestName)
        
        if requestName == "showinfo"
        {
            docURL = docURL?.appendingPathComponent(htmlFileName) as NSURL?

             fileurl = URL(string: "https://gjepc.org/gjepc_mobile/uploads/showinfo/\(htmlFileName)")
            
            let urlRequest = NSURLRequest(url: fileurl!)
            
            do
            {
                let data = try NSURLConnection.sendSynchronousRequest(urlRequest as URLRequest, returning: nil)
                
                try data.write(to: docURL! as URL)
                print(docURL!)
            }
            catch
            {
                
            }
        }
            
        else if requestName == "venue"
        {
            docURL = docURL?.appendingPathComponent(htmlFileName) as NSURL?
            fileurl = URL(string: "https://gjepc.org/gjepc_mobile/uploads/venue/\(htmlFileName)")
            let urlRequest = NSURLRequest(url: fileurl!)
            do
            {
                let data = try NSURLConnection.sendSynchronousRequest(urlRequest as URLRequest, returning: nil)
                try data.write(to: docURL! as URL)
                print(docURL!)
            }
            catch
                {
                    }
        }
            else if requestName == "reach"
            {
                docURL = docURL?.appendingPathComponent(htmlFileName) as NSURL?
                fileurl = URL(string: "https://gjepc.org/gjepc_mobile/uploads/reach/\(htmlFileName)")
                let urlRequest = NSURLRequest(url: fileurl!)
                do
                {
                    let data = try NSURLConnection.sendSynchronousRequest(urlRequest as URLRequest, returning: nil)
                    try data.write(to: docURL! as URL)
                    print(docURL!)
                }
                catch
                {
                }
        }
                else if requestName == "operation"
                {
                    docURL = docURL?.appendingPathComponent(htmlFileName) as NSURL?
                    fileurl = URL(string: "https://gjepc.org/gjepc_mobile/uploads/oops/\(htmlFileName)")
                    let urlRequest = NSURLRequest(url: fileurl!)
                    do
                    {
                        let data = try NSURLConnection.sendSynchronousRequest(urlRequest as URLRequest, returning: nil)
                        try data.write(to: docURL! as URL)
                        print(docURL!)
                    }
                    catch
                    {
                    }
        }
        
        else if requestName == "floorPlan"
        {
            if !comingFromExhiProfile {
                fileurl = URL(string: "https://gjepc.org/gjepc_mobile/floor_plan/\(htmlFileName)")
              print(fileurl)

                
            }
            else {
                fileurl = URL(string: "https://gjepc.org/gjepc_mobile/floor_plan/floor.html?search=\(self.exhibitorId)")
                print(fileurl)
            }
           
//            var WebView = WKWebView()
//
//           let preferences = WKPreferences()
//            preferences.javaScriptEnabled = true
//
//            let configuration = WKWebViewConfiguration()
//            configuration.preferences = preferences
//
//            WebView = WKWebView(frame: viewForWeb.bounds, configuration: configuration)
//            let urlRequest = NSURLRequest(url: fileurl)
//                WebView.load(urlRequest as URLRequest)
//
//
//
//                viewForWeb.addSubview(WebView)
        
        }
        if requestName == "floorPlan"
        {
            headerLbl.text = "Floor Plan"
        }
        else
        {
        headerLbl.text = fileTitle
        }
         loadActivityIndicator()
        print(fileurl!)
        
        
        let request = URLRequest(url: fileurl! as URL)
        webview.loadRequest(request)
    }
    
        func loadActivityIndicator() {
            activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
            activityLoader?.label.text = "Loading";
            activityLoader?.detailsLabel.text = "Please Wait";
            activityLoader?.isUserInteractionEnabled = false;
           
        }
    //    func webViewDidFinishLoad(_ webView : UIWebView) {
    //      //  self.activityLoader?.hide(animated: true)
    //    }
        func webViewDidFinishLoad(_ webView: UIWebView) {
        activityLoader?.hide(animated: true)
           
        }
        func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
        {
            activityLoader?.hide(animated: true)

             print("called finish load................................\(error)")
        }
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
            return true
        }
   
    @IBAction func back(_ sender: Any) {
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
}
