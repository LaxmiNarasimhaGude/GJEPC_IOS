//
//  InternationalVisitorsViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 26/03/2021.
//  Copyright © 2021 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
class InternationalVisitorsViewController: UIViewController, dataReceivedFromServerDelegate,UIDocumentInteractionControllerDelegate,URLSessionDownloadDelegate {

    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var otpTxtField: UITextField!
    @IBOutlet weak var otpLbl: UILabel!
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var emailHeadingLbl: UILabel!
    
    var requestToServer = RequestToServer()
    var activityLoader : MBProgressHUD? = nil
    var requestBool:Bool!
    var type = ""
    var parameterString = ""
    var callUrl:URL!
    var submitUrl:URL!
    var pdfUrl : URL?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if type == "InternationalVisitors"{
            emailHeadingLbl.text = "Email"
            parameterString = "email"
            callUrl = internationalVisitorsUrl
            submitUrl = internationalVisitorsVerificationUrl
        }
        else{
            emailHeadingLbl.text = "Mobile"
            parameterString = "mobile"
            callUrl = exhibitorVisitorsUrl
            submitUrl = exhibitorVerificationUrl
        }
        requestToServer.delegate = self
        submitBtn.isHidden = true
        otpTxtField.isHidden = true
        otpLbl.isHidden = true
    }
    

   
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    func isValidPhone(testStr:String) -> Bool {
        let phoneRegEx = "^[6-9]\\d{9}$"
        var phoneNumber = NSPredicate(format:"SELF MATCHES %@", phoneRegEx)
        return phoneNumber.evaluate(with: testStr)
    }
    @IBAction func goTapped(_ sender: Any) {
        
        if emailTxtField.text != ""{
            
            if  (parameterString == "email"){
                if(isValidEmail(emailTxtField.text!)) {
                    requestBool = true
                    if CheckInternet.isConnectedToNetwork() == true{
                        DispatchQueue.main.async { [self] in
                            loadActivityIndicator()
                            submitBtn.isHidden = true
                            otpTxtField.isHidden = true
                            otpLbl.isHidden = true
                            
                            let params = ["\(parameterString)":"\(emailTxtField.text!)"]
                            
                            requestToServer.connectToServer(myUrl: callUrl! as URL, params: params as AnyObject)
                        }
                   
                }
                    else{
                    DispatchQueue.main.async { [self] in
                        alert(message: "Please check your internet connection", title: "Alert")
                    }
                    }
                }
                else{
                DispatchQueue.main.async { [self] in
                    alert(message: "Please enter perfect email", title: "Alert")
                }
                }
            }
            else{
                if  (parameterString == "mobile"){
                    if(isValidPhone(testStr: emailTxtField.text!)) {
                        requestBool = true
                        if CheckInternet.isConnectedToNetwork() == true{
                            DispatchQueue.main.async { [self] in
                                loadActivityIndicator()
                                submitBtn.isHidden = true
                                otpTxtField.isHidden = true
                                otpLbl.isHidden = true
                                
                                let params = ["\(parameterString)":"\(emailTxtField.text!)"]
                                
                                requestToServer.connectToServer(myUrl: callUrl! as URL, params: params as AnyObject)
                            }
                       
                    }
                        else{
                        DispatchQueue.main.async { [self] in
                            alert(message: "Please check your internet connection", title: "Alert")
                        }
                        }
                    }
                    else{
                    DispatchQueue.main.async { [self] in
                        alert(message: "Please enter perfect mobile number", title: "Alert")
                    }
                    }
                }

            }
        
        
       
        
        
        
  
        }
        else{
            if  (parameterString == "email"){
                alert(message: "Please enter email", title: "Alert")
            }
            else{
                alert(message: "Please enter mobile number", title: "Alert")
            }
            
        }
    }
    func dataReceivedFromServer(data: NSDictionary, url: URL) {
        
        if ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "true" {
            if (requestBool) == true{
                let temporaryArray = ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "Result") as! NSDictionary)

    //
                
//                print(temporaryArray["company_name"] as! String)
                
                DispatchQueue.main.async { [self] in
//                    nameHeadingLbl.isHidden = false
//                    nameLbl.isHidden = false
//                    companyHeadingLbl.isHidden = false
//                    companyLbl.isHidden = false
                    otpLbl.isHidden = false
                    otpTxtField.isHidden = false
                    submitBtn.isHidden = false
                    
//                    nameLbl.text = (temporaryArray["visitor_name"] as! String)
//                    companyLbl.text = (temporaryArray["company_name"] as! String)
                    activityLoader?.hide(animated: true)
                }

            }
            else{
                let temporaryArray = ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "Result") as! NSDictionary)
                print(temporaryArray)
                
                UserDefaults.standard.setValue("true", forKey: "BadgeLogin")
                
                guard let url = URL(string: "\((temporaryArray["badgeLink"])!)")else {return}

                
                let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                  let text = UserDefaults.standard.string(forKey: "path")
                  let dataPath = documentsDirectory.appendingPathComponent("badge.html")
                  print("path is:\(dataPath)")
                  do
                      {
                        let url: NSURL = NSURL(string: (temporaryArray["badgeLink"])! as! String)!
                          let urlData: NSData = try NSData(contentsOf: url as URL)
                          urlData.write(toFile: dataPath.path, atomically: true)

                      } catch let error as NSError {
                      print("Ooops! Something went wrong: \(error)")
                  }
                

                
                DispatchQueue.main.async {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "BadgeWebViewController") as! BadgeWebViewController
//                    vc.urlString = ((temporaryArray["badgeLink"])!) as! String
//                    print((temporaryArray["badgeLink"])!)
                   self.navigationController?.pushViewController(vc, animated: true)
                }
                
                
                
                
                
   
            }
        }
        else{
            DispatchQueue.main.async { [self] in
                activityLoader?.hide(animated: true)
                alert(message: ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "Message") as! String), title: "Alert")
            }
           
        }
    }

    @IBAction func submitTapped(_ sender: Any) {
        if otpTxtField.text != ""{
        if CheckInternet.isConnectedToNetwork() == true{
            requestBool = false
            let params = ["\(parameterString)":"\(emailTxtField.text!)","otp_number":"\(otpTxtField.text!)"]
            print(submitUrl)
            requestToServer.connectToServer(myUrl: submitUrl! as URL, params: params as AnyObject)
        }
        else{
            DispatchQueue.main.async { [self] in
                alert(message: "Please check internet connection", title: "Alert")
            }
            }
        
    }
        else if (otpTxtField.text == "") {
        DispatchQueue.main.async { [self] in
            alert(message: "Please enter otp", title: "Alert")
        }
        }
    }
    
    
    func loadActivityIndicator()
    {
        activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
        activityLoader?.label.text = "Loading";
        activityLoader?.detailsLabel.text = "Please Wait";
        activityLoader?.isUserInteractionEnabled = false;
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        print("File Downloaded Location- ",  location)
        
        guard let url = downloadTask.originalRequest?.url else {
            return
        }
        let docsPath = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0]
        let destinationPath = docsPath.appendingPathComponent(url.lastPathComponent)
        
        try? FileManager.default.removeItem(at: destinationPath)
        
        do{
            try FileManager.default.copyItem(at: location, to: destinationPath)
            
            UserDefaults.standard.setValue("\(destinationPath)", forKey: "Key")
            self.pdfUrl = destinationPath
            
            
            
            
            print("File Downloaded Location- ",  self.pdfUrl ?? "NOT")
            DispatchQueue.main.async {
                let pdfView = PDFViewController()
                       pdfView.pdfURL = self.pdfUrl

                self.navigationController?.pushViewController(pdfView, animated: true)
            }
           
        }catch let error {
            print("Copy Error: \(error.localizedDescription)")
        }
    }

    
}
//extension InternationalVisitorsViewController : URLSessionDownloadDelegate {
//    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
//        print("File Downloaded Location- ",  location)
//
//        guard let url = downloadTask.originalRequest?.url else {
//            return
//        }
//        let docsPath = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0]
//        let destinationPath = docsPath.appendingPathComponent(url.lastPathComponent)
//
//        try? FileManager.default.removeItem(at: destinationPath)
//
//        do{
//            try FileManager.default.copyItem(at: location, to: destinationPath)
//
////            UserDefaults.standard.set("\(destinationPath)", forKey: "Key") //setObject
//            UserDefaults.standard.setValue("\(destinationPath)", forKey: "Key")
////            print(UserDefaults.standard.value(forKey: "Key"))
//            self.pdfUrl = destinationPath
//            print("File Downloaded Location- ",  self.pdfUrl ?? "NOT")
//            DispatchQueue.main.async {
//                let pdfView = PDFViewController()
//                       pdfView.pdfURL = self.pdfUrl
//
////                self.present(pdfView, animated: true, completion: nil)
//                self.navigationController?.pushViewController(pdfView, animated: true)
//            }
//
//        }catch let error {
//            print("Copy Error: \(error.localizedDescription)")
//        }
//    }
//}
