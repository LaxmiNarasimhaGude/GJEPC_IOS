//
//  MyBadgeViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 26/03/2021.
//  Copyright © 2021 Kwebmaker. All rights reserved.
//

import UIKit

class MyBadgeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func domesticVisitors(_ sender: Any) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "DomesticVisitorViewController") as! DomesticVisitorViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
   
    @IBAction func internationalVisitors(_ sender: Any) {
        
//        let badgeBool = UserDefaults.standard.value(forKey: "BadgeLogin")
//        print(UserDefaults.standard.value(forKey: "BadgeLogin"))
//        if (badgeBool) as! String == "true"{
//          print("hello")
//        }
//        else{
            let vc = storyboard?.instantiateViewController(withIdentifier: "InternationalVisitorsViewController") as! InternationalVisitorsViewController
            vc.type = "InternationalVisitors"
            self.navigationController?.pushViewController(vc, animated: true)
//        }
       
    }
    
    @IBAction func exhibitors(_ sender: Any) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "InternationalVisitorsViewController") as! InternationalVisitorsViewController
        vc.type = "Exhibitors"
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
