//
//  DomesticVisitorViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 26/03/2021.
//  Copyright © 2021 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
import PDFKit
class DomesticVisitorViewController: UIViewController,dataReceivedFromServerDelegate {
   
    
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var companyLbl: UILabel!
    @IBOutlet weak var companyHeadingLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var nameHeadingLbl: UILabel!
    @IBOutlet weak var pancardTextField: UITextField!
    @IBOutlet weak var otpLbl: UILabel!
    
    @IBOutlet weak var otpTxtField: UITextField!
    var requestToServer = RequestToServer()
    var activityLoader : MBProgressHUD? = nil
    var requestBool:Bool!
    var pdfUrl : URL?
    override func viewDidLoad() {
        super.viewDidLoad()
        requestToServer.delegate = self
        nameHeadingLbl.isHidden = true
        nameLbl.isHidden = true
        companyHeadingLbl.isHidden = true
        companyLbl.isHidden = true
        otpLbl.isHidden = true
        otpTxtField.isHidden = true
        submitBtn.isHidden = true
        
    }
    

    @IBAction func goTapped(_ sender: Any) {
        if pancardTextField.text != nil{
            if validatePANCardNumber(pancardTextField.text!){
        requestBool = true
        
        if CheckInternet.isConnectedToNetwork() == true{
            DispatchQueue.main.async { [self] in
                loadActivityIndicator()
                nameHeadingLbl.isHidden = true
                nameLbl.isHidden = true
                companyHeadingLbl.isHidden = true
                companyLbl.isHidden = true
                otpLbl.isHidden = true
                otpTxtField.isHidden = true
                submitBtn.isHidden = true
                let params = ["pan_no":"\(pancardTextField.text!)"]
                
                requestToServer.connectToServer(myUrl: pancardUrl! as URL, params: params as AnyObject)
            }
       
    }
        
        else{
        DispatchQueue.main.async { [self] in
            alert(message: "Please check your internet connection", title: "Alert")
        }
        }
        }
            else{
                alert(message: "Please enter Perfect PanCard Number.", title: "Alert")
            }
        }
        else{
            alert(message: "Please enter PanCard Number.", title: "Alert")
        }
        
    }
    func loadActivityIndicator()
    {
        activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
        activityLoader?.label.text = "Loading";
        activityLoader?.detailsLabel.text = "Please Wait";
        activityLoader?.isUserInteractionEnabled = false;
    }
    func dataReceivedFromServer(data: NSDictionary, url: URL) {
        
        if ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "true" {
            if (requestBool) == true{
                let temporaryArray = ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "Result") as! NSDictionary)

    //
                
                print(temporaryArray["company_name"] as! String)
                
                DispatchQueue.main.async { [self] in
                    nameHeadingLbl.isHidden = false
                    nameLbl.isHidden = false
                    companyHeadingLbl.isHidden = false
                    companyLbl.isHidden = false
                    otpLbl.isHidden = false
                    otpTxtField.isHidden = false
                    submitBtn.isHidden = false
                    
                    nameLbl.text = (temporaryArray["visitor_name"] as! String)
                    companyLbl.text = (temporaryArray["company_name"] as! String)
                    activityLoader?.hide(animated: true)
                }

            }
            else{
                let temporaryArray = ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "Result") as! NSDictionary)
                print(temporaryArray)
                
                
                guard let url = URL(string: "\(String(describing: temporaryArray["badgeLink"]))")else {return}
                       let urlSession = URLSession(configuration: .default, delegate: self, delegateQueue: OperationQueue())
                       let downloadTask = urlSession.downloadTask(with: url)
                       downloadTask.resume()
            }
        }
        else{
            DispatchQueue.main.async { [self] in
                activityLoader?.hide(animated: true)
                alert(message: ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "Message") as! String), title: "Alert")
            }
           
        }
    }
    @IBAction func submitPressed(_ sender: Any) {
        if otpTxtField.text != ""{
            
            
                if CheckInternet.isConnectedToNetwork() == true{
                    requestBool = false
                    let params = ["pan_no":"\(pancardTextField.text!)","otp_number":"\(otpTxtField.text!)"]
                    
                    requestToServer.connectToServer(myUrl: panVerificationUrl! as URL, params: params as AnyObject)
             
            }
                else{
                DispatchQueue.main.async { [self] in
                    alert(message: "Please check your internet connection", title: "Alert")
                }
                }
           
        }
        else if (otpTxtField.text == "") {
        DispatchQueue.main.async { [self] in
            alert(message: "Please enter otp", title: "Alert")
        }
        }
        
        
        
        
    }
    func validatePANCardNumber(_ strPANNumber : String) -> Bool{
            let regularExpression = "[A-Z]{5}[0-9]{4}[A-Z]{1}"
            let panCardValidation = NSPredicate(format : "SELF MATCHES %@", regularExpression)
            return panCardValidation.evaluate(with: strPANNumber)
        }
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}
extension UIViewController {
  func alert(message: String, title: String = "") {
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
    alertController.addAction(OKAction)
    self.present(alertController, animated: true, completion: nil)
  }
}
extension DomesticVisitorViewController : URLSessionDownloadDelegate {
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        print("File Downloaded Location- ",  location)
        
        guard let url = downloadTask.originalRequest?.url else {
            return
        }
        let docsPath = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0]
        let destinationPath = docsPath.appendingPathComponent(url.lastPathComponent)
        
        try? FileManager.default.removeItem(at: destinationPath)
        
        do{
            try FileManager.default.copyItem(at: location, to: destinationPath)
            
//            UserDefaults.standard.set("\(destinationPath)", forKey: "Key") //setObject
            UserDefaults.standard.setValue("\(destinationPath)", forKey: "Key")
//            print(UserDefaults.standard.value(forKey: "Key"))
            self.pdfUrl = destinationPath
            print("File Downloaded Location- ",  self.pdfUrl ?? "NOT")
            DispatchQueue.main.async {
                let pdfView = PDFViewController()
                       pdfView.pdfURL = self.pdfUrl

//                self.present(pdfView, animated: true, completion: nil)
                self.navigationController?.pushViewController(pdfView, animated: true)
            }
           
        }catch let error {
            print("Copy Error: \(error.localizedDescription)")
        }
    }
}
