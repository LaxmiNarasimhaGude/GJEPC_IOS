//
//  BadgeWebViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 29/03/2021.
//  Copyright © 2021 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
class BadgeWebViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
    let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
//
//        var path = documentsDirectory.appendingPathComponent("badge.html")
//        let data: NSData = NSData(contentsOfFile:path)!
//        var html = String(data: data as Data, encoding:String.Encoding.utf8)
//
//        webView.loadHTMLString(html as! String, baseURL: Bundle.main.bundleURL)
        
        
        
            let dataPath = documentsDirectory.appendingPathComponent("badge.html")
            print("path is:\(dataPath)")
            let url = URL.init(fileURLWithPath: dataPath.path);

            let requestObj = URLRequest.init(url: url)

            webView.loadRequest(requestObj)
        
        
        
//        webView.loadRequest(URLRequest(url: URL(fileURLWithPath: Bundle.main.path(forResource: "index", ofType: "html") ?? "", isDirectory: false)))
    }
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        DispatchQueue.main.async {
            MBProgressHUD.hide(for: self.view, animated: true)
            
        }
        
    }
  

}
