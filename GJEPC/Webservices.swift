//
//  Webservices.swift
//  GJEPC
//
//  Created by Kwebmaker on 07/01/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import Foundation
let dailyNewsUrl = URL(string:  "https://gjepc.org/gjepc_mobile/services/newsList")
let ImageUrl = "https://gjepc.org/admin/images/news_images/"
let upload_vc_URL = "https://gjepc.org/gjepc_mobile/services/article"
let fortynightUrl = URL(string : "https://gjepc.org/gjepc_mobile/services/newsletter")
let epatrikaUrl = URL(string : "https://gjepc.org/gjepc_mobile/services/patrika")
let labsInstituteImgUrl = "https://gjepc.org/gjepc_mobile/uploads/labandeducation/"
let laboratoriesUrl = URL(string:"https://gjepc.org/gjepc_mobile/services/laboratorylist/")
let instituesUrl = URL(string:"https://gjepc.org/gjepc_mobile/services/institutesList/")
let cfcURL = URL(string:"https://gjepc.org/gjepc_mobile/services/cfclist/")
let CheckForNotificationURL = URL(string: "https://gjepc.org/gjepc_mobile/services/pushNotification")
let memberDirectoryUrl = URL(string:"https://gjepc.org/gjepc_mobile/services/memberDirectoryList/")
let loadepatrikaUrl = "https://gjepc.org/epatrika/"
var Eventimgeurl = "https://gjepc.org/gjepc_mobile/uploads/gjepcevents/"
let AllEventurl = URL(string:"https://gjepc.org/gjepc_mobile/services/gjepcEventList/")
let gjepcPrivateLimited = URL(string: "https://gjepc.org/gjepc_mobile/services/gjepcIndiaPavEventList")
let bsmEventList = URL(string: "https://gjepc.org/gjepc_mobile/services/gjepcBsmEventList")
let checkEventStatusUrl = URL(string: "https://gjepc.org/gjepc_mobile/services/checkEventStatus")
let UpcomingseminarListUrl = URL(string: "https://gjepc.org/gjepc_mobile/services/upcomingSeminarList")
let postSemisterUrl = URL(string: "https://gjepc.org/gjepc_mobile/services/postSeminarList")
let seminarImageUrl = "https://www.gjepc.org/admin/calendar/"
let defaultImageUrl = "https://gjepc.org/images/mobileDefault/logo.png"
let statisticsImportUrl = URL(string: "https://gjepc.org/gjepc_mobile/services/statisticsImport/")
let statisticImportPdfUrl =  "https://gjepc.org/admin/StatisticsImport/"
let statisticsExportUrl = URL(string: "https://gjepc.org/gjepc_mobile/services/statisticsExport/")
let statisticExportPdfUrl = "https://gjepc.org/admin/StatisticsExport/"
let circularGovUrl = URL(string: "https://gjepc.org/gjepc_mobile/services/circularGov/")
let circularMemberUrl = URL(string: "https://gjepc.org/gjepc_mobile/services/circularMember")
let circularMemberPdfUrl =  "https://gjepc.org/admin/Circulars/"
let circularPdfUrl =  "https://gjepc.org/admin/Circulars/"
let updateList = URL(string : "https://gjepc.org/gjepc_mobile/services/getUpdateList/")
let venueServiceURL = URL(string : "https://gjepc.org/gjepc_mobile/services/serviceVenueList/")
let showInfoUrl = URL(string : "https://gjepc.org/gjepc_mobile/services/showInfoList/")
let howtoreach = URL(string: "https://gjepc.org/gjepc_mobile/services/howReachList/")
let op = URL(string: "https://gjepc.org/gjepc_mobile/services/oopsList/")
let MetalCurrancyRate = URL(string: "https://gjepc.org/gjepc_mobile/services/metalNCurrency")
  let GoldRates =  URL(string:"https://gjepc.org/gjepc_mobile/services/getGoldRates")
let statsSearch = URL(string : "https://gjepc.org/gjepc_mobile/services/statisticsSearch/")
let newsSearchUrl = URL(string: "https://gjepc.org/gjepc_mobile/services/newsSearch")
let upload_vc_searchURL = "https://gjepc.org/gjepc_mobile/services/articleSearch"
let fornightlySearchUrl = URL (string: "https://gjepc.org/gjepc_mobile/services/newsletterSearch")
let epatrikaSearchUrl = URL(string: "https://gjepc.org/gjepc_mobile/services/patrikaSearch")
let footerLink = URL(string: "http://bit.ly/2uSMmFi")
let brochureUrl = URL(string: "https://gjepc.org/gjepc_mobile/services/gjepcBrochureList")
let exportArchive = URL(string: "https://gjepc.org/gjepc_mobile/services/getArchivesStatisticsExport")
let archivePdfurl =  "https://gjepc.org/admin/StatisticsExport/"
let importArchive = URL(string: "https://gjepc.org/gjepc_mobile/services/getArchivesStatisticsImport")
let archiveImportPdfurl = "https://gjepc.org/admin/StatisticsImport/"
let analyticsUrl = URL(string: "https://gjepc.org/gjepc_mobile/services/getApp_analytics_report")
let leftSliderImages = URL(string: "https://gjepc.org/gjepc_mobile/services/getApp_leftStatistics_slider")
let RightSliderImages = URL(string: "https://gjepc.org/gjepc_mobile/services/getApp_rightStatistics_slider")
let sliderImagePath = "https://gjepc.org/images/graph/new/"
let rightSliderImagePath = "https://gjepc.org/images/graph/02/"
let tradeInfo = URL(string: "https://gjepc.org/gjepc_mobile/services/getApp_TradeInfo")
let statisticsImagrUrl = "https://gjepc.org/assets/images/statistics/"
let statisticsTradePdfurl = "https://gjepc.org/pdf/"
let exhibitorDirectoryUrl = URL(string: "https://gjepc.org/gjepc_mobile/services/exhibitorList")
let exhibitorListUrl = URL(string: "https://gjepc.org/gjepc_mobile/services/exhibitorMenuList")
let exhibitorlistImgUrl = "https://gjepc.org/gjepc_mobile/uploads/exhibitorlist/"
let zoneUrl = URL(string: "https://gjepc.org/gjepc_mobile/services/zoneList/")
let pancardUrl = URL(string: "https://gjepc.org/gjepc_mobile/badge/checkVisitorPan")
let panVerificationUrl = URL(string: "https://gjepc.org/gjepc_mobile/badge/verifyMobileOtpAction")
let internationalVisitorsUrl = URL(string: "https://gjepc.org/gjepc_mobile/badge/checkIntlVisitorEmail")
let exhibitorVisitorsUrl = URL(string: "https://gjepc.org/gjepc_mobile/badge/checkExhMobile")
let internationalVisitorsVerificationUrl = URL(string: "https://gjepc.org/gjepc_mobile/badge/verifyEmailOtpAction")
let exhibitorVerificationUrl = URL(string: "https://gjepc.org/gjepc_mobile/badge/verifyExhMobileOtpAction")
let appBannerUrl = URL(string: "https://gjepc.org/gjepc_mobile/services/getAppBanner")
let appBannerBaseUrl = "https://gjepc.org/assets/images/banner/"
