//
//  AppDelegate.swift
//  GJEPC
//
//  Created by Kwebmaker on 07/01/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import UserNotifications
import CoreData
import GoogleMaps
import Firebase
import FirebaseInstanceID
import FirebaseMessaging

@available(iOS 13.0, *)
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate ,UNUserNotificationCenterDelegate,MessagingDelegate {

//   let gcmMessageIDKey = "gcm.message_id"

 var window: UIWindow?
// var drawerContainer:MMDrawerController?
 var navigation:UINavigationController!
 var tabbar:UITabBarController?
 var isOpen = true
       var responsedata : NSDictionary = [:]
       var callUrls : URL!
       var date = Date()
       var todaysDate = ""
       var isFirebaseNotification = false
       
       var bodyStr = 0
    
   
    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        
        
        let state : UIApplication.State = UIApplication.shared.applicationState
        switch state {
        case UIApplicationState.active:
            
            //ForgroundMethod Call
            print("If needed notify user about the message")
            
            //            self.foregroundMethod()
            
        case UIApplicationState.inactive:
            print("Inactive")
            let notification = launchOptions?[.remoteNotification]
            if notification != nil {
                
                self.application(application, didReceiveRemoteNotification: notification as! [AnyHashable : Any])
                
            }
            
            
            
        default:
            
            //BackgroundMethod Call
            print("Run code to download content")
            
            //            self.backMethod()
            
        }
        
        return true
        
    }
    
    func connectToServer(myUrl : URL, params : AnyObject) {
         
         let request = NSMutableURLRequest(url:myUrl as URL)
         
         request.httpMethod = "POST";
         request.httpBody = try! JSONSerialization.data(withJSONObject: params, options: [])
         request.addValue("application/json", forHTTPHeaderField: "Content-Type")
         request.addValue("application/json", forHTTPHeaderField: "Accept")
         //create the session object
         let session = URLSession.shared
         
         //create dataTask using the session object to send data to the server
         let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
             
             guard error == nil else {
                 return
             }
             
             guard let data = data else {
                 return
             }
             
             do {
                 //create json object from data
                 if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                     //                    print(json)
                     self.responsedata = json as NSDictionary
                     print("Here:Response \(self.responsedata)")
                 }
             } catch let error {
                 print(error.localizedDescription)
             }
         })
         task.resume()
     }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        
        // ON CLICK NOTIFICATION
        
        
        UserDefaults.standard.setValue("true", forKey: "openNotification")
        
        let userInfo = response.notification.request.content.userInfo
        print("did receive notification\(userInfo)")
        
        
        
        
        if let aps = userInfo[AnyHashable("u")] as? String {
            
            let URL1 = NSURL(string: aps)! as URL
            DispatchQueue.main.async {
                if UIApplication.shared.canOpenURL(URL1) {
                    
                    UIApplication.shared.open(URL1, options: [:], completionHandler: nil)
                    
                }
            }
        }
        
        
        
        let state : UIApplication.State = UIApplication.shared.applicationState
        switch state {
        case UIApplicationState.active:
            
            //ForgroundMethod Call
            print("If needed notify user about the message")
            
            self.foregroundMethod()
            
        case UIApplicationState.inactive:
            print("Inactive")
            isFirebaseNotification = true
            self.inActiveMethod()
            
            
        default:
            
            //BackgroundMethod Call
            print("Run code to download content")
            
            self.backMethod()
            
        }
        
        
        completionHandler()
        
        
        
        
        
        
        
    }

    
    func notificationAction1() {
           redirectToVC()
       }
    func redirectToVC() {
        
        
        
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        // On receive notification
        
     //   UserDefaults.standard.set("1", forKey: "didEnterBackground")
        
        print("will present notification \(notification)")
        
        
        let dict =  notification.request.content.userInfo
        
        let _:[String:Any] = dict[AnyHashable("aps")] as! [String : Any]
        
        
        
        let state : UIApplication.State = UIApplication.shared.applicationState
        switch state {
        case UIApplicationState.active:
            
            //ForgroundMethod Call
            print("If needed notify user about the message")
            
                self.backMethod()
            //
            
        case UIApplicationState.inactive:
            
            
            
            isFirebaseNotification = true
            
            print("If needed notify user about the message")
            
            self.inActiveMethod()
            
        default:
            
            //BackgroundMethod Call
            print("Run code to download content")
            
            //            self.backMethod1()
            
        }
        
        // completionHandler([.badge, .alert, .sound])
        
        completionHandler([UNNotificationPresentationOptions.alert,UNNotificationPresentationOptions.sound,UNNotificationPresentationOptions.badge])
        
    }

   
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        //Onnotification Received In FOreground
        
        
        
        
        
        
        
        let userInfo1:[String:Any] = userInfo[AnyHashable("aps")] as! [String : Any]
        
        
        
        
        
        var dict:[String:Any] = userInfo1
        
        print(dict)
        
        bodyStr = ((dict["badge"] as? Int)!)
        
        
        
        
        DispatchQueue.main.async {
            
            UIApplication.shared.applicationIconBadgeNumber = self.bodyStr
        }
        
       
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        
        dict["Date"] = formatter.string(from: date)
        
        print(" ******* Entire message \(userInfo)")
        
        if var array:[AnyObject] = UserDefaults.standard.value(forKey: "NotificationList") as? [AnyObject]
        {
            print(array)
            
            
            
            print(array)
            var testFlag = false
            
            for obj in array
            {
                
                let  test = obj["alert"] as! [String:Any]
                let  test1 = dict["alert"] as! [String:Any]
                
                if (test["body"]) as? String == test1["body"] as? String
                    
                {
                    testFlag = true
                }
                
            }
            
            if(!testFlag)
            {
            
            
            
            
            array.append(dict as AnyObject)
            
            UserDefaults.standard.set(array, forKey:"NotificationList")
                
            }
            
            
            
            
            
        }
        else
        {
            
            var array:[AnyObject] = []
            
            array.append(dict as AnyObject)
            
            UserDefaults.standard.set(array, forKey:"NotificationList")
            
        }
        
        
        //print("Article avaialble for download: \(userInfo["articleId"]!)")
        
        let state : UIApplication.State = application.applicationState
        switch state {
        case UIApplicationState.active:
            
            //ForgroundMethod Call
            print("If needed notify user about the message")
            
            self.backMethod()
            
            
        case UIApplicationState.inactive:
            
            isFirebaseNotification = true
            print("If needed notify user about the message")
            
            self.inActiveMethod()
            
            //            completionHandler(UIBackgroundFetchResult.newData)
            // self.foregroundMethod()
            
        default:
            
            //BackgroundMethod Call
            print("Run code to download content")
            
            self.backMethod1()
            
        }
        
        completionHandler(UIBackgroundFetchResult.newData)
        
        
    }

    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
        
        let userInfo1:[String:Any] = userInfo[AnyHashable("aps")] as! [String : Any]
        
        var dict:[String:Any] = userInfo1
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        
        dict["Date"] = formatter.string(from: date)
        
        print(" ******* Entire message \(userInfo)")
        
        if var array:[AnyObject] = UserDefaults.standard.value(forKey: "NotificationList") as? [AnyObject]
        {
            print(array)
            
            print(array)
            var testFlag = false
            
            for obj in array
            {
                
                let  test = obj["alert"] as! [String:Any]
                let  test1 = dict["alert"] as! [String:Any]
                
                if (test["body"]) as? String == test1["body"] as? String
                    
                {
                    testFlag = true
                }
                
            }
            
            if(!testFlag)
            {
            
            array.append(dict as AnyObject)
            
            UserDefaults.standard.set(array, forKey:"NotificationList")
            }
        }
        else
        {
            
            var array:[AnyObject] = []
            
            array.append(dict as AnyObject)
            
            UserDefaults.standard.set(array, forKey:"NotificationList")
            
        }
        
        
        //print("Article avaialble for download: \(userInfo["articleId"]!)")
        
        let state : UIApplication.State = application.applicationState
        switch state {
        case UIApplicationState.active:
            
            //ForgroundMethod Call
            print("If needed notify user about the message")
            
            self.backMethod()
            
            
        case UIApplicationState.inactive:
            
            
            isFirebaseNotification = true
            
            //ForgroundMethod Call
            print("If needed notify user about the message")
            
            //self.inActiveMethod()
            self.inActiveMethod()
            
            
            
        default:
            
            //BackgroundMethod Call
            print("Run code to download content")
            
            self.backMethod1()
            
        }
        
        
        
        NotificationCenter.default.post(name: Notification.Name("didReceiveData"), object: nil)
        
        
        
        
    }
    func backMethod1(){
           
           
           if  UserDefaults.standard.value(forKey: "userSessionBadgeCnt") != nil
           {
               let cnt = 1 + ((UserDefaults.standard.value(forKey: "userSessionBadgeCnt") as! Int) )
               
               
               UserDefaults.standard.set(cnt, forKey: "userSessionBadgeCnt")
           }
           else
           {
               UserDefaults.standard.set(1, forKey: "userSessionBadgeCnt")
               
               
           }
           
           
           print("notificationCOunt on receive \(UserDefaults.standard.value(forKey: "userSessionBadgeCnt") ?? 0)")
           

           
           NotificationCenter.default.post(name: Notification.Name("updateBadgeCount"), object: nil)
           
           
           
       }
    
    func foregroundMethod(){
           
           
          
           
           NotificationCenter.default.post(name: Notification.Name("didReceiveData"), object: nil)
           
           
       }
    
    func backMethod(){
        
        
        if  UserDefaults.standard.value(forKey: "userSessionBadgeCnt") != nil
        {
            let cnt = 1 + ((UserDefaults.standard.value(forKey: "userSessionBadgeCnt") as! Int) )
            
            UserDefaults.standard.set(cnt, forKey: "userSessionBadgeCnt")
        }
        else
        {
            UserDefaults.standard.set(1, forKey: "userSessionBadgeCnt")
        }
        
        
        print("notificationCOunt on receive \(UserDefaults.standard.value(forKey: "userSessionBadgeCnt") ?? 0)")
        

        
        NotificationCenter.default.post(name: Notification.Name("updateBadgeCount"), object: nil)
        
        
        
        
    }

    
    func inActiveMethod(){
        
        
        
        UNUserNotificationCenter.current().getDeliveredNotifications { notifications in
            
            
            let array:[UNNotification]? = notifications
            print(array)
            
            
            if array != nil && array?.count != 0{
                
                //  let dict = array[0].request.content.userInfo
                
                for objectInstance in array! {
                    
                    
                    var dict:[String:Any] = ((objectInstance.request.content.userInfo as! [String : Any])["aps"] as? [String:Any])!
                    
                    let date = Date()
                    let formatter = DateFormatter()
                    formatter.dateFormat = "dd.MM.yyyy"
                    
                    dict["Date"] = formatter.string(from: date)
                    
                    print(" ******* Entire message \(dict)")
                    
                    if var array:[AnyObject] = UserDefaults.standard.value(forKey: "NotificationList") as? [AnyObject]
                    {
                        print(array)
                        var testFlag = false
                        
                        for obj in array
                        {
                            
                            let  test = obj["alert"] as! [String:Any]
                            let  test1 = dict["alert"] as! [String:Any]
                            
                            if (test["body"]) as? String == test1["body"] as? String
                                
                            {
                                testFlag = true
                            }
                            
                        }
                        
                        if(!testFlag)
                        {
                            array.append(dict as AnyObject)
                            
                            UserDefaults.standard.set(array, forKey:"NotificationList")
                            
                            
                            
                        }
                        
                        
                    }
                    else
                    {
                        
                        var array:[AnyObject] = []
                        
                        array.append(dict as AnyObject)
                        
                        UserDefaults.standard.set(array, forKey:"NotificationList")
                        
                        
                        
                    }
                    
                    
                    
                }
                
                
            }
            else
            {
                
            }
            
        }
        
        
        
        if  UserDefaults.standard.value(forKey: "userSessionBadgeCnt") != nil
        {
            let cnt = 1 + ((UserDefaults.standard.value(forKey: "userSessionBadgeCnt") as! Int) )
            
            UserDefaults.standard.set(cnt, forKey: "userSessionBadgeCnt")
        }
        else
        {
            UserDefaults.standard.set(1, forKey: "userSessionBadgeCnt")
        }
        
        
        print("notificationCOunt on receive \(UserDefaults.standard.value(forKey: "userSessionBadgeCnt") ?? 0)")
        

        
        UserDefaults.standard.set("1", forKey: "didEnterBackground")
        NotificationCenter.default.post(name: Notification.Name("updateBadgeCount"), object: nil)
        
        
        
        
    }
    func isAppAlreadyLaunchedOnce(){
            let defaults = UserDefaults.standard

            if let isAppAlreadyLaunchedOnce = defaults.string(forKey: "isAppAlreadyLaunchedOnce"){
                print("App already launched : \(isAppAlreadyLaunchedOnce)")
//                return true
            }
            else{
                defaults.set(true, forKey: "isAppAlreadyLaunchedOnce")
                UserDefaults.standard.setValue("false", forKey: "BadgeLogin")
                print("App launched first time")
//                return false
            }
        }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        self.isAppAlreadyLaunchedOnce()
      
        
              UserDefaults.standard.set("false", forKey: "isUpdateAvailable")
               UserDefaults.standard.removeObject(forKey: "openNotification")
               UserDefaults.standard.setValue("false", forKey: "callNotificationVC")
        
        
       UITabBar.appearance().tintColor = UIColor(red: 168/255.0, green: 156/255.0, blue: 93/255.0, alpha: 1.0)
        
//        if #available(iOS 13, *) {
               

//           }
        
       GMSServices.provideAPIKey("AIzaSyAzjoodx6QVkI9TdiYgavr7vxgOOzV5gw4")
       
        Messaging.messaging().delegate = self
        UNUserNotificationCenter.current().delegate = self
        application.registerForRemoteNotifications()
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
            // For iOS 10 data message (sent via FCM
            
        } else {
            let settings: UIUserNotificationSettings =
            UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
              application.registerUserNotificationSettings(settings)
        }
         
        
         
        FirebaseApp.configure()
       
                if #available(iOS 10.0, *) {
                    let center = UNUserNotificationCenter.current()
                    center.delegate = self;
                    center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
                        // Enable or disable features based on authorization.
                        if error == nil{
                            DispatchQueue.main.async {
                                UIApplication.shared.registerForRemoteNotifications()
                            }
                        }
                    }
                } else {
                    // Fallback on earlier versions
                    registerForPushNotifications(application: application)
                }
                
                
                
        if let notification = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? [String: AnyObject]
                {
                    let info : NSDictionary! = notification as NSDictionary
                    print(info)
                    if info != nil
                    {
                        
                        UserDefaults.standard.setValue("true", forKey: "showNotification")
                    }
                }
                
                
                
                
                
                
                
                if(self.needsUpdate())
                {
                    
                    
                    UserDefaults.standard.set("0", forKey: "count")
                    
                    UserDefaults.standard.set("true", forKey: "isUpdateAvailable")
                    
                    
                }
                else
                {
                    UserDefaults.standard.set("0", forKey: "count")
                    UserDefaults.standard.set("false", forKey: "isUpdateAvailable")
                    
                }
                
                
                let userInfo:[AnyHashable: Any]? = launchOptions?[.remoteNotification] as? [AnyHashable : Any]
                
                
                if userInfo != nil {
                    
                    
                    let userInfo1:[String:Any] = userInfo![AnyHashable("aps")] as! [String : Any]
                    
                    var dict:[String:Any] = userInfo1
                    
                    let date = Date()
                    let formatter = DateFormatter()
                    formatter.dateFormat = "dd.MM.yyyy"
                    
                    dict["Date"] = formatter.string(from: date)
                    
                    //            print(" ******* Entire message \(userInfo)")
                    
                    if var array:[AnyObject] = UserDefaults.standard.value(forKey: "NotificationList") as? [AnyObject]
                    {
        //                print(array)
                        print(array)
                        var testFlag = false
                        
                        for obj in array
                        {
                            
                            let  test = obj["alert"] as! [String:Any]
                            let  test1 = dict["alert"] as! [String:Any]
                            
                            if (test["body"]) as? String == test1["body"] as? String
                                
                            {
                                testFlag = true
                            }
                            
                        }
                        
                        if(!testFlag)
                        {
                        array.append(dict as AnyObject)
                        
                        UserDefaults.standard.set(array, forKey:"NotificationList")
                        }
                    }
                    else
                    {
                        
                        var array:[AnyObject] = []
                        
                        array.append(dict as AnyObject)
                        
                        UserDefaults.standard.set(array, forKey:"NotificationList")
                        
                    }
                }
                
                

       
           return true
         
      
    }
    
    func needsUpdate() -> Bool {
        
        
        let infoDictionary = Bundle.main.infoDictionary
        let appID = infoDictionary!["CFBundleIdentifier"] as! String

        let url = URL(string: "http://itunes.apple.com/lookup?id=1194017236")

        guard let data = try? Data(contentsOf: url!) else {
            print("There is an error!")
            return false;
        }
        let lookup = (try? JSONSerialization.jsonObject(with: data, options: [])) as? [String: Any]

       // print(lookup)

        if let resultCount = lookup!["resultCount"] as? Int, resultCount == 1 {
            if let results = lookup!["results"] as? [[String:Any]] {


                if let appStoreVersion = results[0]["version"] as? String{

                    let currentVersion = infoDictionary!["CFBundleShortVersionString"] as? String
                    print("current version \(currentVersion as Any)")
                    print("appstoreVersion \(appStoreVersion)")

                    let separatedCurrentVersion = currentVersion?.split(separator: ".")
                    let separatedAppStoreVersion = appStoreVersion.split(separator: ".")
                    print(separatedCurrentVersion as Any)
                    print(separatedAppStoreVersion as Any)

        if(separatedCurrentVersion![0]>separatedAppStoreVersion[0]){


                        return false
                    }
    else if(separatedCurrentVersion![1]>separatedAppStoreVersion[1]){


                        return false
                    }
    else if(separatedCurrentVersion![2]>separatedAppStoreVersion[2]){


                        return false
                    }
        else if !(appStoreVersion == currentVersion) {



                        UserDefaults.standard.set("https://itunes.apple.com/in/app/gjepc/id1194017236?mt=8", forKey: "appStoreURL")

                        return true
                    }
                }
            }
        }
        return false
    }

        func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
            
            
            
            UNUserNotificationCenter.current().getDeliveredNotifications { notifications in
                
                
                let array:[UNNotification]? = notifications
                
                
                if array != nil && array?.count != 0{
                    
                    //  let dict = array[0].request.content.userInfo
                    
                    for objectInstance in array! {
                        
                        
                        var dict:[String:Any] = ((objectInstance.request.content.userInfo as! [String : Any])["aps"] as? [String:Any])!
                        
                        let date = Date()
                        let formatter = DateFormatter()
                        formatter.dateFormat = "dd.MM.yyyy"
                        
                        dict["Date"] = formatter.string(from: date)
                        
                        print(" ******* Entire message \(dict)")
                        
                        if var array:[AnyObject] = UserDefaults.standard.value(forKey: "NotificationList") as? [AnyObject]
                        {
                            print(array)
                            var testFlag = false
                            
                            for obj in array
                            {
                                
                                let  test = obj["alert"] as! [String:Any]
                                let  test1 = dict["alert"] as! [String:Any]
                                
                                if (test["body"]) as? String == test1["body"] as? String
                                    
                                {
                                    testFlag = true
                                }
                                
                            }
                            
                            if(!testFlag)
                            {
                                array.append(dict as AnyObject)
                                
                                UserDefaults.standard.set(array, forKey:"NotificationList")
                                
                               
                                
                            }
                            
                            
                        }
                        else
                        {
                            
                            var array:[AnyObject] = []
                            
                            array.append(dict as AnyObject)
                            
                            UserDefaults.standard.set(array, forKey:"NotificationList")
                            
                           
                            
                        }
                        
                        
                        
                    }
                    
                    
                }
                else
                {
                    
                }
                
            }
            
            
            
            if  UserDefaults.standard.value(forKey: "userSessionBadgeCnt") != nil
            {
                let cnt = 1 + ((UserDefaults.standard.value(forKey: "userSessionBadgeCnt") as! Int) )
                
                UserDefaults.standard.set(cnt, forKey: "userSessionBadgeCnt")
            }
            else
            {
                UserDefaults.standard.set(1, forKey: "userSessionBadgeCnt")
            }
            
            
            print("notificationCOunt on receive \(UserDefaults.standard.value(forKey: "userSessionBadgeCnt") ?? 0)")
            
   
            
            UserDefaults.standard.set("1", forKey: "didEnterBackground")
            NotificationCenter.default.post(name: Notification.Name("updateBadgeCount"), object: nil)
            
            
            
            
        }
    func applicationDidBecomeActive(_ application: UIApplication) {
        
        
        
        // In Swift, to see all your currently scheduled local notifications printed in the console:
        
        
        
        
        
        if let check = UserDefaults.standard.value(forKey: "didEnterBackground") as? String
        {
            
            
            
            if check == "1"
            {
                UserDefaults.standard.set("0", forKey: "didEnterBackground")
                
                NotificationCenter.default.post(name: Notification.Name("didReceiveData"), object: nil)
                
            }
            else{
                
            }
        }
        else
        {
            
        }
        
        
        UNUserNotificationCenter.current().getDeliveredNotifications { notifications in
            
            
            let array:[UNNotification]? = notifications
            
            
            if array != nil && array?.count != 0{
                
                //  let dict = array[0].request.content.userInfo
                
                for objectInstance in array! {
                    
                    
                    var dict:[String:Any] = ((objectInstance.request.content.userInfo as! [String : Any])["aps"] as? [String:Any])!
                    
                    
                    self.bodyStr = (dict["badge"] as? Int)!
                    
                    DispatchQueue.main.async {
                        
                        UIApplication.shared.applicationIconBadgeNumber = self.bodyStr
                        
                    }
                    
                    
                    
                    let date = Date()
                    let formatter = DateFormatter()
                    formatter.dateFormat = "dd.MM.yyyy"
                    
                    dict["Date"] = formatter.string(from: date)
                    
                    print(" ******* Entire message \(dict)")
                    
                    if var array:[AnyObject] = UserDefaults.standard.value(forKey: "NotificationList") as? [AnyObject]
                    {
                        print(array)
                        var testFlag = false
                        
                        for obj in array
                        {
                            
                            let  test = obj["alert"] as! [String:Any]
                            let  test1 = dict["alert"] as! [String:Any]
                            
                            if (test["body"]) as? String == test1["body"] as? String
                                
                            {
                                testFlag = true
                            }
                            
                        }
                        
                        if(!testFlag)
                        {
                            array.append(dict as AnyObject)
                            
                            UserDefaults.standard.set(array, forKey:"NotificationList")
                            
                            if  UserDefaults.standard.value(forKey: "userSessionBadgeCnt") != nil
                            {
                                let cnt = 1 + ((UserDefaults.standard.value(forKey: "userSessionBadgeCnt") as! Int) )
                                
                                
                                UserDefaults.standard.set(cnt, forKey: "userSessionBadgeCnt")
                            }
                            else
                            {
                                UserDefaults.standard.set(1, forKey: "userSessionBadgeCnt")
                                
                                
                            }
                            
                            
                            print("notificationCOunt on receive \(UserDefaults.standard.value(forKey: "userSessionBadgeCnt") ?? 0)")
                            

                            
                            NotificationCenter.default.post(name: Notification.Name("updateBadgeCount"), object: nil)
                            
                        }
                        
                        
                    }
                    else
                    {
                        
                        var array:[AnyObject] = []
                        
                        array.append(dict as AnyObject)
                        
                        UserDefaults.standard.set(array, forKey:"NotificationList")
                        
                        if  UserDefaults.standard.value(forKey: "userSessionBadgeCnt") != nil
                        {
                            let cnt = 1 + ((UserDefaults.standard.value(forKey: "userSessionBadgeCnt") as! Int) )
                            
                            
                            UserDefaults.standard.set(cnt, forKey: "userSessionBadgeCnt")
                        }
                        else
                        {
                            UserDefaults.standard.set(1, forKey: "userSessionBadgeCnt")
                            
                            
                        }
                        
                        
                        print("notificationCOunt on receive \(UserDefaults.standard.value(forKey: "userSessionBadgeCnt") ?? 0)")
                        

                        
                        NotificationCenter.default.post(name: Notification.Name("updateBadgeCount"), object: nil)
                        
                    }
                    
                    
                    
                }
                
                
            }
            else
            {
                
            }
            
        }
        
        
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
              
              print("Firebase registration token: \(fcmToken)")
              
              let dataDict:[String: String] = ["token": fcmToken]
              NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
              
              // TODO: If necessary send token to application server.
              // Note: This callback is fired at each app startup and whenever a new token is generated.
          }
    @objc(application:didRegisterForRemoteNotificationsWithDeviceToken:) func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        
        
        var token = ""
        for i in 0..<deviceToken.count {
            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        print("TOKEN NEW \(token)")

        
        callUrls = CheckForNotificationURL
        print(callUrls)
        
        
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            }
            else if let result = result {
                
                let params = ["deviceType":"I",
                              "deviceId":"\(result.token)"]
                
                
                print("Remote instance ID token: \(result.token)")
                
                UserDefaults.standard.setValue(result.token, forKey: "iOSToken")
                
                DispatchQueue.main.async {
                    self.connectToServer(myUrl: self.callUrls, params: params as AnyObject)
                }
                
            }

        
    }
        
    }

    
    
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register:", error)
    }

    func registerForPushNotifications(application: UIApplication) {
           
           
           let notificationSettings = UIUserNotificationSettings(types: [.badge, .sound, .alert] , categories: nil)
           application.registerUserNotificationSettings(notificationSettings)
           application.registerForRemoteNotifications()
           
       }
    
    func applicationReceivedRemoteMessage(_ remoteMessage: MessagingRemoteMessage) {
       print(remoteMessage.appData)
   }
  
    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    // MARK: - Core Data stack
    lazy var applicationDocumentsDirectory: NSURL = {
           // The directory the application uses to store the Core Data store file. This code uses a directory named "uk.co.plymouthsoftware.core_data" in the application's documents Application Support directory.
           let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
           return urls[urls.count-1] as NSURL
       }()
       
       
       
       
       lazy var managedObjectModel: NSManagedObjectModel = {
           // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
           let modelURL = Bundle.main.url(forResource: "GJEPC", withExtension: "momd")!
           return NSManagedObjectModel(contentsOf: modelURL)!
       }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
           // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
           // Create the coordinator and store
           let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
           let url = self.applicationDocumentsDirectory.appendingPathComponent("GJEPC.sqlite")
           var failureReason = "There was an error creating or loading the application's saved data."
           do {
               try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
           } catch {
               // Report any error we got.
               var dict = [String: AnyObject]()
               dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
               dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
               
               dict[NSUnderlyingErrorKey] = error as NSError
               let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
               // Replace this with code to handle the error appropriately.
               // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
               NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
               
               abort()
           }
           
           return coordinator
       }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "GJEPC")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

