//
//  SubCommitteViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 12/09/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit

class SubCommitteViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
   
    
    @IBOutlet weak var subCommitteScrollView: UIScrollView!
    @IBOutlet weak var sezCompanyHeight: NSLayoutConstraint!
    @IBOutlet weak var promotionCompanyheight: NSLayoutConstraint!
    @IBOutlet weak var nationalTechnicalCompanyHeight: NSLayoutConstraint!
    @IBOutlet weak var microCompanyHeight: NSLayoutConstraint!
    @IBOutlet weak var eventsCompanyHeight: NSLayoutConstraint!
    @IBOutlet weak var internationalCompanyHeight: NSLayoutConstraint!
    @IBOutlet weak var nationalCompanyHeight: NSLayoutConstraint!
    @IBOutlet weak var csrCompanyHeight: NSLayoutConstraint!
    
    @IBOutlet weak var bankingCompanyHeight: NSLayoutConstraint!
    @IBOutlet weak var auditCompanyHeight: NSLayoutConstraint!
    @IBOutlet weak var sezCompanyView: UIView!
    @IBOutlet weak var promotionCompanyView: UIView!
    @IBOutlet weak var nationalTechnicalCompanyView: UIView!
    @IBOutlet weak var microCompanyView: UIView!
    @IBOutlet weak var eventsCompanyView: UIView!
    @IBOutlet weak var internationalCompanyView: UIView!
    @IBOutlet weak var nationalCompanyView: UIView!
    @IBOutlet weak var csrCompanyView: UIView!
    @IBOutlet weak var bankingCompanyView: UIView!
    @IBOutlet weak var auditComapnyView: UIView!
    
    @IBOutlet weak var subCommiteImageView: UIView!
    
    
    @IBOutlet weak var nationalTechnicalImage: UIImageView!
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    @IBOutlet weak var promotingImage: UIImageView!
    @IBOutlet weak var sezImage: UIImageView!
    @IBOutlet weak var sezHeight: NSLayoutConstraint!
    @IBOutlet weak var sezView: UIView!
    @IBOutlet weak var promotionHeight: NSLayoutConstraint!
    @IBOutlet weak var promotionView: UIView!
    @IBOutlet weak var nationalTechnicalHeight: NSLayoutConstraint!
    @IBOutlet weak var nationalTechnicalView: UIView!
    @IBOutlet weak var microImage: UIImageView!
    @IBOutlet weak var microHeight: NSLayoutConstraint!
    @IBOutlet weak var microView: UIView!
    @IBOutlet weak var eventsImage: UIImageView!
    @IBOutlet weak var eventsHeight: NSLayoutConstraint!
    @IBOutlet weak var eventsView: UIView!
    @IBOutlet weak var internationalImage: UIImageView!
    @IBOutlet weak var internationalHeight: NSLayoutConstraint!
    @IBOutlet weak var internationalView: UIView!
    @IBOutlet weak var nationalImage: UIImageView!
    @IBOutlet weak var nationalHeight: NSLayoutConstraint!
    @IBOutlet weak var nationalView: UIView!
    @IBOutlet weak var csrImage: UIImageView!
    @IBOutlet weak var csrHeight: NSLayoutConstraint!
    @IBOutlet weak var csrView: UIView!
    @IBOutlet weak var bankingImage: UIImageView!
    @IBOutlet weak var bankingHeight: NSLayoutConstraint!
    @IBOutlet weak var bankingView: UIView!
    @IBOutlet weak var auditImage: UIImageView!
    @IBOutlet weak var auditHeight: NSLayoutConstraint!
    @IBOutlet weak var auditView: UIView!
    @IBOutlet weak var sezTableView: UITableView!
    @IBOutlet weak var promotionTableView: UITableView!
    @IBOutlet weak var nationalTechniqueTableView: UITableView!
    @IBOutlet weak var microTableView: UITableView!
    @IBOutlet weak var eventsTableView: UITableView!
    @IBOutlet weak var internationalTableView: UITableView!
    @IBOutlet weak var nationalTableView: UITableView!
    @IBOutlet weak var csrTableView: UITableView!
    @IBOutlet weak var bankingTableView: UITableView!
    @IBOutlet weak var auditTableView: UITableView!
    
var CustomArray : NSMutableArray! = [["company_name": "Sanju Kothari","designation": "Co-Convener"],["company_name": "Shailesh Sangani","designation": "Member"],["company_name": "Mansukh Kothari","designation": "Member"],["company_name": "Sanjay Shah","designation": "Member"],["company_name": "Milan Chokshi","designation": "Member"],["company_name": "Dilip Shah","designation": "Member"],["company_name": "Manish Jiwani","designation": "Member"],["company_name": "Russell Mehta","designation": "Member"]]
    
    var BankingCustomArray : NSMutableArray! = [["company_name": "Sanju Kothari","designation": "Convener"],["company_name": "Ajesh Mehta","designation": "Co-Convener"],["company_name": "Maujibhai Patel","designation": "Co-Convener"],["company_name": "Dilip Shah","designation": "Member"],["company_name": "Manish Jiwani","designation": "Member"],["company_name": "K Srinivasan","designation": "Member"],["company_name": "Nirav Maldar","designation": "Member"],["company_name": "Bharat Kakadiya","designation": "Member"],["company_name": "Russell Mehta","designation": "Member"],["company_name": "Somsundaram","designation": "Member"]]
    
    var csrCustomArray : NSMutableArray! = [["company_name": "Mahendra Agrawal","designation": "Convener"],["company_name": "Prakash Pincha","designation": "Co-Convener"],["company_name": "Russell Mehta","designation": "Co-Convener"],["company_name": "Ashok Seth","designation": "Member"],["company_name": "Mahender Tayal","designation": "Member"],["company_name": "Ashok Gajera","designation": "Member"],["company_name": "Nirmal Bardiya","designation": "Member"],["company_name": "Dinesh Navadiya","designation": "Member"],["company_name": "Manish Jiwani","designation": "Member"],["company_name": "Sanjay Shah","designation": "Member"],["company_name": "K Srinivasan","designation": "Member"],["company_name": "Executive Director","designation": "Member"],["company_name": "Chief Financial Officer","designation": "Member"]]
    
    var nationalTechnicalCustomArray : NSMutableArray! = [["company_name": "Colin Shah","designation": "Convener"],["company_name": "Vipul Shah","designation": "Co-Convener"],["company_name": "Dinesh Navadiya","designation": "Member"],["company_name": "Manish Jiwani","designation": "Member"],["company_name": "Dr. Rajendra Bhola","designation": "Member"],["company_name": "Shishir Nevatia","designation": "Member"],["company_name": "Sanjay Kothari","designation": "Member"],["company_name": "Keval Virani","designation": "Member"],["company_name": "Bakul Mehta","designation": "Member"],["company_name": "Kirit Bhansali","designation": "Member"],["company_name": "Dr. Nawal Kishore Agrawal","designation": "Member"],["company_name": "Executive-director-gjepc","designation": "Member"],["company_name": "Chief Financial Officer-gjepc","designation": "Member"],["company_name": "Sanjoy Ghosh","designation": "Member"],["company_name": "Alexander Zickendraht","designation": "Member"],["company_name": "Samir Joshi","designation": "Member"],["company_name": "Pankaj Verma","designation": "Member"]]

    var nationalCustomArray : NSMutableArray! = [["company_name": "Shailesh Sangani","designation": "Convener"],["company_name": "Mansukh Kothari","designation": "Co-Convener"],["company_name": "Milan Chokshi","designation": "Member"],["company_name": "Sanjay Kala","designation": "Member"],["company_name": "Anish Birawat","designation": "Member"],["company_name": "Nirav Bhansali","designation": "Permanent Invitee"],["company_name": "Karan Goradia","designation": "Permanent Invitee"],["company_name": "Sanjay Jain","designation": "Permanent Invitee"],["company_name": "Khushboo Ranawat","designation": "Permanent Invitee"],["company_name": "Joseph Prince","designation": "Invitee"]]
    
     var InternationalCustomArray : NSMutableArray! = [["company_name": "Dilip Shah","designation": "Convener"],["company_name": "Sohil Kothari","designation": "Co-Convener"],["company_name": "Mahender Tayal","designation": "Member"],["company_name": "Vijay Kedia","designation": "Member"],["company_name": "Suvankar Sen","designation": "Member"],["company_name": "Russell Mehta","designation": "Permanent Invitee"]]

//    var InternationalCustomArray : NSMutableArray! = [["company_name": "Dilip Shah","designation": "Convener"],["company_name": "Sohil Kothari","designation": "Co-Convener"],["company_name": "Mahender Tayal","designation": "Co-Convener"],["company_name": "Vijay Kedia","designation": "Member"],["company_name": "Suvankar Sen","designation": "Member"],["company_name": "Russell Mehta","designation": "Permanent Invitee"]]

    var eventsCustomArray : NSMutableArray! = [["company_name": "Mansukh Kothari","designation": "Convener"],["company_name": "Shailesh Sangani","designation": "Co-Convener"],["company_name": "Sanjay Shah","designation": "Member"],["company_name": "Sanju Kothari","designation": "Member"],["company_name": "Kirit Bhansali","designation": "Member"],["company_name": "Nirmal Bardiya","designation": "Member"],["company_name": "Mahender Tayal","designation": "Member"],["company_name": "Milan Chedda","designation": "Member"]]
    
var MicroCustomArray : NSMutableArray! = [["company_name": "Manish Jivani","designation": "Convener"],["company_name": "Rambabu Gupta","designation": "Co-Convener"],["company_name": "Mansukh Kothari","designation": "Member"],["company_name": "Vijay Kedia","designation": "Member"],["company_name": "Ashok Seth","designation": "Member"],["company_name": "Ramesh Balani","designation": "Member"],["company_name": "Mahesh Vaghani( metler)","designation": "Member"],["company_name": "Naresh Lathiya","designation": "Invitee"],["company_name": "Shailesh Lukhi","designation": "Invitee"],["company_name": "Mansukh Khunt","designation": "Invitee"]]
    
    var promotionCustomArray : NSMutableArray! = [["company_name": "Milan Chokshi","designation": "Convener"],["company_name": "Suvankar Sen","designation": "Co-Convener"],["company_name": "Sohil Kothari","designation": "Member"],["company_name": "Rajiv Mehta","designation": "Member"],["company_name": "Khushboo Ranawat","designation": "Member"],["company_name": "Russell Mehta","designation": "Member"],["company_name": "Vijay Kedia","designation": "Permanent Invitee"],["company_name": "Shailesh Sangani","designation": "Permanent Invitee"],["company_name": "Sachin Jain","designation": "Invitee for special purpose"],["company_name": "Vaishali Banerjee","designation": "Invitee for special purpose"]]
    
    var sezCustomArray : NSMutableArray! = [["company_name": "Suvankar Sen","designation": "Convener"],["company_name": "Bobby Kothari","designation": "Co-Convener"],["company_name": "Mehul Vaghani","designation": "Member"],["company_name": "Neville Tata","designation": "Member"],["company_name": "Adil Kotwal","designation": "Member"],["company_name": "Ram Babu Gupta","designation": "Member"]]
    
    
    
    

    var type = "Audit"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
                    subCommiteImageView.backgroundColor = UIColor.white
                    subCommiteImageView.layer.cornerRadius = 3.0
                    subCommiteImageView.layer.masksToBounds = false
                    subCommiteImageView.layer.shadowOffset = CGSize(width: 0, height: 0)
                    subCommiteImageView.layer.shadowOpacity = 0.3
        auditView.dropShadow(view: auditView)
        bankingView.dropShadow(view: bankingView)
         csrView.dropShadow(view: csrView)
        nationalView.dropShadow(view: nationalView)
        internationalView.dropShadow(view: internationalView)
        eventsView.dropShadow(view: eventsView)
        microView.dropShadow(view: microView)
        nationalTechnicalView.dropShadow(view: nationalTechnicalView)
        promotionView.dropShadow(view: promotionView)
        sezView.dropShadow(view: sezView)
        
        
        auditComapnyView.isHidden = false
        auditCompanyHeight.constant = 35
        bankingView.isHidden = true
        bankingCompanyHeight.constant = 0
        csrCompanyView.isHidden = true
        csrCompanyHeight.constant = 0
        nationalCompanyView.isHidden = true
        nationalCompanyHeight.constant = 0
        internationalCompanyView.isHidden = true
        internationalCompanyHeight.constant = 0
        eventsCompanyView.isHidden = true
        eventsCompanyHeight.constant = 0
        microCompanyView.isHidden = true
        microCompanyHeight.constant = 0
        nationalTechnicalCompanyView.isHidden = true
        nationalTechnicalCompanyHeight.constant = 0
        promotionCompanyView.isHidden = true
        promotionCompanyheight.constant = 0
        sezCompanyView.isHidden = true
        sezCompanyHeight.constant = 0

        
        
        
        
        
        
        auditTableView.delegate = self
        auditTableView.dataSource = self
        auditTableView.isScrollEnabled = false
        self.bankingTableView.delegate = self
        self.bankingTableView.dataSource = self
        bankingTableView.isScrollEnabled = false
        csrTableView.delegate = self
        csrTableView.dataSource = self
        csrTableView.isScrollEnabled = false
        nationalTableView.delegate = self
        nationalTableView.dataSource = self
        nationalTableView.isScrollEnabled = false
        internationalTableView.delegate = self
        internationalTableView.dataSource = self
        internationalTableView.isScrollEnabled = false
        eventsTableView.delegate = self
        eventsTableView.dataSource = self
        eventsTableView.isScrollEnabled = false
        microTableView.delegate = self
        microTableView.dataSource = self
        microTableView.isScrollEnabled = false
        nationalTechniqueTableView.delegate = self
        nationalTechniqueTableView.dataSource = self
        nationalTechniqueTableView.isScrollEnabled = false
        promotionTableView.delegate = self
        promotionTableView.dataSource = self
        promotionTableView.isScrollEnabled = false
        sezTableView.delegate = self
        sezTableView.dataSource = self
        sezTableView.isScrollEnabled = false
        
        
        auditView.isHidden = false
        auditHeight.constant = 400
        bankingView.isHidden = true
        bankingHeight.constant = 0
        csrView.isHidden = true
        csrHeight.constant = 0
        nationalView.isHidden = true
        nationalHeight.constant = 0
        internationalView.isHidden = true
        internationalHeight.constant = 0
        eventsView.isHidden = true
        eventsHeight.constant = 0
        microView.isHidden = true
        microHeight.constant = 0
       nationalTechnicalView.isHidden = true
        nationalTechnicalHeight.constant = 0
        promotionView.isHidden = true
        promotionHeight.constant = 0
        sezView.isHidden = true
        sezHeight.constant = 0
        
        
        viewHeight.constant = 1300
        self.auditImage.image = UIImage(named: "icons8-minus-24")
        self.bankingImage.image = UIImage(named: "icons8-plus-24")
        self.csrImage.image = UIImage(named: "icons8-plus-24")
        self.nationalImage.image = UIImage(named: "icons8-plus-24")
        self.nationalImage.image = UIImage(named: "icons8-plus-24")
        self.internationalImage.image = UIImage(named: "icons8-plus-24")
        self.eventsImage.image = UIImage(named: "icons8-plus-24")
        self.microImage.image = UIImage(named: "icons8-plus-24")
        self.nationalTechnicalImage.image = UIImage(named: "icons8-plus-24")
        self.promotingImage.image = UIImage(named: "icons8-plus-24")
        self.sezImage.image = UIImage(named: "icons8-plus-24")
        
        
//        bankingView.dropShadow(scale: true)
        
        self.auditTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if type == "Banking"{
            print(BankingCustomArray.count)
            return BankingCustomArray.count
//            return 10
        }
        if type == "Csr"{
            return csrCustomArray.count
        }
        if type == "National"{
                   return nationalCustomArray.count
               }
        if type == "InterNational"{
//return nationalCustomArray.count
            return InternationalCustomArray.count
            print(InternationalCustomArray.count)
        }
        if type == "Events"{
            return eventsCustomArray.count
        }
        if type == "Micro"{
            return MicroCustomArray.count
        }
        if type == "NationalTechnique"{
            return nationalTechnicalCustomArray.count
               }
        if type == "Promotion"{
                   return promotionCustomArray.count
               }
        if type == "sez"{
            return sezCustomArray.count
        }
        if type == "Audit" {
                    return CustomArray.count
        }
        return 0
       }

       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           let cell = auditTableView.dequeueReusableCell(withIdentifier: "AuditTableViewCell", for: indexPath) as! AuditTableViewCell
        if type == "Audit"{
         cell.companyNameLbl.text = ((self.CustomArray.object(at: indexPath.row) as AnyObject).object(forKey: "company_name")as! String)
         cell.designationLabel.text = ((self.CustomArray.object(at: indexPath.row) as AnyObject).object(forKey: "designation")as! String)
    }
        
        if type == "Banking"{
            print(indexPath.row)
            cell.companyNameLbl.text = ((self.BankingCustomArray.object(at: indexPath.row) as AnyObject).object(forKey: "company_name")as! String)
            cell.designationLabel.text = ((self.BankingCustomArray.object(at: indexPath.row) as AnyObject).object(forKey: "designation")as! String)
        }
        if type == "Csr"{
            cell.companyNameLbl.text = ((self.csrCustomArray.object(at: indexPath.row) as AnyObject).object(forKey: "company_name")as! String)
            cell.designationLabel.text = ((self.csrCustomArray.object(at: indexPath.row) as AnyObject).object(forKey: "designation")as! String)
        }
        if type == "National"{
                   cell.companyNameLbl.text = ((self.nationalCustomArray.object(at: indexPath.row) as AnyObject).object(forKey: "company_name")as! String)
                   cell.designationLabel.text = ((self.nationalCustomArray.object(at: indexPath.row) as AnyObject).object(forKey: "designation")as! String)
               }
        if type == "InterNational"{
                          cell.companyNameLbl.text = ((self.InternationalCustomArray.object(at: indexPath.row) as AnyObject).object(forKey: "company_name")as! String)
                          cell.designationLabel.text = ((self.InternationalCustomArray.object(at: indexPath.row) as AnyObject).object(forKey: "designation")as! String)
//            cell.companyNameLbl.text = ((self.nationalCustomArray.object(at: indexPath.row) as AnyObject).object(forKey: "company_name")as! String)
//                              cell.designationLabel.text = ((self.nationalCustomArray.object(at: indexPath.row) as AnyObject).object(forKey: "designation")as! String)
            
                      }
        if type == "Events"{
                                 cell.companyNameLbl.text = ((self.eventsCustomArray.object(at: indexPath.row) as AnyObject).object(forKey: "company_name")as! String)
                                 cell.designationLabel.text = ((self.eventsCustomArray.object(at: indexPath.row) as AnyObject).object(forKey: "designation")as! String)
                             }
        if type == "Micro"{
            cell.companyNameLbl.text = ((self.MicroCustomArray.object(at: indexPath.row) as AnyObject).object(forKey: "company_name")as! String)
            cell.designationLabel.text = ((self.MicroCustomArray.object(at: indexPath.row) as AnyObject).object(forKey: "designation")as! String)
        }
        if type == "NationalTechnique"{
                   cell.companyNameLbl.text = ((self.nationalTechnicalCustomArray.object(at: indexPath.row) as AnyObject).object(forKey: "company_name")as! String)
                   cell.designationLabel.text = ((self.nationalTechnicalCustomArray.object(at: indexPath.row) as AnyObject).object(forKey: "designation")as! String)
               }
        if type == "Promotion"{
            cell.companyNameLbl.text = ((self.promotionCustomArray.object(at: indexPath.row) as AnyObject).object(forKey: "company_name")as! String)
            cell.designationLabel.text = ((self.promotionCustomArray.object(at: indexPath.row) as AnyObject).object(forKey: "designation")as! String)
        }
        if type == "sez"{
                   cell.companyNameLbl.text = ((self.sezCustomArray.object(at: indexPath.row) as AnyObject).object(forKey: "company_name")as! String)
                   cell.designationLabel.text = ((self.sezCustomArray.object(at: indexPath.row) as AnyObject).object(forKey: "designation")as! String)
               }
        return cell
       }
   
    @IBAction func audit(_ sender: Any) {
        type = "Audit"
        
        if auditHeight.constant == 0{
            subCommitteScrollView.setContentOffset(CGPoint(x: 0, y: 180), animated: true)
            
            auditComapnyView.isHidden = false
            auditCompanyHeight.constant = 35
            bankingView.isHidden = true
            bankingCompanyHeight.constant = 0
            csrCompanyView.isHidden = true
            csrCompanyHeight.constant = 0
            nationalCompanyView.isHidden = true
            nationalCompanyHeight.constant = 0
            internationalCompanyView.isHidden = true
            internationalCompanyHeight.constant = 0
            eventsCompanyView.isHidden = true
            eventsCompanyHeight.constant = 0
            microCompanyView.isHidden = true
            microCompanyHeight.constant = 0
            nationalTechnicalCompanyView.isHidden = true
            nationalTechnicalCompanyHeight.constant = 0
            promotionCompanyView.isHidden = true
            promotionCompanyheight.constant = 0
            sezCompanyView.isHidden = true
            sezCompanyHeight.constant = 0
            
            
            
            DispatchQueue.main.async {
                self.auditHeight.constant = 400
                self.viewHeight.constant = 1400
                self.auditTableView.reloadData()
                         }
                   
                     self.auditImage.image = UIImage(named: "icons8-minus-24")
                    
                    
                    self.auditView.isHidden = false
        //            self.internationalTradeshowView.isHidden = false
                    
                    bankingHeight.constant = 0
                    csrHeight.constant = 0
                    nationalHeight.constant = 0
                    internationalHeight.constant = 0
            eventsHeight.constant = 0
            microHeight.constant = 0
            nationalTechnicalHeight.constant = 0
            promotionHeight.constant = 0
            sezHeight.constant = 0
            

                    bankingView.isHidden = true
                    csrView.isHidden = true
                    nationalView.isHidden = true
                    internationalView.isHidden = true
            eventsView.isHidden = true
            microView.isHidden = true
            nationalTechnicalView.isHidden = true
            promotionView.isHidden = true
            sezView.isHidden = true
            auditCompanyHeight.constant = 35
            auditComapnyView.isHidden = false
            
                    
                    self.bankingImage.image = UIImage(named: "icons8-plus-24")
                    self.csrImage.image = UIImage(named: "icons8-plus-24")
                    self.nationalImage.image = UIImage(named: "icons8-plus-24")
                    self.internationalImage.image = UIImage(named: "icons8-plus-24")
            self.eventsImage.image = UIImage(named: "icons8-plus-24")
            self.microImage.image = UIImage(named: "icons8-plus-24")
            self.nationalTechnicalImage.image = UIImage(named: "icons8-plus-24")
            self.promotingImage.image = UIImage(named: "icons8-plus-24")
            self.sezImage.image = UIImage(named: "icons8-plus-24")
                   
                }
                else{
                                        
                                        DispatchQueue.main.async {
                                            self.viewHeight.constant = 950
                            self.auditHeight.constant = 0
                            self.auditImage.image = UIImage(named: "icons8-plus-24")
                            self.auditView.isHidden = true
                            self.auditCompanyHeight.constant = 0
                            self.auditView.isHidden = true
                
                                        }
                                        
                                    }
                        DispatchQueue.main.async {
                                       UIView.animate(withDuration: 0.4, animations: {
                                           self.view.layoutIfNeeded()
                                       })
                                   }
        
        
    }
    @IBAction func banking(_ sender: Any) {
        type = "Banking"
        print(BankingCustomArray.count)
        
        
        
        if bankingHeight.constant == 0{
            subCommitteScrollView.setContentOffset(CGPoint(x: 0, y: 240), animated: true)
            DispatchQueue.main.async {
                self.bankingHeight.constant = 450
                self.viewHeight.constant = 1400
                self.bankingTableView.reloadData()
                         }
                   
                     self.bankingImage.image = UIImage(named: "icons8-minus-24")
                    
                    
                    self.bankingView.isHidden = false
        //            self.internationalTradeshowView.isHidden = false
                    
                    auditHeight.constant = 0
                    csrHeight.constant = 0
                    nationalHeight.constant = 0
                    internationalHeight.constant = 0
            eventsHeight.constant = 0
            microHeight.constant = 0
            nationalTechnicalHeight.constant = 0
            promotionHeight.constant = 0
            sezHeight.constant = 0
            

                    auditView.isHidden = true
                    csrView.isHidden = true
                    nationalView.isHidden = true
                    internationalView.isHidden = true
            eventsView.isHidden = true
            microView.isHidden = true
            nationalTechnicalView.isHidden = true
            promotionView.isHidden = true
            sezView.isHidden = true
            
            
            auditComapnyView.isHidden = true
            auditCompanyHeight.constant = 0
            bankingCompanyView.isHidden = false
            bankingCompanyHeight.constant = 35
            csrCompanyView.isHidden = true
            csrCompanyHeight.constant = 0
            nationalCompanyView.isHidden = true
            nationalCompanyHeight.constant = 0
            internationalCompanyView.isHidden = true
            internationalCompanyHeight.constant = 0
            eventsCompanyView.isHidden = true
            eventsCompanyHeight.constant = 0
            microCompanyView.isHidden = true
            microCompanyHeight.constant = 0
            nationalTechnicalCompanyView.isHidden = true
            nationalTechnicalCompanyHeight.constant = 0
            promotionCompanyView.isHidden = true
            promotionCompanyheight.constant = 0
            sezCompanyView.isHidden = true
            sezCompanyHeight.constant = 0
            
                    
                    self.auditImage.image = UIImage(named: "icons8-plus-24")
                    self.csrImage.image = UIImage(named: "icons8-plus-24")
                    self.nationalImage.image = UIImage(named: "icons8-plus-24")
                    self.internationalImage.image = UIImage(named: "icons8-plus-24")
            self.eventsImage.image = UIImage(named: "icons8-plus-24")
            self.microImage.image = UIImage(named: "icons8-plus-24")
            self.nationalTechnicalImage.image = UIImage(named: "icons8-plus-24")
            self.promotingImage.image = UIImage(named: "icons8-plus-24")
            self.sezImage.image = UIImage(named: "icons8-plus-24")
                   
                }
                else{
                                        
                                        DispatchQueue.main.async {
                                            self.viewHeight.constant = 950
                            self.bankingHeight.constant = 0
                            self.bankingImage.image = UIImage(named: "icons8-plus-24")
                            self.bankingView.isHidden = true
                                            self.bankingCompanyHeight.constant = 0
                                            self.bankingCompanyView.isHidden = true
                
                                        }
                                        
                                    }
                        DispatchQueue.main.async {
                                       UIView.animate(withDuration: 0.4, animations: {
                                           self.view.layoutIfNeeded()
                                       })
                                   }
        
        
        
        
      
             
        
        
        
        
        
    }
    @IBAction func csr(_ sender: Any) {
        type = "Csr"
        
        
        if csrHeight.constant == 0{
                   subCommitteScrollView.setContentOffset(CGPoint(x: 0, y: 300), animated: true)
                  DispatchQueue.main.async {
                      self.csrHeight.constant = 610
                      self.viewHeight.constant = 1550
                      self.csrTableView.reloadData()
                               }
                         
                           self.csrImage.image = UIImage(named: "icons8-minus-24")
                          
                          
                          self.csrView.isHidden = false
              //            self.internationalTradeshowView.isHidden = false
                          
                          auditHeight.constant = 0
                          bankingHeight.constant = 0
                          nationalHeight.constant = 0
                          internationalHeight.constant = 0
                  eventsHeight.constant = 0
                  microHeight.constant = 0
                  nationalTechnicalHeight.constant = 0
                  promotionHeight.constant = 0
                  sezHeight.constant = 0
                  

                          auditView.isHidden = true
                          bankingView.isHidden = true
                          nationalView.isHidden = true
                          internationalView.isHidden = true
                  eventsView.isHidden = true
                  microView.isHidden = true
                  nationalTechnicalView.isHidden = true
                  promotionView.isHidden = true
                  sezView.isHidden = true
                  
                          
                 self.auditImage.image = UIImage(named: "icons8-plus-24")
                 self.bankingImage.image = UIImage(named: "icons8-plus-24")
                 self.nationalImage.image = UIImage(named: "icons8-plus-24")
                 self.internationalImage.image = UIImage(named: "icons8-plus-24")
                  self.eventsImage.image = UIImage(named: "icons8-plus-24")
                  self.microImage.image = UIImage(named: "icons8-plus-24")
                  self.nationalTechnicalImage.image = UIImage(named: "icons8-plus-24")
                  self.promotingImage.image = UIImage(named: "icons8-plus-24")
                  self.sezImage.image = UIImage(named: "icons8-plus-24")
                         
            
            auditComapnyView.isHidden = true
            auditCompanyHeight.constant = 0
            bankingView.isHidden = true
            bankingCompanyHeight.constant = 0
            csrCompanyView.isHidden = false
            csrCompanyHeight.constant = 35
            nationalCompanyView.isHidden = true
            nationalCompanyHeight.constant = 0
            internationalCompanyView.isHidden = true
            internationalCompanyHeight.constant = 0
            eventsCompanyView.isHidden = true
            eventsCompanyHeight.constant = 0
            microCompanyView.isHidden = true
            microCompanyHeight.constant = 0
            nationalTechnicalCompanyView.isHidden = true
            nationalTechnicalCompanyHeight.constant = 0
            promotionCompanyView.isHidden = true
            promotionCompanyheight.constant = 0
            sezCompanyView.isHidden = true
            sezCompanyHeight.constant = 0
            
            
                      }
                      else{
                                              
                            DispatchQueue.main.async {
                                self.viewHeight.constant = 950
                                  self.csrHeight.constant = 0
                                  self.csrImage.image = UIImage(named: "icons8-plus-24")
                                  self.csrView.isHidden = true
                                self.csrCompanyHeight.constant = 0
                                self.csrCompanyView.isHidden = true
                                              }
                                              
                                          }
                              DispatchQueue.main.async {
                                             UIView.animate(withDuration: 0.4, animations: {
                                                 self.view.layoutIfNeeded()
                                             })
                                         }
              
              
              
        
        
//               DispatchQueue.main.async {
//                   self.csrTableView.reloadData()
//               }
    }
    
    @IBAction func national(_ sender: Any) {
        type = "National"
        if nationalHeight.constant == 0{
             subCommitteScrollView.setContentOffset(CGPoint(x: 0, y: 360), animated: true)
            DispatchQueue.main.async {
                self.nationalHeight.constant = 480
                self.viewHeight.constant = 1400
                self.nationalTableView.reloadData()
                         }
                   
                     self.nationalImage.image = UIImage(named: "icons8-minus-24")
                    
                    
                    self.nationalView.isHidden = false
        //            self.internationalTradeshowView.isHidden = false
                    
                    auditHeight.constant = 0
                    bankingHeight.constant = 0
                    csrHeight.constant = 0
                    internationalHeight.constant = 0
            eventsHeight.constant = 0
            microHeight.constant = 0
            nationalTechnicalHeight.constant = 0
            promotionHeight.constant = 0
            sezHeight.constant = 0
            

                    auditView.isHidden = true
                    bankingView.isHidden = true
                    csrView.isHidden = true
                    internationalView.isHidden = true
            eventsView.isHidden = true
            microView.isHidden = true
            nationalTechnicalView.isHidden = true
            promotionView.isHidden = true
            sezView.isHidden = true
            
                    
           self.auditImage.image = UIImage(named: "icons8-plus-24")
           self.bankingImage.image = UIImage(named: "icons8-plus-24")
           self.csrImage.image = UIImage(named: "icons8-plus-24")
           self.internationalImage.image = UIImage(named: "icons8-plus-24")
            self.eventsImage.image = UIImage(named: "icons8-plus-24")
            self.microImage.image = UIImage(named: "icons8-plus-24")
            self.nationalTechnicalImage.image = UIImage(named: "icons8-plus-24")
            self.promotingImage.image = UIImage(named: "icons8-plus-24")
            self.sezImage.image = UIImage(named: "icons8-plus-24")
                 
            
            auditComapnyView.isHidden = true
            auditCompanyHeight.constant = 0
            bankingView.isHidden = true
            bankingCompanyHeight.constant = 0
            csrCompanyView.isHidden = true
            csrCompanyHeight.constant = 0
            nationalCompanyView.isHidden = false
            nationalCompanyHeight.constant = 35
            internationalCompanyView.isHidden = true
            internationalCompanyHeight.constant = 0
            eventsCompanyView.isHidden = true
            eventsCompanyHeight.constant = 0
            microCompanyView.isHidden = true
            microCompanyHeight.constant = 0
            nationalTechnicalCompanyView.isHidden = true
            nationalTechnicalCompanyHeight.constant = 0
            promotionCompanyView.isHidden = true
            promotionCompanyheight.constant = 0
            sezCompanyView.isHidden = true
            sezCompanyHeight.constant = 0
            
            
                }
                else{
                                        
                      DispatchQueue.main.async {
                          self.viewHeight.constant = 950
                            self.nationalHeight.constant = 0
                            self.nationalImage.image = UIImage(named: "icons8-plus-24")
                            self.nationalView.isHidden = true
                        self.nationalCompanyView.isHidden = true
                        self.nationalCompanyHeight.constant = 0
                                        }
                                        
                                    }
                        DispatchQueue.main.async {
                                       UIView.animate(withDuration: 0.4, animations: {
                                           self.view.layoutIfNeeded()
                                       })
                                   }
    }
    
    @IBAction func international(_ sender: Any) {
        type = "InterNational"
               if internationalHeight.constant == 0{
                    subCommitteScrollView.setContentOffset(CGPoint(x: 0, y: 420), animated: true)
                   DispatchQueue.main.async {
                       self.internationalHeight.constant = 300
                       self.viewHeight.constant = 1200
                       self.internationalTableView.reloadData()
                                }
                          
                            self.internationalImage.image = UIImage(named: "icons8-minus-24")
                           
                           
                           self.internationalView.isHidden = false
               //            self.internationalTradeshowView.isHidden = false
                           
                           auditHeight.constant = 0
                           bankingHeight.constant = 0
                           csrHeight.constant = 0
                           nationalHeight.constant = 0
                   eventsHeight.constant = 0
                   microHeight.constant = 0
                   nationalTechnicalHeight.constant = 0
                   promotionHeight.constant = 0
                   sezHeight.constant = 0
                   

                           auditView.isHidden = true
                           bankingView.isHidden = true
                           csrView.isHidden = true
                           nationalView.isHidden = true
                   eventsView.isHidden = true
                   microView.isHidden = true
                   nationalTechnicalView.isHidden = true
                   promotionView.isHidden = true
                   sezView.isHidden = true
                   
                           
                  self.auditImage.image = UIImage(named: "icons8-plus-24")
                  self.bankingImage.image = UIImage(named: "icons8-plus-24")
                  self.csrImage.image = UIImage(named: "icons8-plus-24")
                  self.nationalImage.image = UIImage(named: "icons8-plus-24")
                   self.eventsImage.image = UIImage(named: "icons8-plus-24")
                   self.microImage.image = UIImage(named: "icons8-plus-24")
                   self.nationalTechnicalImage.image = UIImage(named: "icons8-plus-24")
                   self.promotingImage.image = UIImage(named: "icons8-plus-24")
                   self.sezImage.image = UIImage(named: "icons8-plus-24")
                     
                auditComapnyView.isHidden = true
                auditCompanyHeight.constant = 0
                bankingView.isHidden = true
                bankingCompanyHeight.constant = 0
                csrCompanyView.isHidden = true
                csrCompanyHeight.constant = 0
                nationalCompanyView.isHidden = true
                nationalCompanyHeight.constant = 0
                internationalCompanyView.isHidden = false
                internationalCompanyHeight.constant = 35
                eventsCompanyView.isHidden = true
                eventsCompanyHeight.constant = 0
                microCompanyView.isHidden = true
                microCompanyHeight.constant = 0
                nationalTechnicalCompanyView.isHidden = true
                nationalTechnicalCompanyHeight.constant = 0
                promotionCompanyView.isHidden = true
                promotionCompanyheight.constant = 0
                sezCompanyView.isHidden = true
                sezCompanyHeight.constant = 0
                       }
                       else{
                                               
                             DispatchQueue.main.async {
                                 self.viewHeight.constant = 950
                                   self.internationalHeight.constant = 0
                                   self.internationalImage.image = UIImage(named: "icons8-plus-24")
                                   self.internationalView.isHidden = true
                                self.internationalCompanyView.isHidden = true
                                 self.internationalCompanyHeight.constant = 0
                                               }
                                               
                                           }
                               DispatchQueue.main.async {
                                              UIView.animate(withDuration: 0.4, animations: {
                                                  self.view.layoutIfNeeded()
                                              })
                                          }
    }
    @IBAction func events(_ sender: Any) {
        type = "Events"
                      if eventsHeight.constant == 0{
                subCommitteScrollView.setContentOffset(CGPoint(x: 0, y: 480), animated: true)
                                        DispatchQueue.main.async {
                                            self.eventsHeight.constant = 380
                                            self.viewHeight.constant = 1300
                                            self.eventsTableView.reloadData()
                                                     }
                                               
                                                 self.eventsImage.image = UIImage(named: "icons8-minus-24")
                                                
                                                
                                                self.eventsView.isHidden = false
                                    //            self.internationalTradeshowView.isHidden = false
                                                
                                                auditHeight.constant = 0
                                                bankingHeight.constant = 0
                                                csrHeight.constant = 0
                                                nationalHeight.constant = 0
                                        internationalHeight.constant = 0
                                        microHeight.constant = 0
                                        nationalTechnicalHeight.constant = 0
                                        promotionHeight.constant = 0
                                        sezHeight.constant = 0
                                        

                                                auditView.isHidden = true
                                                bankingView.isHidden = true
                                                csrView.isHidden = true
                                                nationalView.isHidden = true
                                        internationalView.isHidden = true
                                        microView.isHidden = true
                                        nationalTechnicalView.isHidden = true
                                        promotionView.isHidden = true
                                        sezView.isHidden = true
                                        
                                                
                                       self.auditImage.image = UIImage(named: "icons8-plus-24")
                                       self.bankingImage.image = UIImage(named: "icons8-plus-24")
                                       self.csrImage.image = UIImage(named: "icons8-plus-24")
                                       self.nationalImage.image = UIImage(named: "icons8-plus-24")
                                        self.internationalImage.image = UIImage(named: "icons8-plus-24")
                                        self.microImage.image = UIImage(named: "icons8-plus-24")
                                        self.nationalTechnicalImage.image = UIImage(named: "icons8-plus-24")
                                        self.promotingImage.image = UIImage(named: "icons8-plus-24")
                                        self.sezImage.image = UIImage(named: "icons8-plus-24")
                                            
                        auditComapnyView.isHidden = true
                        auditCompanyHeight.constant = 0
                        bankingView.isHidden = true
                        bankingCompanyHeight.constant = 0
                        csrCompanyView.isHidden = true
                        csrCompanyHeight.constant = 0
                        nationalCompanyView.isHidden = true
                        nationalCompanyHeight.constant = 0
                        internationalCompanyView.isHidden = true
                        internationalCompanyHeight.constant = 0
                        eventsCompanyView.isHidden = false
                        eventsCompanyHeight.constant = 35
                        microCompanyView.isHidden = true
                        microCompanyHeight.constant = 0
                        nationalTechnicalCompanyView.isHidden = true
                        nationalTechnicalCompanyHeight.constant = 0
                        promotionCompanyView.isHidden = true
                        promotionCompanyheight.constant = 0
                        sezCompanyView.isHidden = true
                        sezCompanyHeight.constant = 0
                                            }
                                            else{
                                                                    
                                                  DispatchQueue.main.async {
                                                      self.viewHeight.constant = 950
                                                        self.eventsHeight.constant = 0
                                                        self.eventsImage.image = UIImage(named: "icons8-plus-24")
                                                        self.eventsView.isHidden = true
                                            self.eventsCompanyHeight.constant = 0
                                            self.eventsCompanyView.isHidden = true
                                                                    }
                                                                    
                                                                }
                                                    DispatchQueue.main.async {
                                                                   UIView.animate(withDuration: 0.4, animations: {
                                                                       self.view.layoutIfNeeded()
                                                                   })
                                                               }
    }
    @IBAction func micro(_ sender: Any) {
        type = "Micro"
                     if microHeight.constant == 0{
              subCommitteScrollView.setContentOffset(CGPoint(x: 0, y: 540), animated: true)
                          DispatchQueue.main.async {
                              self.microHeight.constant = 470
                              self.viewHeight.constant = 1400
                              self.microTableView.reloadData()
                                       }
                                 
                                   self.microImage.image = UIImage(named: "icons8-minus-24")
                                  
                                  
                                  self.microView.isHidden = false
                      //            self.internationalTradeshowView.isHidden = false
                                  
                                  auditHeight.constant = 0
                                  bankingHeight.constant = 0
                                  csrHeight.constant = 0
                                  nationalHeight.constant = 0
                          internationalHeight.constant = 0
                          eventsHeight.constant = 0
                          nationalTechnicalHeight.constant = 0
                          promotionHeight.constant = 0
                          sezHeight.constant = 0
                          

                                  auditView.isHidden = true
                                  bankingView.isHidden = true
                                  csrView.isHidden = true
                                  nationalView.isHidden = true
                          internationalView.isHidden = true
                          eventsView.isHidden = true
                          nationalTechnicalView.isHidden = true
                          promotionView.isHidden = true
                          sezView.isHidden = true
                          
                                  
                         self.auditImage.image = UIImage(named: "icons8-plus-24")
                         self.bankingImage.image = UIImage(named: "icons8-plus-24")
                         self.csrImage.image = UIImage(named: "icons8-plus-24")
                         self.nationalImage.image = UIImage(named: "icons8-plus-24")
                          self.internationalImage.image = UIImage(named: "icons8-plus-24")
                          self.eventsImage.image = UIImage(named: "icons8-plus-24")
                          self.nationalTechnicalImage.image = UIImage(named: "icons8-plus-24")
                          self.promotingImage.image = UIImage(named: "icons8-plus-24")
                          self.sezImage.image = UIImage(named: "icons8-plus-24")
                        
                        
                        auditComapnyView.isHidden = true
                        auditCompanyHeight.constant = 0
                        bankingView.isHidden = true
                        bankingCompanyHeight.constant = 0
                        csrCompanyView.isHidden = true
                        csrCompanyHeight.constant = 0
                        nationalCompanyView.isHidden = true
                        nationalCompanyHeight.constant = 0
                        internationalCompanyView.isHidden = true
                        internationalCompanyHeight.constant = 0
                        eventsCompanyView.isHidden = true
                        eventsCompanyHeight.constant = 0
                        microCompanyView.isHidden = false
                        microCompanyHeight.constant = 35
                        nationalTechnicalCompanyView.isHidden = true
                        nationalTechnicalCompanyHeight.constant = 0
                        promotionCompanyView.isHidden = true
                        promotionCompanyheight.constant = 0
                        sezCompanyView.isHidden = true
                        sezCompanyHeight.constant = 0
                                 
                              }
                              else{
                                                      
                                    DispatchQueue.main.async {
                                          self.viewHeight.constant = 950
                                          self.microHeight.constant = 0
                                          self.microImage.image = UIImage(named: "icons8-plus-24")
                                          self.microView.isHidden = true
                              self.microCompanyHeight.constant = 0
                              self.microCompanyView.isHidden = true
                                                      }
                                                      
                                                  }
                                      DispatchQueue.main.async {
                                                     UIView.animate(withDuration: 0.4, animations: {
                                                         self.view.layoutIfNeeded()
                                                     })
                                                 }
    }
    
    @IBAction func nationalTechnique(_ sender: Any) {
        type = "NationalTechnique"
                       if nationalTechnicalHeight.constant == 0{
                  subCommitteScrollView.setContentOffset(CGPoint(x: 0, y: 600), animated: true)
                                               DispatchQueue.main.async {
                                                   self.nationalTechnicalHeight.constant = 770
                                                   self.viewHeight.constant = 1700
                                                   self.nationalTechniqueTableView.reloadData()
                                                            }
                                                      
                                                        self.nationalTechnicalImage.image = UIImage(named: "icons8-minus-24")
                                                       
                                                       
                                                       self.nationalTechnicalView.isHidden = false
                                           //            self.internationalTradeshowView.isHidden = false
                                                       
                                                       auditHeight.constant = 0
                                                       bankingHeight.constant = 0
                                                       csrHeight.constant = 0
                                                       nationalHeight.constant = 0
                                               internationalHeight.constant = 0
                                               eventsHeight.constant = 0
                                               microHeight.constant = 0
                                               promotionHeight.constant = 0
                                               sezHeight.constant = 0
                                               

                                                       auditView.isHidden = true
                                                       bankingView.isHidden = true
                                                       csrView.isHidden = true
                                                       nationalView.isHidden = true
                                               internationalView.isHidden = true
                                               eventsView.isHidden = true
                                               microView.isHidden = true
                                               promotionView.isHidden = true
                                               sezView.isHidden = true
                                               
                                                       
                                              self.auditImage.image = UIImage(named: "icons8-plus-24")
                                              self.bankingImage.image = UIImage(named: "icons8-plus-24")
                                              self.csrImage.image = UIImage(named: "icons8-plus-24")
                                              self.nationalImage.image = UIImage(named: "icons8-plus-24")
                                               self.internationalImage.image = UIImage(named: "icons8-plus-24")
                                               self.eventsImage.image = UIImage(named: "icons8-plus-24")
                                               self.microImage.image = UIImage(named: "icons8-plus-24")
                                               self.promotingImage.image = UIImage(named: "icons8-plus-24")
                                               self.sezImage.image = UIImage(named: "icons8-plus-24")
                                                      
                        
                        auditComapnyView.isHidden = true
                        auditCompanyHeight.constant = 0
                        bankingView.isHidden = true
                        bankingCompanyHeight.constant = 0
                        csrCompanyView.isHidden = true
                        csrCompanyHeight.constant = 0
                        nationalCompanyView.isHidden = true
                        nationalCompanyHeight.constant = 0
                        internationalCompanyView.isHidden = true
                        internationalCompanyHeight.constant = 0
                        eventsCompanyView.isHidden = true
                        eventsCompanyHeight.constant = 0
                        microCompanyView.isHidden = true
                        microCompanyHeight.constant = 0
                        nationalTechnicalCompanyView.isHidden = false
                        nationalTechnicalCompanyHeight.constant = 35
                        promotionCompanyView.isHidden = true
                        promotionCompanyheight.constant = 0
                        sezCompanyView.isHidden = true
                        sezCompanyHeight.constant = 0
                                                   }
                                                   else{
                                                                           
                                                         DispatchQueue.main.async {
                                                               self.viewHeight.constant = 950
                                                               self.nationalTechnicalHeight.constant = 0
                                                               self.nationalTechnicalImage.image = UIImage(named: "icons8-plus-24")
                                                               self.nationalTechnicalView.isHidden = true
                                                            
                                                            self.nationalTechnicalCompanyHeight.constant = 0
                                                                                                   self.nationalTechnicalCompanyView.isHidden = true
                                                            
                                                   
                                                                           }
                                                                           
                                                                       }
                                                           DispatchQueue.main.async {
                                                                          UIView.animate(withDuration: 0.4, animations: {
                                                                              self.view.layoutIfNeeded()
                                                                          })
                                                                      }
    }
    @IBAction func promotion(_ sender: Any) {
        type = "Promotion"
                    if promotionHeight.constant == 0{
           subCommitteScrollView.setContentOffset(CGPoint(x: 0, y: 660), animated: true)
                          DispatchQueue.main.async {
                              self.promotionHeight.constant = 470
                              self.viewHeight.constant = 1400
                              self.promotionTableView.reloadData()
                                       }
                                 
                                   self.promotingImage.image = UIImage(named: "icons8-minus-24")
                                  
                                  
                                  self.promotionView.isHidden = false
                      //            self.internationalTradeshowView.isHidden = false
                                  
                                  auditHeight.constant = 0
                                  bankingHeight.constant = 0
                                  csrHeight.constant = 0
                                  nationalHeight.constant = 0
                          internationalHeight.constant = 0
                          eventsHeight.constant = 0
                          nationalTechnicalHeight.constant = 0
                          microHeight.constant = 0
                          sezHeight.constant = 0
                          

                                  auditView.isHidden = true
                                  bankingView.isHidden = true
                                  csrView.isHidden = true
                                  nationalView.isHidden = true
                          internationalView.isHidden = true
                          eventsView.isHidden = true
                          nationalTechnicalView.isHidden = true
                          microView.isHidden = true
                          sezView.isHidden = true
                          
                                  
                         self.auditImage.image = UIImage(named: "icons8-plus-24")
                         self.bankingImage.image = UIImage(named: "icons8-plus-24")
                         self.csrImage.image = UIImage(named: "icons8-plus-24")
                         self.nationalImage.image = UIImage(named: "icons8-plus-24")
                          self.internationalImage.image = UIImage(named: "icons8-plus-24")
                          self.eventsImage.image = UIImage(named: "icons8-plus-24")
                          self.nationalTechnicalImage.image = UIImage(named: "icons8-plus-24")
                          self.microImage.image = UIImage(named: "icons8-plus-24")
                          self.sezImage.image = UIImage(named: "icons8-plus-24")
                        
                        auditComapnyView.isHidden = true
                        auditCompanyHeight.constant = 0
                        bankingView.isHidden = true
                        bankingCompanyHeight.constant = 0
                        csrCompanyView.isHidden = true
                        csrCompanyHeight.constant = 0
                        nationalCompanyView.isHidden = true
                        nationalCompanyHeight.constant = 0
                        internationalCompanyView.isHidden = true
                        internationalCompanyHeight.constant = 0
                        eventsCompanyView.isHidden = true
                        eventsCompanyHeight.constant = 0
                        microCompanyView.isHidden = true
                        microCompanyHeight.constant = 0
                        nationalTechnicalCompanyView.isHidden = true
                        nationalTechnicalCompanyHeight.constant = 0
                        promotionCompanyView.isHidden = false
                        promotionCompanyheight.constant = 35
                        sezCompanyView.isHidden = true
                        sezCompanyHeight.constant = 0
                                 
                              }
                              else{
                                                      
                                    DispatchQueue.main.async {
                                          self.viewHeight.constant = 950
                                          self.promotionHeight.constant = 0
                                          self.promotingImage.image = UIImage(named: "icons8-plus-24")
                                          self.promotionView.isHidden = true
                                        self.promotionCompanyheight.constant = 0
                                        self.promotionCompanyView.isHidden = true
                              
                                                      }
                                                      
                                                  }
                                      DispatchQueue.main.async {
                                                     UIView.animate(withDuration: 0.4, animations: {
                                                         self.view.layoutIfNeeded()
                                                     })
                                                 }
    }
    
    @IBAction func sez(_ sender: Any) {
        type = "sez"
                     if sezHeight.constant == 0{
                     subCommitteScrollView.setContentOffset(CGPoint(x: 0, y: 720), animated: true)
                          DispatchQueue.main.async {
                              self.sezHeight.constant = 300
                              self.viewHeight.constant = 1200
                              self.sezTableView.reloadData()
                                       }
                                 
                                   self.sezImage.image = UIImage(named: "icons8-minus-24")
                                  
                                  
                                  self.sezView.isHidden = false
                      //            self.internationalTradeshowView.isHidden = false
                                  
                                  auditHeight.constant = 0
                                  bankingHeight.constant = 0
                                  csrHeight.constant = 0
                                  nationalHeight.constant = 0
                          internationalHeight.constant = 0
                          eventsHeight.constant = 0
                          nationalTechnicalHeight.constant = 0
                          microHeight.constant = 0
                          promotionHeight.constant = 0
                          

                                  auditView.isHidden = true
                                  bankingView.isHidden = true
                                  csrView.isHidden = true
                                  nationalView.isHidden = true
                          internationalView.isHidden = true
                          eventsView.isHidden = true
                          nationalTechnicalView.isHidden = true
                          microView.isHidden = true
                          promotionView.isHidden = true
                          
                                  
                         self.auditImage.image = UIImage(named: "icons8-plus-24")
                         self.bankingImage.image = UIImage(named: "icons8-plus-24")
                         self.csrImage.image = UIImage(named: "icons8-plus-24")
                         self.nationalImage.image = UIImage(named: "icons8-plus-24")
                          self.internationalImage.image = UIImage(named: "icons8-plus-24")
                          self.eventsImage.image = UIImage(named: "icons8-plus-24")
                          self.nationalTechnicalImage.image = UIImage(named: "icons8-plus-24")
                          self.microImage.image = UIImage(named: "icons8-plus-24")
                          self.promotingImage.image = UIImage(named: "icons8-plus-24")
                        
                        auditComapnyView.isHidden = true
                        auditCompanyHeight.constant = 0
                        bankingView.isHidden = true
                        bankingCompanyHeight.constant = 0
                        csrCompanyView.isHidden = true
                        csrCompanyHeight.constant = 0
                        nationalCompanyView.isHidden = true
                        nationalCompanyHeight.constant = 0
                        internationalCompanyView.isHidden = true
                        internationalCompanyHeight.constant = 0
                        eventsCompanyView.isHidden = true
                        eventsCompanyHeight.constant = 0
                        microCompanyView.isHidden = true
                        microCompanyHeight.constant = 0
                        nationalTechnicalCompanyView.isHidden = true
                        nationalTechnicalCompanyHeight.constant = 0
                        promotionCompanyView.isHidden = true
                        promotionCompanyheight.constant = 0
                        sezCompanyView.isHidden = false
                        sezCompanyHeight.constant = 35
                                 
                              }
                              else{
                                                      
                                    DispatchQueue.main.async {
                                          self.viewHeight.constant = 950
                                          self.sezHeight.constant = 0
                                          self.sezView.isHidden = true
                                        self.sezCompanyHeight.constant = 0
                                        self.sezCompanyView.isHidden = true
                                          self.sezImage.image = UIImage(named: "icons8-plus-24")
                                         
                              
                                                      }
                                                      
                                                  }
                                      DispatchQueue.main.async {
                                                     UIView.animate(withDuration: 0.4, animations: {
                                                         self.view.layoutIfNeeded()
                                                     })
                                                 }
    }
    
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
extension UIView {

// OUTPUT 1
func dropShadow(view: UIView) {
  view.backgroundColor = UIColor.white
  view.layer.cornerRadius = 3.0
  view.layer.masksToBounds = false
  view.layer.shadowOffset = CGSize(width: 0, height: 0)
  view.layer.shadowOpacity = 0.3

}
    
}
