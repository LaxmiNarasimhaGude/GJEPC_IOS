//
//  CFCViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 17/01/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
class CFCViewController: UIViewController,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource,dataReceivedFromServerDelegate {
   
       var activityLoader : MBProgressHUD? = nil
       var tableViewMenuArray : NSMutableArray = []
       var replacedImgstring  = ""
       var requestToServer = RequestToServer()
       var callUrls : URL!
       var type = "true"
    @IBOutlet weak var cfcCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
            cfcCollectionView.collectionViewLayout = layout
        
            requestToServer.delegate = self
            cfcCollectionView.delegate = self
            cfcCollectionView.dataSource = self
            
            checkForInternet()
        
    }
    func checkForInternet() {
        if CheckInternet.isConnectedToNetwork() == true
        {
            print("internet available")
            self.cfcCollectionView.isHidden = false
            DispatchQueue.main.async {
//                self.loadActivityIndicator()
            }
            onFirstTimeLoad()
        }
        else
        {
            print("no internet")
        cfcCollectionView.isHidden = true
            activityLoader?.hide(animated: true)
        }
    }
//    func loadActivityIndicator()
//    {
//        activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
//        activityLoader?.label.text = "Loading";
//        activityLoader?.detailsLabel.text = "Please Wait";
//        activityLoader?.isUserInteractionEnabled = false;
//    }
    func onFirstTimeLoad() {
          callUrls = cfcURL
           let params = [""]
           requestToServer.connectToServer(myUrl: callUrls as URL, params: params as AnyObject)
       }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout:

        UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let width = UIScreen.main.bounds.size.width
       
        return CGSize(width: ((width / 2) - 15)   , height: 120)



    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier:"LabContentViewController") as! LabContentViewController
        let title = ((self.tableViewMenuArray.object(at: indexPath.row) as AnyObject).object(forKey: "title") as! String)
        vc.Ltitle = title
        let desc = ((self.tableViewMenuArray.object(at: indexPath.row) as AnyObject).object(forKey: "description") as! String)
        vc.Ldesc = desc
        self.navigationController?.pushViewController(vc, animated: true)
    }
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return tableViewMenuArray.count
          }
       
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = cfcCollectionView.dequeueReusableCell(withReuseIdentifier: "CFCCollectionViewCell", for: indexPath) as! CFCCollectionViewCell
        
        cell.cfcImage.startShimmeringEffect()
        cell.cfcLabel.startShimmeringEffect()
        
        
               cell.contentView.layer.cornerRadius = 2.0
               cell.contentView.layer.borderWidth = 1.0
               cell.contentView.layer.borderColor = UIColor.clear.cgColor
               cell.contentView.layer.masksToBounds = true
              
               cell.layer.backgroundColor = UIColor.white.cgColor
               cell.layer.shadowColor = UIColor.lightGray.cgColor
               cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)//CGSizeMake(0, 2.0);
                           cell.layer.shadowRadius = 2.0
                           cell.layer.shadowOpacity = 1.0
                           cell.layer.masksToBounds = false
               cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
        
        cell.cfcImage.layer.masksToBounds = true
               cell.cfcImage.layer.borderWidth = 1
               cell.cfcImage.layer.borderColor = UIColor.darkGray.cgColor
               cell.cfcImage.layer.cornerRadius = 5
        
        cell.cfcLabel.text = ((self.tableViewMenuArray.object(at: indexPath.row) as AnyObject).object(forKey: "title")as! String)
        let imgdata = ((self.tableViewMenuArray.object(at: indexPath.row) as AnyObject).object(forKey: "image")as! String)
        if(imgdata == " "){
            cell.cfcImage.image = UIImage(named: "logo (1).png")
            cell.cfcImage.layer.sublayers = nil
            cell.cfcLabel.layer.sublayers = nil
        }
        else{
            
            if type == "false"{
            
            let imgfilterUrl = labsInstituteImgUrl + imgdata
            replacedImgstring = imgfilterUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!
            URLSession.shared.dataTask(with: NSURL(string: replacedImgstring)! as URL, completionHandler: { (data, response, error) -> Void in
                if error != nil {
                    return
                }
                else {
                    
                    DispatchQueue.main.async {
                        let image = UIImage(data: data!)

                        if (image != nil) {
                                cell.cfcImage.image = image
                           // cell1.menuImg.contentMode = UIViewContentMode.scaleToFill
                        }
                        else{
                             cell.cfcImage.image = UIImage(named: "logo-1.png")
                        }
                    }
                }
            }).resume()
                 cell.cfcImage.layer.sublayers = nil
            }
           
        }
        return cell
       }
       
       func dataReceivedFromServer(data: NSDictionary, url: URL) {
           
           if ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "true" {
               
               let temporaryArray = ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "Result") as! NSArray)
                let sortedArray = (temporaryArray as NSArray).sortedArray(using: [NSSortDescriptor(key: "title", ascending: true)]) as! [[String:AnyObject]] as NSArray
               
               
               tableViewMenuArray = (sortedArray.mutableCopy() as! NSMutableArray)
               print("tableViewMenuArray.....\(tableViewMenuArray)")
               DispatchQueue.main.async {
                   self.activityLoader?.hide(animated: true)
                   self.cfcCollectionView.reloadData()
                DispatchQueue.main.asyncAfter(deadline: .now()+1.0) {
                    self.changeImage()
                }
               }
           }
           
       }
    
    @objc func changeImage()  {
                   self.type = "false"
                   self.cfcCollectionView.reloadData()
               }
}
