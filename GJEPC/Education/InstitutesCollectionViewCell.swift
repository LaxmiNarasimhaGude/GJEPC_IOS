//
//  InstitutesCollectionViewCell.swift
//  GJEPC
//
//  Created by Kwebmaker on 17/01/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit

class InstitutesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var institutesImage: UIImageView!
    
    @IBOutlet weak var instituteLbl: UILabel!
}
