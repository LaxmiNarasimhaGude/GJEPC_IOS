//
//  LabContentViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 11/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
class LabContentViewController: UIViewController,UIWebViewDelegate {

    @IBOutlet weak var labWebview: UIWebView!
    @IBOutlet weak var heading: UILabel!
    
    var Ltitle : String = ""
    var Ldesc : String = ""
     var header:String = ""
     var LImage : String = ""
    var activityLoader : MBProgressHUD? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        labWebview.delegate = self
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0
        putData()
        DispatchQueue.main.async {
            self.loadActivityIndicator()
        }
        loadHtmlInAWebView()
    }
    func loadActivityIndicator()
       {
           activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
           activityLoader?.label.text = "Loading";
           activityLoader?.detailsLabel.text = "Please Wait!!!";
           activityLoader?.isUserInteractionEnabled = false;
       }
    func loadHtmlInAWebView() {
        let htmlString : String! = "<html><head><style type='text/css'>span {    text-align: justify;line-height: auto;float: left;font-size:14px;font-family:futura}</style></head><body><font color ='#000000'><span>\(Ldesc)<br/><br></span></font></body></html>"
        labWebview.backgroundColor = UIColor.white
        labWebview.loadHTMLString(htmlString, baseURL: nil)
        DispatchQueue.main.async {
              self.activityLoader?.hide(animated: true)
        }
    }

   func putData() {
          heading.text = Ltitle
//          event.text  = Ltitle
      }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        activityLoader?.hide(animated: true)
    }
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}
