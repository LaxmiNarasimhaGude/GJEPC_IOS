//
//  CFCCollectionViewCell.swift
//  GJEPC
//
//  Created by Kwebmaker on 17/01/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit

class CFCCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var cfcImage: UIImageView!
    @IBOutlet weak var cfcLabel: UILabel!
    
}
