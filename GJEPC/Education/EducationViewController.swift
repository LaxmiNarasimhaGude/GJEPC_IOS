//
//  EducationViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 17/01/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD

@available(iOS 13.0, *)
class EducationViewController: UIViewController,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource,dataReceivedFromServerDelegate {
   
   
    

    @IBOutlet weak var notificationCount: UILabel!
    @IBOutlet weak var cfcLbl: UIImageView!
    @IBOutlet weak var institutesLbl: UIImageView!
    
    
    @IBOutlet weak var laboratoryLbl: UIImageView!
    
    @IBOutlet weak var laboratoriesCollectionView: UICollectionView!
    
     var responseNotificationArray:NSMutableArray = []
    var isChange:Bool = false
    var activityLoader : MBProgressHUD? = nil
    var tableViewMenuArray : NSMutableArray = []
    var replacedImgstring  = ""
    var requestToServer = RequestToServer()
    var callUrls : URL!
     var isOpen = false
    var type = "true"
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.view.showAnimatedGradientSkeleton()
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        laboratoriesCollectionView.collectionViewLayout = layout
    
        requestToServer.delegate = self
        
        
        checkForInternet()
        
        setView()
        let name = Notification.Name("didReceiveData")
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: name, object: nil)
                         
        let updateBadgeCount = Notification.Name("updateBadgeCount")
        NotificationCenter.default.addObserver(self, selector: #selector(updateBadgeCount(_:)), name: updateBadgeCount, object: nil)
        
        self.notificationCount.layer.cornerRadius = 20/2
        self.notificationCount.layer.masksToBounds = true
        
        // Do any additional setup after loading the view.
    }
    @objc func onDidReceiveData(_ notification:Notification) {
        
        // Do something now
        
        if  UserDefaults.standard.value(forKey: "openNotification") != nil
        {
            if(UserDefaults.standard.value(forKey: "openNotification") as! String == "true")
            {
                ///NotiifcationViewController
                
                
                
                
                let viewControllerArray = self.navigationController?.viewControllers
                
                var checkFlag:Bool
                checkFlag = false
                for controller in viewControllerArray! {
                    
                    
                    print(controller)
                    if controller.classForCoder == NotificationViewController.self {
                        checkFlag = true
                        print(controller)
                    }
                    
                }
                
                
                if(checkFlag)
                {
                    
                    NotificationCenter.default.post(name: Notification.Name("updateTable"), object: nil)
                    
                }
                else
                {
                    let view1 = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController
                    
                    let data:[AnyObject]? = UserDefaults.standard.value(forKey: "NotificationList") as? [AnyObject]
                    
                    view1?.dataArray = (UserDefaults.standard.value(forKey: "NotificationList") as? [AnyObject])!
                    
                    view1?.globalArray = data!.reversed()
                    
                    
                    DispatchQueue.main.async {
                        self.navigationController?.pushViewController(view1!, animated: true)
                    }
                }
                
                
                
                
                
                
                
            }
            else
            {
                // getData()
            }
            
        }
        else
        {
            // getData()
            
        }
        
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
      
            tabBarController?.tabBar.isHidden = false
            self.navigationController?.setNavigationBarHidden(true, animated: true)
            
        
                    
                    
                    
                    let data:[AnyObject]? = UserDefaults.standard.value(forKey: "NotificationList") as? [AnyObject]
                    
                    
                    
                    print("\(UserDefaults.standard.value(forKey: "userSessionBadgeCnt") ?? "0")")
                    
            //        DispatchQueue.main.async {
                    
                     DispatchQueue.main.async {
                    
                        self.notificationCount.isHidden = false
                        
                        let cnt:Int? = UIApplication.shared.applicationIconBadgeNumber
                        if(cnt == 0 || cnt == nil)
                        {
                            DispatchQueue.main.async {
                                self.notificationCount.text = "0"
                            }
                        }
                        else
                        {   DispatchQueue.main.async {
                            self.notificationCount.text = "\(String(cnt!))"
                            }
                        }
                        
                  
                        
                        // UserDefaults.standard.value(forKey: "userSessionBadgeCnt") as? String
                        
                    //}
                    
                    
               
            
        }
    }
   
//    override func viewDidAppear(_ animated: Bool) {
//            isOpen = false
//        if self.children.count > 0{
//        let viewControllers:[UIViewController] = self.children
//        for viewContoller in viewControllers{
//            viewContoller.willMove(toParent: nil)
//            viewContoller.view.removeFromSuperview()
//            viewContoller.removeFromParent()
//        }
//    }
//            
//                 }
    
    
    func checkForInternet() {
            if CheckInternet.isConnectedToNetwork() == true
            {
                print("internet available")
//                self.laboratoriesCollectionView.isHidden = false
                DispatchQueue.main.async {
                    
//                    self.loadActivityIndicator()
                }
                onFirstTimeLoad()
            }
            else
            {
                print("no internet")
//                laboratoriesCollectionView.isHidden = true
                activityLoader?.hide(animated: true)
            }
        }
//    func loadActivityIndicator()
//       {
//           activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
//           activityLoader?.label.text = "Loading";
//           activityLoader?.detailsLabel.text = "Please Wait";
//           activityLoader?.isUserInteractionEnabled = false;
//       }
    
    func onFirstTimeLoad() {
       callUrls = laboratoriesUrl
        let params = [""]
        requestToServer.connectToServer(myUrl: callUrls as URL, params: params as AnyObject)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout:

        UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let width = UIScreen.main.bounds.size.width
       
        return CGSize(width: ((width / 2) - 15)   , height: 120)



    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          return tableViewMenuArray.count
             }
       
      func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
             let cell = laboratoriesCollectionView.dequeueReusableCell(withReuseIdentifier: "LaboratoriesCollectionViewCell", for: indexPath) as! LaboratoriesCollectionViewCell
             
        cell.laboratoriesImage.startShimmeringEffect()
        cell.laboratoriesLabel.startShimmeringEffect()
        
                    cell.contentView.layer.cornerRadius = 2.0
                    cell.contentView.layer.borderWidth = 1.0
                    cell.contentView.layer.borderColor = UIColor.clear.cgColor
                    cell.contentView.layer.masksToBounds = true
                   
                    cell.layer.backgroundColor = UIColor.white.cgColor
                    cell.layer.shadowColor = UIColor.lightGray.cgColor
                    cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)//CGSizeMake(0, 2.0);
                                cell.layer.shadowRadius = 2.0
                                cell.layer.shadowOpacity = 1.0
                                cell.layer.masksToBounds = false
                    cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
             
             cell.laboratoriesImage.layer.masksToBounds = true
             cell.laboratoriesImage.layer.borderWidth = 1
             cell.laboratoriesImage.layer.borderColor = UIColor.darkGray.cgColor
             cell.laboratoriesImage.layer.cornerRadius = 5
             
             
        cell.laboratoriesLabel.text = ((self.tableViewMenuArray.object(at: indexPath.row) as AnyObject).object(forKey: "title")as! String)
             let imgdata = ((self.tableViewMenuArray.object(at: indexPath.row) as AnyObject).object(forKey: "image")as! String)
             if(imgdata == " "){
                 cell.laboratoriesImage.image = UIImage(named: "logo-1.png")
             }
             else{
                if type=="false"{
                
                 let imgfilterUrl = labsInstituteImgUrl + imgdata
                 replacedImgstring = imgfilterUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!
                 URLSession.shared.dataTask(with: NSURL(string: replacedImgstring)! as URL, completionHandler: { (data, response, error) -> Void in
                     if error != nil {
                         return
                     }
                     else {
                         
                         DispatchQueue.main.async {
                             let image = UIImage(data: data!)

                             if (image != nil) {
                                     cell.laboratoriesImage.image = image
                                // cell1.menuImg.contentMode = UIViewContentMode.scaleToFill
                             }
                             else{
                                  cell.laboratoriesImage.image = UIImage(named: "logo-1.png")
                             }
                         }
                     }
                 }).resume()
                    cell.laboratoriesImage.layer.sublayers = nil
                    cell.laboratoriesLabel.layer.sublayers = nil
                }
                
             }
             return cell
            }
       
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier:"LabContentViewController") as! LabContentViewController
        let title = ((self.tableViewMenuArray.object(at: indexPath.row) as AnyObject).object(forKey: "title") as! String)
        vc.Ltitle = title
        let desc = ((self.tableViewMenuArray.object(at: indexPath.row) as AnyObject).object(forKey: "description") as! String)
        vc.Ldesc = desc
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
   
       
      
    
    @IBAction func laboratoriesBtnAction(_ sender: Any) {
       DispatchQueue.main.async {
                     self.laboratoryLbl.isHidden = false
                     self.institutesLbl.isHidden = true
                     self.cfcLbl.isHidden = true

        }
        if self.children.count >= 0{
            let viewControllers:[UIViewController] = self.children
            for viewContoller in viewControllers{
                viewContoller.willMove(toParent: nil)
                viewContoller.view.removeFromSuperview()
                viewContoller.removeFromParent()
            }
        }
        
    }
    @IBAction func cfcBtnAction(_ sender: Any) {
DispatchQueue.main.async {
                    self.laboratoryLbl.isHidden = true
                    self.institutesLbl.isHidden = true
                    self.cfcLbl.isHidden = false

       }
               if self.children.count >= 0{
                          let viewControllers:[UIViewController] = self.children
                          for viewContoller in viewControllers{
                              viewContoller.willMove(toParent: nil)
                              viewContoller.view.removeFromSuperview()
                              viewContoller.removeFromParent()
                          }
                      }
                      
                      let controller:CFCViewController =
                             self.storyboard!.instantiateViewController(withIdentifier: "CFCViewController") as!
                             CFCViewController

                            let statusbarHeight = UIApplication.shared.statusBarFrame.height

                             
                             let tabBarHeight = self.tabBarController!.tabBar.frame.size.height+50;

                             
                             
                             controller.view.frame = CGRect(x: 0, y: statusbarHeight+85, width: UIScreen.main.bounds.size.width, height: (UIScreen.main.bounds.size.height-tabBarHeight))
                             controller.willMove(toParent: self)
                             self.view.addSubview(controller.view)
                             self.addChild(controller)
                             controller.didMove(toParent: self)
        
    }
    @IBAction func institutesBtnAction(_ sender: Any) {
DispatchQueue.main.async {
             self.laboratoryLbl.isHidden = true
             self.institutesLbl.isHidden = false
             self.cfcLbl.isHidden = true

}
        let controller:InstitutesViewController =
               self.storyboard!.instantiateViewController(withIdentifier: "InstitutesViewController") as!
               InstitutesViewController

              let statusbarHeight = UIApplication.shared.statusBarFrame.height

               
               let tabBarHeight = self.tabBarController!.tabBar.frame.size.height+50;

               
               
               controller.view.frame = CGRect(x: 0, y: statusbarHeight+85, width: UIScreen.main.bounds.size.width, height: (UIScreen.main.bounds.size.height-tabBarHeight))
               controller.willMove(toParent: self)
               self.view.addSubview(controller.view)
               self.addChild(controller)
               controller.didMove(toParent: self)
        
    }
    func dataReceivedFromServer(data: NSDictionary, url: URL) {
        
        if ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "true" {
            
            let temporaryArray = ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "Result") as! NSArray)
//             let sortedArray = (temporaryArray as NSArray).sortedArray(using: [NSSortDescriptor(key: "title", ascending: true)]) as! [[String:AnyObject]] as NSArray
            
            
            tableViewMenuArray = (temporaryArray.mutableCopy() as! NSMutableArray)
            print("tableViewMenuArray.....\(tableViewMenuArray)")
            DispatchQueue.main.async {
//                self.activityLoader?.hide(animated: true)
                self.laboratoriesCollectionView.reloadData()
                
              self.laboratoriesCollectionView.delegate = self
                self.laboratoriesCollectionView.dataSource = self
//                self.view.hideSkeleton()
                DispatchQueue.main.asyncAfter(deadline: .now()+1.0) {
                    self.changeImage()
                }
            }
        }
        
    }
    
    @IBAction func menu(_ sender: Any) {
        

        
//        if(!isOpen)
//
//        {
            isOpen = true

            let menuVC = self.storyboard!.instantiateViewController(withIdentifier: "SlideViewController") as! SlideViewController
            self.view.addSubview(menuVC.view)
            self.addChild(menuVC)
            menuVC.view.layoutIfNeeded()

            menuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width-90, height: UIScreen.main.bounds.size.height);

            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width-90, height: UIScreen.main.bounds.size.height);
        }, completion:nil)

//        }else if(isOpen)
//        {
//            isOpen = false
//          let viewMenuBack : UIView = view.subviews.last!
//
//            UIView.animate(withDuration: 0.3, animations: { () -> Void in
//                var frameMenu : CGRect = viewMenuBack.frame
//                frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
//                viewMenuBack.frame = frameMenu
//                viewMenuBack.layoutIfNeeded()
//                viewMenuBack.backgroundColor = UIColor.clear
//            }, completion: { (finished) -> Void in
//                viewMenuBack.removeFromSuperview()
//
//            })
//        }

        
        
        
        
        
        
        
        
        
    }
    
    
    
       func setView()  {
        
            DispatchQueue.main.async {
                      self.laboratoryLbl.isHidden = false
                      self.institutesLbl.isHidden = true
                      self.cfcLbl.isHidden = true
//                      self.updatesLbl.backgroundColor = UIColor.white

                  }
                  
                    if self.children.count >= 0{
                              let viewControllers:[UIViewController] = self.children
                              for viewContoller in viewControllers{
                                  viewContoller.willMove(toParent: nil)
                                  viewContoller.view.removeFromSuperview()
                                  viewContoller.removeFromParent()
                              }
                          }
       }
    
    
    @objc func updateBadgeCount(_ notification:Notification){
           
           
           print("\(UserDefaults.standard.value(forKey: "userSessionBadgeCnt") ?? "0")")
           
           DispatchQueue.main.async {
               
               self.notificationCount.isHidden = false
               
               
               let cnt:Int? = UIApplication.shared.applicationIconBadgeNumber
           
           
               if(cnt == 0 || cnt == nil)
               {
                      DispatchQueue.main.async {
                   self.notificationCount.text = "0"
                   }
               }
               else
               {   DispatchQueue.main.async {
                   self.notificationCount.text = "\(String(cnt!))"
                   }
               }
           }
          
           
       }

    func getData()  {
        
        var responseDict : NSMutableDictionary = [:]
        var dataTask: URLSessionDataTask
        
        let url = NSURL(string: "https://gjepc.org/gjepc_mobile/services/firebaseNotificationList")
        
        let request = NSMutableURLRequest(url:url! as URL)
        
        
        
        let session = URLSession.shared
        dataTask = session.dataTask(with: request as URLRequest){(data, response, error) in
            guard error == nil else {
                
                
                return
            }
            guard let data = data else {
                return
            }
            do {
                if let dict = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! NSMutableDictionary{
                    
                    // let dict = json as! NSMutableDictionary
                    //  print(dict)
                    if (dict["Response"] as! NSDictionary)["status"] as! String == "true" {
                        responseDict = (dict["Response"] as! NSMutableDictionary)
                        
                        print(responseDict)
                        
                        
                        
                        self.responseNotificationArray = (dict["Response"] as! NSDictionary)["Result"] as! NSMutableArray
                        
                        
                        
                        
                        
                        let lastNotificationCount:String? = UserDefaults.standard.value(forKey: "lastNotificationCOunt")  as? String
                        
                        let mastercnt:String? = UserDefaults.standard.value(forKey: "MasterNotificationCount")  as? String
                        
                        
                        var master :Int? = 0
                        
                        if(mastercnt != nil)
                        {
                            master = Int(mastercnt!)
                        }
                        else
                            
                        {
                            master = 0
                        }
                        
                        
                        
                        
                        var lastCnt:Int? = 0
                        
                        
                        if(lastNotificationCount != nil)
                        {
                            lastCnt = Int(lastNotificationCount!)
                        }
                        else
                        {
                            lastCnt = 0
                            
                        }
                        
                        
                        
                        
                        
                        
                        let isRead:String? = UserDefaults.standard.value(forKey: "isRead") as? String
                        
                        
                        
                        
                        
                        
                        if(isRead != nil)
                        {
                            if(lastCnt == self.responseNotificationArray.count)
                            {
                                if(isRead == "true")
                                {
                                    
                                    
                                }
                                else
                                {
                                    
                                    UserDefaults.standard.setValue(lastCnt, forKey: "lastNotificationCOunt")
                                    
                                    
                                    UserDefaults.standard.setValue("0", forKey: "MasterNotificationCount")
                                    
                                    UserDefaults.standard.setValue("false", forKey: "isRead")
                                    
                                }
                                
                            }
                            else
                            {
                                
                                if(isRead == "true")
                                {
                                    //when new notification come show new notification count only
                                    
                                    
                                    
                                    let rCount:Int = self.responseNotificationArray.count
                                    
                                   
                                    let newCount:Int = rCount - master!
                                    
                                    //let latestCount = self.responseNotificationArray.count - newCount
                                    
                                    let newCountStr:String = String(newCount)
                                    
                                    UserDefaults.standard.setValue(String(newCountStr), forKey: "lastNotificationCOunt")
                                    
                                    UserDefaults.standard.setValue("false", forKey: "isRead")
                                    //                                    }
                                    
                                    
                                    
                                }
                                else
                                {
                                    
                                    if(master == 0)
                                    {
                                        
                                        let rCount:Int = self.responseNotificationArray.count
                                        
                                        let newCount1:Int = rCount - lastCnt!
                                        
                                        let newCount:Int = newCount1 + lastCnt!
                                        
                                        UserDefaults.standard.setValue(String(newCount), forKey: "lastNotificationCOunt")
                                        
                                        UserDefaults.standard.setValue("false", forKey: "isRead")
                                        
                                    }
                                    else
                                        
                                    {
                                        
                                        
                                        let rCount:Int = self.responseNotificationArray.count
                                        
                                        var newCount1:Int = 0
                                        
                                        
                                        newCount1 = rCount - master!
                                        
                                        UserDefaults.standard.setValue(String(newCount1), forKey: "lastNotificationCOunt")
                                        
                                        UserDefaults.standard.setValue("false", forKey: "isRead")
                                        
                                        //                                        }
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                    }
                                    
                                    
                                    
                                }
                                
                                
                            }
                        }
                        else
                        {
                            
                            
                            
                            let lastNotificationCount1 = self.responseNotificationArray.count
                            
                            UserDefaults.standard.setValue(String(lastNotificationCount1), forKey: "lastNotificationCOunt")
                            
                            
                            UserDefaults.standard.setValue("0", forKey: "MasterNotificationCount")
                            
                            UserDefaults.standard.setValue("false", forKey: "isRead")
                            
                            
                        }
                        
                        
                        
                        
                        
                        
                        var lastNotificationCount1:String? = nil
                        
                        if let lastCount = UserDefaults.standard.value(forKey: "lastNotificationCOunt") as? NSNumber {
                            
                            
                            lastNotificationCount1 = lastCount.stringValue
                            
                            
                            
                        }
                        else if let quantity = UserDefaults.standard.value(forKey: "lastNotificationCOunt") as? String {
                            
                            
                            lastNotificationCount1 = quantity
                            
                            
                        }
                        else
                            
                        {
                            lastNotificationCount1 = "0"
                        }
                        
                        
                        
                        
                        
                        
                       
                        
                        
                        let masterCOunt:String? = (UserDefaults.standard.value(forKey: "MasterNotificationCount") as! String)
                        
                        print("MASTER \(masterCOunt!)")
                        
                        
                        let new = Int(lastNotificationCount1!)
                        
                        if (new! >= 0)
                        {
                            // do positive stuff
                        }
                        else
                            
                        {
                            self.isChange = true
                            
                            UserDefaults.standard.setValue("0", forKey: "lastNotificationCOunt")
                            
                            UserDefaults.standard.setValue("true", forKey: "isRead")
                            
                        }
                        
                        let isRead1:String? = (UserDefaults.standard.value(forKey: "isRead") as! String)
                        
                        
                        if(isRead1 == "true")
                        {
                            //UserDefaults.standard.setValue("0", forKey: "lastNotificationCOunt")
                            
                            
                            DispatchQueue.main.async {
                                
                                
                                self.notificationCount.isHidden = false
                                
                                self.notificationCount.text = "0"
                                
                            }
                        }
                        else
                        {
                            
                            //show count on bell icon
                            
                            DispatchQueue.main.async {
                                
                                self.notificationCount.isHidden = false
                                
                                self.notificationCount.text = lastNotificationCount1
                                
                            }
                            
                            
                            
                            
                            
                            
                            //  print(lastNotificationCount1)
                            
                            
                            
                        }
                        
                        
                        
                        
                        
                        
                    } else {
                        
                        return
                        
                    }
                }
                
            } catch let error {
                print("Json Error",error.localizedDescription)
                
            }
            
            
            
            
        }
        
        dataTask.resume()
        
        
        
        
        
        
        
        
        
    }
    
    
    @IBAction func notification(_ sender: Any) {





        let data:[AnyObject]? = UserDefaults.standard.value(forKey: "NotificationList") as? [AnyObject]
        print(data)

        if  data != nil {



            DispatchQueue.main.async {


                let view1 = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController

                view1?.dataArray = (UserDefaults.standard.value(forKey: "NotificationList") as? [AnyObject])!

                view1?.globalArray = data!.reversed()
                self.navigationController?.pushViewController(view1!, animated: true)
            }


        }
        else
        {




            DispatchQueue.main.async {

                let view1 = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController

                view1?.dataArray = []

                view1?.globalArray = []
                self.navigationController?.pushViewController(view1!, animated: true)
            }
        }






    }
    
    @objc func changeImage()  {
                      self.type = "false"
                      self.laboratoriesCollectionView.reloadData()
                  }
}

    
   
    

