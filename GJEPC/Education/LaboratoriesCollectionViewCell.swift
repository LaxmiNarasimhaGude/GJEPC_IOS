//
//  LaboratoriesCollectionViewCell.swift
//  GJEPC
//
//  Created by Kwebmaker on 17/01/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit

class LaboratoriesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var laboratoriesImage: UIImageView!
    
    @IBOutlet weak var laboratoriesLabel: UILabel!
}
