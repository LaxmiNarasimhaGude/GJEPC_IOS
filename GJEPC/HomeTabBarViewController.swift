//
//  HomeTabBarViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 04/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class HomeTabBarViewController: UITabBarController {

    @IBOutlet weak var tabbar: UITabBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        tabbar.unselectedItemTintColor = UIColor.darkGray

    }
    

    

}
