//
//  EventsAndTradeShowCollectionViewCell.swift
//  GJEPC
//
//  Created by Kwebmaker on 08/01/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit

class EventsAndTradeShowCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var eventHeadingLabel: UILabel!
    @IBOutlet weak var eventImage: UIImageView!
    
}
