//
//  HomeViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 07/01/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
@available(iOS 13.0, *)
class HomeViewController: UIViewController,dataReceivedFromServerDelegate,UIScrollViewDelegate {
    func dataReceivedFromServer(data: NSDictionary, url: URL) {
            if ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "true" {
                let temporaryArray = ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "Result") as! NSMutableArray)
                let checkStatus  =  (((temporaryArray.object(at: 0)) as AnyObject).value(forKey: "status") as! String)
                let isTemp = (((temporaryArray.object(at: 0)) as AnyObject).value(forKey: "isTemp") as! String)
                let year = (((temporaryArray.object(at: 0)) as AnyObject).value(forKey: "year") as! String)
                print(year)
                if checkStatus == "0" {
                    
                    
                    DispatchQueue.main.async {
                    self.activityLoader?.hide(animated: true)
                                    }
                    
                    let alert = UIAlertController(title: "", message: "Coming Soon", preferredStyle: UIAlertController.Style.alert)
                    DispatchQueue.main.sync {
                         self.present(alert, animated: true, completion: nil)
                    }
                       
    //
                    
                    let when = DispatchTime.now() + 3
                    DispatchQueue.main.asyncAfter(deadline: when){
                        
                        alert.dismiss(animated: true, completion: nil)
                    }
                } else {
                    
                    if(checkStatus == "1"){
                        
                        if(isTemp == "0"){
                            DispatchQueue.main.async {
                                         self.activityLoader?.hide(animated: true)
                                     }
                                     if signaturetapped == true {
                                       
                                         DispatchQueue.main.async {
                                            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SignatureViewController") as! SignatureViewController

                                             vc.category = "national"
                                             vc.year = year
                                             vc.exhibitorSelected = "signature"
                                            vc.titleName =  self.sendTitleName
                                             self.navigationController?.pushViewController(vc, animated: true)
                                         }
                                         signaturetapped = false
                                     }
                                     else if igemstapped == true {
                                         let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SignatureViewController") as! SignatureViewController
                                         vc.category = "national"
                                        vc.year = year
                                         vc.exhibitorSelected = "igjme"
                                         vc.titleName = "IGJME"
                                         DispatchQueue.main.async {
                                             self.navigationController?.pushViewController(vc, animated: true)
                                         }
                                         igemstapped = false
                                         
                                     } else {
                                         let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SignatureViewController") as! SignatureViewController
                                         vc.category = "national"
                                        vc.year = year
                                         vc.exhibitorSelected = "iijs"
                                         vc.titleName = "IIJS Premiere 2019"
                                         DispatchQueue.main.async {
                                             self.navigationController?.pushViewController(vc, animated: true)
                                         }
                                         iijstapped = false
                                     }
                                 }
                        else{
                             let Urldescription = (((temporaryArray.object(at: 0)) as AnyObject).value(forKey: "description") as! String)
                            
                            DispatchQueue.main.async {
                                self.activityLoader?.hide(animated: true)
                                                       
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "SignatureWebViewController") as! SignatureWebViewController
                               
                                vc.des = Urldescription
                                vc.headingDesc = self.sendTitleName
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                            
                           
                          
                            

                            
                        }
                        
                        
                        }
                        
                        
                        
                    }
                    
                    
             
            }
        }
    
   
    

    @IBOutlet weak var addPageView: UIPageControl!
    
    
    @IBOutlet weak var addcollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var sliderScrollView: UIScrollView!
    @IBOutlet weak var helpDeskInnerView: UIView!
    @IBOutlet weak var circularsInnerView: UIView!
    
    @IBOutlet weak var statisticsInnerView: UIView!
    @IBOutlet weak var webinarInnerView: UIView!
    @IBOutlet weak var membershipInnerView: UIView!
    
    @IBOutlet weak var viewHelpDesk: UIView!
    @IBOutlet weak var statisticsView: UIView!
    @IBOutlet weak var webinarsView: UIView!
    @IBOutlet weak var membershipView: UIView!
    @IBOutlet weak var gjepcUpdatesBtnHeight: NSLayoutConstraint!
    @IBOutlet weak var circularsImage: UIImageView!
    @IBOutlet weak var webinarImage: UIImageView!
    @IBOutlet weak var statisticsImage: UIImageView!
    @IBOutlet weak var membershipImage: UIImageView!
    @IBOutlet weak var sliderLowerLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var sliderLabelHeight: NSLayoutConstraint!
    
    @IBOutlet weak var exhibitorRegistrationLbl: UILabel!
    @IBOutlet weak var visitorRegistrationLbl: UILabel!
    
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    @IBOutlet weak var sliderLbl: UILabel!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var pageView: UIPageControl!
    @IBOutlet weak var sliderImageCollectionView: UICollectionView!
    @IBOutlet weak var gjepcUpdatesLbl: UILabel!
    @IBOutlet weak var helpdeskView: UIView!
    @IBOutlet weak var gjepcApplicationBtn: UIButton!
    @IBOutlet weak var addCollectionView: UICollectionView!
    @IBOutlet weak var giaImage: UIImageView!
    @IBOutlet weak var helpdeskImage: UIImageView!
    @IBOutlet weak var giaView: UIView!
    @IBOutlet weak var helpDeskView: UIView!
    @IBOutlet weak var circularView: UIView!
    @IBOutlet weak var researchAndStatisticsView: UIView!
    @IBOutlet weak var notificationCount: UILabel!
    @IBOutlet weak var seminarView: UIView!
    @IBOutlet weak var memberView: UIView!
    @IBOutlet weak var eventsAndTradeshowCollectionView: UICollectionView!
    @IBOutlet weak var newsCollectionView: UICollectionView!
    @IBOutlet weak var visitorRegistrationBtn: UIButton!
    @IBOutlet weak var exhibitorRegistrationBtn: UIButton!
    @IBOutlet weak var gjepcUpdatesBtn: UIButton!
    
    @IBOutlet weak var myBadge: UIButton!
    @IBOutlet weak var solitareBtn: UIButton!
    
    @IBOutlet weak var sliderLowerLbl: UILabel!
    var isOpen = false
     
//    var sideMenuViewController = SlideViewController()
//    var isMenuOpened:Bool = false\
    var activityLoader : MBProgressHUD? = nil
    var requestServer = RequestToServer()
    var responseNotificationArray:NSMutableArray = []
     var responseButtonArray:NSMutableArray = []
    var isChange:Bool = false
    var switching = ""
    var sendTitleName = ""
    var igemstapped = false
    var iijstapped = false
    var signaturetapped = false
    var requestToServer = RequestToServer()
    var callUrls : URL!
    var customArray : NSMutableArray = []
    var temporaryArray : NSArray = []
    var collectionViewArrayForNews : NSMutableArray = []
    var myMutableString = NSMutableAttributedString()
    var myMutableString1 = NSMutableAttributedString()
//    var scrollBool = false
     var loadNew = true;
    var offSet: CGFloat = 0
    var responsedata : NSMutableArray = []
    var currentcellIndex = 0
    
//    var tap = UITapGestureRecognizer()
//    var eventsImages:NSMutableArray = [["image" : "jewellery_stone.png" , "name" : "IIJS Premiere 2020"],["image" : "iijs.png", "name" : "IIJS Signature 2020"],["image" : "igjme.png", "name" : "IGJME 2020"]]
    var eventsImages:NSMutableArray = [["image" : "iijs.png", "name" : "IIJS Signature 2020"],["image" : "igjme.png", "name" : "IGJME 2020"]]

    var imgArr  = ["add-1","downloaded"]

    var timer = Timer()
    var adTimer:Timer?
    var counter = 0
    
    

    
    
    @objc func autoScroll() {
        let totalPossibleOffset = CGFloat(responseNotificationArray.count - 1) * self.view.bounds.size.width
        print(offSet)
        if offSet == totalPossibleOffset {
            offSet = 0 // come back to the first image after the last image
           
        }
        else {
        offSet += self.view.bounds.size.width
            
            
            
        }
        DispatchQueue.main.async() {
            UIView.animate(withDuration: 0.3, delay: 0, options: UIView.AnimationOptions.curveLinear, animations: {
                self.sliderScrollView.contentOffset.x = CGFloat(self.offSet)
                
            }, completion: nil)
            self.changeImage()
        }
    }
    
    
    func changeImage(){
//        scrollBool = true
//       sliderLbl.setCharacterSpacing(1.1)
//    sliderLbl.setLineSpacing(lineSpacing: 10,lineHeightMultiple: 0)
//            if counter == 0{
                DispatchQueue.main.async {
                    self.sliderLabelHeight.constant = 60
//                    self.sliderLowerLabelHeight.constant = 0
                }
              


                DispatchQueue.main.async {
                    self.sliderLbl.text = (((self.responseNotificationArray.object(at: self.counter) as! NSDictionary).value(forKey: "text1") as? String)?.withoutHtml)?.uppercased()
                    self.sliderLowerLbl.text = (((self.responseNotificationArray.object(at: self.counter) as! NSDictionary).value(forKey: "text2") as? String)?.withoutHtml)?.uppercased()

                    if (((self.responseNotificationArray.object(at: self.counter) as! NSDictionary).value(forKey: "text2") as? String)==""){
                        DispatchQueue.main.async {
//                                           self.sliderLabelHeight.constant = 60
                                           self.sliderLowerLabelHeight.constant = 0
                                       }
                    }
                    else{
                        DispatchQueue.main.async {
//                                           self.sliderLabelHeight.constant = 60
                                           self.sliderLowerLabelHeight.constant = 40
                                       }
                    }

                    self.myMutableString = NSMutableAttributedString(string: self.sliderLbl.text!, attributes: [NSAttributedString.Key.font:UIFont(name: "Gotham-Bold", size: 14.0)!])
                    self.myMutableString1 = NSMutableAttributedString(string: self.sliderLowerLbl.text!, attributes: [NSAttributedString.Key.font:UIFont(name: "Gotham-Bold", size: 14.0)!])



                                            var style = NSMutableParagraphStyle()
                                            style.lineSpacing = 20
                    self.myMutableString.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: NSRange(location: 0, length: ((self.sliderLbl.text))!.count))
                    self.myMutableString1.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: NSRange(location: 0, length: ((self.sliderLowerLbl.text))!.count))
                    self.sliderLbl.attributedText = self.myMutableString
                    self.sliderLbl.textAlignment = NSTextAlignment.center;
                    self.sliderLowerLbl.attributedText = self.myMutableString1
                    self.sliderLowerLbl.textAlignment = NSTextAlignment.center;
                                    self.activityLoader?.hide(animated: true)

//                }
            }

        }
    
    @IBAction func pdfdownloader(_ sender: Any) {
        
        
    }
    

    
    func loadImages(){
        
        DispatchQueue.main.async {
            for i in 0..<self.responseNotificationArray.count{
                  
              
                  let imageview = UIImageView()
                  imageview.clipsToBounds = true
                  imageview.contentMode = .scaleAspectFit


                  
                  
                  let xpos = CGFloat(i)*self.view.bounds.size.width
                imageview.frame = CGRect(x: xpos, y: 0, width: self.view.frame.size.width, height: self.sliderScrollView.frame.size.height)
                  
                
                let touchButton:UIButton = UIButton()
                        touchButton.frame = imageview.frame
                        touchButton.backgroundColor = UIColor.clear
                        touchButton.tag = i
                touchButton.addTarget(self, action: #selector(self.touchCategoryAction(sender:)), for: .touchUpInside)

                touchButton.setTitle("", for: UIControl.State.normal)
                  
                              let suffixImageString = (self.responseNotificationArray.object(at: i) as! NSDictionary).value(forKey: "banner") as? String
                              let prefixImageUrl = "https://gjepc.org/assets/images/banner/"
                  
                                 let imageUrl = prefixImageUrl + suffixImageString!
                              print(imageUrl)
                                 URLSession.shared.dataTask(with: NSURL(string: imageUrl)! as URL, completionHandler: { (data, response, error) -> Void in
                                     if error != nil {
                                         return
                                     }else {
                                         DispatchQueue.main.async(execute: { () -> Void in
                                             let image = UIImage(data: data!)
                                             print(data as Any)
                                             if (image != nil) {
                                                  imageview.image = image
                                             }
                                         })
                                     }
                                 }).resume()
                  
                  
                  
                self.sliderScrollView.contentSize.width = (self.view.frame.size.width)*CGFloat(i+1)
                self.sliderScrollView.addSubview(imageview)
                self.sliderScrollView.addSubview(touchButton)
                  
                  
                  
                  
              }
        }
  

    }
    @objc func touchCategoryAction(sender: UIButton)  {
        print(sender.tag)
//        ((self.responseNotificationArray.object(at: self.counter) as! NSDictionary).value(forKey: "text2") as? String)
        
        
        guard let url = URL(string: (((self.responseNotificationArray.object(at: self.counter) as! NSDictionary).value(forKey: "link") as? String)!)) else { return }
        UIApplication.shared.open(url)
    }
   @objc func slideToNext() {
        
    if currentcellIndex < imgArr.count{
        
        let index = IndexPath.init(item: currentcellIndex, section: 0)
        self.addCollectionView.isPagingEnabled = false
        addCollectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
        self.addCollectionView.isPagingEnabled = true
        addPageView.currentPage = currentcellIndex
        currentcellIndex += 1
    }
    else{
        currentcellIndex = 0
        let index = IndexPath.init(item: currentcellIndex, section: 0)
        self.addCollectionView.isPagingEnabled = false
        addCollectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
        self.addCollectionView.isPagingEnabled = true
        addPageView.currentPage = currentcellIndex
//        addCollectionView.scrollToItem(at: IndexPath(item: currentcellIndex, section: 0), at: .centeredHorizontally, animated: true)
    }
    print(currentcellIndex)
   
    
    }
    
    func getAddImagesData()  {
        let params = ["type":"app","section":"brand_slider"]
        let request = NSMutableURLRequest(url:appBannerUrl! as URL)
        request.httpMethod = "POST";
        request.httpBody = try! JSONSerialization.data(withJSONObject: params, options: [])
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        //create the session object
        let session = URLSession.shared
        //create dataTask using the session object to send data to the server
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            guard error == nil else {
                return
            }
            guard let data = data else {
                return
            }
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                   print(json)

                    if json.count > 0 {
//                        self.responsedata = json as NSDictionary
                        if ((json.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "true"{
                            DispatchQueue.main.async {
                            self.addcollectionViewHeight.constant = 190
                                self.viewHeight.constant = 1750
                            }
                            self.temporaryArray = ((json.value(forKey: "Response") as! NSDictionary).value(forKey: "Result") as! NSArray)
                            print(self.temporaryArray)
                            if self.temporaryArray.count > 0 {
                                for i in 0..<self.temporaryArray.count {

        self.responsedata.add(self.temporaryArray[i]);
                                    
                                                   }
                            }
                           
                            DispatchQueue.main.async { [self] in
                                addPageView.numberOfPages = responsedata.count
                                addCollectionView.delegate = self
                                addCollectionView.dataSource = self
                                 self.addCollectionView.reloadData()
                            }
                           
                            print(self.responsedata)
                        }
                        else{

                            print("no images")
                            DispatchQueue.main.async {
                                self.addcollectionViewHeight.constant = 0
                                self.viewHeight.constant = 1750-190
                                
                            }
                           
                        }
                    }
//                    else{
//                        print("no images")
//                    }
                    print("Here: \(self.responsedata)")
                }
                
                

                
                
                
                
                
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        task.resume()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if CheckInternet.isConnectedToNetwork() == true{
            
            
            
                          
                          let controller:HomePopupVc =
                                  self.storyboard!.instantiateViewController(withIdentifier: "HomePopupVc") as!
                                  HomePopupVc



                                  
                                  
                                  controller.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: (UIScreen.main.bounds.size.height))
                                  controller.willMove(toParent: self)
                                  self.view.addSubview(controller.view)
                                  self.addChild(controller)
                                  controller.didMove(toParent: self)
            
            
            
            getData()
            getAddImagesData()
           getButtonData()
           
            
            
           DispatchQueue.main.async {
               let timer = Timer.scheduledTimer(timeInterval: 8, target: self, selector: #selector(self.autoScroll), userInfo: nil, repeats: true)
            
            self.adTimer = Timer.scheduledTimer(timeInterval: 8, target: self, selector: #selector(self.slideToNext), userInfo: nil, repeats: true)

           }
        }
        else{
            print(" no internet")
        }
        
        
        

        
        
      
        
        
        


     
      backgroundView.backgroundColor = UIColor.white
      backgroundView.layer.cornerRadius = 3.0
      backgroundView.layer.masksToBounds = false
      backgroundView.layer.shadowOffset = CGSize(width: 0, height: 0)
        backgroundView.layer.shadowOpacity = 0.3
        


        
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.tabBarController?.tabBar.isHidden = false
        
        requestServer.delegate = self
        
        print(switching)

       updateView()
        if CheckInternet.isConnectedToNetwork() == true{
             onFirstTimeLoad()
        }
        else{
            
        }
        
//        addCollectionView.delegate = self
//        addCollectionView.dataSource = self
        
        newsCollectionView.delegate = self
        newsCollectionView.dataSource = self

        
        eventsAndTradeshowCollectionView.delegate = self
        eventsAndTradeshowCollectionView.dataSource = self
        
        
             
            DispatchQueue.main.async {
                     
                     self.notificationCount.layer.cornerRadius = 20/2
                     self.notificationCount.layer.masksToBounds = true
                     
                 }
                 
                 
                 
                 
               
                 
                 
                 
                 
                 if(UserDefaults.standard.value(forKey: "isUpdateAvailable") as! String == "true")
                 {
                     
                     
                     if(UserDefaults.standard.value(forKey: "count") as! String == "0")
                     {
                         
                         UserDefaults.standard.set("1", forKey: "count")
                         
                         
                        let refreshAlert = UIAlertController(title: "Update Available", message: "A new version of GJEPC is available.", preferredStyle: UIAlertController.Style.alert)
                         
                         refreshAlert.addAction(UIAlertAction(title: "Update", style: .default, handler: { (action: UIAlertAction!) in
                             print("Handle Ok logic here")
                             
                             
                             
                             
                             let url:URL? = URL(string: "https://itunes.apple.com/in/app/gjepc/id1194017236?mt=8")
                             
                             print(url!)
                             
                             /* First create a URL, then check whether there is an installed app that can
                              open it on the device. */
                             if UIApplication.shared.canOpenURL(url! as URL) {
                                 if #available(iOS 10.0, *) {
                                     UIApplication.shared.open(url! as URL, options: [:], completionHandler: { (success) in
                                         
                                     })
                                 } else {
                                     UIApplication.shared.openURL(url! as URL)
                                 }
                             }
                             
                             
                         }))
                         
                         refreshAlert.addAction(UIAlertAction(title: "Next time", style: .cancel, handler: { (action: UIAlertAction!) in
                             print("Handle Cancel Logic here")
                         }))
                         
                         present(refreshAlert, animated: true, completion: nil)
                         
                         
                     }
                     else
                     {
                         
                     }
                     
                     
                     
                     
                     
                 }
                 else
                 {
                     
                     
                 }
                 
                 
                 let name = Notification.Name("didReceiveData")
                 NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: name, object: nil)
                 
                 let updateBadgeCount = Notification.Name("updateBadgeCount")
                 NotificationCenter.default.addObserver(self, selector: #selector(updateBadgeCount(_:)), name: updateBadgeCount, object: nil)
           
         
           
//           self.popUp()
        
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let page = (sliderScrollView.contentOffset.x/sliderScrollView.frame.width)

        pageView.currentPage = Int(page)
        counter = Int(page)

        
        
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {

        changeImage()
    }


    
   
//    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
////        tap.isEnabled = true
//       if isOpen{
//           isOpen = false
//
////        tap.isEnabled = false
//
//
//
//         let viewMenuBack : UIView = view.subviews.last!
//
//           UIView.animate(withDuration: 0.3, animations: { () -> Void in
//               var frameMenu : CGRect = viewMenuBack.frame
//               frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
//               viewMenuBack.frame = frameMenu
//               viewMenuBack.layoutIfNeeded()
//               viewMenuBack.backgroundColor = UIColor.clear
//           }, completion: { (finished) -> Void in
//               viewMenuBack.removeFromSuperview()
//
//           })
//       }
//    }
        override  func viewWillAppear(_ animated: Bool)
        {
                
                
                
                let data:[AnyObject]? = UserDefaults.standard.value(forKey: "NotificationList") as? [AnyObject]
                
                
                
                print("\(UserDefaults.standard.value(forKey: "userSessionBadgeCnt") ?? "0")")
                
        //        DispatchQueue.main.async {
                
                 DispatchQueue.main.async {
                
                    self.notificationCount.isHidden = false
                    
                    let cnt:Int? = UIApplication.shared.applicationIconBadgeNumber
                    if(cnt == 0 || cnt == nil)
                    {
                        DispatchQueue.main.async {
                            self.notificationCount.text = "0"
                        }
                    }
                    else
                    {   DispatchQueue.main.async {
                        self.notificationCount.text = "\(String(cnt!))"
                        }
                    }
                    
                }
                    
                    // UserDefaults.standard.value(forKey: "userSessionBadgeCnt") as? String
                    
                //}
                
                
            }

    
    override func viewDidAppear(_ animated: Bool) {
       
//        if self.children.count >= 0{
//                          let viewControllers:[UIViewController] = self.children
//                          for viewContoller in viewControllers{
//                              viewContoller.willMove(toParent: nil)
//                              viewContoller.view.removeFromSuperview()
//                              viewContoller.removeFromParent()
//                          }
//                      }
        
//        isOpen = false
//        self.view.isUserInteractionEnabled  = true
//    if self.children.count > 0{
//    let viewControllers:[UIViewController] = self.children
//    for viewContoller in viewControllers{
//        viewContoller.willMove(toParent: nil)
//        viewContoller.view.removeFromSuperview()
//        viewContoller.removeFromParent()
//    }
//}
        
             }
    
    @IBAction func menuaction(_ sender: Any) {
        


            let menuVC : SlideViewController = self.storyboard!.instantiateViewController(withIdentifier: "SlideViewController") as! SlideViewController
            self.view.addSubview(menuVC.view)
            self.addChild(menuVC)
            menuVC.view.layoutIfNeeded()

            menuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width-60, height: UIScreen.main.bounds.size.height);

            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width-60, height: UIScreen.main.bounds.size.height);
        }, completion:nil)




    }
    
    @objc func updateBadgeCount(_ notification:Notification){
        
        
        print("\(UserDefaults.standard.value(forKey: "userSessionBadgeCnt") ?? "0")")
        
        DispatchQueue.main.async {
            
            self.notificationCount.isHidden = false
            
            
            let cnt:Int? = UIApplication.shared.applicationIconBadgeNumber
        
        
            if(cnt == 0 || cnt == nil)
            {
                   DispatchQueue.main.async {
                self.notificationCount.text = "0"
                }
            }
            else
            {   DispatchQueue.main.async {
                self.notificationCount.text = "\(String(cnt!))"
                }
            }
        }
       
        
    }

    
    
    @objc func onDidReceiveData(_ notification:Notification) {
        
        // Do something now
        
        if  UserDefaults.standard.value(forKey: "openNotification") != nil
        {
            if(UserDefaults.standard.value(forKey: "openNotification") as! String == "true")
            {
                ///NotiifcationViewController
                
                
                
                
                let viewControllerArray = self.navigationController?.viewControllers
                
                var checkFlag:Bool
                checkFlag = false
                for controller in viewControllerArray! {
                    
                    
                    print(controller)
                    if controller.classForCoder == NotificationViewController.self {
                        checkFlag = true
                        print(controller)
                    }
                    
                }
                
                
                if(checkFlag)
                {
                    
                    NotificationCenter.default.post(name: Notification.Name("updateTable"), object: nil)
                    
                }
                else
                {
                    let view1 = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController
                    
                    let data:[AnyObject]? = UserDefaults.standard.value(forKey: "NotificationList") as? [AnyObject]
                    
                    view1?.dataArray = (UserDefaults.standard.value(forKey: "NotificationList") as? [AnyObject])!
                    
                    view1?.globalArray = data!.reversed()
                    
                    
                    DispatchQueue.main.async {
                        self.navigationController?.pushViewController(view1!, animated: true)
                    }
                }
                
                
                
                
                
                
                
            }
            else
            {
                // getData()
            }
            
        }
        else
        {
            // getData()
            
        }
        
    }

    
    
    @IBAction func mybadgeBtnTapped(_ sender: Any) {
        
                let badgeBool = UserDefaults.standard.value(forKey: "BadgeLogin")
                print(UserDefaults.standard.value(forKey: "BadgeLogin"))
                if (badgeBool) as! String == "true"{
                    let vc = storyboard?.instantiateViewController(withIdentifier: "BadgeWebViewController") as! BadgeWebViewController
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else{
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "MyBadgeViewController") as! MyBadgeViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
                }
    }
    
    
    
    
    func getData()
    {

        if CheckInternet.isConnectedToNetwork() == true {
        
        var responseDict : NSMutableDictionary = [:]
        var dataTask: URLSessionDataTask

        let url = NSURL(string: "https://gjepc.org/gjepc_mobile/services/getHomepageBanner")

        let request = NSMutableURLRequest(url:url! as URL)



        let session = URLSession.shared
        dataTask = session.dataTask(with: request as URLRequest){(data, response, error) in
            guard error == nil else {


                return
            }
            guard let data = data else {
                return
            }
            do {
                if let dict = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! NSMutableDictionary{

                    // let dict = json as! NSMutableDictionary
                    //  print(dict)
                    if (dict["Response"] as! NSDictionary)["status"] as! String == "true" {
                        responseDict = (dict["Response"] as! NSMutableDictionary)





                        self.responseNotificationArray = (dict["Response"] as! NSDictionary)["Result"] as! NSMutableArray

                                               print(self.responseNotificationArray)

                        print((self.responseNotificationArray).count)
                        DispatchQueue.main.async {
                            self.pageView.numberOfPages = (self.responseNotificationArray).count

                        }
                        self.changeImage()
                        self.loadImages()

                    } else {

                        return

                    }
                }

            } catch let error {
                print("Json Error",error.localizedDescription)

            }

        }

        dataTask.resume()

        }
        else{
            
            let alert = UIAlertController(title: "", message: "Please Check Your Internet Connection", preferredStyle: UIAlertController.Style.alert)
            self.present(alert, animated: true, completion: nil)
            let when = DispatchTime.now() + 3
            DispatchQueue.main.asyncAfter(deadline: when){
                alert.dismiss(animated: true, completion: nil)
            }
        }

    }

    func getButtonData()
        {

            if CheckInternet.isConnectedToNetwork() == true {
            
            var responseDict : NSMutableDictionary = [:]
            var dataTask: URLSessionDataTask

            let url = NSURL(string: "https://gjepc.org/gjepc_mobile/services/getButtonDetails")

            let request = NSMutableURLRequest(url:url! as URL)



            let session = URLSession.shared
            dataTask = session.dataTask(with: request as URLRequest){(data, response, error) in
                guard error == nil else {


                    return
                }
                guard let data = data else {
                    return
                }
                do {
                    if let dict = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! NSMutableDictionary{

                        // let dict = json as! NSMutableDictionary
                        //  print(dict)
                        if (dict["Response"] as! NSDictionary)["status"] as! String == "true" {
                            responseDict = (dict["Response"] as! NSMutableDictionary)

    //                        print(responseDict)



                            self.responseButtonArray = (dict["Response"] as! NSDictionary)["Result"] as! NSMutableArray

                            if((self.responseNotificationArray.object(at: 0) as! NSDictionary).value(forKey: "status") as? String == "1"){
                                DispatchQueue.main.async {
                                      self.gjepcUpdatesBtn.isHidden = false
                                      self.gjepcUpdatesBtnHeight.constant = 35
                                      self.gjepcUpdatesLbl.isHidden = false
                                                  print(((self.responseButtonArray.object(at: 0) as! NSDictionary).value(forKey: "text") as? String))
                                                                  self.gjepcUpdatesBtn.setTitle(((self.responseButtonArray.object(at: 0) as! NSDictionary).value(forKey: "text") as? String)?.uppercased(), for: UIControl.State.normal)
                                }
                              
                                    
//
                            }
                            else{
                                self.gjepcUpdatesLbl.isHidden = true
                                self.gjepcUpdatesBtn.isHidden = true
                                self.gjepcUpdatesBtnHeight.constant = 0
                            }
                            

                        } else {

                            return

                        }
                    }

                } catch let error {
                    print("Json Error",error.localizedDescription)

                }

            }

            dataTask.resume()



        }
            else{
                let alert = UIAlertController(title: "", message: "Please Check Your Internet Connection", preferredStyle: UIAlertController.Style.alert)
                
                 self.present(alert, animated: true, completion: nil)
                let when = DispatchTime.now()+2
                DispatchQueue.main.asyncAfter(deadline: when){
                    alert.dismiss(animated: true, completion: nil)
                }
            }
    }
    
    
    @IBAction func solitare(_ sender: Any) {
        
        if CheckInternet.isConnectedToNetwork() == true {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "HelpDeskViewController") as! HelpDeskViewController
            obj.urlString = "https://gjepc.org/solitaire/"
            obj.headingLbl = "SOLITARE INTERNATIONAL"
            DispatchQueue.main.async {
                self.navigationController?.pushViewController(obj, animated: true)
            }
        }else{
            
            let alert = UIAlertController(title: "", message: "Please Check Your Internet Connection", preferredStyle: UIAlertController.Style.alert)
            self.present(alert, animated: true, completion: nil)
            let when = DispatchTime.now() + 3
            DispatchQueue.main.asyncAfter(deadline: when){
                alert.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    
  
    func loadActivityIndicator()
        {
            activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true)
            activityLoader?.bezelView.color = UIColor.clear
    
            activityLoader?.bezelView.style = .solidColor
            activityLoader?.isUserInteractionEnabled = false
        }
    func updateView()  {
        
        
//        let width:CGFloat = UIScreen.main.bounds.width*0.0533
//        membershipView.frame = CGRect(0,0,width,width)
        
        membershipView.layer.cornerRadius = membershipView.frame.size.height/2;
        membershipView.layer.masksToBounds = true
        
        webinarsView.layer.cornerRadius = webinarsView.frame.size.height/2;
        webinarsView.layer.masksToBounds = true
        
        statisticsView.layer.cornerRadius = statisticsView.frame.size.height/2;
        statisticsView.layer.masksToBounds = true

        
        circularView.layer.cornerRadius = circularView.frame.size.height/2;
        circularView.layer.masksToBounds = true

        viewHelpDesk.layer.cornerRadius = viewHelpDesk.frame.size.height/2;
        viewHelpDesk.layer.masksToBounds = true
        
        
        membershipInnerView.layer.cornerRadius = 5;
        membershipInnerView.layer.masksToBounds = true;
        
        circularsInnerView.layer.cornerRadius = 5;
        circularsInnerView.layer.masksToBounds = true;

        webinarInnerView.layer.cornerRadius = 5;
        webinarInnerView.layer.masksToBounds = true;

        statisticsInnerView.layer.cornerRadius = 5;
        statisticsInnerView.layer.masksToBounds = true;
        
        helpDeskInnerView.layer.cornerRadius = 5;
        helpDeskInnerView.layer.masksToBounds = true;
        
        
        
        
        //        membershipView?.layer.cornerRadius = (membershipImage?.frame.size.width ?? 0.0) / 2
//        membershipView?.clipsToBounds = true
//        membershipView?.layer.borderWidth = 0
//        membershipView?.layer.borderColor = UIColor.white.cgColor
                
        
            //helpdeskImage.layer.cornerRadius = 5;
           // helpdeskImage.contentMode = .scaleAspectFill
            //helpdeskImage.clipsToBounds = true;
            
            
            //giaImage.layer.cornerRadius = 5;
           // giaImage.contentMode = .scaleAspectFill
           // giaImage.clipsToBounds = true;
            
//            memberView.backgroundColor = UIColor.white
//            memberView.layer.shadowColor = UIColor.lightGray.cgColor
//            memberView.layer.shadowOpacity = 1
//            memberView.layer.shadowOffset = CGSize.zero
//            memberView.layer.shadowRadius = 2
//            memberView.layer.cornerRadius = 5
//            memberView.layer.borderWidth = 1
//            memberView.layer.borderColor = UIColor.white.cgColor
//            
//            seminarView.backgroundColor = UIColor.white
//            seminarView.layer.shadowColor = UIColor.lightGray.cgColor
//            seminarView.layer.shadowOpacity = 1
//            seminarView.layer.shadowOffset = CGSize.zero
//            seminarView.layer.shadowRadius = 2
//            seminarView.layer.cornerRadius = 5
//            seminarView.layer.borderWidth = 1
//            seminarView.layer.borderColor = UIColor.white.cgColor
//            
//            researchAndStatisticsView.backgroundColor = UIColor.white
//            researchAndStatisticsView.layer.shadowColor = UIColor.lightGray.cgColor
//            researchAndStatisticsView.layer.shadowOpacity = 1
//            researchAndStatisticsView.layer.shadowOffset = CGSize.zero
//            researchAndStatisticsView.layer.shadowRadius = 2
//            researchAndStatisticsView.layer.cornerRadius = 5
//            researchAndStatisticsView.layer.borderWidth = 1
//            researchAndStatisticsView.layer.borderColor = UIColor.white.cgColor
//            
//            circularView.backgroundColor = UIColor.white
//            circularView.layer.shadowColor = UIColor.lightGray.cgColor
//            circularView.layer.shadowOpacity = 1
//            circularView.layer.shadowOffset = CGSize.zero
//            circularView.layer.shadowRadius = 2
//            circularView.layer.cornerRadius = 5
//            circularView.layer.borderWidth = 1
//            circularView.layer.borderColor = UIColor.white.cgColor
            
//            helpDeskView.backgroundColor = UIColor.white
//            helpDeskView.layer.shadowColor = UIColor.lightGray.cgColor
//            helpDeskView.layer.shadowOpacity = 1
//            helpDeskView.layer.shadowOffset = CGSize.zero
//            helpDeskView.layer.shadowRadius = 2
//            helpDeskView.layer.cornerRadius = 5
//            helpDeskView.layer.borderWidth = 1
//            helpDeskView.layer.borderColor = UIColor.white.cgColor
            
//            giaView.backgroundColor = UIColor.white
//            giaView.layer.shadowColor = UIColor.lightGray.cgColor
//            giaView.layer.shadowOpacity = 1
//            giaView.layer.shadowOffset = CGSize.zero
//            giaView.layer.shadowRadius = 2
//            giaView.layer.cornerRadius = 5
//            giaView.layer.borderWidth = 1
//            giaView.layer.borderColor = UIColor.white.cgColor
        
        
//                   helpdeskView.backgroundColor = UIColor.darkGray
//                    helpdeskView.layer.shadowColor = UIColor.lightGray.cgColor
//                    helpdeskView.layer.shadowOpacity = 1
//                    helpdeskView.layer.shadowOffset = CGSize.zero
//                    helpdeskView.layer.shadowRadius = 2
//                    helpdeskView.layer.cornerRadius = 5
//                    helpdeskView.layer.borderWidth = 1
//                    helpdeskView.layer.borderColor = UIColor.white.cgColor
        
            
                        gjepcUpdatesBtn.backgroundColor = .darkGray
                        gjepcUpdatesBtn.layer.cornerRadius = 10
                        gjepcUpdatesBtn.layer.borderWidth = 1
                         gjepcUpdatesBtn.layer.borderColor = UIColor.clear.cgColor
        
//        solitareBtn.backgroundColor = .darkGray
//        solitareBtn.layer.cornerRadius = 10
//        solitareBtn.layer.borderWidth = 1
//        solitareBtn.layer.borderColor = UIColor.clear.cgColor
        
        myBadge.backgroundColor = .darkGray
        myBadge.layer.cornerRadius = 10
        myBadge.layer.borderWidth = 1
        myBadge.layer.borderColor = UIColor.clear.cgColor
        
//        gjepcUpdatesLbl.backgroundColor = .darkGray
//        gjepcUpdatesLbl.layer.cornerRadius = 10
//        gjepcUpdatesLbl.layer.borderWidth = 1
//        gjepcUpdatesLbl.layer.borderColor = UIColor.gray.cgColor
        
        
           
    //            self.view.addSubview(viewShadow)
            
                visitorRegistrationBtn.backgroundColor = .clear
                visitorRegistrationBtn.layer.cornerRadius = 5
                visitorRegistrationBtn.layer.borderWidth = 1
            visitorRegistrationBtn.layer.borderColor = UIColor.gray.cgColor
        
        visitorRegistrationLbl.layer.masksToBounds = true
        visitorRegistrationLbl.backgroundColor = .darkGray
                       visitorRegistrationLbl.layer.cornerRadius = 5
                       visitorRegistrationLbl.layer.borderWidth = 1
//                   visitorRegistrationLbl.layer.borderColor = UIColor.gray.cgColor
        
//        gjepcApplicationBtn.backgroundColor = .clear
//            gjepcApplicationBtn.layer.cornerRadius = 10
//            gjepcApplicationBtn.layer.borderWidth = 1
//        gjepcApplicationBtn.layer.borderColor = UIColor.gray.cgColor

    //            visitorRegistrationBtn.layer.borderColor = UIColor(red:222/255, green:225/255, blue:227/255, alpha: 1).cgColor
         exhibitorRegistrationLbl.layer.masksToBounds = true
        exhibitorRegistrationLbl.backgroundColor = .darkGray
                              exhibitorRegistrationLbl.layer.cornerRadius = 5
                              exhibitorRegistrationLbl.layer.borderWidth = 1
                          exhibitorRegistrationLbl.layer.borderColor = UIColor.gray.cgColor

                exhibitorRegistrationBtn.backgroundColor = .clear
                exhibitorRegistrationBtn.layer.cornerRadius = 5
                exhibitorRegistrationBtn.layer.borderWidth = 1

        
        
//                exhibitorRegistration.layer.borderColor = UIColor(red:222/255, green:225/255, blue:227/255, alpha: 1).cgColor
            exhibitorRegistrationBtn.layer.borderColor = UIColor.gray.cgColor
            }
    @IBAction func gjepcApplication(_ sender: Any) {
        
        if CheckInternet.isConnectedToNetwork() == true{
            let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
                   nextVC!.headingtitle = "GJEPC RELIEF AID APPLICATIONS FORM";
                   
                   self.navigationController?.pushViewController(nextVC!, animated: true)
        }
        else{
            let alert = UIAlertController(title: "", message: "Please Check Your Internet Connection", preferredStyle: UIAlertController.Style.alert)
            
             self.present(alert, animated: true, completion: nil)
            let when = DispatchTime.now()+2
            DispatchQueue.main.asyncAfter(deadline: when){
                alert.dismiss(animated: true, completion: nil)
            }
            
           
        }


    }
   
    @IBAction func iijsvirtualImageButton(_ sender: Any) {
        
        if CheckInternet.isConnectedToNetwork() == true{
            let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
                   nextVC!.headingtitle = "IIJS VIRTUAL"
                   
                   self.navigationController?.pushViewController(nextVC!, animated: true)
        }
        else{
            let alert = UIAlertController(title: "", message: "Please Check Your Internet Connection", preferredStyle: UIAlertController.Style.alert)
            
             self.present(alert, animated: true, completion: nil)
            let when = DispatchTime.now()+2
            DispatchQueue.main.asyncAfter(deadline: when){
                alert.dismiss(animated: true, completion: nil)
            }
            
           
        }

    }
    
    
    
   func onFirstTimeLoad() {
    
    
    
          customArray = [];
          callUrls = dailyNewsUrl
          let params = [ "start":"0",
                         "limit":"4"]
//          self.loading = true;
//          loadNew = true;
             let request = NSMutableURLRequest(url:callUrls as URL)
                  request.httpMethod = "POST";
                  request.httpBody = try! JSONSerialization.data(withJSONObject: params, options: [])
                  request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                  request.addValue("application/json", forHTTPHeaderField: "Accept")
                  //create the session object
                  let session = URLSession.shared
                  //create dataTask using the session object to send data to the server
                  let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                      guard error == nil else {
                          return
                      }
                      guard let data = data else {
                          return
                      }
                      do {
                          //create json object from data
                          if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                             print(json)
                            if ((json.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "true"{
                                self.temporaryArray = ((json.value(forKey: "Response") as! NSDictionary).value(forKey: "Result") as! NSArray)
                                print(self.temporaryArray)
                                if self.temporaryArray.count > 0 {
                                    for i in 0..<self.temporaryArray.count {

            self.collectionViewArrayForNews.add(self.temporaryArray[i]);
                                                       }
                                }
                                DispatchQueue.main.async {
                                     self.newsCollectionView.reloadData()
                                }
                               
                                print(self.collectionViewArrayForNews)
                            }
                          }
                      } catch let error {
                          print(error.localizedDescription)
                      }
                  })
                  task.resume()
      }
   
    @IBAction func newsBtnAction(_ sender: Any) {
        
        if CheckInternet.isConnectedToNetwork() == true{
            self.tabBarController?.selectedIndex = 1
        }
        else{
            let alert = UIAlertController(title: "", message: "Please Check Your Internet Connection", preferredStyle: UIAlertController.Style.alert)
            
             self.present(alert, animated: true, completion: nil)
            let when = DispatchTime.now()+2
            DispatchQueue.main.asyncAfter(deadline: when){
                alert.dismiss(animated: true, completion: nil)
            }
            
           
        }
        
        


        
    }
    @IBAction func artisanAwards(_ sender: Any) {
        let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
        
        nextVC!.headingtitle = (((self.responseButtonArray.object(at: 0) as! NSDictionary).value(forKey: "text") as? String)!).uppercased()
        nextVC!.url = (((self.responseButtonArray.object(at: 0) as! NSDictionary).value(forKey: "link") as? String)!)
                         
                         self.navigationController?.pushViewController(nextVC!, animated: true)
    }
    
    @IBAction func seminarAndWorkshops(_ sender: Any) {
        
        if CheckInternet.isConnectedToNetwork() == true{
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                          let vc = storyboard.instantiateViewController(withIdentifier: "SeminarsViewController") as! SeminarsViewController
//                     self.navigationController?.pushViewController(vc, animated: true)
            
            let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
                  nextVC!.headingtitle = "WEBINARS";
                  
                  self.navigationController?.pushViewController(nextVC!, animated: true)
        }
        else{
            let alert = UIAlertController(title: "", message: "Please Check Your Internet Connection", preferredStyle: UIAlertController.Style.alert)
            
             self.present(alert, animated: true, completion: nil)
            let when = DispatchTime.now()+2
            DispatchQueue.main.asyncAfter(deadline: when){
                alert.dismiss(animated: true, completion: nil)
            }
            
           
        }
        
        
       
        
    }
    
    @IBAction func researchAndStatistics(_ sender: Any) {
        
        
       
            
            if CheckInternet.isConnectedToNetwork() == true {
                let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
                       nextVC!.headingtitle = "RESEARCH AND STATISTICS";
                       
                       self.navigationController?.pushViewController(nextVC!, animated: true)
            }else{
                
                let alert = UIAlertController(title: "", message: "Please Check Your Internet Connection", preferredStyle: UIAlertController.Style.alert)
                self.present(alert, animated: true, completion: nil)
                let when = DispatchTime.now() + 3
                DispatchQueue.main.asyncAfter(deadline: when){
                    alert.dismiss(animated: true, completion: nil)
                }
    
            }
        
//        if CheckInternet.isConnectedToNetwork() == true {
//            let name : UIStoryboard!
//            name = UIStoryboard(name: "Main", bundle: nil)
//            let vc = name.instantiateViewController(withIdentifier: "StatisticsViewController") as! StatisticsViewController
////            vc.whereToGo = "Statistics"
//            DispatchQueue.main.async {
//                self.navigationController?.pushViewController(vc, animated: true)
//            }
//
//        }
//        else{
//            let alert = UIAlertController(title: "", message: "Please Check Your Internet Connection", preferredStyle: UIAlertController.Style.alert)
//            self.present(alert, animated: true, completion: nil)
//            let when = DispatchTime.now() + 2
//            DispatchQueue.main.asyncAfter(deadline: when){
//                alert.dismiss(animated: true, completion: nil)
//            }
//        }
        
    }
    
    @IBAction func circulars(_ sender: Any) {
        if CheckInternet.isConnectedToNetwork() == true{
            let vc = storyboard?.instantiateViewController(withIdentifier: "CircularMemberViewController") as! CircularMemberViewController
            navigationController?.pushViewController(vc, animated: true)
        }
        else{
            let alert = UIAlertController(title: "", message: "Please Check Your Internet Connection", preferredStyle: UIAlertController.Style.alert)
            
             self.present(alert, animated: true, completion: nil)
            let when = DispatchTime.now()+2
            DispatchQueue.main.asyncAfter(deadline: when){
                alert.dismiss(animated: true, completion: nil)
            }
            
           
        }
        
        
    }
    
   
    
    
    @IBAction func eventsAndTradeshows(_ sender: Any) {
        

        
         let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                 let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "EventsSignatureViewController") as! EventsSignatureViewController

        
        navigationController?.pushViewController(redViewController, animated: true)
        
    }
    
    @IBAction func helpDesk(_ sender: Any) {
        
        if CheckInternet.isConnectedToNetwork() == true {
                   let obj = self.storyboard?.instantiateViewController(withIdentifier: "HelpDeskViewController") as! HelpDeskViewController
                   obj.urlString = "https://gjepc.org/helpdesk/index.php"
                   DispatchQueue.main.async {
                    self.navigationController?.pushViewController(obj, animated: true)
                   }
               }else{
                   
            let alert = UIAlertController(title: "", message: "Please Check Your Internet Connection", preferredStyle: UIAlertController.Style.alert)
                   self.present(alert, animated: true, completion: nil)
                   let when = DispatchTime.now() + 2
                   DispatchQueue.main.asyncAfter(deadline: when){
                       alert.dismiss(animated: true, completion: nil)
                   }
               }
    }
    
    @IBAction func gia(_ sender: Any) {
        
        
        if CheckInternet.isConnectedToNetwork() == true {
                              DispatchQueue.main.async {
                              
                              
                              if #available(iOS 10.0, *) {
                                  UIApplication.shared.open(footerLink!, options: [:], completionHandler: nil)
                              } else {
                                  UIApplication.shared.openURL(footerLink!)
                              }
                          }

                      }else{
                          
                   let alert = UIAlertController(title: "", message: "Please Check Your Internet Connection", preferredStyle: UIAlertController.Style.alert)
                          self.present(alert, animated: true, completion: nil)
                          let when = DispatchTime.now() + 2
                          DispatchQueue.main.asyncAfter(deadline: when){
                              alert.dismiss(animated: true, completion: nil)
                          }
                      }
        
        
        
    }
    @IBAction func facebook(_ sender: Any) {
        if let linkedinUrl = URL(string: "https://www.facebook.com/GJEPC"),
                  UIApplication.shared.canOpenURL(linkedinUrl) {
                  UIApplication.shared.open(linkedinUrl, options: [:], completionHandler: nil)
               }
    }
    @IBAction func instagram(_ sender: Any) {
       if let linkedinUrl = URL(string: "https://www.instagram.com/gjepcindia/"),
           UIApplication.shared.canOpenURL(linkedinUrl) {
           UIApplication.shared.open(linkedinUrl, options: [:], completionHandler: nil)
        }
    }
    @IBAction func linkedIn(_ sender: Any) {
        if let linkedinUrl = URL(string: "https://www.linkedin.com/in/sabyaray/"),
           UIApplication.shared.canOpenURL(linkedinUrl) {
           UIApplication.shared.open(linkedinUrl, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func twitter(_ sender: Any) {
        if let linkedinUrl = URL(string: "https://twitter.com/GJEPCIndia"),
           UIApplication.shared.canOpenURL(linkedinUrl) {
           UIApplication.shared.open(linkedinUrl, options: [:], completionHandler: nil)
        }
    }
    
    func removeSpecialCharsFromString(text: String) -> String {
          let okayChars = Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-=().!_</p><p>")
          return text.filter {okayChars.contains($0) }
      }
    
    @IBAction func membership(_ sender: Any) {
        
        if CheckInternet.isConnectedToNetwork() == true{
                   self.tabBarController?.selectedIndex = 2
               }
               else{
                   let alert = UIAlertController(title: "", message: "Please Check Your Internet Connection", preferredStyle: UIAlertController.Style.alert)
                   
                    self.present(alert, animated: true, completion: nil)
                   let when = DispatchTime.now()+2
                   DispatchQueue.main.asyncAfter(deadline: when){
                       alert.dismiss(animated: true, completion: nil)
                   }
                   
                  
               }
        
        
       
        
        
        
        
    }
    
    
    @IBAction func notificationBtn(_ sender: Any) {





        let data:[AnyObject]? = UserDefaults.standard.value(forKey: "NotificationList") as? [AnyObject]
        print(data)

        if  data != nil {



            DispatchQueue.main.async {


                let view1 = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController

                view1?.dataArray = (UserDefaults.standard.value(forKey: "NotificationList") as? [AnyObject])!

                view1?.globalArray = data!.reversed()
                self.navigationController?.pushViewController(view1!, animated: true)
            }


        }
        else
        {




            DispatchQueue.main.async {

                let view1 = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController

                view1?.dataArray = []

                view1?.globalArray = []
                self.navigationController?.pushViewController(view1!, animated: true)
            }
        }






    }
    
    func isFirstLuanch()-> Bool{
        if UserDefaults.standard.bool(forKey: "isAppAlreadyLaunchedOnce") {
            return false
        }else{
            UserDefaults.standard.set(true, forKey: "isAppAlreadyLaunchedOnce")
            return true
        }
    }
    
    
    @IBAction func visitor(_ sender: Any) {
      
        if CheckInternet.isConnectedToNetwork() == true{
            let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
                   nextVC!.headingtitle = "IIJS VIRTUAL VISITOR REGISTRATION";
                   
                   self.navigationController?.pushViewController(nextVC!, animated: true)
        }
        else{
            let alert = UIAlertController(title: "", message: "Please Check Your Internet Connection", preferredStyle: UIAlertController.Style.alert)
            
             self.present(alert, animated: true, completion: nil)
            let when = DispatchTime.now()+2
            DispatchQueue.main.asyncAfter(deadline: when){
                alert.dismiss(animated: true, completion: nil)
            }
            
           
        }

        
        
        
      
        
    }
//    func gradiant(){
//           let gradiantLayer = CAGradientLayer()
//        var gradientBackgroundColor : CGColor = UIColor(white: 0.85, alpha: 1.0).cgColor
//        var gradientMovingColor : CGColor = UIColor(white: 0.75, alpha: 1.0).cgColor
//           gradiantLayer.colors = [
//              gradientBackgroundColor,
//              gradientMovingColor,
//            gradientBackgroundColor
//           ]
//        gradiantLayer.locations = [0.0,0.5,1.0]
//           gradiantLayer.frame = self.view.frame
//
//
//
//
//           let angle = 45*CGFloat.pi/180
//           gradiantLayer.transform = CATransform3DMakeRotation(angle, 0, 0, 1)
//           self.view.layer.mask = gradiantLayer
//
//           let animation = CABasicAnimation(keyPath: "transform.translation.x")
//           animation.duration = 2
//           animation.fromValue = -view.frame.width
//           animation.toValue = view.frame.width
//           animation.repeatCount = Float.infinity
//           gradiantLayer.add(animation, forKey: "doesn'tmatterJustSomekey")
//       }
    @IBAction func exhibitor(_ sender: Any) {
        
        if CheckInternet.isConnectedToNetwork() == true{
            let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
                   nextVC!.headingtitle = "EXHIBITOR REGISTRATION";
                   
                   self.navigationController?.pushViewController(nextVC!, animated: true)
        }
        else{
            let alert = UIAlertController(title: "", message: "Please Check Your Internet Connection", preferredStyle: UIAlertController.Style.alert)
            
             self.present(alert, animated: true, completion: nil)
            let when = DispatchTime.now()+2
            DispatchQueue.main.asyncAfter(deadline: when){
                alert.dismiss(animated: true, completion: nil)
            }
            
           
        }

    }
    
    var startLocations : [NSNumber] = [-1.0,-0.5, 0.0]
    var endLocations : [NSNumber] = [1.0,1.5, 2.0]
    
    var gradientBackgroundColor : CGColor = UIColor.clear.cgColor
    var gradientMovingColor : CGColor = UIColor(white: 0.75, alpha: 1.0).cgColor
    
    var movingAnimationDuration : CFTimeInterval = 0.8
    var delayBetweenAnimationLoops : CFTimeInterval = 1.0
    

    lazy var gradientLayer : CAGradientLayer = {
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradientLayer.colors = [
            gradientBackgroundColor,
            gradientMovingColor,
            gradientBackgroundColor
        ]
        gradientLayer.locations = self.startLocations
        self.view.layer.addSublayer(gradientLayer)
        return gradientLayer
    }()
    
//    func startAnimating(){
//           let animation = CABasicAnimation(keyPath: "locations")
//           animation.fromValue = self.startLocations
//           animation.toValue = self.endLocations
//           animation.duration = self.movingAnimationDuration
//        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//
//
//           let animationGroup = CAAnimationGroup()
//           animationGroup.duration = self.movingAnimationDuration + self.delayBetweenAnimationLoops
//           animationGroup.animations = [animation]
//           animationGroup.repeatCount = .infinity
//           self.gradientLayer.add(animationGroup, forKey: animation.keyPath)
//       }
       
//       func stopAnimating() {
//           self.gradientLayer.removeAllAnimations()
//       }
    
    
}





@available(iOS 13.0, *)
extension HomeViewController:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.newsCollectionView{
            return collectionViewArrayForNews.count
        }

        else if (collectionView == self.addCollectionView){
            return responsedata.count
        }
        
        else {
            return eventsImages.count
        }
        
    }
    
    

    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        if collectionView == self.newsCollectionView{
                    let cell = newsCollectionView.dequeueReusableCell(withReuseIdentifier: "NewsCollectionViewCell", for: indexPath) as? NewsCollectionViewCell



            cell!.contentView.layer.cornerRadius = 2.0
                                  cell!.contentView.layer.borderWidth = 1.0
                                  cell!.contentView.layer.borderColor = UIColor.clear.cgColor
                                  cell!.contentView.layer.masksToBounds = true
                                 
                                  cell!.layer.backgroundColor = UIColor.white.cgColor
                                  cell!.layer.shadowColor = UIColor.lightGray.cgColor
                                  cell!.layer.shadowOffset = CGSize(width: 0, height: 2.0)//CGSizeMake(0, 2.0);
                                              cell!.layer.shadowRadius = 2.0
                                              cell!.layer.shadowOpacity = 1.0
                                              cell!.layer.masksToBounds = false
                                  cell!.layer.shadowPath = UIBezierPath(roundedRect:cell!.bounds, cornerRadius:cell!.contentView.layer.cornerRadius).cgPath
                    

                    
        
            let htmlString = ((((self.collectionViewArrayForNews.object(at: indexPath.row) as AnyObject).object(forKey: "name") as! String)).htmlString)

            cell?.descriptionLbl.text = htmlString.withoutHtml
            cell?.descriptionLbl.setCharacterSpacing(1)
            
            
            let attrString = NSMutableAttributedString(string: htmlString)
            var style = NSMutableParagraphStyle()
            style.lineSpacing = 5 // change line spacing between paragraph like 36 or 48
//            style.minimumLineHeight = 5// change line spacing between each line like 30 or 40
            attrString.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: NSRange(location: 0, length: htmlString.count))
            cell?.descriptionLbl.attributedText = attrString
            
            
            
                    let dateFormatterGet = DateFormatter()
                    dateFormatterGet.dateFormat = "yyyy-MM-dd"

                    let dateFormatterPrint = DateFormatter()
                    dateFormatterPrint.dateFormat = "dd MMM, yyyy"

                    let date: NSDate? = dateFormatterGet.date(from: "\((self.collectionViewArrayForNews.object(at: indexPath.row) as AnyObject).object(forKey: "post_date")as! String)") as NSDate?
            print(date)
                    print(dateFormatterPrint.string(from: date! as Date))
                    cell!.dateLbl.text = "\(dateFormatterPrint.string(from: date! as Date))"
            cell?.descriptionLbl.labelSpacing(label: cell!.descriptionLbl, font: "Book")
                    return cell!
                }
        else if (collectionView == self.addCollectionView){
            
            let cell = addCollectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! AddCollectionViewCell
//            cell.addImage.image = UIImage(named: imgArr[indexPath.row])
            
            
                           DispatchQueue.main.async {

                            let Imageurl = ((self.responsedata.object(at: indexPath.row) as AnyObject).object(forKey: "banner")as! String)
                            
                            let imgfilterUrl = "\(appBannerBaseUrl)/\(Imageurl)"
                            print(imgfilterUrl)
                                        URLSession.shared.dataTask(with: NSURL(string: imgfilterUrl)! as URL, completionHandler: { (data, response, error) -> Void in
                                                       if error != nil {
                                                           return
                                                       }
                                                       else {

                                                           DispatchQueue.main.async {
                                                               let image = UIImage(data: data!)
                                                               if (image != nil) {

                                                                cell.addImage.image = image
//                                                                cell.addImage.clipsToBounds = true
                                                                self.activityLoader?.hide(animated: true)

                                                               }
                                                               else{

                                                               }
                                                           }

                                                       }
                                                   }).resume()

                                                 }

               
            
            return cell
            
        }


            
        else  {
                     let cell = eventsAndTradeshowCollectionView.dequeueReusableCell(withReuseIdentifier: "EventsAndTradeShowCollectionViewCell", for: indexPath) as? EventsAndTradeShowCollectionViewCell
                    cell!.eventImage.image = UIImage (named : ((self.eventsImages.object(at: indexPath.row) as! NSDictionary)["image"]) as! String)
                    
        
                    
        cell!.eventImage.layer.cornerRadius = 5;
        cell!.eventImage.contentMode = .scaleAspectFill
        cell!.eventImage.clipsToBounds = true;
                    
                    

            cell!.contentView.layer.cornerRadius = 2.0
            cell!.contentView.layer.borderWidth = 1.0
            cell!.contentView.layer.borderColor = UIColor.clear.cgColor
            cell!.contentView.layer.masksToBounds = true
                                         
            cell!.layer.backgroundColor = UIColor.white.cgColor
            cell!.layer.shadowColor = UIColor.lightGray.cgColor
            cell!.layer.shadowOffset = CGSize(width: 0, height: 2.0)//CGSizeMake(0, 2.0);
            cell!.layer.shadowRadius = 2.0
            cell!.layer.shadowOpacity = 1.0
            cell!.layer.masksToBounds = false
           cell!.layer.shadowPath = UIBezierPath(roundedRect:cell!.bounds, cornerRadius:cell!.contentView.layer.cornerRadius).cgPath

                    
                    cell?.eventHeadingLabel.text = (self.eventsImages.object(at: indexPath.row) as! NSDictionary)["name"] as? String
                    return cell!
                }
        
    }
//         @objc func handleSwipes(sender: UISwipeGestureRecognizer) {
//    //                let eventCell = EventCell()
//
//                    if (sender.direction == .left) {
//                        print("Swipe Left")
//                    }
//
//                    if (sender.direction == .right) {
//                        print("Swipe Right")
//                        //let popoverView = CGPointMake(EventCell.popoverView.frame.origin.x + 50.0, self.popoverView.frame.origin.y);
//                       // popoverView.frame = CGRectMake(popoverView.x, popoverView.y, self.popoverView.frame.size.width, self.popoverView.frame.size.height)
//                        //popoverView.isHidden = true
//                    }
//
//                }
                
    
//    @objc func leftSwipe(){
//        print("left")
//    }
//   @objc func rightSwipe() {
//       print("right")
//    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        

            let inset: CGFloat = collectionView.frame.width * 0.5 - 255 * 0.61
               
            return UIEdgeInsets(top: 0, left: 1, bottom: 0, right: inset)

        
           
       }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {

            return 10.0
//        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {

            return 10.0
//        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == newsCollectionView{
             return CGSize(width: 250, height: 155)
        }

        else if collectionView == addCollectionView{
            return CGSize(width: addCollectionView.frame.width, height: addCollectionView.frame.height)
        }
        else{
            return CGSize(width: 280, height: 155)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            
            if collectionView == newsCollectionView{
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "NewsContentViewController") as? NewsContentViewController

                       var replacedImgstring : String = ""
                              
                              DispatchQueue.main.async {
                                  let getExhibitorImage = ((self.collectionViewArrayForNews.object(at: indexPath.row) as AnyObject).object(forKey: "news_pic") as! String)
                                  print(getExhibitorImage)
                                  let imgfilterUrl = ImageUrl + getExhibitorImage
                                  print(imgfilterUrl)
                                  replacedImgstring = imgfilterUrl.replacingOccurrences(of: " ", with: "%20")
                                  print(replacedImgstring)
                                  
                                  URLSession.shared.dataTask(with: NSURL(string: replacedImgstring)! as URL, completionHandler: { (data, response, error) -> Void in
                                      
                                      if error != nil {
                                          return
                                      }else {
                                          DispatchQueue.main.async(execute: { () -> Void in
                                              let image = UIImage(data: data!)
                                              print(data as Any)
                                              if (image != nil) {
                                               vc!.newsImage.image = image
                                                  print(image as Any)
                                              }
                                          })
                                      }
                                  }).resume()
                              }
                        let getnewsListDate = ((self.collectionViewArrayForNews.object(at: indexPath.row) as AnyObject).object(forKey: "post_date") as! String)
                               

       vc!.getnewsListDate  = ((self.collectionViewArrayForNews.object(at: indexPath.row) as AnyObject).object(forKey: "post_date")as! String)
                
                
                
                
                               let removedSpecialCharactersGetnewsList = removeSpecialCharsFromString(text:((self.collectionViewArrayForNews.object(at: indexPath.row) as AnyObject).object(forKey: "name") as! String))
                               

                               vc!.getnewsListName = removedSpecialCharactersGetnewsList
                               let getnewsLongDesc = self.removeSpecialCharsFromString(text: ((self.collectionViewArrayForNews.object(at: indexPath.row) as AnyObject).object(forKey: "long_desc") as! String))
                               vc!.getnewsLongDesc = getnewsLongDesc
                               DispatchQueue.main.async {
                                self.navigationController?.pushViewController(vc!, animated: true)
                               }
            }
            else if (collectionView == eventsAndTradeshowCollectionView){
                if indexPath.row == 0{
                    signaturetapped = true
                    if CheckInternet.isConnectedToNetwork() == true {
                        DispatchQueue.main.async {
//                            self.startAnimating()
                        }

                        let params = ["event":"signature"]
                        sendTitleName = "IIJS SIGNATURE 2020"
                        
                        self.requestServer.connectToServer(myUrl: checkEventStatusUrl!, params: params as AnyObject)
                    }
                    else {
                        let alert = UIAlertController(title: "", message: "Please Check Your Internet Connection", preferredStyle: UIAlertController.Style.alert)
                        self.present(alert, animated: true, completion: nil)
                        let when = DispatchTime.now() + 3
                        DispatchQueue.main.asyncAfter(deadline: when){
                            alert.dismiss(animated: true, completion: nil)
                        }
                    }
                }
                else if indexPath.row == 2{
                    if CheckInternet.isConnectedToNetwork() == true {
                        DispatchQueue.main.async {
//                            self.startAnimating()
                        }
                        
                        iijstapped = true
                        signaturetapped = false
                        igemstapped = false
                        let params = ["event":"iijs"]
                        sendTitleName = "IIJS PREMIERE 2020"
                        requestServer.connectToServer(myUrl: checkEventStatusUrl!, params: params as AnyObject)
                    } else {
                        let alert = UIAlertController(title: "", message: "Please Check Your Internet Connection", preferredStyle: UIAlertController.Style.alert)
                        self.present(alert, animated: true, completion: nil)
                        let when = DispatchTime.now() + 3
                        DispatchQueue.main.asyncAfter(deadline: when){
                            alert.dismiss(animated: true, completion: nil)
                        }
                    }
                }
                else{
                    if CheckInternet.isConnectedToNetwork() == true {
                        DispatchQueue.main.async {
//                            self.startAnimating()
                        }
                        igemstapped = true
                        signaturetapped = false
                        iijstapped = false
                        sendTitleName = "IGJME 2020"
                        let params = ["event": "igjme"]
                        requestServer.connectToServer(myUrl: checkEventStatusUrl!, params: params as AnyObject)
                    } else {
                        let alert = UIAlertController(title: "", message: "Please Check Your Internet Connection", preferredStyle: UIAlertController.Style.alert)
                        self.present(alert, animated: true, completion: nil)
                        let when = DispatchTime.now() + 3
                        DispatchQueue.main.asyncAfter(deadline: when){
                            alert.dismiss(animated: true, completion: nil)
                        }
                    }
                }
            }
            else if collectionView == addCollectionView {
                print("add collection")
                
                DispatchQueue.main.async {
                    guard let url = URL(string: ((self.responsedata.object(at: indexPath.row) as AnyObject).object(forKey: "link")as! String)) else { return }
                    UIApplication.shared.open(url)
                }
                
                ((self.responsedata.object(at: indexPath.row) as AnyObject).object(forKey: "link")as! String)

        }
    //        navigationController?.pushViewController(vc!, animated: true)
        }
    
}
//extension String {
   extension String {
       init(htmlEncodedString: String) {
            self.init()
            guard let encodedData = htmlEncodedString.data(using: .utf8) else {
                self = htmlEncodedString
                return
            }

            let attributedOptions: [NSAttributedString.DocumentReadingOptionKey : Any] = [
                .documentType: NSAttributedString.DocumentType.html,
                .characterEncoding: String.Encoding.utf8.rawValue
            ]

            do {
                let attributedString = try NSAttributedString(data: encodedData, options: attributedOptions, documentAttributes: nil)
                self = attributedString.string
            }
            catch {
                print("Error: \(error)")
                self = htmlEncodedString
            }
        }
    }
    
//    extension UIView{
//        func rotate() {
//            let rotation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
//            rotation.toValue = NSNumber(value: M_PI * 2)
//            rotation.duration = 1
//            rotation.isCumulative = true
//            rotation.repeatCount = FLT_MAX
//            self.layer.add(rotation, forKey: "rotationAnimation")
//        }
//    }
    
//}
extension UILabel{
func setCharacterSpacing(_ spacing: CGFloat){
    let attributedStr = NSMutableAttributedString(string: self.text ?? "")
    attributedStr.addAttribute(NSAttributedString.Key.kern, value: spacing, range: NSMakeRange(0, attributedStr.length))
    self.attributedText = attributedStr
 }
    
    
   

  func setLineSpacing(lineSpacing: CGFloat = 0.0, lineHeightMultiple: CGFloat = 0.0) {

            guard let labelText = self.text else { return }

            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = lineSpacing
            paragraphStyle.lineHeightMultiple = lineHeightMultiple

            let attributedString:NSMutableAttributedString
            if let labelattributedText = self.attributedText {
                attributedString = NSMutableAttributedString(attributedString: labelattributedText)
            } else {
                attributedString = NSMutableAttributedString(string: labelText)
            }

            // Line spacing attribute
            attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))

            self.attributedText = attributedString
        }
  
}
extension UILabel {

    func addImageWith(name: String, behindText: Bool) {

        let attachment = NSTextAttachment()
        attachment.image = UIImage(named: name)
        let attachmentString = NSAttributedString(attachment: attachment)

        guard let txt = self.text else {
            return
        }

        if behindText {
            let strLabelText = NSMutableAttributedString(string: txt)
            strLabelText.append(attachmentString)
            self.attributedText = strLabelText
        } else {
            let strLabelText = NSAttributedString(string: txt)
            let mutableAttachmentString = NSMutableAttributedString(attributedString: attachmentString)
            mutableAttachmentString.append(strLabelText)
            self.attributedText = mutableAttachmentString
        }
    }

    func removeImage() {
        let text = self.text
        self.attributedText = nil
        self.text = text
    }
    
    func labelSpacing(label: UILabel, font : String){

         var myMutableString = NSMutableAttributedString()
        myMutableString = NSMutableAttributedString(string: label.text!, attributes: [NSAttributedString.Key.font:UIFont(name: "Gotham-\(font)", size: 12.0)!])



                              
        let style = NSMutableParagraphStyle()
                           style.lineSpacing = 10
        myMutableString.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: NSRange(location: 0, length: ((label.text))!.count))
        label.attributedText = myMutableString

    }
    
    
}
