//
//  HomePopupVc.swift
//  GJEPC
//
//  Created by Kwebmaker on 03/04/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
@available(iOS 13.0, *)
class HomePopupVc: UIViewController,dataReceivedFromServerDelegate {
    
    

    @IBOutlet weak var homePopupImage: UIImageView!
    
    var activityLoader : MBProgressHUD? = nil
    var requestServer = RequestToServer()
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.gray.withAlphaComponent(0.5)




        requestServer.delegate = self
        if CheckInternet.isConnectedToNetwork() == true{
            DispatchQueue.main.async { [self] in
       loadActivityIndicator()
            }

            let params = ["type":"app","section":"home_popup"]
//            sendTitleName = "IIJS SIGNATURE 2020"
            
            self.requestServer.connectToServer(myUrl: appBannerUrl!, params: params as AnyObject)
        }
       
    }
    func loadActivityIndicator()
        {
            activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true)
            activityLoader?.bezelView.color = UIColor.clear
    
            activityLoader?.bezelView.style = .solidColor
            activityLoader?.isUserInteractionEnabled = false
        }
    func getPopUpImageData(Imageurl:String)  {
                   DispatchQueue.main.async {

                    let imgfilterUrl = "\(appBannerBaseUrl)/\(Imageurl)"
                    print(imgfilterUrl)
                                URLSession.shared.dataTask(with: NSURL(string: imgfilterUrl)! as URL, completionHandler: { (data, response, error) -> Void in
                                               if error != nil {
                                                   return
                                               }
                                               else {

                                                   DispatchQueue.main.async {
                                                       let image = UIImage(data: data!)
                                                       if (image != nil) {

                                                         self.homePopupImage.image = image
                                                        
                                                        self.homePopupImage.contentMode = .scaleToFill
//                                                        self.homePopupImage.alpha = 0.9
                                                        self.activityLoader?.hide(animated: true)

                                                       }
                                                       else{

                                                       }
                                                   }

                                               }
                                           }).resume()

                                         }

       }

    
    func dataReceivedFromServer(data: NSDictionary, url: URL) {
        if ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "true" {
            let temporaryArray = ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "Result") as! NSMutableArray)
            let bannerImage  =  (((temporaryArray.object(at: 0)) as AnyObject).value(forKey: "banner") as! String)
            getPopUpImageData(Imageurl: bannerImage)
            print(bannerImage)
                
                
         
        }
    }
    
    @IBAction func cancel(_ sender: Any) {
         self.view.removeFromSuperview()
    }
    
}
extension UIImage {

    func alpha(_ value:CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        draw(at: CGPoint.zero, blendMode: .normal, alpha: value)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}
