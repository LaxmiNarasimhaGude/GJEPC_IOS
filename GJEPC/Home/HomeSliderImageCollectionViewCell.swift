//
//  HomeSliderImageCollectionViewCell.swift
//  GJEPC
//
//  Created by Kwebmaker on 15/09/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit

class HomeSliderImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var sliderImage: UIImageView!
    
}
