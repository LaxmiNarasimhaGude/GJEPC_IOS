//
//  VisitorRegistrationVC.swift
//  GJEPC
//
//  Created by Kwebmaker on 24/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
class VisitorRegistrationVC: UIViewController,UIWebViewDelegate {

    @IBOutlet weak var headingTitle: UILabel!
    @IBOutlet weak var webView: UIWebView!
     var headingtitle = String()
    var pdf = ""
     var activityLoader : MBProgressHUD? = nil
    var url:String!
    
    override func viewWillAppear(_ animated: Bool) {
//         webView.mediaPlaybackRequiresUserAction = true
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.delegate = self
//          MBProgressHUD.showAdded(to: self.view, animated: true)
        
        webView.allowsInlineMediaPlayback = true
        webView.mediaPlaybackRequiresUserAction = false
        
        
        DispatchQueue.main.async {
            self.headingTitle.text = self.headingtitle
        }
        
        if(headingtitle == "IIJS VIRTUAL VISITOR REGISTRATION"){
            let url = URL (string: "https://registration.gjepc.org/single_visitor.php")
            
            let requestObj = URLRequest(url: url!)
            webView!.loadRequest(requestObj)
        }
        else if(headingtitle == "EXHIBITOR REGISTRATION"){
          let url = URL (string: "https://registration.gjepc.org/login.php")
            
            let requestObj = URLRequest(url: url!)
            webView!.loadRequest(requestObj)
        }
        else if(headingtitle == "AATMAN JEWELLERY TREND BOOK 2020"){
          let url = URL (string: "https://gjepc.org/aatman-jewellery-trend-book-2020.php")
            
            let requestObj = URLRequest(url: url!)
            webView!.loadRequest(requestObj)
        }
            else if(headingtitle == "DESIGN INSPIRATION"){
              let url = URL (string: "https://gjepc.org/design-inspiration.php")
                
                let requestObj = URLRequest(url: url!)
                webView!.loadRequest(requestObj)
            }
            else if(headingtitle == "DESIGN WORKSHOP"){
              let url = URL (string: "https://gjepc.org/design-workshop.php")
                
                let requestObj = URLRequest(url: url!)
                webView!.loadRequest(requestObj)
            }
            else if(headingtitle == "ARTISAN AWARDS"){
                         let url = URL (string: "http://theartisanawards.com/")
                           
                           let requestObj = URLRequest(url: url!)
                           webView!.loadRequest(requestObj)
                       }
            else if(headingtitle == "VIRTUAL BSM"){
                                    let url = URL (string: "https://gjepc.org/vbsm/")
                                      
                                      let requestObj = URLRequest(url: url!)
                                      webView!.loadRequest(requestObj)
                                  }
            else if(headingtitle == "INTERNATIONAL GEM & JEWELLERY SHOW"){
              let url = URL (string: "https://intl.gjepc.org/jaipur")
                
                let requestObj = URLRequest(url: url!)
                webView!.loadRequest(requestObj)
            }
            else if(headingtitle == "INDIA PAVILIONS"){
                         let url = URL (string: "https://gjepc.org/india-pavilions.php")
                           
                           let requestObj = URLRequest(url: url!)
                           webView!.loadRequest(requestObj)
                       }
            else if(headingtitle == "BUYER-SELLER MEETS"){
              let url = URL (string: "https://gjepc.org/buyer-seller-meets.php")
                
                let requestObj = URLRequest(url: url!)
                webView!.loadRequest(requestObj)
            }
            else if(headingtitle == "MICRO MEMBERSHIP"){
                         let url = URL (string: "https://gjepc.org/registration.php")
                           
                           let requestObj = URLRequest(url: url!)
                           webView!.loadRequest(requestObj)
                       }
            else if(headingtitle == "CENTRAL GOVERNMENT SCHEME"){
                         let url = URL (string: "https://gjepc.org/pdf/MSME/Central-Government-Scheme.pdf")
                           
                           let requestObj = URLRequest(url: url!)
                           webView!.loadRequest(requestObj)
                       }
            else if(headingtitle == "STATE GOVERNMENT SCHEME"){
                         let url = URL (string: "https://gjepc.org/pdf/MSME/State-Government-Scheme.pdf")
                           
                           let requestObj = URLRequest(url: url!)
                           webView!.loadRequest(requestObj)
                       }
            else if(headingtitle == "UDYAM REGISTRATION"){
                         let url = URL (string: "https://gjepc.org/udhyam-registration.php")
                           
                           let requestObj = URLRequest(url: url!)
                           webView!.loadRequest(requestObj)
                       }
            else if(headingtitle == "MSME DEFINITION"){
                         let url = URL (string: "https://gjepc.org/msme-defination.php")
                           
                           let requestObj = URLRequest(url: url!)
                           webView!.loadRequest(requestObj)
                       }
            else if(headingtitle == "NEW MSME SCHEMES"){
                         let url = URL (string: "https://gjepc.org/new-msme-schemes.php")
                           
                           let requestObj = URLRequest(url: url!)
                           webView!.loadRequest(requestObj)
                       }
            else if(headingtitle == "EXISTING SCHEMES"){
                         let url = URL (string: "https://gjepc.org/existing-schemes.php")
                           
                           let requestObj = URLRequest(url: url!)
                           webView!.loadRequest(requestObj)
                       }
            else if(headingtitle == "SKILL DEVELOPMENT & TRAINING FOR ARTISANS"){
                         let url = URL (string: "https://nsdcindia.org/")
                           
                           let requestObj = URLRequest(url: url!)
                           webView!.loadRequest(requestObj)
                       }
            else if(headingtitle == "MSME DI CONTACTS"){
                         let url = URL (string: "https://gjepc.org/MSME-DI-contact-details.php")
                           
                           let requestObj = URLRequest(url: url!)
                           webView!.loadRequest(requestObj)
                       }
            else if(headingtitle == "NOTIFICATIONS/CIRCULARS"){
              let url = URL (string: "https://gjepc.org/msme-notification-circulars.php")
                
                let requestObj = URLRequest(url: url!)
                webView!.loadRequest(requestObj)
            }
            else if(headingtitle == "GEMS & JEWELLERY CONCLAVE"){
                         let url = URL (string: "https://gjepc.org/gems-and-jewellery-conclave.php")
                           
                           let requestObj = URLRequest(url: url!)
                           webView!.loadRequest(requestObj)
                       }
            else if(headingtitle == "INDIA GOLD AND JEWELLERY SUMMIT"){
                         let url = URL (string: "https://gjepc.org/india-gold-and-jewellery-summit.php")
                           
                           let requestObj = URLRequest(url: url!)
                           webView!.loadRequest(requestObj)
                       }
            else if(headingtitle == "INDIA GEM & JEWELLERY AWARDS"){
                         let url = URL (string: "https://gjepc.org/igja-awards.php")
                           
                           let requestObj = URLRequest(url: url!)
                           webView!.loadRequest(requestObj)
                       }
            
        else if(headingtitle == "Metal & Currency Rates"){
            let pdfBaseUrl = "https://gjepc.org/admin/GoldRate/"
            let pdfURL = pdfBaseUrl + pdf
            
            let url = URL (string: pdfURL)
                                      
                                      let requestObj = URLRequest(url: url!)
                                      webView!.loadRequest(requestObj)
        }
            else if(headingtitle == "WEBINARS"){
                       let pdfBaseUrl = "https://gjepc.org/gjepc-webinar.php"
                       let pdfURL = pdfBaseUrl + pdf
                       
                       let url = URL (string: pdfURL)
                                                 
                                                 let requestObj = URLRequest(url: url!)
                                                 webView!.loadRequest(requestObj)
                   }
            else if(headingtitle == "IIJS VIRTUAL") {
                       let url = URL (string: "https://gjepc.org/iijs-virtual/")
                       
                       let requestObj = URLRequest(url: url!)
                       webView!.loadRequest(requestObj)
                   }
            else if(headingtitle == "IIGJ MUMBAI") {
                let url = URL (string: "https://www.iigj.org/")
                
                let requestObj = URLRequest(url: url!)
                webView!.loadRequest(requestObj)
            }
            else if(headingtitle == "IIGJ DELHI") {
                let url = URL (string: "http://iigjdelhi.org/")
                
                let requestObj = URLRequest(url: url!)
                webView!.loadRequest(requestObj)
            }
            else if(headingtitle == "IIGJ VARANASI") {
                let url = URL (string: "http://iigj.org/varanasi/")
                
                let requestObj = URLRequest(url: url!)
                webView!.loadRequest(requestObj)
            }
            else if(headingtitle == "IIGJ UDUPI") {
                let url = URL (string: "https://iigj.org/udupi/")
                
                let requestObj = URLRequest(url: url!)
                webView!.loadRequest(requestObj)
            }
            else if(headingtitle == "IIGJ JAIPUR") {
                let url = URL (string: "http://www.iigjjaipur.com/")
                
                let requestObj = URLRequest(url: url!)
                webView!.loadRequest(requestObj)
            }
            else if(headingtitle == "GEMMOLIJICAL INSTITUTE OF INDIA") {
                let url = URL (string: "https://giionline.com/")
                
                let requestObj = URLRequest(url: url!)
                webView!.loadRequest(requestObj)
            }
            else if(headingtitle == "GEM TESTING LABORATORY") {
                let url = URL (string: "http://www.gtljaipur.info/")
                
                let requestObj = URLRequest(url: url!)
                webView!.loadRequest(requestObj)
            }
            else if(headingtitle == "IGI-GTL") {
                let url = URL (string: "http://www.igi-gtl.org/")
                
                let requestObj = URLRequest(url: url!)
                webView!.loadRequest(requestObj)
            }
            else if(headingtitle == "INDIAN DIAMOND INSTITUTE") {
                let url = URL (string: "http://www.diamondinstitute.net/the-institute.html")
                
                let requestObj = URLRequest(url: url!)
                webView!.loadRequest(requestObj)
            }
            else if(headingtitle == "RESEARCH AND STATISTICS") {
                let url = URL (string: "https://gjepc.org/statistics.php")
                
                let requestObj = URLRequest(url: url!)
                webView!.loadRequest(requestObj)
            }
           // "ARTISAN AWARDS 2021"
//            else if(headingtitle == "ARTISAN AWARDS 2021") {
//                           let url = URL (string: "http://theartisanawards.com/")
//                           
//                           let requestObj = URLRequest(url: url!)
//                           webView!.loadRequest(requestObj)
//                       }
        else{
            let url = URL (string: self.url)
            
            let requestObj = URLRequest(url: url!)
            webView!.loadRequest(requestObj)
        }
       loadActivityIndicator()
    }
    
//    func webViewDidFinishLoad(_ webView: UIWebView) {
//           MBProgressHUD.hide(for: self.view, animated: true)
//       }
    
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    func loadActivityIndicator() {
        activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
        activityLoader?.label.text = "Loading";
        activityLoader?.detailsLabel.text = "Please Wait";
        activityLoader?.isUserInteractionEnabled = false;
       
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
           activityLoader?.hide(animated: true)
              
           }
           func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
           {
               activityLoader?.hide(animated: true)

                print("called finish load................................\(error)")
           }
       func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
               return true
           }
}
