//
//  CircularMemberMapViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 17/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
class CircularMemberMapViewController: UIViewController,UIWebViewDelegate {

    @IBOutlet weak var webview: UIWebView!
    @IBOutlet weak var filetitle: UILabel!
    
     var pdfUrl1 : String = ""
     var activityLoader : MBProgressHUD? = nil
      var pagetitle = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0
        CheckNetwork()
        webview.delegate = self

    }
    func CheckNetwork(){
           if CheckInternet.isConnectedToNetwork() == true{
               getData()
                 webview.isHidden = false
           }else{
               webview.isHidden = true
           }
           
           
       }
    func getData(){
        let url1 : String = circularPdfUrl
        let url2 : String =  pdfUrl1
        
        filetitle.text = pagetitle
        let url3 = url1 + url2
        let replacedString = url3.replacingOccurrences(of: " ", with: "%20")
        webview.backgroundColor = UIColor.white
        let url = NSURL(string: replacedString)
        print(replacedString)
        DispatchQueue.main.async {
            self.loadActivityIndicator()
        }
        if  url != nil {
            webview.loadRequest(URLRequest(url: url! as URL) as URLRequest)
            webview.delegate = self
        }else{
            let alert = UIAlertController(title: "", message: "Pdf File Not Available", preferredStyle: UIAlertController.Style.alert)
            self.present(alert, animated: true, completion: nil)
            let when = DispatchTime.now() + 3
            DispatchQueue.main.asyncAfter(deadline: when){
                
                alert.dismiss(animated: true, completion: nil)
                self.navigationController?.popViewController(animated: true)
                self.activityLoader?.hide(animated: true)
            }
        }
    
    }
    func loadActivityIndicator()
       {
           
           activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
           
           activityLoader?.label.text = "Loading";
           activityLoader?.detailsLabel.text = "Please Wait!!!";
           activityLoader?.isUserInteractionEnabled = false;
       }
    func webViewDidFinishLoad(_ webView: UIWebView) {
           DispatchQueue.main.async {
               self.activityLoader?.hide(animated: true)

           }
       }
       func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
           print("URL NOT Fount")
       }
   
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
