//
//  AboutUsViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 04/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class AboutUsViewController: UIViewController {

   
    @IBOutlet weak var fosterDevelopmentLbel: NSLayoutConstraint!
    @IBOutlet weak var fosterLabel: UILabel!
    @IBOutlet weak var laedingLabel: UILabel!
    @IBOutlet weak var governmentLabel: UILabel!
    @IBOutlet weak var fosterLabel9: UILabel!
    @IBOutlet weak var fosterLabel8: UILabel!
    @IBOutlet weak var fosterLabel7: UILabel!
    @IBOutlet weak var fosterLabel6: UILabel!
    @IBOutlet weak var fosterLabel5: UILabel!
    @IBOutlet weak var fosterLabel4: UILabel!
    @IBOutlet weak var fosterLabel3: UILabel!
    @IBOutlet weak var fosterLabel2: UILabel!
    @IBOutlet weak var fosterLabel1: UILabel!
    @IBOutlet weak var governmentLabel4: UILabel!
    @IBOutlet weak var governmentLabel3: UILabel!
    @IBOutlet weak var governmentLabel2: UILabel!
    @IBOutlet weak var governmentLabel1: UILabel!
    @IBOutlet weak var promotionalLabel10: UILabel!
    @IBOutlet weak var promotionalLabel9: UILabel!
    @IBOutlet weak var promotionalLabel8: UILabel!
    @IBOutlet weak var promotionalLabel7: UILabel!
    @IBOutlet weak var promotionalLabel6: UILabel!
    @IBOutlet weak var promotionalLabel5: UILabel!
    @IBOutlet weak var promotionalLabel4: UILabel!
    @IBOutlet weak var promotionalLabel3: UILabel!
    @IBOutlet weak var promotionalLabel2: UILabel!
    @IBOutlet weak var promotionalLabel1: UILabel!
    @IBOutlet weak var leadingLabel3: UILabel!
    @IBOutlet weak var leadingLabel2: UILabel!
    @IBOutlet weak var leadingLabel1: UILabel!
    @IBOutlet weak var leadingLbl: UILabel!
    @IBOutlet weak var fosteringImage: UIImageView!
    @IBOutlet weak var governmentImage: UIImageView!
    @IBOutlet weak var promotionalImage: UIImageView!
    @IBOutlet weak var leadingimage: UIImageView!
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    @IBOutlet weak var fosterHeight: NSLayoutConstraint!
    @IBOutlet weak var govenmentHeight: NSLayoutConstraint!
    @IBOutlet weak var promotionHeight: NSLayoutConstraint!
    @IBOutlet weak var leadingHeight: NSLayoutConstraint!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var fosterView: UIView!
    @IBOutlet weak var leadingView: UIView!
    @IBOutlet weak var promotionView: UIView!
    @IBOutlet weak var governmentView: UIView!
    @IBOutlet weak var promotionLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
       setUpView()

    }
    
    func setUpView()  {
        fosterDevelopmentLbel.constant = 30
       
        governmentLabel.setCharacterSpacing(1)
         governmentLabel.setLineSpacing(lineSpacing: 5, lineHeightMultiple: 0)
        
        leadingLbl.setLineSpacing(lineSpacing: 5, lineHeightMultiple: 0)
        leadingLbl.setCharacterSpacing(1)
        
        promotionLabel.setLineSpacing(lineSpacing: 5, lineHeightMultiple: 0)
        promotionLabel.setCharacterSpacing(1)
        
        fosterLabel.setCharacterSpacing(1)
        fosterLabel.setLineSpacing(lineSpacing: 5, lineHeightMultiple: 0)

        
        
        
        
        topView.layer.shadowColor = UIColor.gray.cgColor
        topView.layer.shadowOpacity = 1
        topView.layer.shadowOffset = CGSize.zero
        topView.layer.shadowRadius = 5
        
        leadingHeight.constant = 0
        leadingView.isHidden = true
        promotionHeight.constant = 0
        promotionView.isHidden = true
        govenmentHeight.constant = 0
        governmentView.isHidden = true
        fosterHeight.constant = 0
        fosterView.isHidden = true
        
        viewHeight.constant = 600
        
    }
    
    @IBAction func leadingFromFront(_ sender: Any) {
        
        if leadingHeight.constant == 0
        {
            DispatchQueue.main.async {
                
                self.leadingLabel1.setCharacterSpacing(1)
                self.leadingLabel1.setLineSpacing(lineSpacing: 5, lineHeightMultiple: 0)
                self.leadingLabel2.setCharacterSpacing(1)
                self.leadingLabel2.setLineSpacing(lineSpacing: 5, lineHeightMultiple: 0)
                self.leadingLabel3.setCharacterSpacing(1)
                self.leadingLabel3.setLineSpacing(lineSpacing: 5, lineHeightMultiple: 0)
                
                self.promotionalImage.image = UIImage(named: "icons8-plus-24.png")
                self.governmentImage.image = UIImage(named: "icons8-plus-24.png")
                self.fosteringImage.image = UIImage(named: "icons8-plus-24.png")
                
                self.leadingView.isHidden = false
                self.leadingHeight.constant = 450
                self.promotionView.isHidden = true
                self.promotionHeight.constant = 0
                self.governmentView.isHidden = true
                self.govenmentHeight.constant = 0
                self.fosterView.isHidden = true
                self.fosterHeight.constant = 0
                
                self.viewHeight.constant = 900
                
                DispatchQueue.main.async {
                     UIView.animate(withDuration: 0.4, animations: {
           
                                       })
                    self.leadingimage.image = UIImage(named: "icons8-minus-24.png")
                }
                
            }
        }
        else{
            DispatchQueue.main.async {
                self.leadingHeight.constant = 0
                self.leadingView.isHidden = true
                self.leadingimage.image = UIImage(named: "icons8-plus-24.png")
            }
        }
        
//        DispatchQueue.main.async {
//                   UIView.animate(withDuration: 0.4, animations: {
//                       self.view.layoutIfNeeded()
//                   })
//               }
    }
    
    @IBAction func promotion(_ sender: Any) {
        
        if promotionHeight.constant == 0{
            DispatchQueue.main.async {
                
             self.promotionalLabel1.setCharacterSpacing(1)
             self.promotionalLabel1.setLineSpacing(lineSpacing: 5, lineHeightMultiple: 0)
                
            self.promotionalLabel2.setCharacterSpacing(1)
            self.promotionalLabel2.setLineSpacing(lineSpacing: 5, lineHeightMultiple: 0)
                
            self.promotionalLabel3.setCharacterSpacing(1)
            self.promotionalLabel3.setLineSpacing(lineSpacing: 5, lineHeightMultiple: 0)
                
            self.promotionalLabel4.setCharacterSpacing(1)
            self.promotionalLabel4.setLineSpacing(lineSpacing: 5, lineHeightMultiple: 0)
                
            self.promotionalLabel4.setCharacterSpacing(1)
            self.promotionalLabel4.setLineSpacing(lineSpacing: 5, lineHeightMultiple: 0)
                
            self.promotionalLabel5.setCharacterSpacing(1)
            self.promotionalLabel5.setLineSpacing(lineSpacing: 5, lineHeightMultiple: 0)
                
            self.promotionalLabel6.setCharacterSpacing(1)
            self.promotionalLabel6.setLineSpacing(lineSpacing: 5, lineHeightMultiple: 0)
                
                
            self.promotionalLabel7.setCharacterSpacing(1)
            self.promotionalLabel7.setLineSpacing(lineSpacing: 5, lineHeightMultiple: 0)
                
            self.promotionalLabel8.setCharacterSpacing(1)
            self.promotionalLabel8.setLineSpacing(lineSpacing: 5, lineHeightMultiple: 0)
                
            self.promotionalLabel9.setCharacterSpacing(1)
            self.promotionalLabel9.setLineSpacing(lineSpacing: 5, lineHeightMultiple: 0)
                
            self.promotionalLabel10.setCharacterSpacing(1)
            self.promotionalLabel10.setLineSpacing(lineSpacing: 5, lineHeightMultiple: 0)
                
                self.leadingimage.image = UIImage(named: "icons8-plus-24.png")
                self.governmentImage.image = UIImage(named: "icons8-plus-24.png")
                self.fosteringImage.image = UIImage(named: "icons8-plus-24.png")
                
                 self.leadingView.isHidden = true
                 self.leadingHeight.constant = 0
                 self.promotionView.isHidden = false
                 self.promotionHeight.constant = 1360
                 self.governmentView.isHidden = true
                 self.govenmentHeight.constant = 0
                 self.fosterView.isHidden = true
                 self.fosterHeight.constant = 0
                
                self.viewHeight.constant = 1750
                 
                 DispatchQueue.main.async {
                      UIView.animate(withDuration: 0.4, animations: {
            
                                        })
                     self.promotionalImage.image = UIImage(named: "icons8-minus-24.png")
                 }
                 
             }
        }
        else{
           DispatchQueue.main.async {
            self.viewHeight.constant = 600
                self.promotionHeight.constant = 0
                self.promotionView.isHidden = true
            self.promotionalImage.image = UIImage(named: "icons8-plus-24.png")
            }
        }
        
    }
    @IBAction func governmentAction(_ sender: Any) {
        
        if govenmentHeight.constant == 0{
            DispatchQueue.main.async {
                 
        self.governmentLabel1.setCharacterSpacing(1)
        self.governmentLabel1.setLineSpacing(lineSpacing: 5, lineHeightMultiple: 0)
       
                self.governmentLabel2.setCharacterSpacing(1)
                self.governmentLabel2.setLineSpacing(lineSpacing: 5, lineHeightMultiple: 0)
                
                self.governmentLabel3.setCharacterSpacing(1)
                self.governmentLabel3.setLineSpacing(lineSpacing: 5, lineHeightMultiple: 0)
                
                self.governmentLabel4.setCharacterSpacing(1)
                self.governmentLabel4.setLineSpacing(lineSpacing: 5, lineHeightMultiple: 0)
                
                
                self.promotionalImage.image = UIImage(named: "icons8-plus-24.png")
                self.leadingimage.image = UIImage(named: "icons8-plus-24.png")
                self.fosteringImage.image = UIImage(named: "icons8-plus-24.png")
                
                
                
                 self.leadingView.isHidden = true
                 self.leadingHeight.constant = 0
                 self.promotionView.isHidden = true
                 self.promotionHeight.constant = 0
                self.fosterView.isHidden = true
                self.fosterHeight.constant = 0
                self.governmentView.isHidden = false
                self.govenmentHeight.constant = 370
                
                self.viewHeight.constant = 760
                 
                 DispatchQueue.main.async {
                      UIView.animate(withDuration: 0.4, animations: {
            
                                        })
                    self.governmentImage.image = UIImage(named: "icons8-minus-24.png")
                 }
                 
             }
        }
        else{
            DispatchQueue.main.async {
                           self.govenmentHeight.constant = 0
                           self.governmentView.isHidden = true
                self.governmentImage.image = UIImage(named: "icons8-plus-24.png")

                       }
        }
    }
    @IBAction func fosterIndustry(_ sender: Any) {
        if fosterHeight.constant == 0{
               DispatchQueue.main.async {
                
                self.fosterLabel1.setCharacterSpacing(1)
                self.fosterLabel1.setLineSpacing(lineSpacing: 5, lineHeightMultiple: 0)
                
                self.fosterLabel2.setCharacterSpacing(1)
                self.fosterLabel2.setLineSpacing(lineSpacing: 5, lineHeightMultiple: 0)
                
                self.fosterLabel3.setCharacterSpacing(1)
                self.fosterLabel3.setLineSpacing(lineSpacing: 5, lineHeightMultiple: 0)

                
                self.fosterLabel4.setCharacterSpacing(1)
                self.fosterLabel4.setLineSpacing(lineSpacing: 5, lineHeightMultiple: 0)

                
                self.fosterLabel5.setCharacterSpacing(1)
                self.fosterLabel5.setLineSpacing(lineSpacing: 5, lineHeightMultiple: 0)

                
                self.fosterLabel6.setCharacterSpacing(1)
                self.fosterLabel6.setLineSpacing(lineSpacing: 5, lineHeightMultiple: 0)

                
                self.fosterLabel7.setCharacterSpacing(1)
                self.fosterLabel7.setLineSpacing(lineSpacing: 5, lineHeightMultiple: 0)
                
                self.fosterLabel8.setCharacterSpacing(1)
                self.fosterLabel8.setLineSpacing(lineSpacing: 5, lineHeightMultiple: 0)

                
                self.fosterLabel9.setCharacterSpacing(1)
                self.fosterLabel9.setLineSpacing(lineSpacing: 5, lineHeightMultiple: 0)



                
                
                
                
                self.promotionalImage.image = UIImage(named: "icons8-plus-24.png")
                self.governmentImage.image = UIImage(named: "icons8-plus-24.png")
                self.leadingimage.image = UIImage(named: "icons8-plus-24.png")
                    
                    self.leadingView.isHidden = true
                    self.leadingHeight.constant = 0
                    self.promotionView.isHidden = true
                    self.promotionHeight.constant = 0
                    self.governmentView.isHidden = true
                    self.govenmentHeight.constant = 0
                    self.fosterView.isHidden = false
                    self.fosterHeight.constant = 1000
                    
                    self.viewHeight.constant = 1520
                
                    DispatchQueue.main.async {
                         UIView.animate(withDuration: 0.4, animations: {
               
                                           })
                        self.fosteringImage.image = UIImage(named: "icons8-minus-24.png")
                    }
                    
                }
           }
           else{
               DispatchQueue.main.async {
                              self.viewHeight.constant = 600
                              self.fosterHeight.constant = 0
                              self.fosterView.isHidden = true
                self.fosteringImage.image = UIImage(named: "icons8-plus-24.png")

                          }
           }
        
    }
//       func setCharacterSpacing(_ spacing: CGFloat){
//          let attributedStr = NSMutableAttributedString(string: self.text ?? "")
//          attributedStr.addAttribute(NSAttributedString.Key.kern, value: spacing, range: NSMakeRange(0, attributedStr.length))
//          self.attributedText = attributedStr
//       }
    
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
   
   
}

//extension UILabel{
//func CharacterSpacing(_ spacing: CGFloat){
//    let attributedStr = NSMutableAttributedString(string: self.text ?? "")
//    attributedStr.addAttribute(NSAttributedString.Key.kern, value: spacing, range: NSMakeRange(0, attributedStr.length))
//    self.attributedText = attributedStr
// }
//}
