//
//  coaViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 10/09/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit

class coaViewController: UIViewController {

    @IBOutlet weak var coaScrollView: UIScrollView!
    @IBOutlet weak var coaImageView: UIView!
    @IBOutlet weak var silverImage: UIImageView!
    @IBOutlet weak var salesImage: UIImageView!
    @IBOutlet weak var goldImage: UIImageView!
    @IBOutlet weak var diamondImage: UIImageView!
    @IBOutlet weak var colouredImage: UIImageView!
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    @IBOutlet weak var silverMainView: UIView!
    @IBOutlet weak var salesMainView: UIView!
    @IBOutlet weak var goldMainView: UIView!
    @IBOutlet weak var diamondMainView: UIView!
    @IBOutlet weak var colouredMainView: UIView!
    @IBOutlet weak var colouredHeight: NSLayoutConstraint!
   
    @IBOutlet weak var diamondHeight: NSLayoutConstraint!
    @IBOutlet weak var goldHeight: NSLayoutConstraint!
    @IBOutlet weak var salesHeight: NSLayoutConstraint!
    @IBOutlet weak var silverHeight: NSLayoutConstraint!
    @IBOutlet weak var silverView: UIView!
    @IBOutlet weak var salesView: UIView!
    @IBOutlet weak var goldView: UIView!
    @IBOutlet weak var diamondView: UIView!
    @IBOutlet weak var colouredView: UIView!
    @IBOutlet weak var chairman: UIImageView!
    @IBOutlet weak var viceChairman: UIImageView!
    @IBOutlet weak var vijayKedia: UIImageView!
    
    @IBOutlet weak var rambabu: UIImageView!
    @IBOutlet weak var badrinarayan: UIImageView!
    @IBOutlet weak var shailesh: UIImageView!
    @IBOutlet weak var suvankar: UIImageView!
    @IBOutlet weak var mansukh: UIImageView!
    @IBOutlet weak var ksrinivasan: UIImageView!
    @IBOutlet weak var sanjuKotari: UIImageView!
    @IBOutlet weak var russelMehta: UIImageView!
    @IBOutlet weak var milanChoksi: UIImageView!
    @IBOutlet weak var manishJivani: UIImageView!
    @IBOutlet weak var sanjayShah: UIImageView!
    @IBOutlet weak var dilipShah: UIImageView!
    @IBOutlet weak var mahenderAgarwal: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

             coaImageView.backgroundColor = UIColor.white
             coaImageView.layer.cornerRadius = 3.0
             coaImageView.layer.masksToBounds = false
             coaImageView.layer.shadowOffset = CGSize(width: 0, height: 0)
             coaImageView.layer.shadowOpacity = 0.3
        
        self.silverImage.image = UIImage(named: "icons8-plus-24")
        self.colouredImage.image = UIImage(named: "icons8-minus-24")
        self.salesImage.image = UIImage(named: "icons8-plus-24")
        self.goldImage.image = UIImage(named: "icons8-plus-24")
        self.diamondImage.image = UIImage(named: "icons8-plus-24")
        
        
        viewHeight.constant = 1150
        
        
        silverMainView.layer.cornerRadius = 5.0
        goldMainView.layer.cornerRadius = 5.0
        salesMainView.layer.cornerRadius = 5.0
        diamondMainView.layer.cornerRadius = 5.0
        colouredMainView.layer.cornerRadius = 5.0
        
        
        
        
        imageRoundRect(imageRect: chairman, type: "Non")
        imageRoundRect(imageRect: viceChairman, type: "Non")
        imageRoundRect(imageRect: rambabu, type: "REGIONAL")
        imageRoundRect(imageRect: badrinarayan, type: "REGIONAL")
        imageRoundRect(imageRect: suvankar, type: "REGIONAL")
        imageRoundRect(imageRect: shailesh, type: "REGIONAL")
        imageRoundRect(imageRect: ksrinivasan, type: "REGIONAL")
        imageRoundRect(imageRect: mansukh, type: "REGIONAL")
        imageRoundRect(imageRect: russelMehta, type: "REGIONAL")
        imageRoundRect(imageRect: sanjuKotari, type: "REGIONAL")
        imageRoundRect(imageRect: manishJivani, type: "REGIONAL")
        imageRoundRect(imageRect: milanChoksi, type: "REGIONAL")
        imageRoundRect(imageRect: dilipShah, type: "REGIONAL")
        imageRoundRect(imageRect: sanjayShah, type: "REGIONAL")
        imageRoundRect(imageRect: vijayKedia, type: "REGIONAL")
         imageRoundRect(imageRect: mahenderAgarwal, type: "REGIONAL")
        
        
        
        colouredHeight.constant = 200
        diamondHeight.constant = 0
        goldHeight.constant = 0
        salesHeight.constant = 0
        silverHeight.constant = 0
        
        colouredView.isHidden = false
        diamondView.isHidden = true
        goldView.isHidden = true
        salesView.isHidden = true
        silverView.isHidden = true
        
       
    }
    func imageRoundRect(imageRect: UIImageView,type: String)  {
        imageRect.clipsToBounds = true
        imageRect.layer.cornerRadius = 10
        if type == "REGIONAL"{
            imageRect.layer.borderColor = UIColor.black.cgColor
        }
        else{
            imageRect.layer.borderColor = UIColor(red: 168/256, green: 156/256, blue: 93/256, alpha: 1.0).cgColor

        }
        imageRect.layer.borderWidth = 1
       }

    @IBAction func coloured(_ sender: Any) {
                
                if colouredHeight.constant == 0{
                    viewHeight.constant = 1150
                     self.colouredImage.image = UIImage(named: "icons8-minus-24")
                    self.colouredHeight.constant = 200
                    
                    self.colouredView.isHidden = false
                       diamondHeight.constant = 0
                           goldHeight.constant = 0
                           salesHeight.constant = 0
                           silverHeight.constant = 0

                           diamondView.isHidden = true
                           goldView.isHidden = true
                           salesView.isHidden = true
                           silverView.isHidden = true
                    
                    self.goldImage.image = UIImage(named: "icons8-plus-24")
                    self.salesImage.image = UIImage(named: "icons8-plus-24")
                    self.silverImage.image = UIImage(named: "icons8-plus-24")
                    self.diamondImage.image = UIImage(named: "icons8-plus-24")
                }
                else{
                                        
                                        DispatchQueue.main.async {
                                            self.viewHeight.constant = 950
                            self.colouredHeight.constant = 0
                            self.colouredImage.image = UIImage(named: "icons8-plus-24")
                            self.colouredView.isHidden = true
                
                                        }
                                        
                                    }
                        DispatchQueue.main.async {
                                       UIView.animate(withDuration: 0.4, animations: {
                                           self.view.layoutIfNeeded()
                                       })
                                   }
                
            }
    
    @IBAction func diamond(_ sender: Any) {
                
                if diamondHeight.constant == 0{
                    coaScrollView.setContentOffset(CGPoint(x: 0, y: 600), animated: true)

                    viewHeight.constant = 1550
                     self.diamondImage.image = UIImage(named: "icons8-minus-24")
                    self.diamondHeight.constant = 620
                    
                    self.diamondView.isHidden = false
        //            self.internationalTradeshowView.isHidden = false
                    
                    colouredHeight.constant = 0
                    goldHeight.constant = 0
                    salesHeight.constant = 0
                    silverHeight.constant = 0

                    colouredView.isHidden = true
                    goldView.isHidden = true
                    salesView.isHidden = true
                    silverView.isHidden = true
                    
                    self.goldImage.image = UIImage(named: "icons8-plus-24")
                    self.salesImage.image = UIImage(named: "icons8-plus-24")
                    self.silverImage.image = UIImage(named: "icons8-plus-24")
                    self.colouredImage.image = UIImage(named: "icons8-plus-24")
                   
                }
                else{
                                        
                                        DispatchQueue.main.async {
                                            self.viewHeight.constant = 950
                            self.diamondHeight.constant = 0
                            self.diamondImage.image = UIImage(named: "icons8-plus-24")
                            self.diamondView.isHidden = true
                
                                        }
                                        
                                    }
                        DispatchQueue.main.async {
                                       UIView.animate(withDuration: 0.4, animations: {
                                           self.view.layoutIfNeeded()
                                       })
                                   }
                
            }
    
    @IBAction func gold(_ sender: Any) {
            
            if goldHeight.constant == 0{
                viewHeight.constant = 1350
                coaScrollView.setContentOffset(CGPoint(x: 0, y: 600), animated: true)
                 self.goldImage.image = UIImage(named: "icons8-minus-24")
                self.goldHeight.constant = 420
                
                self.goldView.isHidden = false
    //            self.internationalTradeshowView.isHidden = false
                
                colouredHeight.constant = 0
                diamondHeight.constant = 0
                salesHeight.constant = 0
                silverHeight.constant = 0

                colouredView.isHidden = true
                diamondView.isHidden = true
                salesView.isHidden = true
                silverView.isHidden = true
                
                self.diamondImage.image = UIImage(named: "icons8-plus-24")
                self.salesImage.image = UIImage(named: "icons8-plus-24")
                self.silverImage.image = UIImage(named: "icons8-plus-24")
                self.colouredImage.image = UIImage(named: "icons8-plus-24")
               
            }
            else{
                                    
                                    DispatchQueue.main.async {
                                        self.viewHeight.constant = 950
                        self.goldHeight.constant = 0
                        self.goldImage.image = UIImage(named: "icons8-plus-24")
                        self.goldView.isHidden = true
            
                                    }
                                    
                                }
                    DispatchQueue.main.async {
                                   UIView.animate(withDuration: 0.4, animations: {
                                       self.view.layoutIfNeeded()
                                   })
                               }
            
        }
    
    @IBAction func sales(_ sender: Any) {
            
            if salesHeight.constant == 0{
                viewHeight.constant = 1150
                coaScrollView.setContentOffset(CGPoint(x: 0, y: 600), animated: true)
                 self.salesImage.image = UIImage(named: "icons8-minus-24")
                self.salesHeight.constant = 200
                
                self.salesView.isHidden = false
    //            self.internationalTradeshowView.isHidden = false
                
                colouredHeight.constant = 0
                goldHeight.constant = 0
                diamondHeight.constant = 0
                silverHeight.constant = 0

                colouredView.isHidden = true
                goldView.isHidden = true
                diamondView.isHidden = true
                silverView.isHidden = true
                
                self.diamondImage.image = UIImage(named: "icons8-plus-24")
                self.goldImage.image = UIImage(named: "icons8-plus-24")
                self.silverImage.image = UIImage(named: "icons8-plus-24")
                self.colouredImage.image = UIImage(named: "icons8-plus-24")
                
                
                
               
            }
            else{
                                    
                                    DispatchQueue.main.async {
                                        self.viewHeight.constant = 950
                        self.salesHeight.constant = 0
                        self.salesImage.image = UIImage(named: "icons8-plus-24")
                        self.salesView.isHidden = true
            
                                    }
                                    
                                }
                    DispatchQueue.main.async {
                                   UIView.animate(withDuration: 0.4, animations: {
                                       self.view.layoutIfNeeded()
                                   })
                               }
            
        }
    
    @IBAction func silver(_ sender: Any) {
            
            if silverHeight.constant == 0{
                viewHeight.constant = 1150
                coaScrollView.setContentOffset(CGPoint(x: 0, y: 600), animated: true)
                 self.silverImage.image = UIImage(named: "icons8-minus-24")
                self.silverHeight.constant = 200
                
                self.silverView.isHidden = false
    //            self.internationalTradeshowView.isHidden = false
                
                colouredHeight.constant = 0
                goldHeight.constant = 0
                diamondHeight.constant = 0
                salesHeight.constant = 0

                colouredView.isHidden = true
                goldView.isHidden = true
                diamondView.isHidden = true
                salesView.isHidden = true
                
                self.diamondImage.image = UIImage(named: "icons8-plus-24")
                self.salesImage.image = UIImage(named: "icons8-plus-24")
                self.goldImage.image = UIImage(named: "icons8-plus-24")
                self.colouredImage.image = UIImage(named: "icons8-plus-24")
               
            }
            else{
                                    
                                    DispatchQueue.main.async {
                                        self.viewHeight.constant = 950
                        self.silverHeight.constant = 0
                        self.silverImage.image = UIImage(named: "icons8-plus-24")
                        self.silverView.isHidden = true
            
                                    }
                                    
                                }
                    DispatchQueue.main.async {
                                   UIView.animate(withDuration: 0.4, animations: {
                                       self.view.layoutIfNeeded()
                                   })
                               }
            
        }
    
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}
