//
//  VissionAndMissionViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 09/09/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit

class VissionAndMissionViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        spacing(string: ourMissionLabel.text!, label: ourMissionLabel)
        spacing(string: ourvisionLabel.text!, label: ourvisionLabel)
    }
    @IBOutlet weak var ourMissionLabel: UILabel!
    @IBOutlet weak var ourvisionLabel: UILabel!
    
    @IBAction func back(_ sender: Any) {
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
   func spacing(string:String,label:UILabel)  {
       label.setCharacterSpacing(1)
       let attrString = NSMutableAttributedString(string: string)
                   var style = NSMutableParagraphStyle()
                   style.lineSpacing = 5
       attrString.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: NSRange(location: 0, length:label.text!.count))
                   label.attributedText = attrString
   
   }


}
