//
//  OurStructureViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 09/09/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit

class OurStructureViewController: UIViewController {

    
    @IBOutlet weak var payingLabel2: UILabel!
    @IBOutlet weak var payingLabel1: UILabel!
    @IBOutlet weak var healthLabel1: UILabel!
    @IBOutlet weak var innovationLabel2: UILabel!
    @IBOutlet weak var innovationLabel1: UILabel!
    @IBOutlet weak var speakingLabel: UILabel!
    @IBOutlet weak var upholdingLabel: UILabel!
    @IBOutlet weak var connectingGovnLabel4: UILabel!
    @IBOutlet weak var connectingGovnLabel3: UILabel!
    @IBOutlet weak var connectingGovnLabel2: UILabel!
    @IBOutlet weak var connectingGovnLabel1: UILabel!
    @IBOutlet weak var organisationHeadingLabelLower: UILabel!
    @IBOutlet weak var organisationHeadingLbl: UILabel!
    @IBOutlet weak var ourStructureScrollView: UIScrollView!
    @IBOutlet weak var organisationImageView: UIView!
    @IBOutlet weak var payingBackToSocietyView: UIView!
    @IBOutlet weak var healthAndWellView: UIView!
    @IBOutlet weak var innovationAndInfrastructureView: UIView!
    @IBOutlet weak var spreadingEducationView: UIView!
    @IBOutlet weak var upholdingDiamondView: UIView!
    @IBOutlet weak var connectingView: UIView!
    @IBOutlet weak var promotingView: UIView!
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    @IBOutlet weak var payingBackImage: UIImageView!
    @IBOutlet weak var healthImage: UIImageView!
    @IBOutlet weak var innovationImage: UIImageView!
    @IBOutlet weak var spreadingImage: UIImageView!
    @IBOutlet weak var upholdingImage: UIImageView!
    @IBOutlet weak var connectingGovnImage: UIImageView!
    @IBOutlet weak var promotingImage: UIImageView!
    @IBOutlet weak var payingbackHeight: NSLayoutConstraint!
    @IBOutlet weak var payingBackView: UIView!
    @IBOutlet weak var healthHeight: NSLayoutConstraint!
    @IBOutlet weak var healthView: UIView!
    @IBOutlet weak var innovationHeight: NSLayoutConstraint!
    @IBOutlet weak var innovationView: UIView!
    @IBOutlet weak var spreadingHeight: NSLayoutConstraint!
    @IBOutlet weak var spreadingView: UIView!
    @IBOutlet weak var upholdingHeight: NSLayoutConstraint!
    @IBOutlet weak var upholdingView: UIView!
    @IBOutlet weak var connectingGovnHeight: NSLayoutConstraint!
    @IBOutlet weak var connectingGovnView: UIView!
    @IBOutlet weak var promotingBrandIndiaLabel5: UILabel!
    @IBOutlet weak var promotingBrandIndiaLabel4: UILabel!
    @IBOutlet weak var promotingBrandIndiaLabel3: UILabel!
    @IBOutlet weak var promotingBrandIndiaLabel2: UILabel!
    @IBOutlet weak var promotingBrandIndiaLabel1: UILabel!
    @IBOutlet weak var organisationHeadingLabel2: UILabel!
    @IBOutlet weak var organisationHeadingLabel1: UILabel!
    @IBOutlet weak var promotingBrandIndiaHeight: NSLayoutConstraint!
    @IBOutlet weak var promotingBrandIndiaSubview: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
//        organisationHeadingLbl.text = "The GJEPC is a apex body driving India's export - led growth in the gem and jewellery sector,since 1966"
//        organisationHeadingLbl.labelSpacing(label: organisationHeadingLbl, font: "Bold")
//        organisationHeadingLabelLower.text = "Headquartered in Mumbai with regional offices across the country, the Council has over 7,000 members in its fold."
//         organisationHeadingLbl.labelSpacing(label: organisationHeadingLabelLower, font: "Bold")
        
        
        
        organisationImageView.backgroundColor = UIColor.white
        organisationImageView.layer.cornerRadius = 3.0
        organisationImageView.layer.masksToBounds = false
        organisationImageView.layer.shadowOffset = CGSize(width: 0, height: 0)
        organisationImageView.layer.shadowOpacity = 0.3
        
        self.promotingImage.image = UIImage(named: "icons8-minus-24")
        self.connectingGovnImage.image = UIImage(named: "icons8-plus-24")
        self.upholdingImage.image = UIImage(named: "icons8-plus-24")
        self.spreadingImage.image = UIImage(named: "icons8-plus-24")
        self.innovationImage.image = UIImage(named: "icons8-plus-24")
        self.healthImage.image = UIImage(named: "icons8-plus-24")
        self.payingBackImage.image = UIImage(named: "icons8-plus-24")
//        self.promotingImage.image = UIImage(named: "icons8-plus-24")
        
        
        promotingView.layer.cornerRadius = 5.0
        connectingView.layer.cornerRadius = 5.0
        upholdingDiamondView.layer.cornerRadius = 5.0
        spreadingEducationView.layer.cornerRadius = 5.0
        innovationAndInfrastructureView.layer.cornerRadius = 5.0
        healthAndWellView.layer.cornerRadius = 5.0
        payingBackToSocietyView.layer.cornerRadius = 5.0
//          promotingView.layer.shadowColor = UIColor.darkGray.cgColor
//          promotingView.layer.shadowOffset = CGSize(width: 5.0, height: 5.0)
//          promotingView.layer.shadowRadius = 25.0
//        promotingView.layer.shadowOpacity = 0.5
        
        
        

         spacing(string: promotingBrandIndiaLabel1.text!, label: promotingBrandIndiaLabel1)
        spacing(string: promotingBrandIndiaLabel2.text!, label: promotingBrandIndiaLabel2)
        spacing(string: promotingBrandIndiaLabel3.text!, label: promotingBrandIndiaLabel3)
        spacing(string: promotingBrandIndiaLabel4.text!, label: promotingBrandIndiaLabel4)
        spacing(string: promotingBrandIndiaLabel5.text!, label: promotingBrandIndiaLabel5)
        spacing(string: organisationHeadingLabelLower.text!, label: organisationHeadingLabelLower)
        spacing(string: connectingGovnLabel1.text!, label: connectingGovnLabel1)
        spacing(string: connectingGovnLabel2.text!, label: connectingGovnLabel2)
        spacing(string: connectingGovnLabel3.text!, label: connectingGovnLabel3)
        spacing(string: connectingGovnLabel4.text!, label: connectingGovnLabel4)
        spacing(string: upholdingLabel.text!, label: upholdingLabel)
        spacing(string: speakingLabel.text!, label: speakingLabel)
        spacing(string: innovationLabel1.text!, label: innovationLabel1)
        spacing(string: innovationLabel2.text!, label: innovationLabel2)
         spacing(string: healthLabel1.text!, label: healthLabel1)
        spacing(string: payingLabel1.text!, label: payingLabel1)
        spacing(string: payingLabel2.text!, label: payingLabel2)
        
        
        
        
        
        
//        spacing(string: organisationHeadingLbl.text!, label: organisationHeadingLbl)

        
        viewHeight.constant = 1350
        promotingBrandIndiaHeight.constant = 500
        promotingBrandIndiaSubview.isHidden = false
        connectingGovnHeight.constant = 0
        connectingGovnView.isHidden = true
        upholdingHeight.constant = 0
        upholdingView.isHidden = true
        spreadingHeight.constant = 0
        spreadingView.isHidden = true
        innovationHeight.constant = 0
        innovationView.isHidden = true
        healthHeight.constant = 0
        healthView.isHidden = true
        payingbackHeight.constant = 0
        payingBackView.isHidden = true
    }
    func spacing(string:String,label:UILabel)  {
//        organisationHeadingLabel1.setCharacterSpacing(1)
        let attrString = NSMutableAttributedString(string: string)
                    var style = NSMutableParagraphStyle()
                    style.lineSpacing = 5
        attrString.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: NSRange(location: 0, length:label.text!.count))
                    label.attributedText = attrString
    
    }
    @IBAction func promotingBrandIndia(_ sender: Any) {
        
        if promotingBrandIndiaHeight.constant == 0{
            ourStructureScrollView.setContentOffset(CGPoint(x: 0, y: 400), animated: true)
             self.promotingImage.image = UIImage(named: "icons8-minus-24")
            self.promotingBrandIndiaHeight.constant = 500
            
            self.promotingBrandIndiaSubview.isHidden = false
//            self.internationalTradeshowView.isHidden = false
            
            viewHeight.constant = 1350
            connectingGovnHeight.constant = 0
            upholdingHeight.constant = 0
            spreadingHeight.constant = 0
            innovationHeight.constant = 0
            healthHeight.constant = 0
            payingbackHeight.constant = 0
            connectingGovnView.isHidden = true
            upholdingView.isHidden = true
            spreadingView.isHidden = true
            innovationView.isHidden = true
            healthView.isHidden = true
            payingBackView.isHidden = true
        }
        else{
                                
                                DispatchQueue.main.async {
                    self.viewHeight.constant = 850
                    self.promotingImage.image = UIImage(named: "icons8-plus-24")
                     self.promotingBrandIndiaHeight.constant = 0
                    self.promotingBrandIndiaSubview.isHidden = true
        
                                    
                                    

                                }
                                
                            }
                DispatchQueue.main.async {
                               UIView.animate(withDuration: 0.4, animations: {
                                   self.view.layoutIfNeeded()
                               })
                           }
        
    }
    
    @IBAction func connectingGovn(_ sender: Any) {
            
            if connectingGovnHeight.constant == 0{
                ourStructureScrollView.setContentOffset(CGPoint(x: 0, y: 400), animated: true)
                viewHeight.constant = 1200
                self.connectingGovnImage.image = UIImage(named: "icons8-minus-24")
                self.promotingImage.image = UIImage(named: "icons8-plus-24")
                self.upholdingImage.image = UIImage(named: "icons8-plus-24")
                self.spreadingImage.image = UIImage(named: "icons8-plus-24")
                self.innovationImage.image = UIImage(named: "icons8-plus-24")
                self.healthImage.image = UIImage(named: "icons8-plus-24")
                self.payingBackImage.image = UIImage(named: "icons8-plus-24")
                self.connectingGovnHeight.constant = 500
                self.connectingGovnView.isHidden = false
                promotingBrandIndiaHeight.constant = 0
                upholdingHeight.constant = 0
                spreadingHeight.constant = 0
                innovationHeight.constant = 0
                healthHeight.constant = 0
                payingbackHeight.constant = 0
                promotingBrandIndiaSubview.isHidden = true
                upholdingView.isHidden = true
                spreadingView.isHidden = true
                innovationView.isHidden = true
                healthView.isHidden = true
                payingBackView.isHidden = true
                
            }
            else{
                                    
                                    DispatchQueue.main.async {
                        self.connectingGovnImage.image = UIImage(named: "icons8-plus-24")
                        self.viewHeight.constant = 850
                        self.connectingGovnHeight.constant = 0
                        self.connectingGovnView.isHidden = true
                                    }
                                    
                                }
                    DispatchQueue.main.async {
                                   UIView.animate(withDuration: 0.4, animations: {
                                       self.view.layoutIfNeeded()
                                   })
                               }
            
        }
    
    @IBAction func upholding(_ sender: Any) {
             
             if upholdingHeight.constant == 0{
                ourStructureScrollView.setContentOffset(CGPoint(x: 0, y: 400), animated: true)
     self.upholdingImage.image = UIImage(named: "icons8-minus-24")
    self.promotingImage.image = UIImage(named: "icons8-plus-24")
                               self.connectingGovnImage.image = UIImage(named: "icons8-plus-24")
                               self.spreadingImage.image = UIImage(named: "icons8-plus-24")
                               self.innovationImage.image = UIImage(named: "icons8-plus-24")
                               self.healthImage.image = UIImage(named: "icons8-plus-24")
                               self.payingBackImage.image = UIImage(named: "icons8-plus-24")
                self.upholdingHeight.constant = 230
                  viewHeight.constant = 1100
                 self.upholdingView.isHidden = false
                
                
                      promotingBrandIndiaHeight.constant = 0
                               connectingGovnHeight.constant = 0
                               spreadingHeight.constant = 0
                               innovationHeight.constant = 0
                               healthHeight.constant = 0
                               payingbackHeight.constant = 0
                connectingGovnView.isHidden = true
                promotingBrandIndiaSubview.isHidden = true
                spreadingView.isHidden = true
                innovationView.isHidden = true
                healthView.isHidden = true
                payingBackView.isHidden = true
     
             }
             else{
                                     
                                     DispatchQueue.main.async {
    self.upholdingImage.image = UIImage(named: "icons8-plus-24")
    self.viewHeight.constant = 850
    self.upholdingHeight.constant = 0
    self.upholdingView.isHidden = true
                    
                                     }
                                     
                                 }
                     DispatchQueue.main.async {
                                    UIView.animate(withDuration: 0.4, animations: {
                                        self.view.layoutIfNeeded()
                                    })
                                }
             
         }
    
    @IBAction func spreadingEducation(_ sender: Any) {
             
             if spreadingHeight.constant == 0{
                ourStructureScrollView.setContentOffset(CGPoint(x: 0, y: 400), animated: true)
                self.promotingImage.image = UIImage(named: "icons8-plus-24")
                               self.upholdingImage.image = UIImage(named: "icons8-plus-24")
                               self.connectingGovnImage.image = UIImage(named: "icons8-plus-24")
                               self.innovationImage.image = UIImage(named: "icons8-plus-24")
                               self.healthImage.image = UIImage(named: "icons8-plus-24")
                               self.payingBackImage.image = UIImage(named: "icons8-plus-24")
     self.spreadingImage.image = UIImage(named: "icons8-minus-24")
                 self.spreadingHeight.constant = 250
                 viewHeight.constant = 1100
                 self.spreadingView.isHidden = false
                
                
                promotingBrandIndiaHeight.constant = 0
                connectingGovnHeight.constant = 0
                upholdingHeight.constant = 0
                innovationHeight.constant = 0
                healthHeight.constant = 0
                payingbackHeight.constant = 0
                
                connectingGovnView.isHidden = true
                upholdingView.isHidden = true
                promotingBrandIndiaSubview.isHidden = true
                innovationView.isHidden = true
                healthView.isHidden = true
                payingBackView.isHidden = true
     
             }
             else{
                                     
                                     DispatchQueue.main.async {
    self.spreadingImage.image = UIImage(named: "icons8-plus-24")
                            self.viewHeight.constant = 850
                            self.spreadingHeight.constant = 0
                                           
                            self.spreadingView.isHidden = true
             
                                         
                                         

                                     }
                                     
                                 }
                     DispatchQueue.main.async {
                                    UIView.animate(withDuration: 0.4, animations: {
                                        self.view.layoutIfNeeded()
                                    })
                                }
             
         }
    
    @IBAction func innovation(_ sender: Any) {
             
             if innovationHeight.constant == 0{
                ourStructureScrollView.setContentOffset(CGPoint(x: 0, y: 570), animated: true)
     self.innovationImage.image = UIImage(named: "icons8-minus-24")
                self.promotingImage.image = UIImage(named: "icons8-plus-24")
                               self.upholdingImage.image = UIImage(named: "icons8-plus-24")
                               self.spreadingImage.image = UIImage(named: "icons8-plus-24")
                               self.connectingGovnImage.image = UIImage(named: "icons8-plus-24")
                               self.healthImage.image = UIImage(named: "icons8-plus-24")
                               self.payingBackImage.image = UIImage(named: "icons8-plus-24")
                 self.innovationHeight.constant = 250
                 viewHeight.constant = 1100
                 self.innovationView.isHidden = false
                
                promotingBrandIndiaHeight.constant = 0
                               connectingGovnHeight.constant = 0
                               upholdingHeight.constant = 0
                               spreadingHeight.constant = 0
                               healthHeight.constant = 0
                               payingbackHeight.constant = 0
                connectingGovnView.isHidden = true
                upholdingView.isHidden = true
                spreadingView.isHidden = true
                promotingBrandIndiaSubview.isHidden = true
                healthView.isHidden = true
                payingBackView.isHidden = true
     
             }
             else{
                                     
                                     DispatchQueue.main.async {
    self.innovationImage.image = UIImage(named: "icons8-plus-24")
                                        self.viewHeight.constant = 850
                             self.innovationHeight.constant = 0
                                            
                                            self.innovationView.isHidden = true
             
                                         
                                         

                                     }
                                     
                                 }
                     DispatchQueue.main.async {
                                    UIView.animate(withDuration: 0.4, animations: {
                                        self.view.layoutIfNeeded()
                                    })
                                }
             
         }
    
    @IBAction func health(_ sender: Any) {
             
             if healthHeight.constant == 0{
                ourStructureScrollView.setContentOffset(CGPoint(x: 0, y: 550), animated: true)
     self.healthImage.image = UIImage(named: "icons8-minus-24")
                self.promotingImage.image = UIImage(named: "icons8-plus-24")
                               self.upholdingImage.image = UIImage(named: "icons8-plus-24")
                               self.spreadingImage.image = UIImage(named: "icons8-plus-24")
                               self.innovationImage.image = UIImage(named: "icons8-plus-24")
                               self.connectingGovnImage.image = UIImage(named: "icons8-plus-24")
                               self.payingBackImage.image = UIImage(named: "icons8-plus-24")
                 self.healthHeight.constant = 220
                 viewHeight.constant = 1100
                 self.healthView.isHidden = false
                promotingBrandIndiaHeight.constant = 0
                connectingGovnHeight.constant = 0
                upholdingHeight.constant = 0
                spreadingHeight.constant = 0
                innovationHeight.constant = 0
                payingbackHeight.constant = 0
                
                connectingGovnView.isHidden = true
                upholdingView.isHidden = true
                spreadingView.isHidden = true
                innovationView.isHidden = true
                promotingBrandIndiaSubview.isHidden = true
                payingBackView.isHidden = true
     
             }
             else{
                                     
                                     DispatchQueue.main.async {
    self.healthImage.image = UIImage(named: "icons8-plus-24")
                                        self.viewHeight.constant = 850
                              self.healthHeight.constant = 0
                                             
                                             self.healthView.isHidden = true
             
                                         
                                         

                                     }
                                     
                                 }
                     DispatchQueue.main.async {
                                    UIView.animate(withDuration: 0.4, animations: {
                                        self.view.layoutIfNeeded()
                                    })
                                }
             
         }
    
    @IBAction func payingback(_ sender: Any) {
             
             if payingbackHeight.constant == 0{
                
     self.payingBackImage.image = UIImage(named: "icons8-minus-24")
                self.promotingImage.image = UIImage(named: "icons8-plus-24")
                               self.upholdingImage.image = UIImage(named: "icons8-plus-24")
                               self.spreadingImage.image = UIImage(named: "icons8-plus-24")
                               self.innovationImage.image = UIImage(named: "icons8-plus-24")
                               self.healthImage.image = UIImage(named: "icons8-plus-24")
                               self.connectingGovnImage.image = UIImage(named: "icons8-plus-24")
                 self.payingbackHeight.constant = 260
                 viewHeight.constant = 1100
                ourStructureScrollView.setContentOffset(CGPoint(x: 0, y: 550), animated: true)
                 self.payingBackView.isHidden = false
                
                              promotingBrandIndiaHeight.constant = 0
                               connectingGovnHeight.constant = 0
                               upholdingHeight.constant = 0
                               spreadingHeight.constant = 0
                               innovationHeight.constant = 0
                               healthHeight.constant = 0
                
                connectingGovnView.isHidden = true
                upholdingView.isHidden = true
                spreadingView.isHidden = true
                innovationView.isHidden = true
                healthView.isHidden = true
                promotingBrandIndiaSubview.isHidden = true
     
             }
             else{
                                     
                                     DispatchQueue.main.async {
                                        self.viewHeight.constant = 850
    self.payingBackImage.image = UIImage(named: "icons8-plus-24")

                              self.payingbackHeight.constant = 0
                                             
                                             self.payingBackView.isHidden = true
             
                                         
                                         

                                     }
                                     
                                 }
                     DispatchQueue.main.async {
                                    UIView.animate(withDuration: 0.4, animations: {
                                        self.view.layoutIfNeeded()
                                    })
                                }
             
         }
    
    @IBAction func back(_ sender: Any) {
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
}
