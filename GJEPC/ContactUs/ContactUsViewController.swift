//
//  ContactUsViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 06/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import GoogleMaps

class ContactUsViewController: UIViewController,CLLocationManagerDelegate {

    @IBOutlet weak var regionalImage: UIImageView!
    @IBOutlet weak var exhibitionImage: UIImageView!
    @IBOutlet weak var headImage: UIImageView!
    @IBOutlet weak var regionalMainView: UIView!
    @IBOutlet weak var exhibitionMainView: UIView!
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    @IBOutlet weak var headOfficeMainView: UIView!
    
    @IBOutlet weak var suratHeight: NSLayoutConstraint!
    @IBOutlet weak var kolkataHeight: NSLayoutConstraint!
    @IBOutlet weak var jaipurHeight: NSLayoutConstraint!
    @IBOutlet weak var delhiHeight: NSLayoutConstraint!
    @IBOutlet weak var chennaiHeight: NSLayoutConstraint!
    @IBOutlet weak var exhibitionHeight: NSLayoutConstraint!
    @IBOutlet weak var headOfficeHeight: NSLayoutConstraint!
    @IBOutlet weak var headOffice: UIView!
    @IBOutlet weak var exhibition: UIView!
    @IBOutlet weak var chennai: UIView!
    @IBOutlet weak var delhi: UIView!
    @IBOutlet weak var jaipur: UIView!
    @IBOutlet weak var kolkata: UIView!
    @IBOutlet weak var surat: UIView!
    
        var exhibitorSelected : String = ""
        var whatToShow = ""
        var i = 0
        var locationLatLongArray : NSMutableArray = []
        var tempDictForLocation : NSMutableDictionary = [:]
        var addressArray : NSMutableArray = []
        var locationManager = CLLocationManager()
        var currentlatitude = Double()
        var currentLongitude = Double()
        var addressLatitude = Double()
        var addressLongitude = Double()
        var distanceInKm = Double()
        var distanceInKmArray : NSMutableArray = []
        var distanceInKmString = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpView()
        addressArray.add(headOfficeAddress)
        addressArray.add(exhibitionCellAddress)
        addressArray.add(chennaiAddress)
        addressArray.add(jaipurAddress)
        addressArray.add(kolkataAddress)
        addressArray.add(suratAddress)
        addressArray.add(delhiAddress)
        addLatLong()
    }
    func addLatLong() {
           let dict1 : NSMutableDictionary = ["lat": 19.06507, "lng" : 72.8644613] // head office
           let dict2 : NSMutableDictionary = ["lat": 19.0726132, "lng" : 72.8711244] // exhibition cell
           let dict3 : NSMutableDictionary = ["lat": 26.9150094, "lng" : 75.826638]  // chennai
           let dict4 : NSMutableDictionary = ["lat": 28.6473406, "lng" : 77.2049081] // jaipur
           let dict5 : NSMutableDictionary = ["lat": 21.1810826, "lng" : 72.82286089999999] // kollkata
           let dict6 : NSMutableDictionary = ["lat": 13.0442576, "lng" : 80.23917019999999] // surat
           let dict7 : NSMutableDictionary = ["lat": 22.5478832, "lng" : 88.35439079999999] // delhi
           
           locationLatLongArray.add(dict1)
           locationLatLongArray.add(dict2)
           locationLatLongArray.add(dict6)
           locationLatLongArray.add(dict3)
           locationLatLongArray.add(dict7)
           locationLatLongArray.add(dict5)
           locationLatLongArray.add(dict4)
           self.findLatlong()
       }
    func findLatlong() {
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        if (CLLocationManager.locationServicesEnabled()) {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location : CLLocation = locations[0]
        print(location.coordinate.latitude)
        print(location.coordinate.longitude)
        self.currentlatitude = location.coordinate.latitude
        self.currentLongitude = location.coordinate.longitude
        // locationManager.stopUpdatingLocation()
        getlatitudeLongitude()
        
    }
    func getlatitudeLongitude() {
        distanceInKmArray = []
        print(locationLatLongArray)
        for v in 0 ..< self.locationLatLongArray.count {
            self.addressLatitude = (self.locationLatLongArray.object(at: v) as! NSDictionary).value(forKey: "lat") as! Double
            self.addressLongitude = (self.locationLatLongArray.object(at: v) as! NSDictionary).value(forKey: "lng") as! Double
            let loc1 = CLLocation(latitude: self.currentlatitude, longitude: self.currentLongitude)
            let loc2 = CLLocation(latitude: self.addressLatitude, longitude: self.addressLongitude)
            let distance = (loc1.distance(from: loc2) / 1000) as Double
            
            print("loc1 : \(loc1)")
            print("loc2 : \(loc2)")
            print("dis : \(distance)")
            
            self.distanceInKmArray.add(distance.roundTo(places: 2))
            print("oo :\(self.distanceInKmArray)")
        }
        
        for m in 0 ..< distanceInKmArray.count {
            distanceInKmString = "\(distanceInKmArray.object(at: m) as! Double)"
            print("nn :\(distanceInKmString)")
        }
        
//        self.contactDetailsTableView.delegate = self
//        self.contactDetailsTableView.dataSource = self
        
        DispatchQueue.main.async {
//            self.contactDetailsTableView.reloadData()
        }
    }
    func getLocationDetails() {
        var urlStr : String = " "
        
        if i < self.addressArray.count {
            urlStr = self.addressArray[i] as! String
        }
        
        let urlString = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=\(urlStr)"
        let url = urlString.replacingOccurrences(of: "", with: "%20")
        print("shoer,,,,,,,\(url)")
        let temp = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        let finalurl = URL(string: temp!)
        print(finalurl as Any)
        let request = NSMutableURLRequest(url: finalurl!)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        //create the session object
        let session = URLSession.shared
        //create dataTask using the session object to send data to the server
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            guard error == nil else {
                return
            }
            guard let data = data else {
                return
            }
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSMutableDictionary {
                    print(json)
                    
                    let status = json.value(forKey: "status") as! String
                    if (status == "OK") {
                        self.tempDictForLocation = [:]
                        let  lat = ((((json.value(forKey: "results") as! NSArray).object(at: 0) as! NSDictionary).value(forKey: "geometry") as! NSDictionary).value(forKey: "location")  as! NSDictionary).value(forKey: "lat") as! CLLocationDegrees
                        let  long = ((((json.value(forKey: "results") as! NSArray).object(at: 0) as! NSDictionary).value(forKey: "geometry") as! NSDictionary).value(forKey: "location")  as! NSDictionary).value(forKey: "lng") as! CLLocationDegrees
                        self.tempDictForLocation = [ "lat" : lat ,"lng" : long]
                        print("jij : \(self.locationLatLongArray.count)")
                        print("kik : \(self.tempDictForLocation.count)")
                        self.locationLatLongArray.add(self.tempDictForLocation)
                        self.triggerLocationUpdate()
                    }
                }
            } catch let error {
                print( error.localizedDescription)
            }
        })
        task.resume()
        // print("locat:\(locationLatLongArray)")
        if self.i < (self.addressArray.count) {
            self.i = self.i + 1
            self.getLocationDetails()
        }
    }
    func triggerLocationUpdate() {
           if locationLatLongArray.count == 7 {
               self.findLatlong()
           }
       }
    func setUpView()  {

        self.headImage.image = UIImage(named: "icons8-plus-24")
        self.regionalImage.image = UIImage(named: "icons8-plus-24")
        self.exhibitionImage.image = UIImage(named: "icons8-plus-24")
        viewHeight.constant = 600
        headOfficeHeight.constant = 0
        headOffice.isHidden = true
        exhibitionHeight.constant = 0
        exhibition.isHidden = true
        chennaiHeight.constant = 0
        chennai.isHidden = true
        delhiHeight.constant = 0
        delhi.isHidden = true
        jaipurHeight.constant = 0
        jaipur.isHidden = true
        kolkataHeight.constant = 0
        kolkata.isHidden = true
        suratHeight.constant = 0
        surat.isHidden = true
        headOfficeMainView.layer.cornerRadius = 5.0
        exhibitionMainView.layer.cornerRadius = 5.0
        regionalMainView.layer.cornerRadius = 5.0
        
        
        
    }

//    @IBAction func back(_ sender: Any) {
//        navigationController?.popViewController(animated: true)
//    }
    @IBAction func head(_ sender: Any) {
        
        if headOfficeHeight.constant == 0{
            self.headImage.image = UIImage(named: "icons8-minus-24")
            self.exhibitionImage.image = UIImage(named: "icons8-plus-24")
            self.regionalImage.image = UIImage(named: "icons8-plus-24")
            
            viewHeight.constant = 800
            headOfficeHeight.constant = 300
            headOffice.isHidden = false
            exhibitionHeight.constant = 0
            exhibition.isHidden = true
            chennaiHeight.constant = 0
            chennai.isHidden = true
            delhiHeight.constant = 0
            delhi.isHidden = true
            jaipurHeight.constant = 0
            jaipur.isHidden = true
            kolkataHeight.constant = 0
            kolkata.isHidden = true
            suratHeight.constant = 0
            surat.isHidden = true
        }
        else{
            self.headImage.image = UIImage(named: "icons8-plus-24")
            headOfficeHeight.constant = 0
                       headOffice.isHidden = true
            viewHeight.constant = 600
        }
        DispatchQueue.main.async {
                                          UIView.animate(withDuration: 0.4, animations: {
                                              self.view.layoutIfNeeded()
                                          })
                                      }
    }
    @IBAction func exhibition(_ sender: Any) {
        
        if exhibitionHeight.constant == 0{
            self.exhibitionImage.image = UIImage(named: "icons8-minus-24")
            self.headImage.image = UIImage(named: "icons8-plus-24")
           
            self.regionalImage.image = UIImage(named: "icons8-plus-24")
            headOfficeHeight.constant = 0
            headOffice.isHidden = true
            exhibitionHeight.constant = 300
            exhibition.isHidden = false
            chennaiHeight.constant = 0
            chennai.isHidden = true
            delhiHeight.constant = 0
            delhi.isHidden = true
            jaipurHeight.constant = 0
            jaipur.isHidden = true
            kolkataHeight.constant = 0
            kolkata.isHidden = true
            suratHeight.constant = 0
            surat.isHidden = true
            viewHeight.constant = 800
        }
        else{
            self.exhibitionImage.image = UIImage(named: "icons8-plus-24")
            exhibitionHeight.constant = 0
                       exhibition.isHidden = true
            viewHeight.constant = 600
        }
        DispatchQueue.main.async {
                                          UIView.animate(withDuration: 0.4, animations: {
                                              self.view.layoutIfNeeded()
                                          })
                                      }
    }
    @IBAction func regional(_ sender: Any) {
        
        if chennaiHeight.constant == 0{
            self.regionalImage.image = UIImage(named: "icons8-minus-24")
            self.headImage.image = UIImage(named: "icons8-plus-24")
            self.exhibitionImage.image = UIImage(named: "icons8-plus-24")
            
            headOfficeHeight.constant = 0
            headOffice.isHidden = true
            exhibitionHeight.constant = 0
            exhibition.isHidden = true
            chennaiHeight.constant = 300
            chennai.isHidden = false
            delhiHeight.constant = 350
            delhi.isHidden = false
            jaipurHeight.constant = 350
            jaipur.isHidden = false
            kolkataHeight.constant = 350
            kolkata.isHidden = false
            suratHeight.constant = 350
            surat.isHidden = false
            viewHeight.constant = 2150
        }
        else{
            self.regionalImage.image = UIImage(named: "icons8-plus-24")
                      chennaiHeight.constant = 0
                      chennai.isHidden = true
                      delhiHeight.constant = 0
                      delhi.isHidden = true
                      jaipurHeight.constant = 0
                      jaipur.isHidden = true
                      kolkataHeight.constant = 0
                      kolkata.isHidden = true
                      suratHeight.constant = 0
                      surat.isHidden = true
            viewHeight.constant = 600
        }
        DispatchQueue.main.async {
                                          UIView.animate(withDuration: 0.4, animations: {
                                              self.view.layoutIfNeeded()
                                          })
                                      }
    }
    
    @IBAction func headoffice(_ sender: Any) {
       
        print(self.currentlatitude)
        let contactDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "ContactDetailsViewController") as! ContactDetailsViewController
        contactDetailsVC.currentLatitude = self.currentlatitude
        contactDetailsVC.currentLongitude = self.currentLongitude
//        print(locationLatLongArray)
//        print(distanceInKmArray)
         contactDetailsVC.lat = (locationLatLongArray.object(at: 0) as! NSDictionary).value(forKey: "lat") as! Double
               contactDetailsVC.long = (locationLatLongArray.object(at: 0) as! NSDictionary).value(forKey: "lng") as! Double
//               contactDetailsVC.distanceFromCurrentlocationString =  "\( distanceInKmArray[0] as! Double)" + "  Kms"
               contactDetailsVC.markerTitle = "Mumbai"
        
         self.navigationController?.pushViewController(contactDetailsVC, animated: true)
    }
    @IBAction func mumbai(_ sender: Any) {
        let contactDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "ContactDetailsViewController") as! ContactDetailsViewController
        contactDetailsVC.currentLatitude = self.currentlatitude
               contactDetailsVC.currentLongitude = self.currentLongitude
        contactDetailsVC.lat = (locationLatLongArray.object(at: 1) as! NSDictionary).value(forKey: "lat") as! Double
        contactDetailsVC.long = (locationLatLongArray.object(at: 1) as! NSDictionary).value(forKey: "lng") as! Double
//                       contactDetailsVC.distanceFromCurrentlocationString =  "\( distanceInKmArray[1] as! Double)" + "  Kms"
                       contactDetailsVC.markerTitle = "Mumbai"
        self.navigationController?.pushViewController(contactDetailsVC, animated: true)
        
        
    }
    
    @IBAction func chennai(_ sender: Any) {
        let contactDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "ContactDetailsViewController") as! ContactDetailsViewController
        contactDetailsVC.currentLatitude = self.currentlatitude
               contactDetailsVC.currentLongitude = self.currentLongitude
        
        contactDetailsVC.lat = (locationLatLongArray.object(at: 2) as! NSDictionary).value(forKey: "lat") as! Double
        contactDetailsVC.long = (locationLatLongArray.object(at: 2) as! NSDictionary).value(forKey: "lng") as! Double
//        contactDetailsVC.distanceFromCurrentlocationString =  "\( distanceInKmArray[2] as! Double)" + "  Kms"
         contactDetailsVC.markerTitle = "Chennai"
        self.navigationController?.pushViewController(contactDetailsVC, animated: true)
        
    }
    
    @IBAction func delhi(_ sender: Any) {
         let contactDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "ContactDetailsViewController") as! ContactDetailsViewController
        contactDetailsVC.currentLatitude = self.currentlatitude
               contactDetailsVC.currentLongitude = self.currentLongitude
        contactDetailsVC.lat = (locationLatLongArray.object(at: 6) as! NSDictionary).value(forKey: "lat") as! Double
                      contactDetailsVC.long = (locationLatLongArray.object(at: 6) as! NSDictionary).value(forKey: "lng") as! Double
//                      contactDetailsVC.distanceFromCurrentlocationString =  "\( distanceInKmArray[3] as! Double)" + "  Kms"
                       contactDetailsVC.markerTitle = "Delhi"
        self.navigationController?.pushViewController(contactDetailsVC, animated: true)
        
    }
    
    @IBAction func jaipur(_ sender: Any) {
        let contactDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "ContactDetailsViewController") as! ContactDetailsViewController
                contactDetailsVC.currentLatitude = self.currentlatitude
                       contactDetailsVC.currentLongitude = self.currentLongitude
                contactDetailsVC.lat = (locationLatLongArray.object(at: 3) as! NSDictionary).value(forKey: "lat") as! Double
                              contactDetailsVC.long = (locationLatLongArray.object(at: 3) as! NSDictionary).value(forKey: "lng") as! Double
                               contactDetailsVC.markerTitle = "Jaipur"
        self.navigationController?.pushViewController(contactDetailsVC, animated: true)

    }
    
    @IBAction func kolkata(_ sender: Any) {
        let contactDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "ContactDetailsViewController") as! ContactDetailsViewController
                       contactDetailsVC.currentLatitude = self.currentlatitude
                              contactDetailsVC.currentLongitude = self.currentLongitude
                       contactDetailsVC.lat = (locationLatLongArray.object(at: 4) as! NSDictionary).value(forKey: "lat") as! Double
                                     contactDetailsVC.long = (locationLatLongArray.object(at: 4) as! NSDictionary).value(forKey: "lng") as! Double
                                      contactDetailsVC.markerTitle = "Kolkata"
        self.navigationController?.pushViewController(contactDetailsVC, animated: true)
    }
    
    @IBAction func surat(_ sender: Any) {
        
        let contactDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "ContactDetailsViewController") as! ContactDetailsViewController
                       contactDetailsVC.currentLatitude = self.currentlatitude
                              contactDetailsVC.currentLongitude = self.currentLongitude
                       contactDetailsVC.lat = (locationLatLongArray.object(at: 5) as! NSDictionary).value(forKey: "lat") as! Double
                                     contactDetailsVC.long = (locationLatLongArray.object(at: 5) as! NSDictionary).value(forKey: "lng") as! Double
                                      contactDetailsVC.markerTitle = "Surat"
        self.navigationController?.pushViewController(contactDetailsVC, animated: true)
    }
    
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func headOfficeCall(_ sender: Any) {
        let url = NSURL(string: "tel://+91-2226544600")!
                      UIApplication.shared.openURL(url as URL)
    }
    @IBAction func exhibitioncall(_ sender: Any) {
        
        let url = NSURL(string: "tel://+91-2243541800")!
                      UIApplication.shared.openURL(url as URL)
    }
    
    @IBAction func chennaiCall(_ sender: Any) {
        let url = NSURL(string: "tel://04442124045")!
                      UIApplication.shared.openURL(url as URL)
    }
    
    @IBAction func surathcall(_ sender: Any) {
        let url = NSURL(string: "tel://0261-2209000")!
                      UIApplication.shared.openURL(url as URL)
    }
//    @IBAction func delhi(_ sender: Any) {
//        let actionSheet = UIAlertController(title: "Call", message: "", preferredStyle: UIAlertController.Style.actionSheet)
//        let call1 = UIAlertAction(title: "+91-22-22020032", style: UIAlertAction.Style.default) { (action) in
//            self.callNumber(phoneNumber: "+91-22-22020032")
//        }
//        let call2 = UIAlertAction(title: "9820507570", style: UIAlertAction.Style.default) { (action) in
//            self.callNumber(phoneNumber: "9820507570")
//        }
//        let cancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
//        actionSheet.addAction(cancel)
//        actionSheet.addAction(call1)
//        actionSheet.addAction(call2)
//        self.present(actionSheet, animated: true, completion: nil)
//
//    }
    
    @IBAction func delhicall(_ sender: Any) {
        let actionSheet = UIAlertController(title: "Call", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        let call1 = UIAlertAction(title: "011-46266920", style: UIAlertAction.Style.default) { (action) in
            self.callNumber(phoneNumber: "01146266920")
        }
        let call2 = UIAlertAction(title: "01123675274", style: UIAlertAction.Style.default) { (action) in
            self.callNumber(phoneNumber: "01123675274")
        }
        let cancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
        actionSheet.addAction(cancel)
        actionSheet.addAction(call1)
        actionSheet.addAction(call2)
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    @IBAction func jaipurCall(_ sender: Any) {
        let actionSheet = UIAlertController(title: "Call", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        let call1 = UIAlertAction(title: "00911412568029", style: UIAlertAction.Style.default) { (action) in
            self.callNumber(phoneNumber: "00911412568029")
        }
        let call2 = UIAlertAction(title: "2574074", style: UIAlertAction.Style.default) { (action) in
            self.callNumber(phoneNumber: "2574074")
        }
        let cancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
        actionSheet.addAction(cancel)
        actionSheet.addAction(call1)
        actionSheet.addAction(call2)
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    @IBAction func kolkataCall(_ sender: Any) {
        let actionSheet = UIAlertController(title: "Call", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        let call1 = UIAlertAction(title: "00913322823630", style: UIAlertAction.Style.default) { (action) in
            self.callNumber(phoneNumber: "00913322823630")
        }
        let call2 = UIAlertAction(title: "22823629", style: UIAlertAction.Style.default) { (action) in
            self.callNumber(phoneNumber: "22823629")
        }
        let cancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
        actionSheet.addAction(cancel)
        actionSheet.addAction(call1)
        actionSheet.addAction(call2)
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    
    func callNumber(phoneNumber:String) {
          if let phoneCallURL = URL(string: "telprompt://\(phoneNumber.replacingOccurrences(of: " ", with: ""))"){
              let application:UIApplication = UIApplication.shared
              if (application.canOpenURL(phoneCallURL as URL)) {
                  application.openURL(phoneCallURL as URL)
              }
          }
      }
    
    
    @IBAction func suratEmail(_ sender: Any) {
            email(emailString: "surat@gjepcindia.com")
       }
       
       @IBAction func kolkataMail(_ sender: Any) {
           email(emailString: "kolkata@gjepcindia.com")
       }
       @IBAction func jaipurEmail(_ sender: Any) {
           email(emailString: "jaipur@gjepcindia.com")
       }
       
       @IBAction func delhiEmail(_ sender: Any) {
           email(emailString: "delhi@gjepcindia.com")
       }
       @IBAction func chennaiEmail(_ sender: Any) {
           email(emailString: "chennai@gjepcindia.com")
       }
       @IBAction func mumbaiMail(_ sender: Any) {
           email(emailString: "exhibitions@gjepcindia.com")
       }
       
       @IBAction func headOfficeMail(_ sender: Any) {
           email(emailString: "ho@gjepcindia.com")
       }
    
    func email(emailString:String)  {
           let stringUrl = NSURL(string : "mailto:\(emailString)")
           if UIApplication.shared.canOpenURL(stringUrl! as URL)
           {
               UIApplication.shared.openURL(stringUrl! as URL)
           }
       }
    
    @IBAction func headOfficeWeb(_ sender: Any) {
        UIApplication.shared.open(URL(string: "http://www.gjepc.org")!, options: [:], completionHandler: nil)

    }
    
    @IBAction func mumbaiWeb(_ sender: Any) {
        UIApplication.shared.open(URL(string: "http://www.iijs.org")!, options: [:], completionHandler: nil)
    }
}
extension UIView {
    func setRadius(radius: CGFloat? = nil) {
        self.layer.cornerRadius = radius ?? self.frame.width / 2;
        self.layer.masksToBounds = true;
        self.layer.borderWidth  = 1
        self.layer.borderColor = UIColor.gray.cgColor
    }
}
