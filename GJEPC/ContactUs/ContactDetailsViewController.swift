//
//  ContactDetailsViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 21/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import  GoogleMaps
class ContactDetailsViewController: UIViewController {

    @IBOutlet weak var mapView: GMSMapView!
    
    var lat : Double = 0.0
    var long : Double = 0.0
    var markerTitle = ""
    var markerSnippet = ""
    var distanceFromCurrentlocationString  = ""
    var currentLatitude = Double()
    var currentLongitude = Double()

    override func viewDidLoad() {
        super.viewDidLoad()
        var cameraMap = GMSCameraPosition()
        var bounds = GMSCoordinateBounds()
        
        cameraMap = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 4.0)
        mapView.camera = cameraMap
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
        marker.title = markerTitle
        marker.snippet = markerSnippet
        marker.map = self.mapView
        marker.appearAnimation = .pop
        bounds = bounds.includingCoordinate(marker.position)
        let update = GMSCameraUpdate.fit(bounds, withPadding: 100)
        mapView.animate(with: update)
    }
    

    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func navigateRoute(_ sender: Any) {
        let urlString = "https://maps.google.com/?saddr=\(currentLatitude)%20\(currentLongitude)&daddr=\(lat)%20\(long))"
               
               DispatchQueue.main.async {
                     UIApplication.shared.openURL(NSURL(string:urlString )! as URL)
               }
    }
    
}
