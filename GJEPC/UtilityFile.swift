//
//  UtilityFile.swift
//  GJEPC
//
//  Created by Kwebmaker on 15/01/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import Foundation
import UIKit

internal extension DateComponents {
    mutating func to12am() {
        self.hour = 0
        self.minute = 0
        self.second = 0
    }
    
    mutating func to12pm(){
        self.hour = 23
        self.minute = 59
        self.second = 59
    }
}

extension Date {
    
    
    struct Gregorian {
        static let calendar = Calendar(identifier: .gregorian)
    }
    var startOfWeek: Date? {
        return Gregorian.calendar.date(from: Gregorian.calendar.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))
    }
    
    func startOfWeek(weekday: Int?) -> Date? {
        var cal = Calendar.current
        var component = cal.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)
        component.to12am()

        cal.firstWeekday = weekday ?? 1
        return cal.date(from: component)! as Date?
    }
    
    func endOfWeek(weekday: Int) -> Date? {
        let cal = Calendar.current
        var component = DateComponents()
        component.weekOfYear = 1
        component.day = -1
        component.to12pm()

        return cal.date(byAdding: component, to: startOfWeek(weekday: weekday)! )
    }
    
    
    
    func isBetweeen(date date1: Date, andDate date2: Date) -> Bool {
        return date1.timeIntervalSince1970 <= self.timeIntervalSince1970 && date2.timeIntervalSince1970 > self.timeIntervalSince1970
    }
    
    
    func startOfMonth() -> Date? {
        
        let calendar = Calendar.current
        let currentDateComponents = calendar.dateComponents([.year, .month], from: self)
        let startOfMonth = calendar.date(from: currentDateComponents)
        return startOfMonth
    }
    
    func dateByAddingMonths(_ monthsToAdd: Int) -> Date? {
        
        let calendar = Calendar.current
        var months = DateComponents()
        months.month = monthsToAdd
        
        return calendar.date(byAdding: months, to: self)
    }
    
    func endOfMonth() -> Date? {
        
        guard let plusOneMonthDate = dateByAddingMonths(1) else { return nil }
        
        let calendar = Calendar.current
        let plusOneMonthDateComponents = calendar.dateComponents([.year, .month], from: plusOneMonthDate)
        let endOfMonth = calendar.date(from: plusOneMonthDateComponents)?.addingTimeInterval(-1)
        
        return endOfMonth
        
    }
}

var CUSTOMER_LIST = "CustomerTableViewCell"
var USER_ORDER_LIST = "UserOrderTableViewCell"
var STAFF_ORDER_LIST = "StaffOrderTableViewCell"
var DELIVERY_SHEET = "DeliverySheetTableViewCell"


func sortOrdersByDate(givenArray : NSMutableArray) -> NSMutableDictionary  {
    print(givenArray)
    let sortedOrders : NSMutableDictionary = [:]
    let todaysOrderArray : NSMutableArray = []
    let thisWeekOrdersArray : NSMutableArray = []
    let thisMonthsArray : NSMutableArray = []
    
    let todaysDate = Date()
    
   //        print(todaysDate)
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    dateFormatter.timeZone = NSTimeZone(abbreviation: "GMT") as TimeZone?
    
    let dateStr = dateFormatter.string(from: todaysDate)
    
    let today = dateFormatter.date(from: dateStr)
    
//    let startOfWeek = today?.startOfWeek(weekday: 1)
    let endOfWeek = today?.endOfWeek(weekday: 1)
    
    let endOfMonth = today?.endOfMonth()
    
//    print(endOfWeek)
//    print(endOfMonth)
    for i in 0..<givenArray.count {
        let dateString = (givenArray.object(at: i) as! NSDictionary)["delivery_date"] as! String
        let deliveryDate = dateFormatter.date(from: dateString)
        
        if deliveryDate != nil {
            if (deliveryDate! >= today!) && (deliveryDate! <= endOfMonth!) {
                thisMonthsArray.add((givenArray.object(at: i) as! NSDictionary))
            }
            if (deliveryDate! >= today!) && (deliveryDate! <= endOfWeek!) {
                thisWeekOrdersArray.add((givenArray.object(at: i) as! NSDictionary))
            }
            if today == deliveryDate {
                todaysOrderArray.add((givenArray.object(at: i) as! NSDictionary))
            }
        }
    }
    
    sortedOrders.setObject(todaysOrderArray, forKey: "today" as NSCopying)
    sortedOrders.setObject(thisWeekOrdersArray, forKey: "week" as NSCopying)
    sortedOrders.setObject(thisMonthsArray, forKey: "month" as NSCopying)
    return sortedOrders
    }


    func callWebService(url : String,httpMethod :String,params : NSMutableDictionary,completion : @escaping (NSMutableDictionary) -> Void) {
        print(url)
        print(params)
        var responseDict : NSMutableDictionary = [:]
        var dataTask: URLSessionDataTask
        
        let url = NSURL(string: url)
        
        let request = NSMutableURLRequest(url:url! as URL)
        
        if httpMethod == "POST" {
            request.httpMethod = httpMethod
        
            request.httpBody = try! JSONSerialization.data(withJSONObject: params, options: [])
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
        }
        
        let session = URLSession.shared
        dataTask = session.dataTask(with: request as URLRequest){(data, response, error) in
            guard error == nil else {
                completion(responseDict)
                return
            }
            guard let data = data else {
                return
            }
            do {
                if let dict = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! NSMutableDictionary{
                
                   // let dict = json as! NSMutableDictionary
               print(dict)
                    if (dict["Response"] as! NSDictionary)["status"] as! String == "true" {
                        responseDict = (dict["Response"] as! NSMutableDictionary)
                        completion(responseDict)
                     //   print("inside")
                    } else {
                        completion(responseDict)
                    }
                }
                
            } catch let error {
                print("Json Error",error.localizedDescription)
            }
        }
        dataTask.resume()
    }

    func searchOperation(inputText : String , searchKey : String,givenArray : NSMutableArray,completion : (NSMutableArray) -> Void) {
//        for case sensitive
//        let searchPredicate = NSPredicate(format: "\(searchKey) contains%@", inputText)
        
//        for insensitive case filter
        var searchPredicate = NSPredicate()
        
        if  ((givenArray.object(at: 0) as! NSDictionary).value(forKey: "create_date") != nil) && (searchKey  == "product_name") {
            searchPredicate = NSPredicate(format: "\(searchKey) CONTAINS[c] %@", inputText)
        }
        else if searchKey  == "product_name" {
            searchPredicate = NSPredicate(format: "ANY product.\(searchKey)  CONTAINS[c] %@", inputText)
        }
        else {
            searchPredicate = NSPredicate(format: "\(searchKey) CONTAINS[c] %@", inputText)
        }
        
        
        //let filteredArray = givenArray.filtered(using: searchPredicate)
        
        let finalArray = NSMutableArray(array: givenArray.filtered(using: searchPredicate))
       // print(finalArray)
        completion(finalArray)
        
        
    }

     func createAttributedString(fullString: String, fullStringColor: UIColor, subString: String, subStringColor: UIColor) -> NSMutableAttributedString
    {
        let range = (fullString as NSString).range(of: subString)
        let attributedString = NSMutableAttributedString(string:fullString)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: fullStringColor, range: NSRange(location: 0, length: fullString.count))
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: subStringColor, range: range)
        return attributedString
    }

func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

extension Double {
    func roundTo(places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}








