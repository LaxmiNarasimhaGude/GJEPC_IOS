//
//  ChatViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 12/01/2021.
//  Copyright © 2021 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
class ChatViewController: UIViewController,UIWebViewDelegate  {

    @IBOutlet weak var chatWebview: UIWebView!
    var urlString = "https://gjepc.org/chatboat_view.php"
    override func viewDidLoad() {
        super.viewDidLoad()

        if CheckInternet.isConnectedToNetwork() == true{
            let url = URL(string: urlString)
                         // print(url)
                          let request = NSMutableURLRequest(url: url!)
                          self.chatWebview.delegate = self
                          self.chatWebview.loadRequest(request as URLRequest)
                   DispatchQueue.main.async {
                               MBProgressHUD.showAdded(to: self.view, animated: true)
                   
                           }
        }
       
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
          
          DispatchQueue.main.async {
              MBProgressHUD.hide(for: self.view, animated: true)
              
          }
          
      }

   

}
