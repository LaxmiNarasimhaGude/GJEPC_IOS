//
//  ProductCategoryTableViewCell.swift
//  GJEPC
//
//  Created by Kwebmaker on 17/09/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit

class ProductCategoryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var Backgroundview: UIView!
    
    @IBOutlet weak var dutyLbl: UILabel!
    @IBOutlet weak var igstLbl: UILabel!
    @IBOutlet weak var swsLbl: UILabel!
    @IBOutlet weak var basicLbl: UILabel!
    @IBOutlet weak var productLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }

}
