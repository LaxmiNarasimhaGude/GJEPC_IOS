//
//  MetalAndCurrencyRateViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 15/09/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
class MetalAndCurrencyRateViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
   
    
   
    

    @IBOutlet weak var metalAndCurrencyTableView: UITableView!
    
      var requestToServer = RequestToServer()
      var callUrls : URL!
      var activityLoader : MBProgressHUD? = nil
//     var tableViewMenuArray : NSMutableArray = []
    
    
    var roughDiamondArray : NSMutableArray! = [["product": "Rough Diamond","basic": "NIL","sws": "NIL","igst":"NIL","duty":"0.25"],["product": "Cut & Polished Diamond","basic": "7.5","sws": "10","igst":"0.25","duty":"8.52063"],["product": "Pearls(Process/Drill)","basic": "10","sws": "10","igst":"3","duty":"14.3"],["product": "Pearls(Raw)","basic": "5","sws": "10","igst":"3","duty":"8.665"],["product": "Gold/silver Jewellery","basic": "20","sws": "10","igst":"3","duty":"25.66"],["product": "SYN.IND.Diamond Powder","basic": "10","sws": "10","igst":"3","duty":"14.3"],["product": "Semi Pre Stones (C&p)","basic": "7.5","sws": "10","igst":"3","duty":"11.4974"],["product": "Precious Stones(C&p)","basic": "7.5","sws": "10","igst":"0.25","duty":"8.52063"],["product": "Cubic Zirconia(CPD)","basic": "5","sws": "10","igst":"3","duty":"8.665"],["product": "Synthetic Stones(CPD)","basic": "10","sws": "10","igst":"0.25","duty":"11.2773"],["product": "Gold Findings/mountings","basic": "20","sws": "10","igst":"3","duty":"25.66"],["product": "Imitation Jewellery","basic": "20","sws": "10","igst":"3","duty":"25.66"],["product": "Lab Grown RD","basic": "7.5","sws": "10","igst":"0.25","duty":"8.52063"],["product": "GoldBars","basic": "12.5","sws": "3","igst":"3","duty":"16.3"],["product": "Gold(Grain/alloy/dore Bars)","basic": "12.5","sws": "3","igst":"3","duty":"16.3"]]
//    var cutDiamondArray : NSMutableArray! = [["product": "Cut & Polished Diamond","basic": "7.5","sws": "10","igst":"0.25","duty":"8.52063"]]
//    var pearlsProcessDiamondArray : NSMutableArray! = [["product": "Pearls(Process/Drill)","basic": "10","sws": "10","igst":"3","duty":"14.3"]]
//    var pearlsRawDiamondArray : NSMutableArray! = [["product": "Pearls(Raw)","basic": "5","sws": "10","igst":"3","duty":"8.665"]]

//    var goldSilverArray : NSMutableArray! = [["product": "Gold/silver Jewellery","basic": "20","sws": "10","igst":"3","duty":"25.66"]]
//    var syIndDiamondPowderArray : NSMutableArray! = [["product": "SYN.IND.Diamond Powder","basic": "10","sws": "10","igst":"3","duty":"14.3"]]
//    var semiPreStoneArray : NSMutableArray! = [["product": "Semi Pre Stones (C&p)","basic": "7.5","sws": "10","igst":"3","duty":"11.4974"]]
//    var preciousStoneArray : NSMutableArray! = [["product": "Precious Stones(C&p)","basic": "7.5","sws": "10","igst":"0.25","duty":"8.52063"]]
//     var cubicArray : NSMutableArray! = [["product": "Cubic Zirconia(CPD)","basic": "5","sws": "10","igst":"3","duty":"8.665"]]
//    var syntheticStonesArray : NSMutableArray! = [["product": "Synthetic Stones(CPD)","basic": "10","sws": "10","igst":"0.25","duty":"11.2773"]]
//    var goldFindingArray : NSMutableArray! = [["product": "Gold Findings/mountings","basic": "20","sws": "10","igst":"3","duty":"25.66"]]
//    var imitationJewelleryArray : NSMutableArray! = [["product": "Imitation Jewellery","basic": "20","sws": "10","igst":"3","duty":"25.66"]]
//     var labGrownArray : NSMutableArray! = [["product": "Lab Grown RD","basic": "7.5","sws": "10","igst":"0.25","duty":"8.52063"]]
//    var goldBarGrownArray : NSMutableArray! = [["product": "GoldBars","basic": "12.5","sws": "3","igst":"3","duty":"16.3"]]
//    var goldGrainArray : NSMutableArray! = [["product": "Gold(Grain/alloy/dore Bars)","basic": "12.5","sws": "3","igst":"3","duty":"16.3"]]
    

    
    override func viewDidLoad() {
        super.viewDidLoad()

       
        metalAndCurrencyTableView.delegate = self
        metalAndCurrencyTableView.dataSource = self
               metalAndCurrencyTableView.separatorStyle = .none

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return roughDiamondArray.count
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           let cell = metalAndCurrencyTableView.dequeueReusableCell(withIdentifier: "ProductCategoryTableViewCell", for: indexPath) as! ProductCategoryTableViewCell
        cell.Backgroundview.backgroundColor = UIColor.white
        cell.Backgroundview.layer.cornerRadius = 3.0
        cell.Backgroundview.layer.masksToBounds = false
        cell.Backgroundview.layer.shadowOffset = CGSize(width: 0, height: 0)
        cell.Backgroundview.layer.shadowOpacity = 0.3
        cell.productLbl.text = ((self.roughDiamondArray.object(at: indexPath.row) as AnyObject).object(forKey: "product")as! String).uppercased()
//        cell.productLbl.text?.uppercased()
        cell.basicLbl.text = ((self.roughDiamondArray.object(at: indexPath.row) as AnyObject).object(forKey: "basic")as! String)
        cell.swsLbl.text = ((self.roughDiamondArray.object(at: indexPath.row) as AnyObject).object(forKey: "sws")as! String)
        cell.igstLbl.text = ((self.roughDiamondArray.object(at: indexPath.row) as AnyObject).object(forKey: "igst")as! String)
        cell.dutyLbl.text = ((self.roughDiamondArray.object(at: indexPath.row) as AnyObject).object(forKey: "duty")as! String)
       return cell
       }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return 200
        
       }
}
