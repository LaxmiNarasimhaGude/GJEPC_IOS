//
//  Metal_CurrancyViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 20/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
class Metal_CurrancyViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, dataReceivedFromServerDelegate {
   

    @IBOutlet weak var metalAndCurrencyScrollView: UIScrollView!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var goldRatesImage: UIImageView!
    @IBOutlet weak var metalAndCrrencyRatesImage: UIImageView!
    var type = ""
        var requestToServer = RequestToServer()
        var callUrls : URL!
        var tableViewMenuArray : NSMutableArray = []
        var activityLoader : MBProgressHUD? = nil
    @IBOutlet weak var metalTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        metalTable.delegate = self
        metalTable.dataSource = self
        metalAndCrrencyRatesImage.isHidden = false
        goldRatesImage.isHidden = true
         productImage.isHidden = true
        
        
        requestToServer.delegate = self
        metalTable.separatorStyle = .none
        checkforInternet()
//       retyrBtn.layer.cornerRadius = 5
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewMenuArray.count
    }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           let cell1 = tableView.dequeueReusableCell(withIdentifier: "Metal_CurrancyTableViewCell") as! Metal_CurrancyTableViewCell
        if type == "GOLDRATE"{
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd"

                      let dateFormatterPrint = DateFormatter()
                      dateFormatterPrint.dateFormat = "dd MMM, yyyy"

                      let date: NSDate? = dateFormatterGet.date(from: "\((self.tableViewMenuArray.object(at: indexPath.row) as AnyObject).object(forKey: "post_date")as! String)") as NSDate?
                      
                      
                      cell1.titleLbl.text = "\(dateFormatterPrint.string(from: date! as Date))"
            
//             cell1.titleLbl.text =  ((self.tableViewMenuArray.object(at: indexPath.row) as AnyObject).object(forKey: "post_date")as! String)
            cell1.priceLbl.text = ((self.tableViewMenuArray.object(at: indexPath.row) as AnyObject).object(forKey: "name")as! String)
            
        }
        else{
             cell1.titleLbl.text =  ((self.tableViewMenuArray.object(at: indexPath.row) as AnyObject).object(forKey: "name")as! String)
            //        print(((self.tableViewMenuArray.object(at: indexPath.row) as AnyObject).object(forKey: "rate")as! String))
                       cell1.priceLbl.text = ((self.tableViewMenuArray.object(at: indexPath.row) as AnyObject).object(forKey: "rate")as! String)
        }
          
        
        
            DispatchQueue.main.async {
                    cell1.bacgroundView.backgroundColor = UIColor.white
                               cell1.bacgroundView.layer.cornerRadius = 3.0
                               cell1.bacgroundView.layer.masksToBounds = false
                                          

                               cell1.bacgroundView.layer.shadowOffset = CGSize(width: 0, height: 0)
                               cell1.bacgroundView.layer.shadowOpacity = 0.3
                               

                           }
           return cell1
       }
       
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if type == "GOLDRATE"{
            let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
                   nextVC!.headingtitle = "Metal & Currency Rates";
            nextVC!.pdf = ((self.tableViewMenuArray.object(at: indexPath.row) as AnyObject).object(forKey: "upload_gold_rate")as! String)
                   self.navigationController?.pushViewController(nextVC!, animated: true)
        }
        
    }
       func dataReceivedFromServer(data: NSDictionary, url: URL) {
           
           if ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "true" {
               
               let temporaryArray = ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "Result") as! NSArray)
               let sortedArray = (temporaryArray as NSArray).sortedArray(using: [NSSortDescriptor(key: "event", ascending: true)]) as! [[String:AnyObject]] as NSArray
               
               
               
               
               
               
               
               tableViewMenuArray = (sortedArray.mutableCopy() as! NSMutableArray)
               
               
               print("tableViewMenuArray.....\(tableViewMenuArray)")
               
               DispatchQueue.main.async {
                   self.activityLoader?.hide(animated: true)
                   self.metalTable.reloadData()
               }
           }
       }
       func checkforInternet()
          {
              if CheckInternet.isConnectedToNetwork() == true{
                  DispatchQueue.main.async {
                      self.loadActivityIndicator()
                  }
                  self.metalTable.isHidden = false
                  callUrls = MetalCurrancyRate
                  let params = [""]
                  requestToServer.connectToServer(myUrl: callUrls as URL, params: params as AnyObject)
                  
              }else
              {
                  
                  self.metalTable.isHidden = true
              }
              
              metalTable.reloadData()
          }
          func loadActivityIndicator()
          {
              activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
              activityLoader?.label.text = "Loading";
              activityLoader?.detailsLabel.text = "Please Wait";
              activityLoader?.isUserInteractionEnabled = false;
          }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func metalSegmentTwo(_ sender: Any) {
        
        if self.children.count >= 0{
                                     let viewControllers:[UIViewController] = self.children
                                     for viewContoller in viewControllers{
                                         viewContoller.willMove(toParent: nil)
                                         viewContoller.view.removeFromSuperview()
                                         viewContoller.removeFromParent()
                                     }
                                 }
        
        metalAndCrrencyRatesImage.isHidden = true
        goldRatesImage.isHidden = false
        productImage.isHidden = true
        type = "GOLDRATE"
         self.metalTable.isHidden = true
        
        if CheckInternet.isConnectedToNetwork() == true{
            DispatchQueue.main.async {
                self.loadActivityIndicator()
            }
            self.metalTable.isHidden = false
            callUrls = GoldRates
            let params = [""]
            requestToServer.connectToServer(myUrl: callUrls as URL, params: params as AnyObject)
            
        }else
        {
            
            self.metalTable.isHidden = true
        }
        
        metalTable.reloadData()

        
        
        
        metalAndCurrencyScrollView.contentOffset = CGPoint(x: 0, y: 0);
        
        

       
    }
    
    @IBAction func metalAndCurrencyRates(_ sender: Any) {
        if self.children.count >= 0{
                                 let viewControllers:[UIViewController] = self.children
                                 for viewContoller in viewControllers{
                                     viewContoller.willMove(toParent: nil)
                                     viewContoller.view.removeFromSuperview()
                                     viewContoller.removeFromParent()
                                 }
                             }
        metalAndCrrencyRatesImage.isHidden = false
        goldRatesImage.isHidden = true
        productImage.isHidden = true
        checkforInternet()
//        let newContentOffsetX = ((metalAndCurrencyScrollView.contentSize.width - metalAndCurrencyScrollView.frame.size.width) / 2)+80;
        metalAndCurrencyScrollView.contentOffset = CGPoint(x: 0, y: 0);
    }
    @IBAction func productCatergory(_ sender: Any) {
        if self.children.count >= 0{
                                 let viewControllers:[UIViewController] = self.children
                                 for viewContoller in viewControllers{
                                     viewContoller.willMove(toParent: nil)
                                     viewContoller.view.removeFromSuperview()
                                     viewContoller.removeFromParent()
                                 }
                             }
                DispatchQueue.main.async {
        
                    self.metalAndCrrencyRatesImage.isHidden = true
                    self.goldRatesImage.isHidden = true
                    self.productImage.isHidden = false
        
                    let controller:MetalAndCurrencyRateViewController =
                            self.storyboard!.instantiateViewController(withIdentifier: "MetalAndCurrencyRateViewController") as!
                            MetalAndCurrencyRateViewController
        
                            let statusbarHeight = UIApplication.shared.statusBarFrame.height
                    //        let navigationBarHeight = uiappl
        
                            let tabBarHeight = self.tabBarController!.tabBar.frame.size.height+90;
        
        
        
                            controller.view.frame = CGRect(x: 0, y: statusbarHeight+100, width: UIScreen.main.bounds.size.width, height: (UIScreen.main.bounds.size.height-tabBarHeight))
                            controller.willMove(toParent: self)
                            self.view.addSubview(controller.view)
                            self.addChild(controller)
                            controller.didMove(toParent: self)
                }
        
        let newContentOffsetX = ((metalAndCurrencyScrollView.contentSize.width - metalAndCurrencyScrollView.frame.size.width) / 2)+80;
        metalAndCurrencyScrollView.contentOffset = CGPoint(x: newContentOffsetX, y: 0);
    }
    
}
