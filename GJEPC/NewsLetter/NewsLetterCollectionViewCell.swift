//
//  NewsLetterCollectionViewCell.swift
//  GJEPC
//
//  Created by Kwebmaker on 16/01/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit

class NewsLetterCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var defaultImage: UIImageView!
    
    @IBOutlet weak var pdfImage: UIImageView!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var newsLetterImage: UIImageView!
}
