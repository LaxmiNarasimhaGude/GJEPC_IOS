//
//  NewsLetterViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 16/01/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
//import ListPlaceholder
class NewsLetterViewController: UIViewController,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,dataReceivedFromServerDelegate,UICollectionViewDataSource,UISearchBarDelegate {
    
    
   
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var newsLetterCollectionView: UICollectionView!
    
    var customArray : NSMutableArray = []
    var callUrls : URL!
    var loading = false;
    var loadNew = true;
    var requestToServer = RequestToServer()
    var temporaryArray : NSArray = []
    var tableViewArrayForNews : NSMutableArray = []
    var isSearching = false;
    var activityLoader : MBProgressHUD? = nil
      var type = "true"
    override func viewDidLoad() {
        super.viewDidLoad()
        onFirstTimeLoad()
        searchBar.text = ""
        searchBar.delegate = self
        searchBar.layer.cornerRadius = 8
        searchBar.clipsToBounds = true
        searchBar.layer.borderWidth = 1
        searchBar.layer.borderColor = UIColor.gray.cgColor
        searchBar.showsCancelButton = false



        newsLetterCollectionView.keyboardDismissMode = .onDrag
        
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        newsLetterCollectionView.collectionViewLayout = layout
        
        requestToServer.delegate = self
        newsLetterCollectionView.delegate = self
        newsLetterCollectionView.dataSource = self
    }

//    override func viewDidAppear(_ animated: Bool) {
//        checkForInternet()
//    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout:

              UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

              let width = UIScreen.main.bounds.size.width
             
              return CGSize(width: ((width / 2) - 15)   , height: 180)



          }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tableViewArrayForNews.count
       }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        print(tableViewArrayForNews)
        
         let dict = self.tableViewArrayForNews.object(at: indexPath.row) as? NSDictionary
        
        let newsContentvc = self.storyboard?.instantiateViewController(withIdentifier: "NewsWebViewController") as! NewsWebViewController
//        let htmlFile =  (((self.customArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "Result") as! NSArray).object(at: indexPath.row) as! NSDictionary)["html_files"] as! String
       
//        newsContentvc.newstype = "newsletter"
//        newsContentvc.loadnewsUrl  = htmlFile
        newsContentvc.newstitle = "newsletter"
        newsContentvc.dictUpload = dict!
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(newsContentvc, animated: true)
        }
    }
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = newsLetterCollectionView.dequeueReusableCell(withReuseIdentifier: "NewsLetterCollectionViewCell", for: indexPath) as! NewsLetterCollectionViewCell
        cell.dateLbl.startShimmeringEffect()
        cell.descriptionLbl.startShimmeringEffect()
        cell.defaultImage.startShimmeringEffect()
        cell.pdfImage.startShimmeringEffect()
        
        cell.contentView.layer.cornerRadius = 2.0
         cell.contentView.layer.borderWidth = 1.0
         cell.contentView.layer.borderColor = UIColor.clear.cgColor
         cell.contentView.layer.masksToBounds = true
        
         cell.layer.backgroundColor = UIColor.white.cgColor
         cell.layer.shadowColor = UIColor.lightGray.cgColor
         cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)//CGSizeMake(0, 2.0);
         cell.layer.shadowRadius = 2.0
         cell.layer.shadowOpacity = 1.0
         cell.layer.masksToBounds = false
         cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
        
        if type == "false"{
            cell.dateLbl.text = ((self.tableViewArrayForNews.object(at: indexPath.row) as AnyObject).object(forKey: "year")as! String)
            cell.descriptionLbl.text = ((self.tableViewArrayForNews.object(at: indexPath.row) as AnyObject).object(forKey: "name")as! String)
            cell.descriptionLbl.labelSpacing(label: cell.descriptionLbl, font: "Book")
            cell.dateLbl.layer.sublayers = nil
                       cell.descriptionLbl.layer.sublayers = nil
                       cell.defaultImage.layer.sublayers = nil
            cell.pdfImage.layer.sublayers = nil
        }
        
        
//        cell.descriptionLbl.setCharacterSpacing(1)
//        cell.descriptionLbl.setLineSpacing(lineSpacing: 5, lineHeightMultiple: 0)
        return cell
       }
    
    func onFirstTimeLoad() {
                    customArray = [];
                    callUrls = fortynightUrl
                    let params = [ "start":"0",
                                   "limit":"100"]
                    self.loading = true;
                    loadNew = true;
                    requestToServer.connectToServer(myUrl: callUrls as URL, params: params as AnyObject)
                }
    func checkForInternet() {
                    if CheckInternet.isConnectedToNetwork() == true
                    {
                        print("internet available")
//                        self.newsLetterCollectionView.isHidden = false
                        DispatchQueue.main.async {
                            self.loadActivityIndicator()
                        }
                        onFirstTimeLoad()
                    }
                    else
                    {
                        print("no internet")
//                        newsLetterCollectionView.isHidden = true
//                        activityLoader?.hide(animated: true)
                    }
                }
                
    func loadActivityIndicator()
    {
        activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
        activityLoader?.label.text = "Loading";
        activityLoader?.detailsLabel.text = "Please Wait";
        activityLoader?.isUserInteractionEnabled = false;
    }
    func dataReceivedFromServer(data: NSDictionary, url: URL) {
        loading = false;
        if ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "true" {
            temporaryArray = ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "Result") as! NSArray)
            print(temporaryArray)
            let arrayCount = self.tableViewArrayForNews.count
            var indexArray : [IndexPath] = [IndexPath]()
            if temporaryArray.count > 0 {
                
                if self.tableViewArrayForNews.count != 0 && loadNew == false  && isSearching == false{
                    for i in 0..<temporaryArray.count {
                        indexArray.append(IndexPath(row: arrayCount + i, section: 0));
                        self.tableViewArrayForNews.add(temporaryArray[i]);
                    }
                }
                else {
                    tableViewArrayForNews = (temporaryArray.mutableCopy() as! NSMutableArray)
                    customArray = [];
                    tableViewArrayForNews = (temporaryArray.mutableCopy() as! NSMutableArray)
                    for i in 0..<tableViewArrayForNews.count {
                        
                        let yearString = ((tableViewArrayForNews.object(at: i) as! NSDictionary).value(forKey: "year") as! String)
                        
                        if !customArray.contains(yearString) {
                            
                            customArray.add(yearString)
                            
                        }
                    }
                    print(customArray)
                }
                
                print(tableViewArrayForNews)
                for i in 0..<customArray.count {
                    let currentKey = (customArray.object(at: i) as! String)
                    print(currentKey)
                    let tempArray : NSMutableArray = []
                    for j in 0..<tableViewArrayForNews.count {
                        if ((tableViewArrayForNews.object(at: j) as! NSDictionary).value(forKey: "year") as! String) == currentKey {
                            let temporaryDict = (self.tableViewArrayForNews.object(at: j) as! NSDictionary)
                            print(temporaryDict)
                            tempArray.add(temporaryDict.mutableCopy())
                        }
                        print(tempArray)
                    }
                    customArray.replaceObject(at: i, with: ["sectionName" : currentKey, "Result" : tempArray])
                }
                print(customArray)
                DispatchQueue.main.async {
//                    self.newsLetterCollectionView.isHidden = false
//                    self.noDataLabel.isHidden = true
//                    self.activityLoader?.hide(animated: true)
                    self.newsLetterCollectionView.reloadData()
DispatchQueue.main.asyncAfter(deadline: .now()+1.0) {
                                                      self.changeImage()
                                                  }
               }
                }
         
            else
            {
                DispatchQueue.main.async {
//                    self.noDataLabel.isHidden = false
//                    self.newsLetterCollectionView.isHidden = true
                }
            }
        }
    }
    @objc func changeImage()  {
             self.type = "false"
             self.newsLetterCollectionView.reloadData()
         }
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if let lastVisibleIndexPath = tableView.indexPathsForVisibleRows?.last {
//            if indexPath == lastVisibleIndexPath {
//
//                newsLetterCollectionView.hideLoader()
//            }
//        }
//    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if (indexPath.row == tableViewArrayForNews.count - 1 ) {
//           newsLetterCollectionView.hideLoader()
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        UIApplication.shared.sendAction(
            #selector(UIResponder.resignFirstResponder), to:nil, from:nil, for:nil)
    }
    
    
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        isSearching = true;
        return true;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
//        searchBar.isHidden = true
//        searchButton.isHidden = false
//        newslabel.isHidden = false
//        cancelButton.isHidden = true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        onFirstTimeLoad()
//        inSearchMode = false
        searchBar.text = ""
//        if searchText == ""{
            onFirstTimeLoad()
//        }
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == ""{
            onFirstTimeLoad()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        print(searchBar.text! + text)
        
        if text == "\n" {
            return true
        }
        var params = ["name" : searchBar.text! + text,
                      "start" : "0" ,
                      "limit" : "10"]
        if searchBar.text?.count == 1 && text == "" {
            onFirstTimeLoad()
            return true;
        }else if text == "" {
            let name: String = searchBar.text!
            let truncated = name.substring(to: name.index(before: name.endIndex))
            params = ["name" : truncated];
        }
        
        DispatchQueue.main.async {
            self.callUrls = fornightlySearchUrl
            self.loading = true
            self.loadNew = true;
            self.requestToServer.connectToServer(myUrl: self.callUrls as URL, params: params as AnyObject)
        }
        return true
    }

}
