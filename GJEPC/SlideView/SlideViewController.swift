//
//  SlideViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 04/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
//@available(iOS 13.0, *)
@available(iOS 13.0, *)
class SlideViewController: UIViewController,dataReceivedFromServerDelegate {
    func dataReceivedFromServer(data: NSDictionary, url: URL) {
            
        if  type == "domestic"{
               if ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "true" {
                        let temporaryArray = ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "Result") as! NSMutableArray)
                        let checkStatus  =  (((temporaryArray.object(at: 0)) as AnyObject).value(forKey: "status") as! String)
                        let isTemp = (((temporaryArray.object(at: 0)) as AnyObject).value(forKey: "isTemp") as! String)
                        let year = (((temporaryArray.object(at: 0)) as AnyObject).value(forKey: "year") as! String)
                        print(year)
                        if checkStatus == "0" {
                            
                            
                            DispatchQueue.main.async {
                            self.activityLoader?.hide(animated: true)
                                            }
                            
                            let alert = UIAlertController(title: "", message: "Coming Soon", preferredStyle: UIAlertController.Style.alert)
                            DispatchQueue.main.sync {
                                 self.present(alert, animated: true, completion: nil)
                            }
                               
            //
                            
                            let when = DispatchTime.now() + 3
                            DispatchQueue.main.asyncAfter(deadline: when){
                                
                                alert.dismiss(animated: true, completion: nil)
                            }
                        } else {
                            
                            if(checkStatus == "1"){
                                
                                if(isTemp == "0"){
                                    DispatchQueue.main.async {
                                                 self.activityLoader?.hide(animated: true)
                                             }
                                             if signaturetapped == true {
                                                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SignatureViewController") as! SignatureViewController

                                                 vc.category = "national"
                                                 vc.year = year
                                                 vc.exhibitorSelected = "signature"
                                                 vc.titleName =  sendTitleName
                                                 DispatchQueue.main.async {
                                                     self.navigationController?.pushViewController(vc, animated: true)
                                                 }
                                                 signaturetapped = false
                                             }
                                             else if igemstapped == true {
                                                 let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SignatureViewController") as! SignatureViewController
                                                 vc.category = "national"
                                                vc.year = year
                                                 vc.exhibitorSelected = "igjme"
                                                 vc.titleName = "IGJME"
                                                 DispatchQueue.main.async {
                                                     self.navigationController?.pushViewController(vc, animated: true)
                                                 }
                                                 igemstapped = false
                                                 
                                             } else {
                                                 let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SignatureViewController") as! SignatureViewController
                                                 vc.category = "national"
                                                vc.year = year
                                                 vc.exhibitorSelected = "iijs"
                                                 vc.titleName = "IIJS Premiere 2019"
                                                 DispatchQueue.main.async {
                                                     self.navigationController?.pushViewController(vc, animated: true)
                                                 }
                                                 iijstapped = false
                                             }
                                         }
                                else{
                                     let Urldescription = (((temporaryArray.object(at: 0)) as AnyObject).value(forKey: "description") as! String)
                                    
                                    DispatchQueue.main.async {
                                        self.activityLoader?.hide(animated: true)
                                                               
                                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                        let vc = storyboard.instantiateViewController(withIdentifier: "SignatureWebViewController") as! SignatureWebViewController
                                       
                                        vc.des = Urldescription
                                        vc.headingDesc = self.sendTitleName
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                    
                                   
                                  
                                    

                                    
                                }
                                
                                
                                }
                                
                                
                                
                            }
                            
                            
                     
                    }
        }
        if type == "labs"{
                    if ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "true" {
                        
                        let temporaryArray = ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "Result") as! NSArray)
            
                        
                        tableViewMenuArray.removeAllObjects()
                        tableViewMenuArray = (temporaryArray.mutableCopy() as! NSMutableArray)
                        print("tableViewMenuArray.....\(tableViewMenuArray)")
                        DispatchQueue.main.async {
                            self.activityLoader?.hide(animated: true)
//                            self.laboratoriesCollectionView.reloadData()
                            let vc = self.storyboard?.instantiateViewController(withIdentifier:"LabContentViewController") as! LabContentViewController
                            let title = ((self.tableViewMenuArray.object(at: self.labno) as AnyObject).object(forKey: "title") as! String)
                                   vc.Ltitle = title
                            let desc = ((self.tableViewMenuArray.object(at: self.labno) as AnyObject).object(forKey: "description") as! String)
                                   vc.Ldesc = desc
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                        
                    }
        }
        else{
            if ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "true" {
                            
                            let temporaryArray = ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "Result") as! NSArray)
                //             let sortedArray = (temporaryArray as NSArray).sortedArray(using: [NSSortDescriptor(key: "title", ascending: true)]) as! [[String:AnyObject]] as NSArray
                            
                            
                            tableViewMenuArray = (temporaryArray.mutableCopy() as! NSMutableArray)
                            print("tableViewMenuArray.....\(tableViewMenuArray)")
                            DispatchQueue.main.async {
                                self.activityLoader?.hide(animated: true)
            //                    self.institutesCollectionView.reloadData()
                            }
                        }
        }
            
            
        }
    func checkForInternet() {
               if CheckInternet.isConnectedToNetwork() == true
               {
                   print("internet available")
//                   self.institutesCollectionView.isHidden = false
                   DispatchQueue.main.async {
                       self.loadActivityIndicator()
                   }
                   onFirstTimeLoad()
               }
               else
               {
                   print("no internet")
//                   institutesCollectionView.isHidden = true
                   activityLoader?.hide(animated: true)
               }
           }
    func loadActivityIndicator()
          {
              activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
              activityLoader?.label.text = "Loading";
              activityLoader?.detailsLabel.text = "Please Wait";
              activityLoader?.isUserInteractionEnabled = false;
          }
    func onFirstTimeLoad() {
          callUrls = instituesUrl
           let params = [""]
           requestToServer.connectToServer(myUrl: callUrls as URL, params: params as AnyObject)
       }
    
   var requestToServer = RequestToServer()
   var callUrls : URL!
   var activityLoader : MBProgressHUD? = nil
   var tableViewMenuArray : NSMutableArray = []
   var sendTitleName = ""
   var igemstapped = false
   var iijstapped = false
   var signaturetapped = false
   var requestServer = RequestToServer()
   var type = ""
   var labno:Int!
    
    
    
   
    @IBOutlet weak var slidemenuHeight: NSLayoutConstraint!
    @IBOutlet weak var msmeSchemeImage: UIImageView!
    @IBOutlet weak var govnAsisImage: UIImageView!
    @IBOutlet weak var msmeImage: UIImageView!
    
    @IBOutlet weak var notificationHeight: NSLayoutConstraint!
    @IBOutlet weak var notificationView: UIView!
    @IBOutlet weak var msmeDiHeight: NSLayoutConstraint!
    @IBOutlet weak var msmeDiView: UIView!
    @IBOutlet weak var skillHeight: NSLayoutConstraint!
    @IBOutlet weak var skillDeveView: UIView!
    @IBOutlet weak var msmeSchemeHeight: NSLayoutConstraint!
    @IBOutlet weak var msmeSchemeView: UIView!
    @IBOutlet weak var udyamHeight: NSLayoutConstraint!
    @IBOutlet weak var udyamView: UIView!
    @IBOutlet weak var govrnAsisHeight: NSLayoutConstraint!
    @IBOutlet weak var govrnAsisView: UIView!
    @IBOutlet weak var microMemView: UIView!
    @IBOutlet weak var microMemHeight: NSLayoutConstraint!
    @IBOutlet weak var msmeSubViewHeight: NSLayoutConstraint!
    @IBOutlet weak var govnAssisSubViewHeight: NSLayoutConstraint!
    @IBOutlet weak var msmeSchemeSubView: UIView!
    @IBOutlet weak var govnAssisSubView: UIView!
    
    @IBOutlet weak var leaderShipImage: UIImageView!
    @IBOutlet weak var gjepcImage: UIImageView!
    
    @IBOutlet weak var leadershipSubViewHeight: NSLayoutConstraint!
    @IBOutlet weak var leardershipSubview: UIView!
    @IBOutlet weak var indiaGemAndJewelleryView: UIView!
    @IBOutlet weak var conferences: UIView!
    @IBOutlet weak var designInitiative: UIView!
    @IBOutlet weak var internationalTradeshowView: UIView!
    @IBOutlet weak var domesticSubtradeshowView: UIView!
    @IBOutlet weak var educationSubView: UIView!
    @IBOutlet weak var labsSubView: UIView!
    @IBOutlet weak var conferenceView: UIView!
    @IBOutlet weak var designInitiativeView: UIView!
    @IBOutlet weak var eventsView: UIView!
    @IBOutlet weak var internationalTradeView: UIView!
    @IBOutlet weak var domesticTradeshowView: UIView!
    @IBOutlet weak var newsAndMediaViewHeight: NSLayoutConstraint!
    @IBOutlet weak var eventsAndTradeshowHeight: NSLayoutConstraint!
    
    @IBOutlet weak var newsAndMediaImage: UIImageView!
    @IBOutlet weak var eventAndTradeshowImage: UIImageView!
    @IBOutlet weak var eventsAndTradeshowView: UIView!
    @IBOutlet weak var newsView: UIView!
    @IBOutlet weak var gjepcHeight: NSLayoutConstraint!  //100
    @IBOutlet weak var leaderShipHeight: NSLayoutConstraint! //70
    @IBOutlet weak var ourstuctureHeight: NSLayoutConstraint!
    @IBOutlet weak var domesticTradeshowHeight: NSLayoutConstraint! //40
    @IBOutlet weak var domesticSubtradeshowHeight: NSLayoutConstraint!  //100
    @IBOutlet weak var internationalTradeshowHeight: NSLayoutConstraint!   //40
    @IBOutlet weak var internationalSubtradeshowHeight: NSLayoutConstraint!  //125
    
    @IBOutlet weak var eventsHeight: NSLayoutConstraint! //40
    @IBOutlet weak var designInitiativeHeight: NSLayoutConstraint!  //40
    @IBOutlet weak var designInitiativeSubHeight: NSLayoutConstraint! //125
   
    @IBOutlet weak var conferenceHeight: NSLayoutConstraint!  //40
    @IBOutlet weak var conferencesSubHeight: NSLayoutConstraint! //70
    @IBOutlet weak var indiaGemAndJewelHeight: NSLayoutConstraint! //40
    @IBOutlet weak var educationSubHeight: NSLayoutConstraint!  //165
        @IBOutlet weak var labsSubHeight: NSLayoutConstraint! //125
    
    @IBOutlet weak var businessEventsImage: UIImageView!
    
    @IBOutlet weak var domesticTradeShowImage: UIImageView!
    @IBOutlet weak var internationalTradeShowImage: UIImageView!
    @IBOutlet weak var eventsImage: UIImageView!
    @IBOutlet weak var designInitiativesImage: UIImageView!
    @IBOutlet weak var conferencesImage: UIImageView!
    @IBOutlet weak var educationImage: UIImageView!
    @IBOutlet weak var labsImage: UIImageView!
    
    var window: UIWindow?
    var appdelegate = UIApplication.shared.delegate as! AppDelegate
     var drawerContainer:MMDrawerController?
    var tabBar:UITabBarController?
    override func viewDidLoad() {
        super.viewDidLoad()
       setView()
       checkForInternet()
       requestToServer.delegate = self
         requestServer.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
       
    }

    func setView() {
        
        slidemenuHeight.constant = 700
        
        leardershipSubview.isHidden = true
        designInitiative.isHidden = true
        conferences.isHidden = true
        educationSubView.isHidden = true
        designInitiativeView.isHidden = true
         conferenceView.isHidden = true
         eventsView.isHidden = true
         internationalTradeView.isHidden = true
         domesticTradeshowView.isHidden = true
        labsSubView.isHidden = true
        govnAssisSubView.isHidden = true
        msmeSchemeSubView.isHidden = true
        gjepcHeight.constant = 0
        leaderShipHeight.constant = 0
        ourstuctureHeight.constant = 0
        domesticTradeshowHeight.constant = 0
        domesticSubtradeshowHeight.constant = 0
        internationalTradeshowHeight.constant = 0
        internationalSubtradeshowHeight.constant = 0
        eventsHeight.constant = 0
        designInitiativeHeight.constant = 0
        designInitiativeSubHeight.constant = 0
        conferenceHeight.constant = 0
        conferencesSubHeight.constant = 0
        indiaGemAndJewelHeight.constant = 0
        educationSubHeight.constant = 0
        labsSubHeight.constant = 0
        govnAssisSubViewHeight.constant = 0
        msmeSubViewHeight.constant = 0
        microMemView.isHidden = true
        govrnAsisView.isHidden = true
        udyamView.isHidden=true
        msmeSchemeView.isHidden=true
        skillDeveView.isHidden=true
        msmeDiView.isHidden = true
        notificationView.isHidden=true
        microMemHeight.constant = 0
        govrnAsisHeight.constant = 0
        udyamHeight.constant = 0
        msmeSchemeHeight.constant = 0
        skillHeight.constant = 0
        msmeDiHeight.constant = 0
        notificationHeight.constant = 0
        
        
        
        
        
        
//        newsAndMediaViewHeight.constant = 0
//        newsView.isHidden = true
//
//        eventsAndTradeshowView.isHidden = true
//        eventsAndTradeshowHeight.constant = 0
        
//        self.eventAndTradeshowImage.image = UIImage(named: "")
//        self.newsAndMediaImage.image = UIImage(named: "icons8-plus-math-48")
        self.labsImage.image = UIImage(named: "icons8-plus-math-48")
        gjepcImage.image = UIImage(named: "icons8-plus-math-48")
        self.educationImage.image = UIImage(named: "icons8-plus-math-48")
        self.businessEventsImage.image = UIImage(named: "icons8-plus-math-48")
         self.msmeImage.image = UIImage(named: "icons8-plus-math-48")
    }
    
    @IBAction func coa(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "coaViewController") as! coaViewController
               navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func leadership(_ sender: Any) {
        
        if self.leaderShipHeight.constant == 0
        {
            leaderShipImage.image = UIImage(named: "icons8-minus-48")
            leardershipSubview.isHidden = false
            leaderShipHeight.constant = 60
            slidemenuHeight.constant = 910
            
        }
        else{
            leaderShipImage.image = UIImage(named: "icons8-plus-math-48")
            leaderShipHeight.constant = 0
            leardershipSubview.isHidden = true
        }
        DispatchQueue.main.async {
                       UIView.animate(withDuration: 0.4, animations: {
                           self.view.layoutIfNeeded()
                       })
                   }
    }
    
    @IBAction func GJEPC(_ sender: Any) {
         
            
            if self.gjepcHeight.constant == 0
            {
                          self.msmeImage.image = UIImage(named: "icons8-plus-math-48")
//                          self.gjepcImage.image = UIImage(named: "icons8-plus-math-48")
                          self.businessEventsImage.image = UIImage(named: "icons8-plus-math-48")
                          self.educationImage.image = UIImage(named: "icons8-plus-math-48")
                          self.labsImage.image = UIImage(named: "icons8-plus-math-48")
                leaderShipImage.image = UIImage(named: "icons8-plus-math-48")
                 self.gjepcImage.image = UIImage(named: "icons8-minus-48")
                leaderShipHeight.constant = 0
                self.gjepcHeight.constant = 100
                self.ourstuctureHeight.constant = 40
                self.slidemenuHeight.constant = 840
               
                    
                    DispatchQueue.main.async {
                        UIView.animate(withDuration: 0.4, animations: {

                            self.leardershipSubview.isHidden = true
                            self.designInitiative.isHidden = true
                            self.conferences.isHidden = true
                            self.educationSubView.isHidden = true
                            self.designInitiativeView.isHidden = true
                             self.conferenceView.isHidden = true
                             self.eventsView.isHidden = true
                             self.internationalTradeView.isHidden = true
                             self.domesticTradeshowView.isHidden = true
                            self.labsSubView.isHidden = true
                            self.govnAssisSubView.isHidden = true
                            self.msmeSchemeSubView.isHidden = true
//                            self.gjepcHeight.constant = 0
                            self.leaderShipHeight.constant = 0
                            self.ourstuctureHeight.constant = 40
                            self.domesticTradeshowHeight.constant = 0
                            self.domesticSubtradeshowHeight.constant = 0
                            self.internationalTradeshowHeight.constant = 0
                            self.internationalSubtradeshowHeight.constant = 0
                            self.eventsHeight.constant = 0
                            self.designInitiativeHeight.constant = 0
                            self.designInitiativeSubHeight.constant = 0
                           self.conferenceHeight.constant = 0
                            self.conferencesSubHeight.constant = 0
                            self.indiaGemAndJewelHeight.constant = 0
                            self.educationSubHeight.constant = 0
                            self.labsSubHeight.constant = 0
                            self.govnAssisSubViewHeight.constant = 0
                            self.msmeSubViewHeight.constant = 0
                            self.microMemView.isHidden = true
                            self.govrnAsisView.isHidden = true
                            self.udyamView.isHidden=true
                            self.msmeSchemeView.isHidden=true
                            self.skillDeveView.isHidden=true
                            self.msmeDiView.isHidden = true
                            self.notificationView.isHidden=true
                            self.microMemHeight.constant = 0
                            self.govrnAsisHeight.constant = 0
                            self.udyamHeight.constant = 0
                            self.msmeSchemeHeight.constant = 0
                            self.skillHeight.constant = 0
                            self.msmeDiHeight.constant = 0
                           self.notificationHeight.constant = 0
                            
                            
                        })
                    }
              
            
            }
            else {
                
                DispatchQueue.main.async {
                    self.slidemenuHeight.constant = 700
                    self.gjepcImage.image = UIImage(named: "icons8-plus-math-48")
                    self.gjepcHeight.constant = 0
                    self.leaderShipHeight.constant = 0
                    self.ourstuctureHeight.constant = 0

                }
                
            }
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.4, animations: {
                    self.view.layoutIfNeeded()
                })
            }
        }
    
    @IBAction func businessEvents(_ sender: Any) {
        
        if domesticTradeshowHeight.constant == 0{
            self.businessEventsImage.image = UIImage(named: "icons8-minus-48")
            self.msmeImage.image = UIImage(named: "icons8-plus-math-48")
            self.gjepcImage.image = UIImage(named: "icons8-plus-math-48")
            self.educationImage.image = UIImage(named: "icons8-plus-math-48")
            self.labsImage.image = UIImage(named: "icons8-plus-math-48")
            self.domesticTradeShowImage.image = UIImage(named: "icons8-plus-math-48")
            self.internationalTradeShowImage.image = UIImage(named: "icons8-plus-math-48")
            self.eventsImage.image = UIImage(named: "icons8-plus-math-48")
            self.designInitiativesImage.image = UIImage(named: "icons8-plus-math-48")
            self.conferencesImage.image = UIImage(named: "icons8-plus-math-48")
            self.slidemenuHeight.constant = 860
            
            internationalSubtradeshowHeight.constant = 0
            domesticTradeshowHeight.constant = 40
            internationalTradeshowHeight.constant = 40
            eventsHeight.constant = 40
            indiaGemAndJewelHeight.constant = 40
            domesticSubtradeshowView.isHidden = true
            internationalTradeshowView.isHidden = true
            conferenceView.isHidden = true
            internationalTradeView.isHidden = false
//            conferences.isHidden = false
            eventsView.isHidden = false
//            internationalTradeshowView.isHidden = false
            domesticTradeshowView.isHidden = false
            indiaGemAndJewelleryView.isHidden = true
            self.indiaGemAndJewelHeight.constant = 0
            
             DispatchQueue.main.async {
                                    UIView.animate(withDuration: 0.4, animations: {

                                        self.leardershipSubview.isHidden = true
                                        self.designInitiative.isHidden = true
                                        self.conferences.isHidden = true
                                        self.educationSubView.isHidden = true
                                        self.designInitiativeView.isHidden = true
                                         self.conferenceView.isHidden = true
//                                         self.eventsView.isHidden = true
//                                         self.internationalTradeView.isHidden = true
//                                         self.domesticTradeshowView.isHidden = true
                                        self.labsSubView.isHidden = true
                                        self.govnAssisSubView.isHidden = true
                                        self.msmeSchemeSubView.isHidden = true
                                        self.gjepcHeight.constant = 0
                                        self.leaderShipHeight.constant = 0
                                        self.ourstuctureHeight.constant = 0
//                                        self.domesticTradeshowHeight.constant = 0
                                        self.domesticSubtradeshowHeight.constant = 0
//                                        self.internationalTradeshowHeight.constant = 0
                                        self.internationalSubtradeshowHeight.constant = 0
//                                        self.eventsHeight.constant = 0
                                        self.designInitiativeHeight.constant = 0
                                        self.designInitiativeSubHeight.constant = 0
                                       self.conferenceHeight.constant = 0
                                        self.conferencesSubHeight.constant = 0
                                        self.indiaGemAndJewelHeight.constant = 0
                                        self.educationSubHeight.constant = 0
                                        self.labsSubHeight.constant = 0
                                        self.govnAssisSubViewHeight.constant = 0
                                        self.msmeSubViewHeight.constant = 0
                                        self.microMemView.isHidden = true
                                        self.govrnAsisView.isHidden = true
                                        self.udyamView.isHidden=true
                                        self.msmeSchemeView.isHidden=true
                                        self.skillDeveView.isHidden=true
                                        self.msmeDiView.isHidden = true
                                        self.notificationView.isHidden=true
                                        self.microMemHeight.constant = 0
                                        self.govrnAsisHeight.constant = 0
                                        self.udyamHeight.constant = 0
                                        self.msmeSchemeHeight.constant = 0
                                        self.skillHeight.constant = 0
                                        self.msmeDiHeight.constant = 0
                                       self.notificationHeight.constant = 0
                                        
                                        
                                    })
                                }
            
        }
        else{
                        
                        DispatchQueue.main.async {
                      self.slidemenuHeight.constant = 700
                           
                            self.businessEventsImage.image = UIImage(named: "icons8-plus-math-48")
                            self.domesticTradeshowView.isHidden = true
                            self.internationalTradeView.isHidden = true
                                                       self.eventsView.isHidden = true
                            self.domesticTradeshowHeight.constant = 0
                            self.domesticSubtradeshowHeight.constant = 0
                            self.internationalSubtradeshowHeight.constant = 0
                            self.internationalTradeshowHeight.constant = 0
                            self.eventsHeight.constant = 0
                            self.designInitiativeHeight.constant = 0
                            self.conferenceHeight.constant = 0
                            self.indiaGemAndJewelHeight.constant = 0
                            self.designInitiative.isHidden = true
                            self.conferences.isHidden = true
                            self.indiaGemAndJewelleryView.isHidden = true
                            self.designInitiativeSubHeight.constant = 0
                            self.designInitiativeView.isHidden = true
                            self.conferencesSubHeight.constant = 0
                            self.conferenceView.isHidden = true
                            

                        }
                        DispatchQueue.main.async {

                        }
                    }
        DispatchQueue.main.async {
                       UIView.animate(withDuration: 0.4, animations: {
                           self.view.layoutIfNeeded()
                       })
                   }
    }
    
    
    @IBAction func domesticAction(_ sender: Any) {
        
        if domesticSubtradeshowHeight.constant == 0{
             self.domesticTradeShowImage.image = UIImage(named: "icons8-minus-48")
            self.internationalTradeShowImage.image = UIImage(named: "icons8-plus-math-48")
//                    self.domesticTradeShowImage.image = UIImage(named: "icons8-plus-math-48")
                    self.eventsImage.image = UIImage(named: "icons8-plus-math-48")
            self.domesticSubtradeshowHeight.constant = 120
            self.internationalSubtradeshowHeight.constant = 0
            self.designInitiativeHeight.constant = 0
            self.designInitiativeSubHeight.constant = 0
            self.conferenceHeight.constant = 0
            self.conferencesSubHeight.constant = 0
            self.indiaGemAndJewelHeight.constant = 0
            
            self.slidemenuHeight.constant = 920
            self.domesticSubtradeshowView.isHidden = false
        }
        else{
                                
                                DispatchQueue.main.async {
                
                                self.domesticTradeShowImage.image = UIImage(named: "icons8-plus-math-48")
                        self.domesticSubtradeshowHeight.constant = 0
                    self.domesticSubtradeshowView.isHidden = true
                                    self.slidemenuHeight.constant = 820
        
                                    
                                    

                                }
                                
                            }
                DispatchQueue.main.async {
                               UIView.animate(withDuration: 0.4, animations: {
                                   self.view.layoutIfNeeded()
                               })
                           }
        
    }
    @IBAction func internationalTradeshowsAction(_ sender: Any) {
        
        if internationalSubtradeshowHeight.constant == 0{
             self.internationalTradeShowImage.image = UIImage(named: "icons8-minus-48")
            self.domesticTradeShowImage.image = UIImage(named: "icons8-plus-math-48")
            self.eventsImage.image = UIImage(named: "icons8-plus-math-48")
            self.internationalSubtradeshowHeight.constant = 125
            self.domesticSubtradeshowHeight.constant = 0
//                       self.internationalSubtradeshowHeight.constant = 0
                       self.designInitiativeHeight.constant = 0
                       self.designInitiativeSubHeight.constant = 0
                       self.conferenceHeight.constant = 0
                       self.conferencesSubHeight.constant = 0
                       self.indiaGemAndJewelHeight.constant = 0
            
            self.slidemenuHeight.constant = 920
            self.internationalTradeshowView.isHidden = false
        }
        else{
                                
                                DispatchQueue.main.async {
                self.internationalTradeShowImage.image = UIImage(named: "icons8-plus-math-48")
                                    
                     self.internationalSubtradeshowHeight.constant = 0
                                    self.slidemenuHeight.constant = 825
                    self.internationalTradeshowView.isHidden = true
        
                                    
                                    

                                }
                                
                            }
                DispatchQueue.main.async {
                               UIView.animate(withDuration: 0.4, animations: {
                                   self.view.layoutIfNeeded()
                               })
                           }
        
    }
    
    @IBAction func designInitiativeAction(_ sender: Any) {
        
        if designInitiativeSubHeight.constant == 0{
            self.designInitiativeSubHeight.constant = 125
            self.conferencesSubHeight.constant = 0
            self.slidemenuHeight.constant = 1045
            self.designInitiativeView.isHidden = false
             self.designInitiativesImage.image = UIImage(named: "icons8-minus-48")
            self.conferencesImage.image = UIImage(named: "icons8-plus-math-48")
        }
        else{
                                       
                                       DispatchQueue.main.async {
                                        self.slidemenuHeight.constant = 920
                       self.designInitiativesImage.image = UIImage(named: "icons8-plus-math-48")
                    self.designInitiativeSubHeight.constant = 0
                    self.designInitiativeView.isHidden = true
               
                                       }
                                       
                                   }
                       DispatchQueue.main.async {
                                      UIView.animate(withDuration: 0.4, animations: {
                                          self.view.layoutIfNeeded()
                                      })
                                  }
    }
    @IBAction func conferencesAction(_ sender: Any) {
        
        if conferencesSubHeight.constant == 0{
            self.conferencesSubHeight.constant = 70
            self.designInitiativeSubHeight.constant = 0
            self.slidemenuHeight.constant = 1000
            self.conferenceView.isHidden = false
             self.conferencesImage.image = UIImage(named: "icons8-minus-48")
             self.designInitiativesImage.image = UIImage(named: "icons8-plus-math-48")
        }
        else{
                                       
                                       DispatchQueue.main.async {
                       self.slidemenuHeight.constant = 920
                    self.conferencesSubHeight.constant = 0
                    self.conferenceView.isHidden = true
                                        self.conferencesImage.image = UIImage(named: "icons8-plus-math-48")
               
                                       }
                                       
                                   }
                       DispatchQueue.main.async {
                                      UIView.animate(withDuration: 0.4, animations: {
                                          self.view.layoutIfNeeded()
                                      })
                                  }
    }
    
    @IBAction func eventsAction(_ sender: Any) {
        
        if designInitiativeHeight.constant == 0{
            self.eventsImage.image = UIImage(named: "icons8-minus-48")
            self.internationalTradeShowImage.image = UIImage(named: "icons8-plus-math-48")
            self.domesticTradeShowImage.image = UIImage(named: "icons8-plus-math-48")
//            self.eventsImage.image = UIImage(named: "icons8-plus-math-48")
           
            self.domesticSubtradeshowHeight.constant = 0
            self.internationalSubtradeshowHeight.constant = 0
            self.slidemenuHeight.constant = 920
            self.designInitiativeHeight.constant = 40
            self.conferenceHeight.constant = 40
            self.indiaGemAndJewelHeight.constant = 40
            self.designInitiative.isHidden = false
            self.conferences.isHidden = false
            self.indiaGemAndJewelleryView.isHidden = false
        }
        else{
                                
                                DispatchQueue.main.async {
                                    self.slidemenuHeight.constant = 820
                                    
                                    self.eventsImage.image = UIImage(named: "icons8-plus-math-48")
                
                                self.designInitiativeHeight.constant = 0
                                self.conferenceHeight.constant = 0
                               self.indiaGemAndJewelHeight.constant = 0
                                    self.conferencesSubHeight.constant = 0
                                    self.designInitiativeSubHeight.constant = 0
                                    self.conferenceView.isHidden = true
                                    self.designInitiativeView.isHidden = true
                               self.designInitiative.isHidden = true
                               self.conferences.isHidden = true
                               self.indiaGemAndJewelleryView.isHidden = true
                                    
                                    

                                }
                                
                            }
                DispatchQueue.main.async {
                               UIView.animate(withDuration: 0.4, animations: {
                                   self.view.layoutIfNeeded()
                               })
                           }
    }
    
    
    
    
    @IBAction func education(_ sender: Any) {
        if self.educationSubHeight.constant == 0{
            self.educationImage.image = UIImage(named: "icons8-minus-48")
            self.msmeImage.image = UIImage(named: "icons8-plus-math-48")
                      self.gjepcImage.image = UIImage(named: "icons8-plus-math-48")
                      self.businessEventsImage.image = UIImage(named: "icons8-plus-math-48")
//                      self.educationImage.image = UIImage(named: "icons8-plus-math-48")
                      self.labsImage.image = UIImage(named: "icons8-plus-math-48")
            self.slidemenuHeight.constant = 865
            self.educationSubHeight.constant = 165
            educationSubView.isHidden = false
            
                         DispatchQueue.main.async {
                                                UIView.animate(withDuration: 0.4, animations: {

                                                    self.leardershipSubview.isHidden = true
                                                    self.designInitiative.isHidden = true
                                                    self.conferences.isHidden = true
//                                                    self.educationSubView.isHidden = true
                                                    self.designInitiativeView.isHidden = true
                                                     self.conferenceView.isHidden = true
                                                     self.eventsView.isHidden = true
                                                     self.internationalTradeView.isHidden = true
                                                     self.domesticTradeshowView.isHidden = true
                                                    self.labsSubView.isHidden = true
                                                    self.govnAssisSubView.isHidden = true
                                                    self.msmeSchemeSubView.isHidden = true
                                                    self.gjepcHeight.constant = 0
                                                    self.leaderShipHeight.constant = 0
                                                    self.ourstuctureHeight.constant = 0
                                                    self.domesticTradeshowHeight.constant = 0
                                                    self.domesticSubtradeshowHeight.constant = 0
                                                    self.internationalTradeshowHeight.constant = 0
                                                    self.internationalSubtradeshowHeight.constant = 0
                                                    self.eventsHeight.constant = 0
                                                    self.designInitiativeHeight.constant = 0
                                                    self.designInitiativeSubHeight.constant = 0
                                                   self.conferenceHeight.constant = 0
                                                    self.conferencesSubHeight.constant = 0
                                                    self.indiaGemAndJewelHeight.constant = 0
//                                                    self.educationSubHeight.constant = 0
                                                    self.labsSubHeight.constant = 0
                                                    self.govnAssisSubViewHeight.constant = 0
                                                    self.msmeSubViewHeight.constant = 0
                                                    self.microMemView.isHidden = true
                                                    self.govrnAsisView.isHidden = true
                                                    self.udyamView.isHidden=true
                                                    self.msmeSchemeView.isHidden=true
                                                    self.skillDeveView.isHidden=true
                                                    self.msmeDiView.isHidden = true
                                                    self.notificationView.isHidden=true
                                                    self.microMemHeight.constant = 0
                                                    self.govrnAsisHeight.constant = 0
                                                    self.udyamHeight.constant = 0
                                                    self.msmeSchemeHeight.constant = 0
                                                    self.skillHeight.constant = 0
                                                    self.msmeDiHeight.constant = 0
                                                   self.notificationHeight.constant = 0
                                                    
                                                    
                                                })
                                            }
            
        }
        else{
                        
                        DispatchQueue.main.async {
                            self.slidemenuHeight.constant = 700
             self.educationImage.image = UIImage(named: "icons8-plus-math-48")
                            self.educationSubHeight.constant = 0
                            self.educationSubView.isHidden = true


                            
                            

                        }
                        DispatchQueue.main.async {

                        }
                    }
        DispatchQueue.main.async {
                       UIView.animate(withDuration: 0.4, animations: {
                           self.view.layoutIfNeeded()
                       })
                   }
    }
    
    @IBAction func iigjMumbai(_ sender: Any) {
//        let vc = self.storyboard?.instantiateViewController(withIdentifier:"LabContentViewController") as! LabContentViewController
//let title = ((self.tableViewMenuArray.object(at: 3) as AnyObject).object(forKey: "title") as! String)
//vc.Ltitle = title
//let desc = ((self.tableViewMenuArray.object(at: 3) as AnyObject).object(forKey: "description") as! String)
//print(desc)
//print(title)
//vc.Ldesc = desc
//self.navigationController?.pushViewController(vc, animated: true)
       
        let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
               nextVC!.headingtitle = "IIGJ MUMBAI";
               
               self.navigationController?.pushViewController(nextVC!, animated: true)
        
        
    }
    
    @IBAction func iigjDelhi(_ sender: Any) {
//            let vc = self.storyboard?.instantiateViewController(withIdentifier:"LabContentViewController") as! LabContentViewController
//    let title = ((self.tableViewMenuArray.object(at: 4) as AnyObject).object(forKey: "title") as! String)
//    vc.Ltitle = title
//    let desc = ((self.tableViewMenuArray.object(at: 4) as AnyObject).object(forKey: "description") as! String)
//    print(desc)
//    print(title)
//    vc.Ldesc = desc
//    self.navigationController?.pushViewController(vc, animated: true)
        
        let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
                      nextVC!.headingtitle = "IIGJ DELHI";
                      
                      self.navigationController?.pushViewController(nextVC!, animated: true)
            
        }
    
    @IBAction func iigjJaipur(_ sender: Any) {
//            let vc = self.storyboard?.instantiateViewController(withIdentifier:"LabContentViewController") as! LabContentViewController
//    let title = ((self.tableViewMenuArray.object(at: 1) as AnyObject).object(forKey: "title") as! String)
//    vc.Ltitle = title
//    let desc = ((self.tableViewMenuArray.object(at: 1) as AnyObject).object(forKey: "description") as! String)
//    print(desc)
//    print(title)
//    vc.Ldesc = desc
//    self.navigationController?.pushViewController(vc, animated: true)
       
        let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
                      nextVC!.headingtitle = "IIGJ JAIPUR";
                      
                      self.navigationController?.pushViewController(nextVC!, animated: true)
            
        }
    @IBAction func iigjVaranasi(_ sender: Any) {
//            let vc = self.storyboard?.instantiateViewController(withIdentifier:"LabContentViewController") as! LabContentViewController
//    let title = ((self.tableViewMenuArray.object(at: 2) as AnyObject).object(forKey: "title") as! String)
//    vc.Ltitle = title
//    let desc = ((self.tableViewMenuArray.object(at: 2) as AnyObject).object(forKey: "description") as! String)
//    print(desc)
//    print(title)
//    vc.Ldesc = desc
//    self.navigationController?.pushViewController(vc, animated: true)
        
        let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
                      nextVC!.headingtitle = "IIGJ VARANASI";
                      
                      self.navigationController?.pushViewController(nextVC!, animated: true)
            
        }
    @IBAction func iigjUdupi(_ sender: Any) {
//            let vc = self.storyboard?.instantiateViewController(withIdentifier:"LabContentViewController") as! LabContentViewController
//    let title = ((self.tableViewMenuArray.object(at: 5) as AnyObject).object(forKey: "title") as! String)
//    vc.Ltitle = title
//    let desc = ((self.tableViewMenuArray.object(at: 5) as AnyObject).object(forKey: "description") as! String)
//    print(desc)
//    print(title)
//    vc.Ldesc = desc
//    self.navigationController?.pushViewController(vc, animated: true)
        
        let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
                      nextVC!.headingtitle = "IIGJ UDUPI";
                      
                      self.navigationController?.pushViewController(nextVC!, animated: true)
        
        }
    
    
    
    
    
    
    @IBAction func labs(_ sender: Any) {
        
        if labsSubHeight.constant == 0{
            DispatchQueue.main.async {
                                          UIView.animate(withDuration: 0.4, animations: {
                                              self.view.layoutIfNeeded()
                                          })
                                      }
            self.labsImage.image = UIImage(named: "icons8-minus-48")
            self.slidemenuHeight.constant = 825
            self.labsSubHeight.constant = 125
            self.labsSubView.isHidden = false
            self.msmeImage.image = UIImage(named: "icons8-plus-math-48")
                      self.gjepcImage.image = UIImage(named: "icons8-plus-math-48")
                      self.businessEventsImage.image = UIImage(named: "icons8-plus-math-48")
                      self.educationImage.image = UIImage(named: "icons8-plus-math-48")
//                      self.labsImage.image = UIImage(named: "icons8-plus-math-48")
            
            
               DispatchQueue.main.async {
                                                            UIView.animate(withDuration: 0.4, animations: {

                                                                self.leardershipSubview.isHidden = true
                                                                self.designInitiative.isHidden = true
                                                                self.conferences.isHidden = true
                                                                self.educationSubView.isHidden = true
                                                                self.designInitiativeView.isHidden = true
                                                                 self.conferenceView.isHidden = true
                                                                 self.eventsView.isHidden = true
                                                                 self.internationalTradeView.isHidden = true
                                                                 self.domesticTradeshowView.isHidden = true
//                                                                self.labsSubView.isHidden = true
                                                                self.govnAssisSubView.isHidden = true
                                                                self.msmeSchemeSubView.isHidden = true
                                                                self.gjepcHeight.constant = 0
                                                                self.leaderShipHeight.constant = 0
                                                                self.ourstuctureHeight.constant = 0
                                                                self.domesticTradeshowHeight.constant = 0
                                                                self.domesticSubtradeshowHeight.constant = 0
                                                                self.internationalTradeshowHeight.constant = 0
                                                                self.internationalSubtradeshowHeight.constant = 0
                                                                self.eventsHeight.constant = 0
                                                                self.designInitiativeHeight.constant = 0
                                                                self.designInitiativeSubHeight.constant = 0
                                                               self.conferenceHeight.constant = 0
                                                                self.conferencesSubHeight.constant = 0
                                                                self.indiaGemAndJewelHeight.constant = 0
                                                                self.educationSubHeight.constant = 0
//                                                                self.labsSubHeight.constant = 0
                                                                self.govnAssisSubViewHeight.constant = 0
                                                                self.msmeSubViewHeight.constant = 0
                                                                self.microMemView.isHidden = true
                                                                self.govrnAsisView.isHidden = true
                                                                self.udyamView.isHidden=true
                                                                self.msmeSchemeView.isHidden=true
                                                                self.skillDeveView.isHidden=true
                                                                self.msmeDiView.isHidden = true
                                                                self.notificationView.isHidden=true
                                                                self.microMemHeight.constant = 0
                                                                self.govrnAsisHeight.constant = 0
                                                                self.udyamHeight.constant = 0
                                                                self.msmeSchemeHeight.constant = 0
                                                                self.skillHeight.constant = 0
                                                                self.msmeDiHeight.constant = 0
                                                               self.notificationHeight.constant = 0
                                                                
                                                                
                                                            })
                                                        }
        }
       else{
                                
                                DispatchQueue.main.async {
                                    self.slidemenuHeight.constant = 700
                   self.labsImage.image = UIImage(named: "icons8-plus-math-48")
                                     self.labsSubHeight.constant = 0
                                               self.labsSubView.isHidden = true

                                    
                                    

                                }
                               
                            }
                DispatchQueue.main.async {
                               UIView.animate(withDuration: 0.4, animations: {
                                   self.view.layoutIfNeeded()
                               })
                           }
    }
    
    
    
    @IBAction func aboutUs(_ sender: Any) {
        
//         let vc = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
//        vc.switching = "aboutUS"

//        let vc = storyboard?.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
//
//        let appdelegate = UIApplication.shared.delegate as! AppDelegate
//        appdelegate.drawerContainer?.closeDrawer(animated: true, completion: nil)
//
//        appdelegate.drawerContainer?.centerViewController = vc

//        let currentWindow: UIWindow? = UIApplication.shared.keyWindow
//               print(UIApplication.shared.keyWindow)
    
//        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
//        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.window?.rootViewController = redViewController
        
        
        
        
        
//        
//                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
//                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
//
//        
//        redViewController.view.frame = CGRect(x: 0, y: 70, width: UIScreen.main.bounds.size.width, height: (UIScreen.main.bounds.size.height-150))
//        
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        
//        appDelegate.drawerContainer?.closeDrawer(animated: true, completion: nil)
////        print(appDelegate.tabbarHeight)
//        let rootvc = appDelegate.window?.rootViewController
//        rootvc?.view.addSubview(redViewController.view)
//        rootvc?.addChild(redViewController)
//        redViewController.didMove(toParent: rootvc)
      
                        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.drawerContainer?.centerViewController = redViewController
//        appDelegate.drawerContainer?.closeDrawer(animated: true, completion: nil)
//
////        appDelegate.drawerContainer?.closeDrawer(animated: true, completion: nil)
//        DispatchQueue.main.async {
//            appDelegate.drawerContainer?.rightDrawerViewController = nil
//        }
        
        navigationController?.pushViewController(redViewController, animated: true)
        
    }
    
    @IBAction func contact(_ sender: Any) {
        
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.drawerContainer?.centerViewController = redViewController
//
//        appDelegate.drawerContainer?.closeDrawer(animated: true, completion: nil)
//        DispatchQueue.main.async {
//            appDelegate.drawerContainer?.rightDrawerViewController = nil
//        }
//
        navigationController?.pushViewController(redViewController, animated: true)
    }
    
    @IBAction func newsAndMedia(_ sender: Any) {
     
        
        if self.newsAndMediaViewHeight.constant == 0
        {
            self.newsAndMediaImage.image = UIImage(named: "icons8-minus-48")
            
         
//                self.newsViewCollapse = false
                self.newsAndMediaViewHeight.constant = 100
                self.newsView.isHidden = false

                self.eventsAndTradeshowHeight.constant = 0
                self.eventsAndTradeshowView.isHidden = true
                self.eventAndTradeshowImage.image = UIImage(named: "icons8-plus-math-48");
                
                
                DispatchQueue.main.async {
                    UIView.animate(withDuration: 0.4, animations: {

                    })
                }
          
        
        }
        else {
            
            DispatchQueue.main.async {
//                self.newsViewCollapse = false
                self.newsAndMediaImage.image = UIImage(named: "icons8-plus-math-48"); self.newsAndMediaViewHeight.constant = 0
                self.newsView.isHidden = true

            }
            DispatchQueue.main.async {

            }
        }
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.4, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    @IBAction func eventsAndTradeshow(_ sender: Any) {
         
            
            if self.eventsAndTradeshowHeight.constant == 0
            {
                self.eventAndTradeshowImage.image = UIImage(named: "icons8-minus-48")
                DispatchQueue.main.async {
    //                self.newsViewCollapse = false
                    self.eventsAndTradeshowHeight.constant = 70
                    self.eventsAndTradeshowView.isHidden = false
                    
                    self.newsAndMediaViewHeight.constant = 0
                    self.newsAndMediaImage.image = UIImage(named: "icons8-plus-math-48");
                    self.newsView.isHidden = true
                    
                    
                    DispatchQueue.main.async {
                        UIView.animate(withDuration: 0.4, animations: {

                        })
                    }
                }
            }
            else {
                DispatchQueue.main.async {
    //                self.newsViewCollapse = false
                    self.eventsAndTradeshowHeight.constant = 0
                    self.eventsAndTradeshowView.isHidden = true
                    
                    

                }
                DispatchQueue.main.async {
self.eventAndTradeshowImage.image = UIImage(named: "icons8-plus-math-48")
                }
            }
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.4, animations: {
                    self.view.layoutIfNeeded()
                })
            }
        }
    @IBAction func international(_ sender: Any) {
        
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "InternationalEventsViewController") as! InternationalEventsViewController
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
////        appDelegate.drawerContainer?.centerViewController = redViewController
//        appDelegate.drawerContainer?.setCenterView(redViewController, withFullCloseAnimation: true, completion: nil)
//
//        appDelegate.drawerContainer?.closeDrawer(animated: true, completion: nil)
//        DispatchQueue.main.async {
//            appDelegate.drawerContainer?.rightDrawerViewController = nil
//        }
        navigationController?.pushViewController(redViewController, animated: true)
        
        
    }
    @IBAction func national(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "NationalSignatureViewController") as! NationalSignatureViewController
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func circular(_ sender: Any) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "CircularMemberViewController") as! CircularMemberViewController
        

        DispatchQueue.main.async {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func research(_ sender: Any) {
         let vc = storyboard?.instantiateViewController(withIdentifier: "StatisticsViewController") as! StatisticsViewController
        
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
//    @IBAction func home(_ sender: Any) {
//
//    }
    @IBAction func helpDesk(_ sender: Any) {
        
        if CheckInternet.isConnectedToNetwork() == true {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "HelpDeskViewController") as! HelpDeskViewController
            obj.urlString = "https://gjepc.org/helpdesk/index.php"
            obj.headingLbl = "HELPDESK"
            DispatchQueue.main.async {
                self.navigationController?.pushViewController(obj, animated: true)
            }
        }else{
            
            let alert = UIAlertController(title: "", message: "Please Check Your Internet Connection", preferredStyle: UIAlertController.Style.alert)
            self.present(alert, animated: true, completion: nil)
            let when = DispatchTime.now() + 3
            DispatchQueue.main.asyncAfter(deadline: when){
                alert.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func homeButton(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func metal(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "Metal_CurrancyViewController") as! Metal_CurrancyViewController
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func tollfree(_ sender: Any) {
        let url = NSURL(string: "tel://1800-103-4353")!
               UIApplication.shared.openURL(url as URL)
    }
    
    @IBAction func missedcall(_ sender: Any) {
        let url = NSURL(string: "tel://+91-7208048100")!
               UIApplication.shared.openURL(url as URL)
    }
    
    @IBAction func broucher(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BrochureViewController") as! BrochureViewController
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(vc, animated: true)
        }

    }
    @IBAction func close(_ sender: Any) {
        let viewMenuBack : UIView = self.view!

          UIView.animate(withDuration: 0.3, animations: { () -> Void in
              var frameMenu : CGRect = viewMenuBack.frame
              frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
              viewMenuBack.frame = frameMenu
              viewMenuBack.layoutIfNeeded()
              viewMenuBack.backgroundColor = UIColor.clear
          }, completion: { (finished) -> Void in
              viewMenuBack.removeFromSuperview()

          })

    }
    
    @IBAction func facebook(_ sender: Any) {
        if let linkedinUrl = URL(string: "https://www.facebook.com/GJEPC"),
                  UIApplication.shared.canOpenURL(linkedinUrl) {
                  UIApplication.shared.open(linkedinUrl, options: [:], completionHandler: nil)
               }
    }
    
    @IBAction func linkedin(_ sender: Any) {
        if let linkedinUrl = URL(string: "https://www.linkedin.com/in/sabyaray/"),
           UIApplication.shared.canOpenURL(linkedinUrl) {
           UIApplication.shared.open(linkedinUrl, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func twitter(_ sender: Any) {
        if let linkedinUrl = URL(string: "https://twitter.com/GJEPCIndia"),
           UIApplication.shared.canOpenURL(linkedinUrl) {
           UIApplication.shared.open(linkedinUrl, options: [:], completionHandler: nil)
        }
    }
    
    
    @IBAction func instagram(_ sender: Any) {
       if let linkedinUrl = URL(string: "https://www.instagram.com/gjepcindia/"),
           UIApplication.shared.canOpenURL(linkedinUrl) {
           UIApplication.shared.open(linkedinUrl, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func home(_ sender: Any) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func solitare(_ sender: Any) {
        
        if CheckInternet.isConnectedToNetwork() == true {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "HelpDeskViewController") as! HelpDeskViewController
            obj.urlString = "https://gjepc.org/solitaire/"
            obj.headingLbl = "SOLITARE INTERNATIONAL"
            DispatchQueue.main.async {
                self.navigationController?.pushViewController(obj, animated: true)
            }
        }else{
            
            let alert = UIAlertController(title: "", message: "Please Check Your Internet Connection", preferredStyle: UIAlertController.Style.alert)
            self.present(alert, animated: true, completion: nil)
            let when = DispatchTime.now() + 3
            DispatchQueue.main.asyncAfter(deadline: when){
                alert.dismiss(animated: true, completion: nil)
            }
        }
    }
    @IBAction func statistics(_ sender: Any) {
            
        if CheckInternet.isConnectedToNetwork() == true {
            let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
                   nextVC!.headingtitle = "RESEARCH AND STATISTICS";
                   
                   self.navigationController?.pushViewController(nextVC!, animated: true)
        }else{
            
            let alert = UIAlertController(title: "", message: "Please Check Your Internet Connection", preferredStyle: UIAlertController.Style.alert)
            self.present(alert, animated: true, completion: nil)
            let when = DispatchTime.now() + 3
            DispatchQueue.main.asyncAfter(deadline: when){
                alert.dismiss(animated: true, completion: nil)
            }

        }
            
        }
    @IBAction func iijsPremiere(_ sender: Any) {
        type = "domestic"
        if CheckInternet.isConnectedToNetwork() == true {
            DispatchQueue.main.async {
                self.loadActivityIndicator()
            }
            
            iijstapped = true
            signaturetapped = false
            igemstapped = false
            let params = ["event":"iijs"]
            sendTitleName = "IIJS PREMIERE 2020"
            requestServer.connectToServer(myUrl: checkEventStatusUrl!, params: params as AnyObject)
        } else {
            let alert = UIAlertController(title: "", message: "Please Check Your Internet Connection", preferredStyle: UIAlertController.Style.alert)
            self.present(alert, animated: true, completion: nil)
            let when = DispatchTime.now() + 3
            DispatchQueue.main.asyncAfter(deadline: when){
                alert.dismiss(animated: true, completion: nil)
            }
        }
    }
    @IBAction func iijsSignature(_ sender: Any) {
        type = "domestic"
        signaturetapped = true
                           if CheckInternet.isConnectedToNetwork() == true {
                               DispatchQueue.main.async {
                                   self.loadActivityIndicator()
                               }

                               let params = ["event":"signature"]
                               sendTitleName = "IIJS SIGNATURE 2020"
                               
                               self.requestServer.connectToServer(myUrl: checkEventStatusUrl!, params: params as AnyObject)
                           }
                           else {
                               let alert = UIAlertController(title: "", message: "Please Check Your Internet Connection", preferredStyle: UIAlertController.Style.alert)
                               self.present(alert, animated: true, completion: nil)
                               let when = DispatchTime.now() + 3
                               DispatchQueue.main.asyncAfter(deadline: when){
                                   alert.dismiss(animated: true, completion: nil)
                               }
                           }
    }
    
    @IBAction func igjme(_ sender: Any) {
         type = "domestic"
        if CheckInternet.isConnectedToNetwork() == true {
                          DispatchQueue.main.async {
                              self.loadActivityIndicator()
                          }
                          igemstapped = true
                          signaturetapped = false
                          iijstapped = false
                          sendTitleName = "IGJME 2020"
                          let params = ["event": "igjme"]
                          requestServer.connectToServer(myUrl: checkEventStatusUrl!, params: params as AnyObject)
                      } else {
                          let alert = UIAlertController(title: "", message: "Please Check Your Internet Connection", preferredStyle: UIAlertController.Style.alert)
                          self.present(alert, animated: true, completion: nil)
                          let when = DispatchTime.now() + 3
                          DispatchQueue.main.asyncAfter(deadline: when){
                              alert.dismiss(animated: true, completion: nil)
                          }
                      }
    }
    
    @IBAction func circulars(_ sender: Any) {
        if CheckInternet.isConnectedToNetwork() == true{
            let vc = storyboard?.instantiateViewController(withIdentifier: "CircularMemberViewController") as! CircularMemberViewController
            navigationController?.pushViewController(vc, animated: true)
        }
        else{
            let alert = UIAlertController(title: "", message: "Please Check Your Internet Connection", preferredStyle: UIAlertController.Style.alert)
            
             self.present(alert, animated: true, completion: nil)
            let when = DispatchTime.now()+2
            DispatchQueue.main.asyncAfter(deadline: when){
                alert.dismiss(animated: true, completion: nil)
            }
            
           
        }
        
        
    }
    
    
    @IBAction func organisation(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "OurStructureViewController") as! OurStructureViewController
                   navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func ourStruction(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "StructureViewController") as! StructureViewController
                   navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func vissionAndMission(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "VissionAndMissionViewController") as! VissionAndMissionViewController
                          navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func subCommittee(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "SubCommitteViewController") as! SubCommitteViewController
                                 navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func gii(_ sender: Any) {
//        type = "labs"
//        labno = 1
////        self.tableViewMenuArray.removeAllObjects()
//        callUrls = laboratoriesUrl
//        let params = [""]
//        requestToServer.connectToServer(myUrl: callUrls as URL, params: params as AnyObject)
        let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
        nextVC!.headingtitle = "GEMMOLIJICAL INSTITUTE OF INDIA";
        
        self.navigationController?.pushViewController(nextVC!, animated: true)
       
    }
    
    @IBAction func gtl(_ sender: Any) {
//        type = "labs"
//                labno = 2
//        //        self.tableViewMenuArray.removeAllObjects()
//                callUrls = laboratoriesUrl
//                let params = [""]
//                requestToServer.connectToServer(myUrl: callUrls as URL, params: params as AnyObject)
        let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
        nextVC!.headingtitle = "GEM TESTING LABORATORY";
        
        self.navigationController?.pushViewController(nextVC!, animated: true)
    }
    @IBAction func idi(_ sender: Any) {
//        type = "labs"
//                labno = 0
//        //        self.tableViewMenuArray.removeAllObjects()
//                callUrls = laboratoriesUrl
//                let params = [""]
//                requestToServer.connectToServer(myUrl: callUrls as URL, params: params as AnyObject)
        let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
        nextVC!.headingtitle = "INDIAN GEMMOLOGICAL INSTITUTE";
        
        self.navigationController?.pushViewController(nextVC!, animated: true)
    }
    
    @IBAction func gigtl(_ sender: Any) {
//        type = "labs"
//                      labno = 3
//              //        self.tableViewMenuArray.removeAllObjects()
//                      callUrls = laboratoriesUrl
//                      let params = [""]
//                      requestToServer.connectToServer(myUrl: callUrls as URL, params: params as AnyObject)
        let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
        nextVC!.headingtitle = "IGI-GTL";
        
        self.navigationController?.pushViewController(nextVC!, animated: true)
    }
    
    @IBAction func aatmanJewellery(_ sender: Any) {
        let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
        nextVC!.headingtitle = "AATMAN JEWELLERY TREND BOOK 2020";
        
        self.navigationController?.pushViewController(nextVC!, animated: true)
    }
    
    @IBAction func designWorkshop(_ sender: Any) {
        let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
               nextVC!.headingtitle = "DESIGN WORKSHOP";
               
               self.navigationController?.pushViewController(nextVC!, animated: true)
        
    }
    @IBAction func designInspiration(_ sender: Any) {
        let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
                      nextVC!.headingtitle = "DESIGN INSPIRATION";
                      
                      self.navigationController?.pushViewController(nextVC!, animated: true)
    }
    
    @IBAction func artisanAwards(_ sender: Any) {
        let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
                            nextVC!.headingtitle = "ARTISAN AWARDS";
                            
                            self.navigationController?.pushViewController(nextVC!, animated: true)
    }
    @IBAction func indiaGoldAndJewellrySummit(_ sender: Any) {
        let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
                            nextVC!.headingtitle = "INDIA GOLD AND JEWELLERY SUMMIT";
                            
                            self.navigationController?.pushViewController(nextVC!, animated: true)
    }
    
    @IBAction func gemAndJewelleryConclave(_ sender: Any) {
        let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
                            nextVC!.headingtitle = "GEMS & JEWELLERY CONCLAVE";
                            
                            self.navigationController?.pushViewController(nextVC!, animated: true)
    }
    @IBAction func indiaGemAndJewelleryAwards(_ sender: Any) {
        let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
                            nextVC!.headingtitle = "INDIA GEM & JEWELLERY AWARDS";
                            
                            self.navigationController?.pushViewController(nextVC!, animated: true)
    }
    
    @IBAction func virtualBSM(_ sender: Any) {
        let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
        nextVC!.headingtitle = "VIRTUAL BSM";
        
        self.navigationController?.pushViewController(nextVC!, animated: true)
        
    }
    @IBAction func indiaPavilions(_ sender: Any) {
        let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
        nextVC!.headingtitle = "INDIA PAVILIONS";
        
        self.navigationController?.pushViewController(nextVC!, animated: true)
        
    }
    @IBAction func internationalGJshow(_ sender: Any) {
        let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
        nextVC!.headingtitle = "INTERNATIONAL GEM & JEWELLERY SHOW";
        
        self.navigationController?.pushViewController(nextVC!, animated: true)
        
    }
    @IBAction func buyersSellersMeet(_ sender: Any) {
        let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
        nextVC!.headingtitle = "BUYER-SELLER MEETS";
        
        self.navigationController?.pushViewController(nextVC!, animated: true)
        
    }
    
    @IBAction func msme(_ sender: Any) {
       
        if microMemView.isHidden == true{
            microMemView.isHidden = false
                   govrnAsisView.isHidden = false
                   udyamView.isHidden=false
                   msmeSchemeView.isHidden=false
                   skillDeveView.isHidden=false
                   msmeDiView.isHidden = false
                   notificationView.isHidden=false
            slidemenuHeight.constant = 980
            microMemHeight.constant = 40
            govrnAsisHeight.constant = 40
            udyamHeight.constant = 40
            msmeSchemeHeight.constant = 40
            skillHeight.constant = 40
            msmeDiHeight.constant = 40
            notificationHeight.constant = 40
             self.msmeSchemeImage.image = UIImage(named: "icons8-plus-math-48")
            self.gjepcImage.image = UIImage(named: "icons8-plus-math-48")
            self.businessEventsImage.image = UIImage(named: "icons8-plus-math-48")
            self.educationImage.image = UIImage(named: "icons8-plus-math-48")
            self.labsImage.image = UIImage(named: "icons8-plus-math-48")
             self.govnAsisImage.image = UIImage(named: "icons8-plus-math-48")
            self.msmeImage.image = UIImage(named: "icons8-minus-48")
             DispatchQueue.main.async {
                                                                        UIView.animate(withDuration: 0.4, animations: {

                                                                            self.leardershipSubview.isHidden = true
                                                                            self.designInitiative.isHidden = true
                                                                            self.conferences.isHidden = true
                                                                            self.educationSubView.isHidden = true
                                                                            self.designInitiativeView.isHidden = true
                                                                             self.conferenceView.isHidden = true
                                                                             self.eventsView.isHidden = true
                                                                             self.internationalTradeView.isHidden = true
                                                                             self.domesticTradeshowView.isHidden = true
                                                                            self.labsSubView.isHidden = true
                                                                            self.govnAssisSubView.isHidden = true
//                                                                            self.msmeSchemeSubView.isHidden = true
                                                                            self.gjepcHeight.constant = 0
                                                                            self.leaderShipHeight.constant = 0
                                                                            self.ourstuctureHeight.constant = 0
                                                                            self.domesticTradeshowHeight.constant = 0
                                                                            self.domesticSubtradeshowHeight.constant = 0
                                                                            self.internationalTradeshowHeight.constant = 0
                                                                            self.internationalSubtradeshowHeight.constant = 0
                                                                            self.eventsHeight.constant = 0
                                                                            self.designInitiativeHeight.constant = 0
                                                                            self.designInitiativeSubHeight.constant = 0
                                                                           self.conferenceHeight.constant = 0
                                                                            self.conferencesSubHeight.constant = 0
                                                                            self.indiaGemAndJewelHeight.constant = 0
                                                                            self.educationSubHeight.constant = 0
                                                                            self.labsSubHeight.constant = 0
                                                                            self.govnAssisSubViewHeight.constant = 0

                                                                            
                                                                            
                                                                        })
                                                                    }
            
            
            
        }
        else{
            self.slidemenuHeight.constant = 700
            govnAssisSubViewHeight.constant = 0
            msmeSubViewHeight.constant = 0
            msmeSchemeSubView.isHidden = true
            govnAssisSubView.isHidden = true
            microMemView.isHidden = true
            govrnAsisView.isHidden = true
            udyamView.isHidden=true
            msmeSchemeView.isHidden=true
            skillDeveView.isHidden=true
            msmeDiView.isHidden = true
            notificationView.isHidden=true
            microMemHeight.constant = 0
            govrnAsisHeight.constant = 0
            udyamHeight.constant = 0
            msmeSchemeHeight.constant = 0
            skillHeight.constant = 0
            msmeDiHeight.constant = 0
            notificationHeight.constant = 0
            self.msmeImage.image = UIImage(named: "icons8-plus-math-48")
        }
       
        
    }
    
    @IBAction func msmeScheme(_ sender: Any) {
        if msmeSubViewHeight.constant == 0{
            msmeSchemeImage.image = UIImage(named: "icons8-minus-48")
            govnAsisImage.image = UIImage(named: "icons8-plus-math-48")
            
             msmeSubViewHeight.constant = 100
            govnAssisSubViewHeight.constant = 0
            slidemenuHeight.constant = 1080
            msmeSchemeSubView.isHidden = false
        }
        else{
            msmeSchemeImage.image = UIImage(named: "icons8-plus-math-48")
            slidemenuHeight.constant = 980
            msmeSubViewHeight.constant = 0
           msmeSchemeSubView.isHidden = true
        }
    }
   
    @IBAction func govnAsis(_ sender: Any) {
       if govnAssisSubViewHeight.constant == 0{
        govnAsisImage.image = UIImage(named: "icons8-minus-48")
        msmeSchemeImage.image = UIImage(named: "icons8-plus-math-48")
        slidemenuHeight.constant = 1050
        msmeSubViewHeight.constant = 0
             govnAssisSubViewHeight.constant = 70
            govnAssisSubView.isHidden = false
        }
        else{
        govnAsisImage.image = UIImage(named: "icons8-plus-math-48")
        slidemenuHeight.constant = 980
           govnAssisSubViewHeight.constant = 0
            govnAssisSubView.isHidden = true
        }
    }
    
    @IBAction func microMembership(_ sender: Any) {
        let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
               nextVC!.headingtitle = "MICRO MEMBERSHIP";
               
               self.navigationController?.pushViewController(nextVC!, animated: true)
        
    }
    @IBAction func centralGovnScheme(_ sender: Any) {
        let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
        nextVC!.headingtitle = "CENTRAL GOVERNMENT SCHEME";
        
        self.navigationController?.pushViewController(nextVC!, animated: true)
    }
    
    @IBAction func stateGovnScheme(_ sender: Any) {
        let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
        nextVC!.headingtitle = "STATE GOVERNMENT SCHEME";
        
        self.navigationController?.pushViewController(nextVC!, animated: true)
    }
    
    @IBAction func udyamRegistration(_ sender: Any) {
        let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
               nextVC!.headingtitle = "UDYAM REGISTRATION";
               
               self.navigationController?.pushViewController(nextVC!, animated: true)
    }
    @IBAction func msmeDefinition(_ sender: Any) {
        let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
                      nextVC!.headingtitle = "MSME DEFINITION";
                      
                      self.navigationController?.pushViewController(nextVC!, animated: true)
    }
    
    @IBAction func existingSchemes(_ sender: Any) {
        let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
                      nextVC!.headingtitle = "EXISTING SCHEMES";
                      
                      self.navigationController?.pushViewController(nextVC!, animated: true)
    }
    
    @IBAction func newMsmeSchemes(_ sender: Any) {
        let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
                      nextVC!.headingtitle = "NEW MSME SCHEMES";
                      
                      self.navigationController?.pushViewController(nextVC!, animated: true)
    }
    
    @IBAction func skillDeve(_ sender: Any) {
        let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
                      nextVC!.headingtitle = "SKILL DEVELOPMENT & TRAINING FOR ARTISANS";
                      
                      self.navigationController?.pushViewController(nextVC!, animated: true)
    }
    
    @IBAction func msmeDicontact(_ sender: Any) {
        let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
                      nextVC!.headingtitle = "MSME DI CONTACTS";
                      
                      self.navigationController?.pushViewController(nextVC!, animated: true)
    }
    
    @IBAction func notification(_ sender: Any) {
        let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
                      nextVC!.headingtitle = "NOTIFICATIONS/CIRCULARS";
                      
                      self.navigationController?.pushViewController(nextVC!, animated: true)
        
    }
    
    @IBAction func iijsVirtual(_ sender: Any) {
        
        if CheckInternet.isConnectedToNetwork() == true{
            let nextVC = storyboard?.instantiateViewController(withIdentifier: "VisitorRegistrationVC") as? VisitorRegistrationVC
                   nextVC!.headingtitle = "IIJS VIRTUAL";
                   
                   self.navigationController?.pushViewController(nextVC!, animated: true)
        }
        else{
            let alert = UIAlertController(title: "", message: "Please Check Your Internet Connection", preferredStyle: UIAlertController.Style.alert)
            
             self.present(alert, animated: true, completion: nil)
            let when = DispatchTime.now()+2
            DispatchQueue.main.asyncAfter(deadline: when){
                alert.dismiss(animated: true, completion: nil)
            }
            
           
        }


        
        
       
        
        
    }
    
}
