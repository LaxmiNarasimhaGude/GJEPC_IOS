//
//  StructureViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 09/09/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit

class StructureViewController: UIViewController {

    @IBOutlet weak var ourStructureScrollView: UIScrollView!
    @IBOutlet weak var structureImageView: UIView!
    @IBOutlet weak var regional: UIView!
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    @IBOutlet weak var regionalView: UIView!
    @IBOutlet weak var regionalImage: UIImageView!
    @IBOutlet weak var regionalViewHeight: NSLayoutConstraint!
    @IBOutlet weak var suruchiImage: UIImageView!
    @IBOutlet weak var suryanarayanImage: UIImageView!
    @IBOutlet weak var sanjayImage: UIImageView!
    @IBOutlet weak var dugalImage: UIImageView!
    @IBOutlet weak var jillaImage: UIImageView!
    @IBOutlet weak var rashmiAroraImage: UIImageView!
    @IBOutlet weak var dharmeshImage: UIImageView!
    @IBOutlet weak var jitraniImage: UIImageView!
    @IBOutlet weak var vikrantImage: UIImageView!
    @IBOutlet weak var mithileshImage: UIImageView!
    @IBOutlet weak var krishnaImage: UIImageView!
    @IBOutlet weak var shamalImage: UIImageView!
    @IBOutlet weak var bijalImage: UIImageView!
    @IBOutlet weak var kavithaImage: UIImageView!
    @IBOutlet weak var shridharImage: UIImageView!
    @IBOutlet weak var dollyImage: UIImageView!
    @IBOutlet weak var sandeepSharmaImage: UIImageView!
    @IBOutlet weak var sabyasachiRayImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        structureImageView.backgroundColor = UIColor.white
        structureImageView.layer.cornerRadius = 3.0
        structureImageView.layer.masksToBounds = false
        structureImageView.layer.shadowOffset = CGSize(width: 0, height: 0)
        structureImageView.layer.shadowOpacity = 0.3
        
        regional.layer.cornerRadius = 5.0
        
        viewHeight.constant = 1900
        
        regionalViewHeight.constant = 0
        regionalView.isHidden = true

        self.regionalImage.image = UIImage(named: "icons8-plus-24")
        imageRoundRect(imageRect: sabyasachiRayImage)
        imageRoundRect(imageRect: sandeepSharmaImage)
        imageRoundRect(imageRect: dollyImage)
        imageRoundRect(imageRect: shridharImage)
        imageRoundRect(imageRect: kavithaImage)
        imageRoundRect(imageRect: bijalImage)
        imageRoundRect(imageRect: shamalImage)
        imageRoundRect(imageRect: krishnaImage)
        imageRoundRect(imageRect: mithileshImage)
        imageRoundRect(imageRect: vikrantImage)
        imageRoundRect(imageRect: jitraniImage)
        imageRoundRect(imageRect: dharmeshImage)
        imageRoundRect(imageRect: rashmiAroraImage)
        imageRoundRect(imageRect: jillaImage)
        imageRoundRect(imageRect: dugalImage)
        imageRoundRect(imageRect: sanjayImage)
        imageRoundRect(imageRect: suryanarayanImage)
        imageRoundRect(imageRect: suruchiImage)
        
    }
    func imageRoundRect(imageRect: UIImageView)  {
     imageRect.clipsToBounds = true
     imageRect.layer.cornerRadius = 10
     imageRect.layer.borderColor = UIColor.black.cgColor
     imageRect.layer.borderWidth = 1
    }

    @IBAction func regionalDirectors(_ sender: Any) {
            
            if regionalViewHeight.constant == 0{
                ourStructureScrollView.setContentOffset(CGPoint(x: 0, y: 1800), animated: true)
                viewHeight.constant = 2500
                 self.regionalImage.image = UIImage(named: "icons8-minus-24")
                self.regionalViewHeight.constant = 700
                
                self.regionalView.isHidden = false
    //            self.internationalTradeshowView.isHidden = false
                
//                viewHeight.constant = 1350
//                connectingGovnHeight.constant = 0
//                upholdingHeight.constant = 0
//                spreadingHeight.constant = 0
//                innovationHeight.constant = 0
//                healthHeight.constant = 0
//                payingbackHeight.constant = 0
//                connectingGovnView.isHidden = true
//                upholdingView.isHidden = true
//                spreadingView.isHidden = true
//                innovationView.isHidden = true
//                healthView.isHidden = true
//                payingBackView.isHidden = true
            }
            else{
                                    
                                    DispatchQueue.main.async {
                                        self.viewHeight.constant = 1900
                        self.regionalViewHeight.constant = 0
                        self.regionalImage.image = UIImage(named: "icons8-plus-24")
                        self.regionalView.isHidden = true
            
                                        
                                        

                                    }
                                    
                                }
                    DispatchQueue.main.async {
                                   UIView.animate(withDuration: 0.4, animations: {
                                       self.view.layoutIfNeeded()
                                   })
                               }
            
        }
    
    @IBAction func back(_ sender: Any) {
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
}
