//
//  RequestToServer.swift
//  GJEPC
//
//  Created by Kwebmaker on 07/01/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import Foundation

protocol dataReceivedFromServerDelegate {
    func dataReceivedFromServer(data: NSDictionary, url : URL)
}

class RequestToServer  {
    
    var delegate : dataReceivedFromServerDelegate!
    var responsedata : NSDictionary = [:]
    var custom : Custom_ObjectsViewController!
    var callUrls : NSURL!
    
    func connectToServer(myUrl : URL, params : AnyObject) {
        
        let request = NSMutableURLRequest(url:myUrl as URL)
        request.httpMethod = "POST";
        request.httpBody = try! JSONSerialization.data(withJSONObject: params, options: [])
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        //create the session object
        let session = URLSession.shared
        //create dataTask using the session object to send data to the server
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            guard error == nil else {
                return
            }
            guard let data = data else {
                return
            }
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String : AnyObject] {
                   print(json)
//                    if json != nil{
//
//                    }
                    if json.count > 0 {
                        self.responsedata = json as NSDictionary
                    }
                    print("Here: \(self.responsedata)")
                    self.delegate.dataReceivedFromServer(data: self.responsedata, url : myUrl)
                }
            } catch let error {
                print(error.localizedDescription)
            }
        })
        task.resume()
    }
    
    
    
    func getDataFromServer(myGetUrl : URL) {
        let url = myGetUrl
        
        //create the session object
        let session = URLSession.shared
        
        //now create the URLRequest object using the url object
        let request = URLRequest(url: url )
        
        //create dataTask using the session object to send data to the server
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    print(json)
                    if json.count > 0 {
                    self.responsedata = json as NSDictionary
                    }
                    print("Here: \(self.responsedata)")
                    self.delegate.dataReceivedFromServer(data: self.responsedata, url : myGetUrl)
                }
            } catch let error {
                print(error.localizedDescription)
            }
        })
        task.resume()
    }
}
// End of class RequestToServer

