//
//  AuditTableViewCell.swift
//  GJEPC
//
//  Created by Kwebmaker on 12/09/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit

class AuditTableViewCell: UITableViewCell {

    @IBOutlet weak var designationLabel: UILabel!
    @IBOutlet weak var companyNameLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }

}
