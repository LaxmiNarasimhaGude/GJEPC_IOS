//
//  BrochureWebViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 25/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
class BrochureWebViewController: UIViewController,UIWebViewDelegate{

    @IBOutlet weak var brochureWebView: UIWebView!
    
    var activityLoader : MBProgressHUD? = nil
    var pdf = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        brochureWebView.backgroundColor = UIColor.white
        brochureWebView.delegate = self
        loadActivityIndicator()
      let url = URL(string: "https://www.gjepc.org/admin/brochure/"+"\(pdf)")
        
        let request = URLRequest(url: url!)
        
        brochureWebView.loadRequest(request)
    }
    
    func loadActivityIndicator()
    {
        activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
        activityLoader?.label.text = "Loading";
        activityLoader?.detailsLabel.text = "Please Wait";
        activityLoader?.isUserInteractionEnabled = false;
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
              DispatchQueue.main.async {
                  self.activityLoader?.hide(animated: true)
              }
          }

    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}
