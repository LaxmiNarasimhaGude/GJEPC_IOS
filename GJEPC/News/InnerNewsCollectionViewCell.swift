//
//  InnerNewsCollectionViewCell.swift
//  GJEPC
//
//  Created by Kwebmaker on 14/01/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit

class InnerNewsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var defaultImage: UIImageView!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var newsDescriptionLbl: UILabel!
    
    var shimmer = ""
    
    override func awakeFromNib() {
           super.awakeFromNib()
        
        
            dateLbl.startShimmeringEffect()
            newsImage.startShimmeringEffect()
            newsDescriptionLbl.startShimmeringEffect()
            defaultImage.startShimmeringEffect()
        
        
           
       }
    func stopanimating()  {
       dateLbl.startShimmeringEffect()
       newsImage.startShimmeringEffect()
       newsDescriptionLbl.startShimmeringEffect()
       defaultImage.startShimmeringEffect()
    }
}
