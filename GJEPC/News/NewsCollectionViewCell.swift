//
//  NewsCollectionViewCell.swift
//  GJEPC
//
//  Created by Kwebmaker on 14/01/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit

class NewsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    
}
