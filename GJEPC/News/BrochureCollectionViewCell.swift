//
//  BrochureCollectionViewCell.swift
//  GJEPC
//
//  Created by Kwebmaker on 25/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit

class BrochureCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
}
