//
//  NewsWebViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 11/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
class NewsWebViewController: UIViewController,UIWebViewDelegate {

    @IBOutlet weak var newsWebview: UIWebView!
    
     var dictUpload = NSDictionary()
    var activityLoader:MBProgressHUD!
    var newstitle : String = ""
     var loadnewsUrl : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        newsWebview.backgroundColor = UIColor.white
        newsWebview.delegate = self
        if newstitle == "Updates"{
            DispatchQueue.main.async {
                self.loadActivityIndicator()
            }
            //  webview.delegate = self
//            newsLabel.text = newstitle
            let str = self.dictUpload.object(forKey: "article_url") as? String ?? ""
            let url = URL.init(string: str)
            newsWebview.loadRequest(URLRequest(url: url!))
        }
       else if newstitle == "newsletter"{
//              newsLabel.text = newstitle
              
           loadnewsUrl = self.dictUpload.object(forKey: "html_files") as? String ?? ""
              DispatchQueue.main.async {
                  self.loadActivityIndicator()
              }
              let replacedString = loadnewsUrl.replacingOccurrences(of: "\r\n", with: "")
              
          let request = URLRequest(url: URL(string: replacedString)!)
              
              
              newsWebview.loadRequest(request)
          }
        else if newstitle == "epatrika" {
//                   newsLabel.text = newstitle
             loadnewsUrl =    self.dictUpload.object(forKey: "html_files") as? String ?? ""
                   let url1 : String =  loadepatrikaUrl
                   let url2 :  String = loadnewsUrl
                   let url3 = url1 + url2
                   let replacedString = url3.replacingOccurrences(of: " ", with: "%20")
                   let url = NSURL(string: replacedString)
                   DispatchQueue.main.async {
                       self.loadActivityIndicator()
                   }
                   newsWebview.loadRequest(URLRequest(url: url! as URL) as URLRequest)
                   //  webview.delegate = self
               }
        
    }
    func loadActivityIndicator()
    {
        activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
        activityLoader?.label.text = "Loading";
        activityLoader?.detailsLabel.text = "Please Wait";
        activityLoader?.isUserInteractionEnabled = false;
    }

    @IBAction func backBtn(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        DispatchQueue.main.async {
            self.activityLoader?.hide(animated: true)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = true
    }
}
