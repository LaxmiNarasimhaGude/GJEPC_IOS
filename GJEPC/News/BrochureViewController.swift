//
//  BrochureViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 25/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD

class BrochureViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,dataReceivedFromServerDelegate {
   
    @IBOutlet weak var broucherCollectionView: UICollectionView!
    
    var customArray : NSMutableArray = []
    var activityLoader : MBProgressHUD? = nil
    var callUrls : URL!
    var requestToServer = RequestToServer()
     var temporaryArray : NSArray = []
     var tableViewArrayForNews : NSMutableArray = []

    override func viewDidLoad() {
        super.viewDidLoad()

       broucherCollectionView.delegate = self
       broucherCollectionView.dataSource = self
       requestToServer.delegate = self
    }
    override func viewDidAppear(_ animated: Bool) {
           
           if (self.customArray.count == 0) {
               
               checkForInternet()
           }
       }
    
    func checkForInternet() {
           if CheckInternet.isConnectedToNetwork() == true
           {
               print("internet available")
               self.broucherCollectionView.isHidden = false
               DispatchQueue.main.async {
                       self.loadActivityIndicator()
               }
               onFirstTimeLoad()
           }
           else
           {
               print("no internet")
               broucherCollectionView.isHidden = true
               activityLoader?.hide(animated: true)
           }
       }
    func loadActivityIndicator()
    {
        activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
        activityLoader?.label.text = "Loading";
        activityLoader?.detailsLabel.text = "Please Wait";
        activityLoader?.isUserInteractionEnabled = false;
    }
     func onFirstTimeLoad() {
               customArray = [];
               callUrls = brochureUrl
               let params = [ ""]
    //           self.loading = true;
    //              loadNew = true;
               requestToServer.connectToServer(myUrl: callUrls as URL, params: params as AnyObject)
           }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tableViewArrayForNews.count
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
         let newsContentvc = self.storyboard?.instantiateViewController(withIdentifier: "BrochureWebViewController") as! BrochureWebViewController
//                let htmlFile =    (((self.customArray.object(at: indexPath.section) as! NSDictionary).object(forKey: "Result") as! NSArray).object(at: indexPath.row) as! NSDictionary)["upload_brochure"] as! String
        let htmlFile = ((self.tableViewArrayForNews.object(at: indexPath.row) as AnyObject).object(forKey: "upload_brochure")as! String)
                newsContentvc.pdf  = htmlFile
        
                DispatchQueue.main.async {
                  self.present(newsContentvc, animated: true, completion: nil)
                }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BrochureCollectionViewCell", for: indexPath) as! BrochureCollectionViewCell
        
         cell.contentView.layer.cornerRadius = 2.0
         cell.contentView.layer.borderWidth = 1.0
         cell.contentView.layer.borderColor = UIColor.clear.cgColor
         cell.contentView.layer.masksToBounds = true
        
         cell.layer.backgroundColor = UIColor.white.cgColor
         cell.layer.shadowColor = UIColor.lightGray.cgColor
         cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)//CGSizeMake(0, 2.0);
         cell.layer.shadowRadius = 2.0
         cell.layer.shadowOpacity = 1.0
         cell.layer.masksToBounds = false
         cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
        
        
        cell.titleLabel.text = ((self.tableViewArrayForNews.object(at: indexPath.row) as AnyObject).object(forKey: "name")as! String)
        
        let dateFormatterGet = DateFormatter()
                   dateFormatterGet.dateFormat = "yyyy-MM-dd"

                   let dateFormatterPrint = DateFormatter()
                   dateFormatterPrint.dateFormat = "dd MMM, yyyy"

                   let date: NSDate? = dateFormatterGet.date(from: "\((self.tableViewArrayForNews.object(at: indexPath.row) as AnyObject).object(forKey: "post_date")as! String)") as NSDate?
                   
                   
                   cell.dateLabel.text = "\(dateFormatterPrint.string(from: date! as Date))"
        
        
//        cell.dateLabel.text = ((self.tableViewArrayForNews.object(at: indexPath.row) as AnyObject).object(forKey: "name")as! String)
        
        
        return cell
    }
    func dataReceivedFromServer(data: NSDictionary, url: URL) {
    //    loading = false;
        if ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "true" {
            temporaryArray = ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "Result") as! NSArray)
            print(temporaryArray)
            print(tableViewArrayForNews)
    //        let arrayCount = self.tableViewArrayForNews.count
    //        var indexArray : [IndexPath] = [IndexPath]()
            if temporaryArray.count > 0 {
                
    //            if self.tableViewArrayForNews.count != 0 && loadNew == false  && isSearching == false{
    //                for i in 0..<temporaryArray.count {
    //                    indexArray.append(IndexPath(row: arrayCount + i, section: 0));
    //                    self.tableViewArrayForNews.add(temporaryArray[i]);
    //                    }
    //            }
                   
                        tableViewArrayForNews = (temporaryArray.mutableCopy() as! NSMutableArray)
                        customArray = [];
                        for i in 0..<tableViewArrayForNews.count {
                            
                            let yearString = ((tableViewArrayForNews.object(at: i) as! NSDictionary).value(forKey: "year") as! String)
                            if !customArray.contains(yearString) {
                                customArray.add(yearString)
                            }
                        }
                        
                        for i in 0..<customArray.count {
                            let currentKey = (customArray.object(at: i) as! String)
                            let tempArray : NSMutableArray = []
                            for j in 0..<tableViewArrayForNews.count {
                                if ((tableViewArrayForNews.object(at: j) as! NSDictionary).value(forKey: "year") as! String) == currentKey {
                                    let temporaryDict = (self.tableViewArrayForNews.object(at: j) as! NSDictionary)
                                    tempArray.add(temporaryDict.mutableCopy())
                                }
                            }
                            customArray.replaceObject(at: i, with: ["sectionName" : currentKey, "Result" : tempArray])
                        }
                        DispatchQueue.main.async {
    //                        self.contentView.isHidden = false
    //                        self.noDataLabel.isHidden = true
                            self.activityLoader?.hide(animated: true)
                            self.broucherCollectionView.reloadData()
                        }
                    
                }
            else
            {
                
                let alert  = UIAlertController(title: "", message: "No brouchers available", preferredStyle: UIAlertController.Style.alert)
                let action = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) { (action) in
                    self.navigationController?.popViewController(animated: true)
                    
                }
                alert.addAction(action)
                DispatchQueue.main.async {
                    self.activityLoader?.hide(animated: true)
                    self.present(alert, animated: true, completion: nil)
                }
                }
            }
        }
       
}
