//
//  NewsContentViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 10/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit

class NewsContentViewController: UIViewController {

    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var newsLongDescriptionLbl: UITextView!
    @IBOutlet weak var newsShortDescriptionLbl: UILabel!
    @IBOutlet weak var newsDateLbl: UILabel!
    
     var getnewsListDate : String = ""
     var getnewsListName : String = ""
     var getnewsLongDesc : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()

//
//        newsLongDescriptionLbl.font = UIFont(name: "JosefinSans", size: 50)
        
//        self.tabBarController?.tabBar.isHidden = true
        putData()
        newsImage.clipsToBounds = true
        newsImage.layer.cornerRadius = 10.0
        newsImage.layer.masksToBounds = true
        
//        newsLongDescriptionLbl.font = UIFont(descriptor: UIFontDescriptor.init(name: "Josefin Sans", size: 12), size: 12)
        newsLongDescriptionLbl.textColor = UIColor(red: 92/256, green: 85/256, blue: 77/256, alpha: 1)
//        newsShortDescriptionLbl.setCharacterSpacing(1)
//        newsShortDescriptionLbl.setLineSpacing()
//     newsShortDescriptionLbl.labelSpacing(label: newsShortDescriptionLbl, font: "Bold")
        
        
        var myMutableString = NSMutableAttributedString()
              myMutableString = NSMutableAttributedString(string: newsShortDescriptionLbl.text!, attributes: [NSAttributedString.Key.font:UIFont(name: "Gotham-Bold", size: 13.0)!])



                                    
              let style = NSMutableParagraphStyle()
                                 style.lineSpacing = 20
              myMutableString.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: NSRange(location: 0, length: ((newsShortDescriptionLbl.text))!.count))
              newsShortDescriptionLbl.attributedText = myMutableString
        
    }
    
   

    @IBAction func backBtn(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func putData(){
        newsLongDescriptionLbl.isEditable = false
        
        
        
        newsLongDescriptionLbl.text = getnewsLongDesc.htmlString
        newsShortDescriptionLbl.text  = getnewsListName.htmlString
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"

        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd MMM, yyyy"

        let date: NSDate? = dateFormatterGet.date(from: getnewsListDate) as NSDate?
        
        
        
        
        newsDateLbl.text = "\(dateFormatterPrint.string(from: date! as Date))"
        
//         newsLongDescriptionLbl.labelSpacing(label: newsShortDescriptionLbl, font: "Book")
        newsLongDescriptionLbl.setSpacing(1)
        var myMutableString = NSMutableAttributedString()
              myMutableString = NSMutableAttributedString(string: newsLongDescriptionLbl.text!, attributes: [NSAttributedString.Key.font:UIFont(name: "Gotham-Book", size: 12.0)!])




              let style = NSMutableParagraphStyle()
                                 style.lineSpacing = 10
              myMutableString.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: NSRange(location: 0, length: ((newsLongDescriptionLbl.text))!.count))
              newsLongDescriptionLbl.attributedText = myMutableString

    }
}
extension UITextView{
    

    func setSpacing(_ spacing: CGFloat){
       let attributedStr = NSMutableAttributedString(string: self.text ?? "")
       attributedStr.addAttribute(NSAttributedString.Key.kern, value: spacing, range: NSMakeRange(0, attributedStr.length))
       self.attributedText = attributedStr
    }
}
     func spacing(string:String,label:UILabel)  {
    //        organisationHeadingLabel1.setCharacterSpacing(1)
            let attrString = NSMutableAttributedString(string: string)
                        var style = NSMutableParagraphStyle()
                        style.lineSpacing = 5
            attrString.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: NSRange(location: 0, length:label.text!.count))
                        label.attributedText = attrString
        
        }


extension String {
    var html2AttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data,
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription)
            return nil
        }
        
     
    }

}

