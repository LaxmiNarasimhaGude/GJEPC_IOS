//
//  NewsViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 14/01/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
import SDWebImage

@available(iOS 13.0, *)
class NewsViewController: UIViewController,dataReceivedFromServerDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource,UISearchBarDelegate {
    
    
    @IBOutlet weak var notificationcount: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var newscollectionView: UICollectionView!
    @IBOutlet weak var dailyNewsLbl: UIImageView!
    @IBOutlet weak var updatesLbl: UIImageView!
    @IBOutlet weak var newsletterLbl: UIImageView!
    @IBOutlet weak var epatrikaLbl: UIImageView!
    
     var customArray : NSMutableArray = []
     var isChange:Bool = false
     var responseNotificationArray:NSMutableArray = []
    var callUrls : URL!
    var loading = false;
    var loadNew = true;
    var requestToServer = RequestToServer()
    var activityLoader : MBProgressHUD? = nil
    var temporaryArray : NSArray = []
   var tableViewArrayForNews : NSMutableArray = []
     var isSearching = false;
    var replacedImgstring : String = ""
    var tabIndex = 0
    var selectedIndex = 0
    var isOpen = false
    var inSearchMode = false
    var type = "true"
//     var timer = Timer()

    override func viewDidLoad() {
        super.viewDidLoad()
//        startAnimating()
        setView()
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).clearButtonMode = .never

        self.tabBarController?.selectedIndex = 1
        
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        newscollectionView.collectionViewLayout = layout
        if (self.customArray.count == 0) {
                   
                   checkForInternet()
               }
        newscollectionView.delegate = self
        newscollectionView.dataSource = self
         searchBar.delegate = self
        requestToServer.delegate = self
        newscollectionView.keyboardDismissMode = .onDrag
        
        let name = Notification.Name("didReceiveData")
                  NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: name, object: nil)
                  
                  let updateBadgeCount = Notification.Name("updateBadgeCount")
                  NotificationCenter.default.addObserver(self, selector: #selector(updateBadgeCount(_:)), name: updateBadgeCount, object: nil)
        

    }
    @objc func onDidReceiveData(_ notification:Notification) {
        
        // Do something now
        
        if  UserDefaults.standard.value(forKey: "openNotification") != nil
        {
            if(UserDefaults.standard.value(forKey: "openNotification") as! String == "true")
            {
                ///NotiifcationViewController
                
                
                
                
                let viewControllerArray = self.navigationController?.viewControllers
                
                var checkFlag:Bool
                checkFlag = false
                for controller in viewControllerArray! {
                    
                    
                    print(controller)
                    if controller.classForCoder == NotificationViewController.self {
                        checkFlag = true
                        print(controller)
                    }
                    
                }
                
                
                if(checkFlag)
                {
                    
                    NotificationCenter.default.post(name: Notification.Name("updateTable"), object: nil)
                    
                }
                else
                {
                    let view1 = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController
                    
                    let data:[AnyObject]? = UserDefaults.standard.value(forKey: "NotificationList") as? [AnyObject]
                    
                    view1?.dataArray = (UserDefaults.standard.value(forKey: "NotificationList") as? [AnyObject])!
                    
                    view1?.globalArray = data!.reversed()
                    
                    
                    DispatchQueue.main.async {
                        self.navigationController?.pushViewController(view1!, animated: true)
                    }
                }
                
                
                
                
                
                
                
            }
            else
            {
                // getData()
            }
            
        }
        else
        {
            // getData()
            
        }
        
    }

    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
    
                
                
                
                let data:[AnyObject]? = UserDefaults.standard.value(forKey: "NotificationList") as? [AnyObject]
                
                
                
                print("\(UserDefaults.standard.value(forKey: "userSessionBadgeCnt") ?? "0")")
                
        //        DispatchQueue.main.async {
                
                 DispatchQueue.main.async {
                
                    self.notificationcount.isHidden = false
                    
                    let cnt:Int? = UIApplication.shared.applicationIconBadgeNumber
                    if(cnt == 0 || cnt == nil)
                    {
                        DispatchQueue.main.async {
                            self.notificationcount.text = "0"
                        }
                    }
                    else
                    {   DispatchQueue.main.async {
                        self.notificationcount.text = "\(String(cnt!))"
                        }
                    }
                    
                }
                    
                    // UserDefaults.standard.value(forKey: "userSessionBadgeCnt") as? String
                    
                //}
                
                
           
        
    }
    @objc func updateBadgeCount(_ notification:Notification){
        
        
        print("\(UserDefaults.standard.value(forKey: "userSessionBadgeCnt") ?? "0")")
        
        DispatchQueue.main.async {
            
            self.notificationcount.isHidden = false
            
            
            let cnt:Int? = UIApplication.shared.applicationIconBadgeNumber
        
        
            if(cnt == 0 || cnt == nil)
            {
                   DispatchQueue.main.async {
                self.notificationcount.text = "0"
                }
            }
            else
            {   DispatchQueue.main.async {
                self.notificationcount.text = "\(String(cnt!))"
                }
            }
        }
       
        
    }

//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(true)
//
////       setView()
//    }
    func onFirstTimeLoad() {
           customArray = [];
           tableViewArrayForNews = [];
           callUrls = dailyNewsUrl
           let params = [ "start":"0",
                          "limit":"10"]
           self.loading = true;
           loadNew = true;
           requestToServer.connectToServer(myUrl: callUrls as URL, params: params as AnyObject)
       }
   func checkForInternet() {
          if CheckInternet.isConnectedToNetwork() == true
          {
              print("internet available")
              self.newscollectionView.isHidden = false
              DispatchQueue.main.async {
//                  self.loadActivityIndicator()
                let cell = InnerNewsCollectionViewCell()
                cell.stopShimmeringEffect()
                
              }
              onFirstTimeLoad()
          }
          else
          {
              print("no internet")
//              newscollectionView.isHidden = true
//            let cell = InnerNewsCollectionViewCell()
//            self.view.stopShimmeringViewAnimation()
//            cell.stopanimating()
//              activityLoader?.hide(animated: true)
//            stopShimmeringEffect()
          }
      }
//    func loadActivityIndicator()
//    {
//        activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
//        activityLoader?.label.text = "Loading";
//        activityLoader?.detailsLabel.text = "Please Wait";
//        activityLoader?.isUserInteractionEnabled = false;
//    }
    
    @IBAction func menu(_ sender: Any) {
        

        
//        if(!isOpen)
//
//        {
            isOpen = true

            let menuVC = self.storyboard!.instantiateViewController(withIdentifier: "SlideViewController") as! SlideViewController
            self.view.addSubview(menuVC.view)
            self.addChild(menuVC)
            menuVC.view.layoutIfNeeded()

            menuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width-90, height: UIScreen.main.bounds.size.height);

            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width-90, height: UIScreen.main.bounds.size.height);
        }, completion:nil)

//        }else if(isOpen)
//        {
//            isOpen = false
//          let viewMenuBack : UIView = view.subviews.last!
//
//            UIView.animate(withDuration: 0.3, animations: { () -> Void in
//                var frameMenu : CGRect = viewMenuBack.frame
//                frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
//                viewMenuBack.frame = frameMenu
//                viewMenuBack.layoutIfNeeded()
//                viewMenuBack.backgroundColor = UIColor.clear
//            }, completion: { (finished) -> Void in
//                viewMenuBack.removeFromSuperview()
//
//            })
//        }

        
        
        
        
        
        
        
        
        
    }

    

    
        func dataReceivedFromServer(data: NSDictionary, url: URL) {
            loading = false;
            if ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "true" {
                temporaryArray = ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "Result") as! NSArray)
                print(temporaryArray)
                let arrayCount = self.tableViewArrayForNews.count
                var indexArray : [IndexPath] = [IndexPath]()
                if temporaryArray.count > 0 {
                    
                    if self.tableViewArrayForNews.count != 0 && loadNew == false  && isSearching == false{
                        for i in 0..<temporaryArray.count {
                            indexArray.append(IndexPath(row: arrayCount + i, section: 0));
                            self.tableViewArrayForNews.add(temporaryArray[i]);
                        }
                        
                        DispatchQueue.main.async {
//                            self.newsTableView.beginUpdates()
//                            self.newsTableView.insertRows(at: indexArray, with: UITableViewRowAnimation.none)
//                            self.newscollectionView.endUpdates()
                            self.newscollectionView.reloadData()
                             
                        }
                    }
                    else {
                        
                        if self.tableViewArrayForNews.count != 0 && loadNew == false  && isSearching == true{
                            for i in 0..<temporaryArray.count {
                                indexArray.append(IndexPath(row: arrayCount + i, section: 0));
                                self.tableViewArrayForNews.add(temporaryArray[i]);
                            }
                            
                            DispatchQueue.main.async {
//                                self.newsTableView.beginUpdates()
//                                self.newsTableView.insertRows(at: indexArray, with: UITableViewRowAnimation.none)
//                                self.newsTableView.endUpdates()
                                self.newscollectionView.reloadData()
                                
                            }
                        } else {
    //                        loadNew = false;
                            tableViewArrayForNews = (temporaryArray.mutableCopy() as! NSMutableArray)
                            DispatchQueue.main.async {
//                                self.activityLoader?.hide(animated: true)
//                                self.contentView.isHidden = false
//                                self.noDataLabel.isHidden = true
                                let cell = InnerNewsCollectionViewCell()
                                cell.stopShimmeringEffect()
                                self.newscollectionView.reloadData()
                                
                                DispatchQueue.main.asyncAfter(deadline: .now()+2.0) {
                                    self.changeImage()
                                }
//                                self.timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.changeImage), userInfo: nil, repeats: true)
//                                 cell.stopanimating()
//                                cell.contentView.stopShimmeringEffect()

                                
                            }
                        }
                    }
                }
                else
                {
//                    if searchBtnTapped != true {
//                    DispatchQueue.main.async {
//                    self.noDataLabel.isHidden = false
//                    self.contentView.isHidden = true
//                        }
//                    }
                }
            }
        }

    @objc func changeImage()  {
        self.type = "false"
        self.newscollectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
        return tableViewArrayForNews.count
        
//        if (tableViewArrayForNews.count == 0){
//            return 5
//        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = newscollectionView.dequeueReusableCell(withReuseIdentifier: "InnerNewsCollectionViewCell", for: indexPath) as! InnerNewsCollectionViewCell
                  cell.dateLbl.startShimmeringEffect()
                   cell.defaultImage.startShimmeringEffect()
                   cell.newsImage.startShimmeringEffect()
                   cell.newsDescriptionLbl.startShimmeringEffect()
        
        
        let radius = 5
        cell.newsImage.layer.cornerRadius = CGFloat(radius)
        cell.newsImage.layer.masksToBounds = true
        cell.newsImage.clipsToBounds = true
        
        
        cell.contentView.layer.cornerRadius = 2.0
        cell.contentView.layer.borderWidth = 1.0
        cell.contentView.layer.borderColor = UIColor.clear.cgColor
        cell.contentView.layer.masksToBounds = true
       
        cell.layer.backgroundColor = UIColor.white.cgColor
        cell.layer.shadowColor = UIColor.lightGray.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)//CGSizeMake(0, 2.0);
                    cell.layer.shadowRadius = 2.0
                    cell.layer.shadowOpacity = 1.0
                    cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
        
        
        if type == "false"
        {
                    DispatchQueue.main.async {
                        
                        
                        let dateFormatterGet = DateFormatter()
                        dateFormatterGet.dateFormat = "yyyy-MM-dd"

                        let dateFormatterPrint = DateFormatter()
                        dateFormatterPrint.dateFormat = "dd MMM, yyyy"

                        let date: NSDate? = dateFormatterGet.date(from: "\((self.tableViewArrayForNews.object(at: indexPath.row) as AnyObject).object(forKey: "post_date")as! String)") as NSDate?
                        
                        
                        cell.dateLbl.text = "\(dateFormatterPrint.string(from: date! as Date))"
                        
                        cell.newsDescriptionLbl.text = ((((self.tableViewArrayForNews.object(at: indexPath.row) as AnyObject).object(forKey: "name") as! String)))
                        cell.newsDescriptionLbl.labelSpacing(label: cell.newsDescriptionLbl, font: "Book")

                        
                        
                       
                        
                        
                        
                        
                        let imgURL = (self.tableViewArrayForNews.object(at: indexPath.row) as AnyObject).object(forKey: "news_pic") as! String
                        let imgfilterUrl = ImageUrl + imgURL
                        self.replacedImgstring = imgfilterUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!

                        
                        cell.newsImage.sd_setImage(with: URL(string: self.replacedImgstring))
                        
                        cell.dateLbl.layer.sublayers = nil
                               cell.newsImage.layer.sublayers = nil
                               cell.newsDescriptionLbl.layer.sublayers = nil
                               cell.defaultImage.layer.sublayers = nil
                    }
        }
        

        
        return cell
    }
//   func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//       cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
//       UIView.animate(withDuration: 0.4) {
//           cell.transform = CGAffineTransform.identity
//       }
//   }
//    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//         let cell = newscollectionView.dequeueReusableCell(withReuseIdentifier: "InnerNewsCollectionViewCell", for: indexPath) as! InnerNewsCollectionViewCell
//
//    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "NewsContentViewController") as! NewsContentViewController
         var replacedImgstring : String = ""
        DispatchQueue.main.async {
                  let getExhibitorImage = ((self.tableViewArrayForNews.object(at: indexPath.row) as AnyObject).object(forKey: "news_pic") as! String)
                  print(getExhibitorImage)
                  let imgfilterUrl = ImageUrl + getExhibitorImage
                  print(imgfilterUrl)
                replacedImgstring = imgfilterUrl.replacingOccurrences(of: " ", with: "%20")
                  print(replacedImgstring)
                  
                  URLSession.shared.dataTask(with: NSURL(string: replacedImgstring)! as URL, completionHandler: { (data, response, error) -> Void in
                      
                      if error != nil {
                          return
                      }else {
                          DispatchQueue.main.async(execute: { () -> Void in
                              let image = UIImage(data: data!)
                              print(data as Any)
                              if (image != nil) {
                                  vc.newsImage.image = image
                                  print(image as Any)
                              }
                          })
                      }
                  }).resume()
              }
        let getnewsListDate = ((self.tableViewArrayForNews.object(at: indexPath.row) as AnyObject).object(forKey: "post_date") as! String)
//        let separatedDate = getnewsListDate.split(separator: "-")
        vc.getnewsListDate = getnewsListDate
        
        let getnewsLongDesc =  ((self.tableViewArrayForNews.object(at: indexPath.row) as AnyObject).object(forKey: "long_desc") as! String)
        vc.getnewsLongDesc = getnewsLongDesc
        
        let name = ((self.tableViewArrayForNews.object(at: indexPath.row) as AnyObject).object(forKey: "name") as! String)
        vc.getnewsListName = name


        navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout:

        UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let width = UIScreen.main.bounds.size.width
       
        return CGSize(width: ((width / 2) - 15)   , height: 265)



    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let arrayCount = self.tableViewArrayForNews.count - 1;
        
        let check  = self.isRowZeroVisible(count: arrayCount)
        
        if (check) {
            
            loadNew = false;
            if loading == false && isSearching == false {
                if tabIndex == 0 {
                    callUrls = dailyNewsUrl
                    let params = [ "start":"\(self.tableViewArrayForNews.count + 1)",
                        "limit":"10"]
                    print(params)
                    self.loading = true
                    requestToServer.connectToServer(myUrl: callUrls as URL, params: params as AnyObject)
                }
                else if (loading == false && isSearching == true) {
                    callUrls = newsSearchUrl
                    let params = ["name": self.searchBar.text!,
                                  "start":"\(self.tableViewArrayForNews.count + 1)",
                        "limit":"10"]
                    self.loading = true
                    requestToServer.connectToServer(myUrl: callUrls as URL, params: params as AnyObject)
                }
            }
        }
    }
    func isRowZeroVisible(count : Int)  -> Bool {

        let indexes : [IndexPath] = self.newscollectionView.indexPathsForVisibleItems

           for case let indexPath as IndexPath in indexes {

            print(count)
            print(indexPath.row)

               if indexPath.row == count {
                   return true;
               }
           }
           return false;
       }
    
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
           isSearching = true;
           return true;
       }
    
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        print(searchBar.text!)
        
        if text == "\n" {
            return true
        }
        
        var params = ["name" : searchBar.text! + text,
                      "start" : "0" ,
                      "limit" : "10"]
        if searchBar.text?.count == 1 && text == "" {
            onFirstTimeLoad()
            return true;
        }
        else if text == "" {
            isSearching = false;
            let name: String = searchBar.text!
            
            let truncated = name.substring(to: name.index(before: name.endIndex))
            params = ["name" : truncated,
                      "start" : "0" ,
                      "limit" : "10"]
        }
        
        DispatchQueue.main.async {
            self.callUrls = newsSearchUrl
            self.loading = true
            self.loadNew = true;
            self.requestToServer.connectToServer(myUrl: self.callUrls as URL, params: params as AnyObject)
        }
        return true
    }

    @IBAction func updatesBtnAction(_ sender: Any) {
        
        DispatchQueue.main.async {
            self.updatesLbl.isHidden = false
            self.newsletterLbl.isHidden = true
            self.epatrikaLbl.isHidden = true
            self.dailyNewsLbl.isHidden = true
        }
        if self.children.count >= 0{
                 let viewControllers:[UIViewController] = self.children
                 for viewContoller in viewControllers{
                     viewContoller.willMove(toParent: nil)
                     viewContoller.view.removeFromSuperview()
                     viewContoller.removeFromParent()
                 }
             }
        
        
        let controller:UpdatesViewController =
        self.storyboard!.instantiateViewController(withIdentifier: "UpdatesViewController") as!
        UpdatesViewController

        let statusbarHeight = UIApplication.shared.statusBarFrame.height
//        let navigationBarHeight = uiappl
        
        let tabBarHeight = self.tabBarController!.tabBar.frame.size.height+100;

        
        
        controller.view.frame = CGRect(x: 0, y: statusbarHeight+80, width: UIScreen.main.bounds.size.width, height: (UIScreen.main.bounds.size.height-tabBarHeight))
        controller.willMove(toParent: self)
        self.view.addSubview(controller.view)
        self.addChild(controller)
        controller.didMove(toParent: self)
        
        

        
    }

    @IBAction func dailyNewsBtnAction(_ sender: Any) {
        
DispatchQueue.main.async {
           self.updatesLbl.isHidden = true
           self.newsletterLbl.isHidden = true
           self.epatrikaLbl.isHidden = true
           self.dailyNewsLbl.isHidden = false
       }
        
          if self.children.count >= 0{
                    let viewControllers:[UIViewController] = self.children
                    for viewContoller in viewControllers{
                        viewContoller.willMove(toParent: nil)
                        viewContoller.view.removeFromSuperview()
                        viewContoller.removeFromParent()
                    }
                }
    }
    
    func setView()  {
        self.notificationcount.layer.cornerRadius = 20/2
                             self.notificationcount.layer.masksToBounds = true
        
        searchBar.layer.cornerRadius = 8
        searchBar.clipsToBounds = true
        searchBar.layer.borderWidth = 1
        searchBar.layer.borderColor = UIColor.gray.cgColor

DispatchQueue.main.async {
           self.updatesLbl.isHidden = true
           self.newsletterLbl.isHidden = true
           self.epatrikaLbl.isHidden = true
           self.dailyNewsLbl.isHidden = false
       }
               
//                 if self.children.count >= 0{
//                           let viewControllers:[UIViewController] = self.children
//                           for viewContoller in viewControllers{
//                               viewContoller.willMove(toParent: nil)
//                               viewContoller.view.removeFromSuperview()
//                               viewContoller.removeFromParent()
//                           }
//                       }
    }
    
    @IBAction func newsletterAction(_ sender: Any) {
       DispatchQueue.main.async {
                   self.updatesLbl.isHidden = true
                   self.newsletterLbl.isHidden = false
                   self.epatrikaLbl.isHidden = true
                   self.dailyNewsLbl.isHidden = true
               }
        if self.children.count >= 0{
            let viewControllers:[UIViewController] = self.children
            for viewContoller in viewControllers{
                viewContoller.willMove(toParent: nil)
                viewContoller.view.removeFromSuperview()
                viewContoller.removeFromParent()
            }
        }
        
        let controller:NewsLetterViewController =
               self.storyboard!.instantiateViewController(withIdentifier: "NewsLetterViewController") as!
               NewsLetterViewController

              let statusbarHeight = UIApplication.shared.statusBarFrame.height

               
               let tabBarHeight = self.tabBarController!.tabBar.frame.size.height+100;

               
               
               controller.view.frame = CGRect(x: 0, y: statusbarHeight+80, width: UIScreen.main.bounds.size.width, height: (UIScreen.main.bounds.size.height-tabBarHeight))
               controller.willMove(toParent: self)
               self.view.addSubview(controller.view)
               self.addChild(controller)
               controller.didMove(toParent: self)
        
        
        
    }
    @IBAction func epatrikaBtnAction(_ sender: Any) {
DispatchQueue.main.async {
           self.updatesLbl.isHidden = true
           self.newsletterLbl.isHidden = true
           self.epatrikaLbl.isHidden = false
           self.dailyNewsLbl.isHidden = true
       }
        
        
        if self.children.count >= 0{
                  let viewControllers:[UIViewController] = self.children
                  for viewContoller in viewControllers{
                      viewContoller.willMove(toParent: nil)
                      viewContoller.view.removeFromSuperview()
                      viewContoller.removeFromParent()
                  }
              }
              
              let controller:EpatrikaViewController =
                     self.storyboard!.instantiateViewController(withIdentifier: "EpatrikaViewController") as! EpatrikaViewController
                     

                    let statusbarHeight = UIApplication.shared.statusBarFrame.height

                     
                     let tabBarHeight = self.tabBarController!.tabBar.frame.size.height+100;

                     
                     
                     controller.view.frame = CGRect(x: 0, y: statusbarHeight+80, width: UIScreen.main.bounds.size.width, height: (UIScreen.main.bounds.size.height-tabBarHeight))
                     controller.willMove(toParent: self)
                     self.view.addSubview(controller.view)
                     self.addChild(controller)
                     controller.didMove(toParent: self)
        
    }
 
    func getData()  {
        
        var responseDict : NSMutableDictionary = [:]
        var dataTask: URLSessionDataTask
        
        let url = NSURL(string: "https://gjepc.org/gjepc_mobile/services/firebaseNotificationList")
        
        let request = NSMutableURLRequest(url:url! as URL)
        
        
        
        let session = URLSession.shared
        dataTask = session.dataTask(with: request as URLRequest){(data, response, error) in
            guard error == nil else {
                
                
                return
            }
            guard let data = data else {
                return
            }
            do {
                if let dict = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! NSMutableDictionary{
                    
                    // let dict = json as! NSMutableDictionary
                    //  print(dict)
                    if (dict["Response"] as! NSDictionary)["status"] as! String == "true" {
                        responseDict = (dict["Response"] as! NSMutableDictionary)
                        
                        print(responseDict)
                        
                        
                        
                        self.responseNotificationArray = (dict["Response"] as! NSDictionary)["Result"] as! NSMutableArray
                        
                        
                        
                        
                        
                        let lastNotificationCount:String? = UserDefaults.standard.value(forKey: "lastNotificationCOunt")  as? String
                        
                        let mastercnt:String? = UserDefaults.standard.value(forKey: "MasterNotificationCount")  as? String
                        
                        
                        var master :Int? = 0
                        
                        if(mastercnt != nil)
                        {
                            master = Int(mastercnt!)
                        }
                        else
                            
                        {
                            master = 0
                        }
                        
                        
                        
                        
                        var lastCnt:Int? = 0
                        
                        
                        if(lastNotificationCount != nil)
                        {
                            lastCnt = Int(lastNotificationCount!)
                        }
                        else
                        {
                            lastCnt = 0
                            
                        }
                        
                        
                        
                        
                        
                        
                        let isRead:String? = UserDefaults.standard.value(forKey: "isRead") as? String
                        
                        
                        
                        
                        
                        
                        if(isRead != nil)
                        {
                            if(lastCnt == self.responseNotificationArray.count)
                            {
                                if(isRead == "true")
                                {
                                    
                                    
                                }
                                else
                                {
                                    
                                    UserDefaults.standard.setValue(lastCnt, forKey: "lastNotificationCOunt")
                                    
                                    
                                    UserDefaults.standard.setValue("0", forKey: "MasterNotificationCount")
                                    
                                    UserDefaults.standard.setValue("false", forKey: "isRead")
                                    
                                }
                                
                            }
                            else
                            {
                                
                                if(isRead == "true")
                                {
                                    //when new notification come show new notification count only
                                    
                                    
                                    
                                    let rCount:Int = self.responseNotificationArray.count
                                    
                                   
                                    let newCount:Int = rCount - master!
                                    
                                    //let latestCount = self.responseNotificationArray.count - newCount
                                    
                                    let newCountStr:String = String(newCount)
                                    
                                    UserDefaults.standard.setValue(String(newCountStr), forKey: "lastNotificationCOunt")
                                    
                                    UserDefaults.standard.setValue("false", forKey: "isRead")
                                    //                                    }
                                    
                                    
                                    
                                }
                                else
                                {
                                    
                                    if(master == 0)
                                    {
                                        
                                        let rCount:Int = self.responseNotificationArray.count
                                        
                                        let newCount1:Int = rCount - lastCnt!
                                        
                                        let newCount:Int = newCount1 + lastCnt!
                                        
                                        UserDefaults.standard.setValue(String(newCount), forKey: "lastNotificationCOunt")
                                        
                                        UserDefaults.standard.setValue("false", forKey: "isRead")
                                        
                                    }
                                    else
                                        
                                    {
                                        
                                        
                                        let rCount:Int = self.responseNotificationArray.count
                                        
                                        var newCount1:Int = 0
                                        
                                        
                                        newCount1 = rCount - master!
                                        
                                        UserDefaults.standard.setValue(String(newCount1), forKey: "lastNotificationCOunt")
                                        
                                        UserDefaults.standard.setValue("false", forKey: "isRead")
                                        
                                        //                                        }
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                    }
                                    
                                    
                                    
                                }
                                
                                
                            }
                        }
                        else
                        {
                            
                            
                            
                            let lastNotificationCount1 = self.responseNotificationArray.count
                            
                            UserDefaults.standard.setValue(String(lastNotificationCount1), forKey: "lastNotificationCOunt")
                            
                            
                            UserDefaults.standard.setValue("0", forKey: "MasterNotificationCount")
                            
                            UserDefaults.standard.setValue("false", forKey: "isRead")
                            
                            
                        }
                        
                        
                        
                        
                        
                        
                        var lastNotificationCount1:String? = nil
                        
                        if let lastCount = UserDefaults.standard.value(forKey: "lastNotificationCOunt") as? NSNumber {
                            
                            
                            lastNotificationCount1 = lastCount.stringValue
                            
                            
                            
                        }
                        else if let quantity = UserDefaults.standard.value(forKey: "lastNotificationCOunt") as? String {
                            
                            
                            lastNotificationCount1 = quantity
                            
                            
                        }
                        else
                            
                        {
                            lastNotificationCount1 = "0"
                        }
                        
                        
                        
                        
                        
                        
                       
                        
                        
                        let masterCOunt:String? = (UserDefaults.standard.value(forKey: "MasterNotificationCount") as! String)
                        
                        print("MASTER \(masterCOunt!)")
                        
                        
                        let new = Int(lastNotificationCount1!)
                        
                        if (new! >= 0)
                        {
                            // do positive stuff
                        }
                        else
                            
                        {
                            self.isChange = true
                            
                            UserDefaults.standard.setValue("0", forKey: "lastNotificationCOunt")
                            
                            UserDefaults.standard.setValue("true", forKey: "isRead")
                            
                        }
                        
                        let isRead1:String? = (UserDefaults.standard.value(forKey: "isRead") as! String)
                        
                        
                        if(isRead1 == "true")
                        {
                            //UserDefaults.standard.setValue("0", forKey: "lastNotificationCOunt")
                            
                            
                            DispatchQueue.main.async {
                                
                                
                                self.notificationcount.isHidden = false
                                
                                self.notificationcount.text = "0"
                                
                            }
                        }
                        else
                        {
                            
                            //show count on bell icon
                            
                            DispatchQueue.main.async {
                                
                                self.notificationcount.isHidden = false
                                
                                self.notificationcount.text = lastNotificationCount1
                                
                            }
                            
                            
                            
                            
                            
                            
                            //  print(lastNotificationCount1)
                            
                            
                            
                        }
                        
                        
                        
                        
                        
                        
                    } else {
                        
                        return
                        
                    }
                }
                
            } catch let error {
                print("Json Error",error.localizedDescription)
                
            }
            
            
            
            
        }
        
        dataTask.resume()
        
        
        
        
        
        
        
        
        
    }
    
    @IBAction func notification(_ sender: Any) {





        let data:[AnyObject]? = UserDefaults.standard.value(forKey: "NotificationList") as? [AnyObject]
        print(data)

        if  data != nil {



            DispatchQueue.main.async {


                let view1 = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController

                view1?.dataArray = (UserDefaults.standard.value(forKey: "NotificationList") as? [AnyObject])!

                view1?.globalArray = data!.reversed()
                self.navigationController?.pushViewController(view1!, animated: true)
            }


        }
        else
        {




            DispatchQueue.main.async {

                let view1 = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController

                view1?.dataArray = []

                view1?.globalArray = []
                self.navigationController?.pushViewController(view1!, animated: true)
            }
        }






    }
    
   
        


        
        
    }
extension UIView {
   

    
   func startShimmeringEffect() {
               let gradientLayer = CAGradientLayer()
                     gradientLayer.frame = self.bounds
                     gradientLayer.startPoint = CGPoint(x: 0.0, y: 1.0)
                     gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
                     let gradientColorOne = UIColor(white: 0.90, alpha: 1.0).cgColor
                     let gradientColorTwo = UIColor(white: 0.95, alpha: 1.0).cgColor
                     gradientLayer.colors = [gradientColorOne, gradientColorTwo, gradientColorOne]
                     gradientLayer.locations = [0.0, 0.5, 1.0]
                     self.layer.addSublayer(gradientLayer)

                     let animation = CABasicAnimation(keyPath: "locations")
                     animation.fromValue = [-1.0, -0.5, 0.0]
                     animation.toValue = [1.0, 1.5, 2.0]
                     animation.repeatCount = .infinity
                     animation.duration = 1.25
                     gradientLayer.add(animation, forKey: animation.keyPath)
               }
               func stopShimmeringEffect() {
                   self.layer.sublayers = nil
               }
    

    
    
   }

    
    


