//
//  ZoomSeminarImage.swift
//  GJEPC
//
//  Created by Kwebmaker on 06/03/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit

class ZoomSeminarImage: UIViewController {

    @IBOutlet weak var zoomImage: UIImageView!
    
    
    var image = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        if image == ""||image == nil{
            print("nil")
            
        }
        else{
            zoomImage.sd_setImage(with: URL(string: self.image))

        }
        

//        }

        
    }
    

    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
