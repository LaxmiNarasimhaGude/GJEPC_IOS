//
//  SeminarWebView.swift
//  GJEPC
//
//  Created by Kwebmaker on 14/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit

class SeminarWebView: UIViewController,UIScrollViewDelegate {
    
    var id = ""
    var date = ""
    var imageUrl = ""
    var discription = ""
    var type = "Upcoming"
    var pdf = ""
    var dis = ""
     var previewImage : UIImage? = nil
    
       @IBOutlet weak var postEventUpdate: UIButton!
       @IBOutlet weak var viewDetailsLbl: UILabel!
       @IBOutlet weak var registerNowHeight: NSLayoutConstraint!
       @IBOutlet weak var seminarImage: UIImageView!
       @IBOutlet weak var seminarWebView: UIWebView!
       @IBOutlet weak var seminarDescriptionLabel: UILabel!
       @IBOutlet weak var registerNowButton: UIButton!
       @IBOutlet weak var viewDetailsHeightLbl: NSLayoutConstraint!
       
       @IBOutlet weak var postEventUpdateHeightLbl: NSLayoutConstraint!

    override func viewDidLoad() {
            super.viewDidLoad()
        
        var scrollview = UIScrollView.init(frame: self.view.bounds)
        scrollview.minimumZoomScale = 0.1
        scrollview.maximumZoomScale = 4.0
        scrollview.zoomScale = 1.0
        scrollview.delegate = self as? UIScrollViewDelegate
        
        seminarImage.image = previewImage
        
        
//        let doubleTap =  UITapGestureRecognizer.init(target: self, action: #selector(self.sampleTapGestureTapped(recognizer:)))
//        doubleTap.numberOfTapsRequired = 2
//        seminarImage.addGestureRecognizer(doubleTap)
        
        registerNowButton.layer.cornerRadius = 10; // this value vary as per your desire
        registerNowButton.clipsToBounds = true
        registerNowButton.layer.borderColor = UIColor.darkGray.cgColor
        registerNowButton.layer.borderWidth = 1.0

        
        postEventUpdate.layer.cornerRadius = 10; // this value vary as per your desire
        postEventUpdate.clipsToBounds = true
        postEventUpdate.layer.borderColor = UIColor.darkGray.cgColor
        postEventUpdate.layer.borderWidth = 1.0
            print(pdf)
            
            if pdf == ""{
                DispatchQueue.main.async {
                    self.viewDetailsHeightLbl.constant = 0.0
                    self.postEventUpdateHeightLbl.constant = 0.0
                    self.postEventUpdate.isHidden = true
                }
                
            }
    //        else{
    //
    //        }
            
            seminarImage.isUserInteractionEnabled = true
            let pinchMethod = UIPinchGestureRecognizer(target: self, action: #selector(pinchImage(sender:)))
           seminarImage.addGestureRecognizer(pinchMethod)
            
            DispatchQueue.main.async {
                self.loadHtmlInAWebView()
                
            }
            if(type == "postSeminars"){
                registerNowButton.isHidden = true
                viewDetailsLbl.isHidden = false
                postEventUpdate.isHidden = false
                registerNowHeight.constant = 0.0
            }
            else if (type == "upcoming"){
                viewDetailsLbl.isHidden = true
                postEventUpdate.isHidden = true
                registerNowButton.isHidden = false
                viewDetailsHeightLbl.constant = 0.0
                postEventUpdateHeightLbl.constant = 0.0
            }
            else{
               registerNowButton.isHidden = true
                viewDetailsLbl.isHidden = false
                postEventUpdate.isHidden = false
                registerNowHeight.constant = 0.0
                viewDetailsLbl.isHidden = true
                postEventUpdate.isHidden = true
                registerNowButton.isHidden = false
                viewDetailsHeightLbl.constant = 0.0
                postEventUpdateHeightLbl.constant = 0.0
        }
            seminarDescriptionLabel.text = discription
        print(imageUrl)
        
        if imageUrl == ""||imageUrl == nil{
            print("nil")
            
        }
        else{
            seminarImage.sd_setImage(with: URL(string: self.imageUrl))

        }
        

        }
    
    @objc func sampleTapGestureTapped(recognizer: UITapGestureRecognizer) {
           print("Tapping working")
       }
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return seminarImage
    }
        @objc func pinchImage(sender: UIPinchGestureRecognizer) {
    //      guard let sender.view != nil else { return }

        if let scale = (sender.view?.transform.scaledBy(x: sender.scale, y: sender.scale)) {
          guard scale.a > 1.0 else { return }
          guard scale.d > 1.0 else { return }
          sender.view?.transform = scale
          sender.scale = 1.0
         }
        }
    func loadHtmlInAWebView() {
            let htmlString : String! = "<html><head><style type='text/css'>span {    text-align: justify;line-height: auto;float: left;font-size:14px;font-family:futura}</style></head><body><font color ='#000000'><span>\(dis)<br/><br></span></font></body></html>"
            seminarWebView.backgroundColor = UIColor.white
            seminarWebView.loadHTMLString(htmlString, baseURL: nil)

        }
  
    @IBAction func postEventUpdateBtn(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "postEventUpdateViewController") as! postEventUpdateViewController
        
        print(self.pdf)
        vc.pdf = self.pdf
        
       
        navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func registerNow(_ sender: Any) {
        guard let url = URL(string: "https://www.gjepc.org/seminar_registration.php?id="+"\(id)") else { return }
               UIApplication.shared.open(url)
    }
}
//extension SeminarWebView: UIViewController,UIScrollViewDelegate {
//
//    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
//
//        return seminarImage
//    }
//}
