//
//  SeminarsViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 14/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD

class SeminarsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    
    var requestToServer = RequestToServer()
    var activityLoader : MBProgressHUD? = nil
    var customArray : NSMutableArray = []
    var callUrls : URL!
    var temporaryArray : NSArray = []
    var tableViewArrayForSeminars : NSMutableArray = []
    var replacedImgstring  = ""
    var type = "upcoming"
    var custom  = Custom_ObjectsViewController()

    @IBOutlet weak var postSeminarImage: UIImageView!
    @IBOutlet weak var upcomingSeminarImage: UIImageView!
    @IBOutlet weak var seminarCalenderImage: UIImageView!
   
    @IBOutlet weak var seminarTableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
               UpdateView()
//               checkForInternet()
               seminarTableview.delegate = self
               seminarTableview.dataSource = self
        self.seminarTableview.separatorStyle = UITableViewCell.SeparatorStyle.none
        seminarTableview.bounces = true
        self.seminarTableview.autoresizingMask = UIView.AutoresizingMask.flexibleHeight;
        self.seminarTableview.rowHeight = UITableView.automaticDimension



        

    }
    
        func checkForInternet() {
            
            seminarTableview.isHidden = true
            
               if CheckInternet.isConnectedToNetwork() == true
               {
                   print("internet available")
                   DispatchQueue.main.async {
                       self.loadActivityIndicator()
                   }
                   onFirstTimeLoad()
                   
               }
               else
               {
                   print("no internet")
                

                
                let alert = UIAlertController(title: "", message: "Please check your internet connection", preferredStyle: .alert)
                self.present(alert, animated: true, completion: nil)

                // change to desired number of seconds (in this case 5 seconds)
                let when = DispatchTime.now() + 3
                DispatchQueue.main.asyncAfter(deadline: when){
                  // your code with delay
                  alert.dismiss(animated: true, completion: nil)
                }
                
    //               newsTableView.isHidden = true
                   activityLoader?.hide(animated: true)
               }
           }
   
    func loadActivityIndicator()
       {
           activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
           activityLoader?.label.text = "Loading";
           activityLoader?.detailsLabel.text = "Please Wait";
           activityLoader?.isUserInteractionEnabled = false;
       }
    @IBAction func upcomingSeminars(_ sender: Any) {
        
        if self.children.count >= 0{
            let viewControllers:[UIViewController] = self.children
            for viewContoller in viewControllers{
                viewContoller.willMove(toParent: nil)
                viewContoller.view.removeFromSuperview()
                viewContoller.removeFromParent()
            }
        }
        
        DispatchQueue.main.async {
//            self.seminarCalenderLabel.backgroundColor = UIColor.white
//                        self.upcomingLbl.backgroundColor  = UIColor.black
//                        self.postSeminarsLbl.backgroundColor = UIColor.white
            
            self.seminarCalenderImage.isHidden = true
            self.upcomingSeminarImage.isHidden = false
            self.postSeminarImage.isHidden = true
            
        }
        
        tableViewArrayForSeminars.removeAllObjects()
        self.type = "upcoming"
//        UpdateView()
       checkForInternet()
    }
    
    @IBAction func postSeminarsUpdates(_ sender: Any) {

        if self.children.count >= 0{
            let viewControllers:[UIViewController] = self.children
            for viewContoller in viewControllers{
                viewContoller.willMove(toParent: nil)
                viewContoller.view.removeFromSuperview()
                viewContoller.removeFromParent()
            }
        }
           
             DispatchQueue.main.async {
//                           self.seminarCalenderLabel.backgroundColor = UIColor.white
//                        self.postSeminarsLbl.backgroundColor  = UIColor.black
//                        self.upcomingLbl.backgroundColor = UIColor.white
            
                self.seminarCalenderImage.isHidden = true
                self.upcomingSeminarImage.isHidden = true
                self.postSeminarImage.isHidden = false
                       }
                      
            
            self.type = "postSeminars"
            
             seminarTableview.isHidden = true
            
             loadActivityIndicator()
            
             tableViewArrayForSeminars.removeAllObjects()
            

            
              
                let url = postSemisterUrl
                //change the url

                //create the session object
                let session = URLSession.shared

                //now create the URLRequest object using the url object
                let request = URLRequest(url: url!)

                //create dataTask using the session object to send data to the server
                let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

                    guard error == nil else {
                        return
                    }

                    guard let data = data else {
                        return
                    }

                   do {
                      //create json object from data
                    if let data:NSDictionary = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] as NSDictionary? {
                         
                        if ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "true" {
                            self.temporaryArray = ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "Result") as! NSArray)
                            print(self.temporaryArray)
                            
                            
                            if self.temporaryArray.count>0
                            {
                                
                                
                                for i in 0..<self.temporaryArray.count{
                                    self.tableViewArrayForSeminars.add(self.temporaryArray[i])
                                  
                                }
                                
                                DispatchQueue.main.async {
                                    self.seminarTableview.isHidden = false
                                    self.seminarTableview.contentOffset = CGPoint(x: 0, y: -self.seminarTableview.contentInset.top)
                                self.seminarTableview.reloadData()
                                self.activityLoader?.hide(animated: true)
                                }
                                
        //                        print(self.tableViewArrayForSeminars)
                                
                                
                            }
                        }
                        
                        
                        
                        
                      }
                   } catch let error {
                     print(error.localizedDescription)
                   }
                })

                task.resume()
              
                
            }
    
    func onFirstTimeLoad() {
          
            
             //change the url
            print(UpcomingseminarListUrl)
            let url = URL(string: "https://gjepc.org/gjepc_mobile/services/upcomingSeminarList")
           

            //create the session object
            let session = URLSession.shared

            //now create the URLRequest object using the url object
            let request = URLRequest(url: url!)

            //create dataTask using the session object to send data to the server
            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

                guard error == nil else {
                    return
                }

                guard let data = data else {
                    return
                }

               do {
                  //create json object from data
                if let data:NSDictionary = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] as NSDictionary? {
                     
                    if ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "true" {
                        self.temporaryArray = ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "Result") as! NSArray)
                        print(self.temporaryArray)
                        
                        
                        if self.temporaryArray.count>0
                        {
                            
                            for i in 0..<self.temporaryArray.count{
                                self.tableViewArrayForSeminars.add(self.temporaryArray[i])
                              
                            }
                            
                            DispatchQueue.main.async {
                                self.seminarTableview.isHidden = false
                                self.seminarTableview.contentOffset = CGPoint(x: 0, y: -self.seminarTableview.contentInset.top)
                            self.seminarTableview.reloadData()
                            self.activityLoader?.hide(animated: true)
                            }
                            
    //                        print(self.tableViewArrayForSeminars)
                            
                            
                        }
                        else{
                        

                            DispatchQueue.main.async {
                                self.activityLoader?.hide(animated: true)
                              
                            }
                            
                            
                            let alert = UIAlertController(title: "", message: "No seminars available", preferredStyle: .alert)
                            DispatchQueue.main.async {
                                self.present(alert, animated: true, completion: nil)
                                let when = DispatchTime.now() + 2
                        DispatchQueue.main.asyncAfter(deadline: when){
                                                             // your code with delay
                                                             alert.dismiss(animated: true, completion: nil)
                                                           }
                            }
                            

                            // change to desired number of seconds (in this case 5 seconds)
                           
                        }
                    }
                    
                    
                    
                    
                  }
               } catch let error {
                 print(error.localizedDescription)
               }
            })

            task.resume()
          
            
        }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return tableViewArrayForSeminars.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = seminarTableview.dequeueReusableCell(withIdentifier: "SeminarListTableViewCell") as! SeminarListTableViewCell

           cell.Backgroundview.backgroundColor = UIColor.white
           cell.Backgroundview.layer.cornerRadius = 3.0
           cell.Backgroundview.layer.masksToBounds = false
           cell.Backgroundview.layer.shadowOffset = CGSize(width: 0, height: 0)
           cell.Backgroundview.layer.shadowOpacity = 0.3
        
        
        
            let htmlString = ((((self.tableViewArrayForSeminars.object(at: indexPath.row) as AnyObject).object(forKey: "title")as! String)).htmlString)
        
            cell.seminarEventNameLabel.text = htmlString.withoutHtml
        
                   
        
                  cell.seminarEventNameLabel.setCharacterSpacing(1)
        cell.seminarEventNameLabel.setLineSpacing(lineSpacing: 5, lineHeightMultiple: 0)
                    
//                    let attrString = NSMutableAttributedString(string: htmlString)
//                    var style = NSMutableParagraphStyle()
//                    style.lineSpacing = 10 // change line spacing between
//                    attrString.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: NSRange(location: 0, length: htmlString.count))
//                    cell.seminarEventNameLabel.attributedText = attrString
                    
        
        
     let fakedate = ((self.tableViewArrayForSeminars.object(at: indexPath.row) as AnyObject).object(forKey: "start")as! String)
     let fakedate2 = ((self.tableViewArrayForSeminars.object(at: indexPath.row) as AnyObject).object(forKey: "end")as! String)
        
     let startdate  = convertDateFormater(fakedate)
    let enddate = convertDateFormater(fakedate2)
        
//      cell.dateLabel.text = "\(startdate) TO \(enddate)"
        
   
                    
           
    
      
      
      return cell
      
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(type)
        if(type == "upcoming"){
             let storyboard = UIStoryboard(name: "Main", bundle: nil)
                         let vc = storyboard.instantiateViewController(withIdentifier: "SeminarWebView") as! SeminarWebView
        vc.id = ((self.tableViewArrayForSeminars.object(at: indexPath.row) as AnyObject).object(forKey: "id") as! String)

            vc.type = "upcoming"

            vc.discription = ((self.tableViewArrayForSeminars.object(at: indexPath.row) as AnyObject).object(forKey: "title") as! String).withoutHtml
            //        print(des)


              let imgdata = ((self.tableViewArrayForSeminars.object(at: indexPath.row) as AnyObject).object(forKey: "images")as! String)
                   let imgfilterUrl = seminarImageUrl + imgdata
                   replacedImgstring = imgfilterUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!
            print(replacedImgstring)
                    vc.imageUrl = replacedImgstring


                    let discription =  ((self.tableViewArrayForSeminars.object(at: indexPath.row) as AnyObject).object(forKey: "full_description") as! String)


                    vc.dis = discription
            
            self.navigationController?.pushViewController(vc, animated: true)

                   
        }
        else{
             let storyboard = UIStoryboard(name: "Main", bundle: nil)
                         let vc = storyboard.instantiateViewController(withIdentifier: "SeminarWebView") as! SeminarWebView
            vc.type = "postSeminars"
                    vc.id = ((self.tableViewArrayForSeminars.object(at: indexPath.row) as AnyObject).object(forKey: "id") as! String)

            vc.discription = ((self.tableViewArrayForSeminars.object(at: indexPath.row) as AnyObject).object(forKey: "title") as! String).withoutHtml
            


            vc.pdf = ((self.tableViewArrayForSeminars.object(at: indexPath.row) as AnyObject).object(forKey: "pdf") as! String)


            //        print(des)


              let imgdata = ((self.tableViewArrayForSeminars.object(at: indexPath.row) as AnyObject).object(forKey: "images")as! String)
              if(imgdata == ""){
                 vc.imageUrl = defaultImageUrl
              }
               else{
                let imgfilterUrl = seminarImageUrl + imgdata
                                  replacedImgstring = imgfilterUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!
                                   vc.imageUrl = replacedImgstring
            }




                    let discription =  ((self.tableViewArrayForSeminars.object(at: indexPath.row) as AnyObject).object(forKey: "full_description") as! String)


                    self.navigationController?.pushViewController(vc, animated: true)

                    vc.dis = discription
        }



    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 140.0
//       }

    func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd MMM, yyy"
        return  dateFormatter.string(from: date!).uppercased()

    }
    
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func UpdateView(){
           DispatchQueue.main.async {
            
//            self.seminarCalenderLabel.backgroundColor = UIColor.black
//            self.upcomingLbl.backgroundColor = UIColor.white
//            self.postSeminarsLbl.backgroundColor = UIColor.white
            
            self.seminarCalenderImage.isHidden = false
            self.upcomingSeminarImage.isHidden = true
            self.postSeminarImage.isHidden = true
            
               let controller:CalenderViewController =
                       self.storyboard!.instantiateViewController(withIdentifier: "CalenderViewController") as!
                       CalenderViewController

                       let statusbarHeight = UIApplication.shared.statusBarFrame.height
               //        let navigationBarHeight = uiappl
                       
                       let tabBarHeight = self.tabBarController!.tabBar.frame.size.height+100;

                       
                       
                       controller.view.frame = CGRect(x: 0, y: statusbarHeight+80, width: UIScreen.main.bounds.size.width, height: (UIScreen.main.bounds.size.height-tabBarHeight))
                       controller.willMove(toParent: self)
                       self.view.addSubview(controller.view)
                       self.addChild(controller)
                       controller.didMove(toParent: self)
           }
          
           
           
           
       }
  
    @IBAction func seminarCalender(_ sender: Any) {
        let controller:CalenderViewController =
                self.storyboard!.instantiateViewController(withIdentifier: "CalenderViewController") as!
                CalenderViewController
        DispatchQueue.main.async {
//             self.seminarCalenderLabel.backgroundColor = UIColor.black
//                   self.upcomingLbl.backgroundColor = UIColor.white
//                   self.postSeminarsLbl.backgroundColor = UIColor.white
            
            self.seminarCalenderImage.isHidden = false
            self.upcomingSeminarImage.isHidden = true
            self.postSeminarImage.isHidden = true
        }
       

                let statusbarHeight = UIApplication.shared.statusBarFrame.height
        //        let navigationBarHeight = uiappl
                
                let tabBarHeight = self.tabBarController!.tabBar.frame.size.height+100;

                
                
                controller.view.frame = CGRect(x: 0, y: statusbarHeight+80, width: UIScreen.main.bounds.size.width, height: (UIScreen.main.bounds.size.height-tabBarHeight))
                controller.willMove(toParent: self)
                self.view.addSubview(controller.view)
                self.addChild(controller)
                controller.didMove(toParent: self)
    }
}

extension String {
    public var withoutHtml: String {
        guard let data = self.data(using: .utf8) else {
            return self
        }

        let options: [NSAttributedString.DocumentReadingOptionKey: Any] = [
            .documentType: NSAttributedString.DocumentType.html,
            .characterEncoding: String.Encoding.utf8.rawValue
        ]

        guard let attributedString = try? NSAttributedString(data: data, options: options, documentAttributes: nil) else {
            return self
        }

        return attributedString.string
    }
}
