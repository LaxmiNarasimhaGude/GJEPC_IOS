//
//  CalenderViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 08/04/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import JTAppleCalendar

class CalenderViewController: UIViewController  {

    
    @IBOutlet weak var year: UILabel!
    @IBOutlet weak var calendarView: JTACMonthView!
    
        var calendarDataSource: [String:String] = [:]
    var EndcalendarDataSource: [String:String] = [:]
    var replacedImgstring  = ""
        var formatter: DateFormatter {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MMM-yyyy"
            formatter.timeZone = Calendar.current.timeZone
            formatter.locale = Calendar.current.locale
            return formatter
        }
        var tableViewArrayForNews : NSMutableArray = []

        
        override func viewDidLoad() {
            super.viewDidLoad()
//            calendarView.items
            calendarView.ibCalendarDelegate = self
            calendarView.ibCalendarDataSource = self
            calendarView.scrollDirection = .horizontal
            calendarView.scrollingMode   = .stopAtEachCalendarFrame
            calendarView.showsHorizontalScrollIndicator = false
            DispatchQueue.main.async {
                self.getDataFromServer()

            }
            
            calendarView.visibleDates { (visibleDates) in
                self.setupview(from: visibleDates)
            }
            
//            let d = Date()
//               var dateArr = [Date]()
//               dateArr.append(d)
//
//            JTACMonthView.
            calendarView.reloadData(withAnchor: Date(), completionHandler: nil)
            
    //        populateDataSource()
             
        }
        
        func setupview(from visibleDates: DateSegmentInfo){
               let date = visibleDates.monthDates.first?.date

               //        formatter.dateFormat = "yyy MM dd"
                           formatter.dateFormat = "yyy"
                           let d = formatter.string(from: date!)
                       let a = d.split(separator: "-")
                       year.text = "\(a[1]) \(a[2])"
    //           self.formatter.dateFormat = "MMM"
    //           self.month.text = self.formatter.string(from: date!)
               calendarView.reloadData()
           }
        
    @IBAction func previous(_ sender: Any) {
        calendarView.scrollToSegment(.previous)
    }
    @IBAction func next(_ sender: Any) {
        
        calendarView.scrollToSegment(.next)
    }
    func calendar(_ calendar: JTACMonthView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
    //        formatter.dateFormat = "yyy MM dd"
                let date = visibleDates.monthDates.first?.date

    //        formatter.dateFormat = "yyy MM dd"
                formatter.dateFormat = "yyy"
                let d = formatter.string(from: date!)
            let a = d.split(separator: "-")
            year.text = "\(a[1]) \(a[2])"
    //            formatter.dateFormat = "MMM"
    //            month.text = formatter.string(from: date!)
            }
    //    func populateDataSource() {
    //        // update the datrasource
    //        calendarDataSource = [
    //            "07-Jan-2019": "SomeData",
    //            "15-Jan-2019": "SomeMoreData",
    //            "15-Feb-2019": "MoreData",
    //            "21-Feb-2019": "onlyData",
    //        ]
    //        calendarView.reloadData()
    //    }
        
    func configureCell(view: JTACDayCell?, cellState: CellState) {
            guard let cell = view as? CustomCell  else { return }
    //        print(cellState.date)
            cell.dateLabel.text = cellState.text
            handleCellTextColor(cell: cell, cellState: cellState)
            handleCellEvents(cell: cell, cellState: cellState)
        }
        
        func handleCellTextColor(cell: CustomCell, cellState: CellState) {
            if cellState.dateBelongsTo == .thisMonth {
                cell.dateLabel.textColor = UIColor.black
                cell.dotView.backgroundColor = UIColor.red
                cell.endDotView.backgroundColor = UIColor.black
                cell.contentView.backgroundColor = UIColor.red
    //            cell.dotView.isHidden = false
            } else {
                cell.contentView.backgroundColor = UIColor.clear
                cell.dotView.backgroundColor = UIColor.clear
                cell.endDotView.backgroundColor = UIColor.clear
                cell.dateLabel.textColor = UIColor.clear
                cell.dotView.isHidden = true
            }
        }
        
        func handleCellEvents(cell: CustomCell, cellState: CellState) {
            let dateString = formatter.string(from: cellState.date)
    //        print(dateString)
    //        print(calendarDataSource[dateString])
            if calendarDataSource[dateString] == nil {
                cell.dotView.isHidden = true
//
            } else {
                cell.dotView.isHidden = false
//
            }
            
            if EndcalendarDataSource[dateString] == nil{
                 cell.endDotView.isHidden = true
            }
            else{
                cell.endDotView.isHidden = false
            }
            if cellState.dateBelongsTo == .thisMonth {
                        cell.dateLabel.textColor = UIColor.black
            //            cell.dotView.isHidden = false
                    } else {
                        cell.dateLabel.textColor = UIColor.clear
                        cell.dotView.isHidden = true
                    }
        }
    }

extension CalenderViewController: JTACMonthViewDataSource {
        func configureCalendar(_ calendar: JTACMonthView) -> ConfigurationParameters {
//            let startDate = formatter.date(from: "01-jan-2018")
//            //print(startDate);
//
//            let yearsToAdd = 10
//            let endDate = formatter.date(from: "31-dec-2117")
//
//            let params = ConfigurationParameters(
//                startDate: startDate!,
//                endDate: endDate!,
//                generateInDates: .forAllMonths,
//                generateOutDates: .tillEndOfRow
//            )
//
//            return params
            let startDate = formatter.date(from: "01-jan-2018")!
            let endDate = formatter.date(from: "31-dec-2117")!
            var generateInDates: InDateCellGeneration = .forAllMonths
            var generateOutDates: OutDateCellGeneration = .tillEndOfGrid
            let firstDayOfWeek: DaysOfWeek = .sunday
            var numberOfRows = 6
            var testCalendar = Calendar.current

            let parameters = ConfigurationParameters(startDate: startDate,
                                                     endDate: endDate,
                                                     numberOfRows: numberOfRows,
                                                     calendar: testCalendar,
                                                     generateInDates: generateInDates,
                                                     generateOutDates: generateOutDates,
                                                     firstDayOfWeek: firstDayOfWeek)

            return parameters
        }
    }

extension CalenderViewController: JTACMonthViewDelegate {
    func calendar(_ calendar: JTACMonthView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTACDayCell {
            let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "CustomCell", for: indexPath) as! CustomCell
            self.calendar(calendar, willDisplay: cell, forItemAt: date, cellState: cellState, indexPath: indexPath)
        
        let currentDay = Date()
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd MM yyyy"
                
                let currentDateString = dateFormatter.string(from: currentDay)
                let cellStateDateString = dateFormatter.string(from: cellState.date)
                
                if  currentDateString ==  cellStateDateString {
        //            validCell.selectView.isHidden = false
//                    cell.contentView.backgroundColor = UIColor.red
        //            validCell.selectView.layer.cornerRadius = 22.5
        //            validCell.label.textColor = UIColor.white
                } else {
                    cell.contentView.backgroundColor = UIColor.clear
//                    cell.contentView.backgroundColor = UIColor.black
        //            validCell.currentDaySelectView.isHidden = true
        //            validCell.label.textColor = UIColor.black
                }
            return cell
        }
        
    func calendar(_ calendar: JTACMonthView, willDisplay cell: JTACDayCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
            configureCell(view: cell, cellState: cellState)
        }
        
        
    //    func calendar(_ calendar: JTAppleCalendarView, headerViewForDateRange range: (start: Date, end: Date), at indexPath: IndexPath) -> JTAppleCollectionReusableView {
    //        let formatter = DateFormatter()  // Declare this outside, to avoid instancing this heavy class multiple times.
    //        formatter.timeZone = Calendar.current.timeZone
    //        formatter.locale = Calendar.current.locale
    //        formatter.dateFormat = "MMM"
    //
    //        let header = calendar.dequeueReusableJTAppleSupplementaryView(withReuseIdentifier: "DateHeader", for: indexPath) as! DateHeader
    //        header.monthTitle.text = formatter.string(from: range.start.self)
    ////        header.yearLabel.text = formatter.string(from: start. )
    //        return header
    //    }
       
    //    func calendarSizeForMonths(_ calendar: JTAppleCalendarView?) -> MonthSize? {
    //        return MonthSize(defaultSize: 50)
    //    }
    //    func calendar(_ calendar: JTAppleCalendarView, willDisplaySectionHeader header: JTAppleHeaderView, range: (start: Date, end: Date), identifier: String) {
    //        let headerCell = (header as? PinkSectionHeaderView)
    //        headerCell?.title.text = "S           M           T           W           T           F           S"
    //    }
    //    func calendar(_ calendar: JTAppleCalendarView, headerViewForDateRange range: (start: Date, end: Date), at indexPath: IndexPath) -> JTAppleCollectionReusableView {
    //        <#code#>
    //    }
        
        
            func getDataFromServer() {
                let url = URL(string: "https://gjepc.org/gjepc_mobile/services/seminarCalender")
                
                //create the session object
                let session = URLSession.shared
                
                //now create the URLRequest object using the url object
                let request = URLRequest(url: url! )
                
                //create dataTask using the session object to send data to the server
                let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                    
                    guard error == nil else {
                        return
                    }
                    
                    guard let data = data else {
                        return
                    }
                    
                    do {
                        //create json object from data
                        if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
        //                    print(json)
                            if json.count > 0 {
                                if ((json.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "true"{
                                    self.tableViewArrayForNews = ((json.value(forKey: "Response") as! NSDictionary).value(forKey: "Result") as! NSMutableArray)
        //                            print(temporaryArray)
                                    for i in 0..<self.tableViewArrayForNews.count{
                                      ((self.tableViewArrayForNews.object(at: i) as AnyObject).object(forKey: "start") as! String)
    //                                    print(((self.tableViewArrayForNews.object(at: i) as AnyObject).object(forKey: "start") as! String))
                                        
    //                                    print( self.convertDateFormater())
    //                                    print(((self.tableViewArrayForNews.object(at: i) as AnyObject).object(forKey: "start") as! String))
                                        
                                        let d = ((self.tableViewArrayForNews.object(at: i) as AnyObject).object(forKey: "start") as! String)
                                        let dd = ((self.tableViewArrayForNews.object(at: i) as AnyObject).object(forKey: "full_description") as! String)
                                       
                                        let convertedDate = self.convertDateFormater(date: d)
                                        self.calendarDataSource[convertedDate] = dd
                                        
                                        let e = ((self.tableViewArrayForNews.object(at: i) as AnyObject).object(forKey: "end") as! String)
                                        let endCon = self.convertDateFormater(date: e)
                                       
                                        if !self.EndcalendarDataSource.keys.contains(e){
                                             self.EndcalendarDataSource[endCon] = dd
                                        }
                                       
                                    }
//                                     print(self.calendarDataSource)
                                    DispatchQueue.main.async {
                                        self.calendarView.ibCalendarDelegate = self
                                        self.calendarView.ibCalendarDataSource = self
                                         self.calendarView.reloadData()
    //                                    print(                                    self.calendarDataSource.keys.contains("02-Sep-2018")
    //)
                                    }
                                    

                                }
                            }
                            
                        }
                    } catch let error {
                        print(error.localizedDescription)
                    }
                })
                task.resume()
            }
        func convertDateFormater(date: String) -> String
        {
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = Calendar.current.timeZone
            dateFormatter.locale = Calendar.current.locale
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let date = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "dd-MMM-yyyy"
            return  dateFormatter.string(from: date!)

        }
    func calendar(_ calendar: JTACMonthView, didSelectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath) {

    //        print(formatter.string(from: cellState.date))
    //        let d = formatter.string(from: cellState.date)
                    let dateString = formatter.string(from: cellState.date)
            //        print(dateString)

        
        
//        if !self.calendarDataSource.keys.contains(e){
//             self.calendarDataSource[endCon] = dd
//        }
        
        for i in 0..<tableViewArrayForNews.count{
//             print(convertDateFormater(date: ((self.tableViewArrayForNews.object(at: i) as AnyObject).object(forKey: "start") as! String)))
            let convertedDate = convertDateFormater(date: ((self.tableViewArrayForNews.object(at: i) as AnyObject).object(forKey: "start") as! String))
            let convertedEndDate = convertDateFormater(date: ((self.tableViewArrayForNews.object(at: i) as AnyObject).object(forKey: "end") as! String))
//            print(dateString)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "SeminarWebView") as! SeminarWebView
            if (dateString == convertedDate)||(dateString == convertedEndDate ){
//                print(((self.tableViewArrayForNews.object(at: i) as AnyObject).object(forKey: "images") as! String))
                vc.id = ((self.tableViewArrayForNews.object(at: i) as AnyObject).object(forKey: "id") as! String)
                vc.type = "calender"
                vc.discription = ((self.tableViewArrayForNews.object(at: i) as AnyObject).object(forKey: "title") as! String).withoutHtml
                
                let imgdata = ((self.tableViewArrayForNews.object(at: i) as AnyObject).object(forKey: "images") as! String)
                       let imgfilterUrl = seminarImageUrl + imgdata
                       replacedImgstring = imgfilterUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!
                print(replacedImgstring)
                        vc.imageUrl = replacedImgstring
                

                        let discription =  ((self.tableViewArrayForNews.object(at: i) as AnyObject).object(forKey: "full_description") as! String)


                        vc.dis = discription
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        
        
        

                    if calendarDataSource[dateString] != nil {
//                        print(calendarDataSource[dateString]!)
                    }
                    else{
//                        print("nothing")

            }
    //        calendarDataSource[formatter.string(from: cellState.date)]
        }
    
//    func handleCellCurrentDay(view: JTACDayCell?, cellState: CellState) {
//        guard let validCell = view as? CustomCell else { return }
//
//        let currentDay = Date()
//
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "dd MM yyyy"
//
//        let currentDateString = dateFormatter.string(from: currentDay)
//        let cellStateDateString = dateFormatter.string(from: cellState.date)
//
//        if  currentDateString ==  cellStateDateString {
////            validCell.selectView.isHidden = false
//            validCell.backgroundColor = UIColor.black
////            validCell.selectView.layer.cornerRadius = 22.5
////            validCell.label.textColor = UIColor.white
//        } else {
////            validCell.currentDaySelectView.isHidden = true
////            validCell.label.textColor = UIColor.black
//        }
//    }
    
    }
