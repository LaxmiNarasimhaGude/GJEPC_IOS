//
//  postEventUpdateViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 14/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
class postEventUpdateViewController: UIViewController,UIWebViewDelegate {

    @IBOutlet weak var postUpdateWebview: UIWebView!
    
    var pdf = ""
    var activityLoader : MBProgressHUD? = nil
    override func viewDidLoad() {
            super.viewDidLoad()

            postUpdateWebview.delegate = self
            if pdf == ""{
             print("nothing in pdf")
            }
            else{
                print(pdf)
    //            let url = URL (string: "https://gjepc.org/admin/calendar/"+"\(pdf)")
                
                 guard let url = URL(string: "https://gjepc.org/admin/calendar/"+"\(pdf)") else { return }
                print(url as Any)
                
                let requestObj = URLRequest(url: url)
                DispatchQueue.main.async {
                    self.loadActivityIndicator()
                    self.postUpdateWebview.loadRequest(requestObj)
                }
                      
            }
           
        }
    
    func loadActivityIndicator()
    {
        activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
        activityLoader?.label.text = "Loading";
        activityLoader?.detailsLabel.text = "Please Wait";
        activityLoader?.isUserInteractionEnabled = false;
    }
   func webViewDidFinishLoad(_ webView: UIWebView) {
       activityLoader?.hide(animated: true)
   }
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}
