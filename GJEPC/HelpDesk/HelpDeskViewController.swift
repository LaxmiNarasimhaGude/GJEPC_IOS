//
//  HelpDeskViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 20/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
class HelpDeskViewController: UIViewController,UIWebViewDelegate {

    @IBOutlet weak var heading: UILabel!
    @IBOutlet weak var webView: UIWebView!
    var urlString = ""
    var headingLbl = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        self.heading.text = headingLbl
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    
//        var path = documentsDirectory.appendingPathComponent("badge.html")
//        let data: NSData = NSData(contentsOfFile:path)!
//        var html = String(data: data as Data, encoding:String.Encoding.utf8)
//
//        webView.loadHTMLString(html as! String, baseURL: Bundle.main.bundleURL)
        
        
        
//            let dataPath = documentsDirectory.appendingPathComponent("badge.html")
//            print("path is:\(dataPath)")
//            let url = URL.init(fileURLWithPath: dataPath.path);
//
//            let requestObj = URLRequest.init(url: url)
//
//            webView.loadRequest(requestObj)
        
        
        
        
        
        
         URLCache.shared.removeAllCachedResponses()
               URLCache.shared.diskCapacity = 0
               URLCache.shared.memoryCapacity = 0

        let url = URL(string: urlString)
               print(url)
               let request = NSMutableURLRequest(url: url!)
               self.webView.delegate = self
               self.webView.loadRequest(request as URLRequest)
        DispatchQueue.main.async {
                    MBProgressHUD.showAdded(to: self.view, animated: true)

                }
//                Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
//
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        DispatchQueue.main.async {
            MBProgressHUD.hide(for: self.view, animated: true)
            
        }
        
    }

    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    

}
