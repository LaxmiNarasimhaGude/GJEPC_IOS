//
//  SignatureViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 13/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
@available(iOS 13.0, *)
class SignatureViewController: UIViewController {
    
    
    @IBOutlet weak var exhibitorDirBtn: UIButton!
    @IBOutlet weak var zoneManagerImage: UIImageView!
    @IBOutlet weak var businesstoolsImage: UIImageView!
    @IBOutlet weak var contactusImage: UIImageView!
    @IBOutlet weak var howtoreachImage: UIImageView!
    @IBOutlet weak var serviceImage: UIImageView!
    @IBOutlet weak var showinfoImage: UIImageView!
    @IBOutlet weak var showUpdateImage: UIImageView!
    @IBOutlet weak var floorImage: UIImageView!
    @IBOutlet weak var exhibitorDirectoryImage: UIImageView!
    

       var requestToServer = RequestToServer()
       var exhibitorSelected = ""
       var category = ""
       var titleName = ""
       var EventString: String!
       var checkStatus: String!
       var websiteTitle:String!
       var activityLoader : MBProgressHUD? = nil
       var websitelink:String!
    var year:String!
    override func viewDidLoad() {
        super.viewDidLoad()

        DispatchQueue.main.async {
            
//            self.exhibitorDirBtn.isUserInteractionEnabled = true
            
             self.exhibitorDirectoryImage.layer.cornerRadius = self.exhibitorDirectoryImage.frame.size.width/2
                                   self.exhibitorDirectoryImage.layer.backgroundColor = UIColor.white.cgColor
                                   self.exhibitorDirectoryImage.layer.shadowColor = UIColor.lightGray.cgColor
                                   self.exhibitorDirectoryImage.layer.shadowOffset = CGSize(width: 0, height: 2.0)//CGSizeMake(0, 2.0);
                                   self.exhibitorDirectoryImage.layer.shadowRadius = 2.0
                                   self.exhibitorDirectoryImage.layer.shadowOpacity = 1.0
                                   self.exhibitorDirectoryImage.layer.masksToBounds = false
                                  self.exhibitorDirectoryImage.layer.shadowPath = UIBezierPath(roundedRect:self.exhibitorDirectoryImage.bounds, cornerRadius:self.exhibitorDirectoryImage.layer.cornerRadius).cgPath
            
            
             self.floorImage.layer.cornerRadius = self.floorImage.frame.size.width/2
                        self.floorImage.layer.backgroundColor = UIColor.white.cgColor
                        self.floorImage.layer.shadowColor = UIColor.lightGray.cgColor
                        self.floorImage.layer.shadowOffset = CGSize(width: 0, height: 2.0)//CGSizeMake(0, 2.0);
                        self.floorImage.layer.shadowRadius = 2.0
                        self.floorImage.layer.shadowOpacity = 1.0
                        self.floorImage.layer.masksToBounds = false
                       self.floorImage.layer.shadowPath = UIBezierPath(roundedRect:self.floorImage.bounds, cornerRadius:self.floorImage.layer.cornerRadius).cgPath
            
             self.showinfoImage.layer.cornerRadius = self.showinfoImage.frame.size.width/2
             self.showinfoImage.layer.backgroundColor = UIColor.white.cgColor
             self.showinfoImage.layer.shadowColor = UIColor.lightGray.cgColor
             self.showinfoImage.layer.shadowOffset = CGSize(width: 0, height: 2.0)//CGSizeMake(0, 2.0);
             self.showinfoImage.layer.shadowRadius = 2.0
             self.showinfoImage.layer.shadowOpacity = 1.0
             self.showinfoImage.layer.masksToBounds = false
            self.showinfoImage.layer.shadowPath = UIBezierPath(roundedRect:self.showinfoImage.bounds, cornerRadius:self.showinfoImage.layer.cornerRadius).cgPath
            
            
            self.showUpdateImage.layer.cornerRadius = self.showUpdateImage.frame.size.width/2
            self.showUpdateImage.layer.backgroundColor = UIColor.white.cgColor
            self.showUpdateImage.layer.shadowColor = UIColor.lightGray.cgColor
            self.showUpdateImage.layer.shadowOffset = CGSize(width: 0, height: 2.0)//CGSizeMake(0, 2.0);
            self.showUpdateImage.layer.shadowRadius = 2.0
            self.showUpdateImage.layer.shadowOpacity = 1.0
            self.showUpdateImage.layer.masksToBounds = false
            self.showUpdateImage.layer.shadowPath = UIBezierPath(roundedRect:self.showUpdateImage.bounds, cornerRadius:self.showUpdateImage.layer.cornerRadius).cgPath
            
            self.serviceImage.layer.cornerRadius = self.serviceImage.frame.size.width/2
            self.serviceImage.layer.backgroundColor = UIColor.white.cgColor
            self.serviceImage.layer.shadowColor = UIColor.lightGray.cgColor
            self.serviceImage.layer.shadowOffset = CGSize(width: 0, height: 2.0)//CGSizeMake(0, 2.0);
            self.serviceImage.layer.shadowRadius = 2.0
            self.serviceImage.layer.shadowOpacity = 1.0
            self.serviceImage.layer.masksToBounds = false
            self.serviceImage.layer.shadowPath = UIBezierPath(roundedRect:self.serviceImage.bounds, cornerRadius:self.serviceImage.layer.cornerRadius).cgPath
            
            
            self.howtoreachImage.layer.cornerRadius = self.howtoreachImage.frame.size.width/2
            self.howtoreachImage.layer.backgroundColor = UIColor.white.cgColor
            self.howtoreachImage.layer.shadowColor = UIColor.lightGray.cgColor
            self.howtoreachImage.layer.shadowOffset = CGSize(width: 0, height: 2.0)//CGSizeMake(0, 2.0);
            self.howtoreachImage.layer.shadowRadius = 2.0
            self.howtoreachImage.layer.shadowOpacity = 1.0
            self.howtoreachImage.layer.masksToBounds = false
            self.howtoreachImage.layer.shadowPath = UIBezierPath(roundedRect:self.howtoreachImage.bounds, cornerRadius:self.howtoreachImage.layer.cornerRadius).cgPath
            
            
            self.contactusImage.layer.cornerRadius = self.contactusImage.frame.size.width/2
            self.contactusImage.layer.backgroundColor = UIColor.white.cgColor
            self.contactusImage.layer.shadowColor = UIColor.lightGray.cgColor
            self.contactusImage.layer.shadowOffset = CGSize(width: 0, height: 2.0)//CGSizeMake(0, 2.0);
            self.contactusImage.layer.shadowRadius = 2.0
            self.contactusImage.layer.shadowOpacity = 1.0
            self.contactusImage.layer.masksToBounds = false
            self.contactusImage.layer.shadowPath = UIBezierPath(roundedRect:self.contactusImage.bounds, cornerRadius:self.contactusImage.layer.cornerRadius).cgPath
            
            self.businesstoolsImage.layer.cornerRadius = self.businesstoolsImage.frame.size.width/2
            self.businesstoolsImage.layer.backgroundColor = UIColor.white.cgColor
            self.businesstoolsImage.layer.shadowColor = UIColor.lightGray.cgColor
            self.businesstoolsImage.layer.shadowOffset = CGSize(width: 0, height: 2.0)//CGSizeMake(0, 2.0);
            self.businesstoolsImage.layer.shadowRadius = 2.0
            self.businesstoolsImage.layer.shadowOpacity = 1.0
            self.businesstoolsImage.layer.masksToBounds = false
            self.businesstoolsImage.layer.shadowPath = UIBezierPath(roundedRect:self.businesstoolsImage.bounds, cornerRadius:self.businesstoolsImage.layer.cornerRadius).cgPath
            

           self.zoneManagerImage.layer.cornerRadius = self.zoneManagerImage.frame.size.width/2
           self.zoneManagerImage.layer.backgroundColor = UIColor.white.cgColor
           self.zoneManagerImage.layer.shadowColor = UIColor.lightGray.cgColor
           self.zoneManagerImage.layer.shadowOffset = CGSize(width: 0, height: 2.0)//CGSizeMake(0, 2.0);
           self.zoneManagerImage.layer.shadowRadius = 2.0
           self.zoneManagerImage.layer.shadowOpacity = 1.0
           self.zoneManagerImage.layer.masksToBounds = false
            self.zoneManagerImage.layer.shadowPath = UIBezierPath(roundedRect:self.zoneManagerImage.bounds, cornerRadius:self.zoneManagerImage.layer.cornerRadius).cgPath
            
        }
    }
    

    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func floorPlan(_ sender: Any) {
        
     let vc = self.storyboard?.instantiateViewController(withIdentifier: "ServiceVenueWebVIew") as! ServiceVenueWebVIew
        vc.requestName = "floorPlan"
        vc.htmlFileName = "floor.html"
        //vc.fileTitle = "Floor Plan"
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func showupdate(_ sender: Any) {
        

        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShowUpdate") as! ShowUpdate
        
        vc.eventName = exhibitorSelected
        vc.year = self.year
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func serviceAtVenue(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VenueService") as! VenueService
               vc.eventName = exhibitorSelected
               
               vc.requestName = "venue"
               
               self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func reach(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VenueService") as! VenueService
               vc.requestName = "reach"
               vc.eventName = exhibitorSelected
               self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func showInfo(_ sender: Any) {


        print(exhibitorSelected)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VenueService") as! VenueService
        vc.requestName = "showinfo"

        vc.eventName = exhibitorSelected

        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func contactUs(_ sender: Any) {
        DispatchQueue.main.async {
           
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "IIJSContactViewController") as! IIJSContactViewController
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func exhibitorDirBtn(_ sender: Any) {

        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ExhibitorDirectoryListViewController") as! ExhibitorDirectoryListViewController
        if category == "national"
        {
            vc.eventCategory = "national"
            vc.exhibitorSelected = exhibitorSelected
           // print("selected \(vc.exhibitorName)")
        } else
        {
            vc.eventCategory = "international"
         //   vc.exhibitorName = "international"
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func zoneManager(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ZoneManager") as! ZoneManager
        vc.eventName = self.exhibitorSelected
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
