//
//  EventsSignatureViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 10/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class EventsSignatureViewController: UIViewController {
    @IBOutlet weak var eventsImage: UIImageView!
    @IBOutlet weak var nationalImage: UIImageView!
    @IBOutlet weak var internationalImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        eventsImage.layer.cornerRadius = 5;
        eventsImage.contentMode = .scaleAspectFill
        eventsImage.clipsToBounds = true;
        
        
        nationalImage.contentMode = .scaleAspectFill
        nationalImage.clipsToBounds = true;
        nationalImage.layer.cornerRadius = 5;
        
        internationalImage.layer.cornerRadius = 5;
        internationalImage.contentMode = .scaleAspectFill
        internationalImage.clipsToBounds = true;
        
        
    }
    

    @IBAction func events(_ sender: Any) {
        
        
        if CheckInternet.isConnectedToNetwork() == true{
            let vc = storyboard?.instantiateViewController(withIdentifier: "EventsAndTradeShowsViewController") as! EventsAndTradeShowsViewController
            vc.backType = "back"
            navigationController?.pushViewController(vc, animated: true)
        }
        else{
            let alert = UIAlertController(title: "", message: "Please Check Your Internet Connection", preferredStyle: UIAlertController.Style.alert)
            
             self.present(alert, animated: true, completion: nil)
            let when = DispatchTime.now()+2
            DispatchQueue.main.asyncAfter(deadline: when){
                alert.dismiss(animated: true, completion: nil)
            }
            
           
        }
        
    }
    
    @IBAction func backtn(_ sender: Any) {
//                 let mainPage = storyboard?.instantiateViewController(withIdentifier: "HomeTabBarViewController") as! HomeTabBarViewController
//                 let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                 appDelegate.window?.rootViewController = mainPage
        
        
        navigationController?.popViewController(animated: true)
        
        
    }
    @IBAction func internationalEvents(_ sender: Any) {
        
        if CheckInternet.isConnectedToNetwork() == true{
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                         let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "InternationalEventsViewController") as! InternationalEventsViewController

                 self.navigationController?.pushViewController(redViewController, animated: true)
                         
                     }
        else{
            let alert = UIAlertController(title: "", message: "Please Check Your Internet Connection", preferredStyle: UIAlertController.Style.alert)
            
             self.present(alert, animated: true, completion: nil)
            let when = DispatchTime.now()+2
            DispatchQueue.main.asyncAfter(deadline: when){
                alert.dismiss(animated: true, completion: nil)
            }
            
           
        }

        }
              
    
    @IBAction func national(_ sender: Any) {
       
        let vc = storyboard?.instantiateViewController(withIdentifier: "NationalSignatureViewController") as! NationalSignatureViewController; navigationController?.pushViewController(vc, animated: true)
    }
    
}
