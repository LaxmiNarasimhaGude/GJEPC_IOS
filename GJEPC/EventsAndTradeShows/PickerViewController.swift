//
//  PickerViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 29/07/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit

protocol pickerDelegate
{
    func cancelClicked(sender: PickerViewController)
    func doneClicked(sender: PickerViewController)
}


class PickerViewController: UIViewController ,UIPickerViewDelegate, UIPickerViewDataSource {

    var delegate: pickerDelegate!
       
       var pickerData: NSMutableArray = []
    
    @IBOutlet weak var pickerView: UIPickerView!
    override func viewDidLoad()
       {
           super.viewDidLoad()
           
           self.pickerView.dataSource = self
           self.pickerView.delegate = self
           
       }
       
       func numberOfComponents(in pickerView: UIPickerView) -> Int
       {
           return 1
       }
       
       func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
       {
           return pickerData.count
       }
       
       func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
       {
           return (pickerData[row] as! String)
       }
    
    @IBAction func cancelButtonClicked(_ sender: Any) {
        self.delegate?.cancelClicked(sender: self)
        
    }
    
    @IBAction func doneButtonClicked(_ sender: Any) {
        self.delegate?.doneClicked(sender: self)
        
    }
    

}
