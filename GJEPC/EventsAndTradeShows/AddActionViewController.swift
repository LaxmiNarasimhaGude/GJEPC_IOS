//
//  AddActionViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 28/07/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import CoreData
protocol backBtnTappedDelegate {
    func backBtnTapped()
}
@available(iOS 13.0, *)
class AddActionViewController: UIViewController {

    @IBOutlet weak var addActionButton: UIButton!
    @IBOutlet weak var callActionButton: UIButton!
    @IBOutlet weak var emailActionButton: UIButton!
    @IBOutlet weak var shareActionButton: UIButton!
    @IBOutlet weak var bookmarkActionButton: UIButton!
    @IBOutlet weak var visitedActionButton: UIButton!
    @IBOutlet weak var takeAnoteButton: UIButton!
    @IBOutlet weak var bookmarkLabel: UILabel!
    @IBOutlet weak var visitedLabel: UILabel!
    
       var bckBtnDelegate : backBtnTappedDelegate!
       var gtExhibitorContactno1 = ""
       var gtExhibitorContactno2 = ""
       var gtExhibitorEmail1 = ""
       var gtExhibitorEmail2 =  ""
       var gtExhibitorName = ""
       var gtExhibitorAddress = ""
       var profileBtnState = 1
       var bookmarkBtnState = 1
       var bookmarkedChecked = Bool()
       var bookmarkButtonTapped = Bool()
       var visitedButtonTapped = Bool()
       var isVisitedChecked = Bool()
       var visitedChecked = ""
       var appdelegate : AppDelegate!
       var moc : NSManagedObjectContext!
       var coreDataArray : NSArray  = []
       var coreDataCount = 0
       var exhibitorIdForStarMarked : String = ""
       var exhibitorId = ""
       var custom  = Custom_ObjectsViewController()
       var exhibitorCat = ""
    
    override func viewDidLoad() {
           super.viewDidLoad()
           appdelegate = (UIApplication.shared.delegate as! AppDelegate)
           moc = appdelegate.managedObjectContext
           
           print(exhibitorId)
           
       }
    @IBAction func addActionBtn(_ sender: Any) {
        DispatchQueue.main.async {
            self.callActionButton.center.x += self.view.bounds.width
            self.emailActionButton.center.x -= self.view.bounds.width
            self.takeAnoteButton.center.x += self.view.bounds.width
            self.shareActionButton.center.x -= self.view.bounds.width
            UIView.animate(withDuration: 0.8, delay: 0.3,
                           usingSpringWithDamping: 0.3,
                           initialSpringVelocity: 0.5,
                           options: [], animations: {
                            
                            self.callActionButton.center.x -= self.view.bounds.width
                            self.emailActionButton.center.x += self.view.bounds.width
                            self.takeAnoteButton.center.x -= self.view.bounds.width
                            self.shareActionButton.center.x += self.view.bounds.width
            }, completion: nil)
            
            UIView.animate(withDuration: 0.2) { () -> Void in
                
                self.visitedActionButton.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi))
                self.bookmarkActionButton.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi))
                self.addActionButton.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi))
            }
            
            UIView.animate(withDuration: 0.5, delay: 0.10, options: UIView.AnimationOptions.transitionCurlUp, animations: {
                self.visitedActionButton.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi * 2))
                self.bookmarkActionButton.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi * 2))
                self.addActionButton.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi * 2))
            }, completion: { (true) in
                self.bckBtnDelegate.backBtnTapped()
            })
            
        }
    }
    
    @IBAction func callActionBtnClicked(_ sender: Any) {
        
        if gtExhibitorContactno2 != "" {
            let alert = UIAlertController(title: "Choose a number to call", message: "Please choose which number you want to call", preferredStyle: .alert)
            let contactno1 = UIAlertAction(title: "\(gtExhibitorContactno1)", style: .default, handler: { (UIAlertAction) in
                self.callNumber(phoneNumber: self.gtExhibitorContactno1)
            })
            alert.addAction(contactno1)
            
            let contactno2 = UIAlertAction(title: "\(gtExhibitorContactno2)", style: .default, handler: { (UIAlertAction) in
                self.callNumber(phoneNumber: self.gtExhibitorContactno2)
            })
            alert.addAction(contactno2)
            if alert.actions.count == 0 {
                alert.title = "No numbers to call"
                alert.message = ""
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            } else {
                alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
            }
            self.present(alert, animated: true, completion: nil)
        } else {
            callNumber(phoneNumber: gtExhibitorContactno1)
        }
    }
    func callNumber(phoneNumber:String) {
           
           if let phoneCallURL = URL(string: "telprompt://\(gtExhibitorContactno1.replacingOccurrences(of: " ", with: ""))"){
               let application:UIApplication = UIApplication.shared
               if (application.canOpenURL(phoneCallURL as URL)) {
                   application.openURL(phoneCallURL as URL)
               }
           }
       }
    @IBAction func emailActionBtnClicked(_ sender: Any) {
        if gtExhibitorEmail2 == "" {
            let stringUrl = NSURL(string : "mailto:\(gtExhibitorEmail1)")
            if UIApplication.shared.canOpenURL(stringUrl! as URL)
            {
                UIApplication.shared.openURL(stringUrl! as URL)
            }
        } else {
            let alert = UIAlertController(title: "Choose an emailID to send", message: "", preferredStyle: .alert)
            let contactno1 = UIAlertAction(title: "\(gtExhibitorContactno1)", style: .default, handler: { (UIAlertAction) in
                let stringUrl = NSURL(string : "mailto:\(self.gtExhibitorEmail1)")
                if UIApplication.shared.canOpenURL(stringUrl! as URL)
                {
                    UIApplication.shared.openURL(stringUrl! as URL)
                }
            })
            alert.addAction(contactno1)
            
            let contactno2 = UIAlertAction(title: "\(gtExhibitorContactno2)", style: .default, handler: { (UIAlertAction) in
                let stringUrl = NSURL(string : "mailto:\(self.gtExhibitorEmail2)")
                if UIApplication.shared.canOpenURL(stringUrl! as URL)
                {
                    UIApplication.shared.openURL(stringUrl! as URL)
                }
            })
            alert.addAction(contactno2)
            if alert.actions.count == 0 {
                alert.title = "No numbers to call"
                alert.message = ""
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            } else {
                alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
            }
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func takeANoteActionBtnClicked(_ sender: Any) {
        
//        print(self.exhibitorId)
//
//        let viewnoteVC = self.storyboard?.instantiateViewController(withIdentifier: "ViewYourNotesViewController") as! ViewYourNotesViewController
//        viewnoteVC.exhibitorId = self.exhibitorId
//        viewnoteVC.exhibitorCategory = self.exhibitorCat
//        viewnoteVC.companyName = self.gtExhibitorName
//
//        DispatchQueue.main.async {
//            self.navigationController?.pushViewController(viewnoteVC, animated: true)
//        }
    }
    @IBAction func shareBtnClicked(_ sender: Any) {
        let text = "Name :\(gtExhibitorName) \n Address : \(gtExhibitorAddress) \n Mobile No :\(gtExhibitorContactno1)  \(gtExhibitorContactno2) \n Email ID : \(gtExhibitorEmail1)  \(gtExhibitorEmail2)"
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook, UIActivity.ActivityType.postToTwitter, UIActivity.ActivityType.openInIBooks]
        self.present(activityViewController, animated: true, completion: nil)
    }
    @IBAction func bookmarkActionBtnClicked(_ sender: Any) {
        if bookmarkedChecked == false {
            self.bookmarkButtonTapped = true
            self.visitedButtonTapped = false
            self.manageImageOnBookmarkButton()
        } else {
            self.bookmarkButtonTapped = true
            self.visitedButtonTapped = false
            self.manageImageOnBookmarkButton()
        }
    }
    @IBAction func visitedActionBtnClicked(_ sender: Any) {
        if isVisitedChecked == false {
            self.visitedButtonTapped = true
            self.bookmarkButtonTapped = false
            self.manageImageOnBookmarkButton()
        } else {
            self.visitedButtonTapped = true
            self.bookmarkButtonTapped = false
            self.manageImageOnBookmarkButton()
        }
    }
    func manageImageOnBookmarkButton() {
        if bookmarkButtonTapped == true {
            if bookmarkedChecked == true {
                let onBtnTapped = UIImage(named: "bookmark_grey.png") as UIImage?
                self.bookmarkActionButton.setImage(onBtnTapped, for: .normal)
                bookmarkLabel.text = "UnBookmarked"
                bookmarkedChecked = false
                deleteCoreData(cat: "isBookmarked")
            } else if bookmarkedChecked == false {
                bookmarkedChecked = true
                let onBtnTapped = UIImage(named: "bookmark-1.png") as UIImage?
                bookmarkLabel.text = "Bookmarked"
                self.bookmarkActionButton.setImage(onBtnTapped, for: .normal)
                saveCoreData()
            }
        }
        if visitedButtonTapped == true {
            if isVisitedChecked == true {
                let onBtnTapped = UIImage(named: "visited_grey.png") as UIImage?
                self.visitedActionButton.setImage(onBtnTapped, for: .normal)
                visitedLabel.text = "UnVisited"
                isVisitedChecked = false
                deleteCoreData(cat: "isVisited")
            } else if isVisitedChecked == false {
                isVisitedChecked = true
                let onBtnTapped = UIImage(named: "visited-1.png") as UIImage?
                self.visitedActionButton.setImage(onBtnTapped, for: .normal)
                visitedLabel.text = "Visited"
                saveCoreData()
            }
        }
    }

    func saveCoreData(){
           do {
               let getObject = NSEntityDescription.insertNewObject(forEntityName: "Exhibitor_StarMarked", into: moc)
               getObject.setValue(exhibitorId, forKey: "exhibitorId")
               getObject.setValue(exhibitorCat, forKey: "exhibitorCategory")
               getObject.setValue(gtExhibitorName, forKey: "exhibitorName")
               if bookmarkButtonTapped == true
               {
                   getObject.setValue("true", forKey: "isBookmarked")
                   getObject.setValue("", forKey: "isVisited")
               }
               if visitedButtonTapped == true{
                   getObject.setValue("true", forKey: "isVisited")
                   getObject.setValue("", forKey: "isBookmarked")
               }
               try moc.save()
               
           } catch {
               fatalError("Failure to save context: \(error)")
           }
       }
    
    func deleteCoreData(cat : String)
    {
        let request:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName:"Exhibitor_StarMarked")
        
        request.predicate = NSPredicate(format: "exhibitorId = %@",exhibitorId)
        var i = 0
        
        do {
            coreDataArray = try self.moc.fetch(request) as NSArray
            coreDataCount = coreDataArray.count
            //   print("m1 : \(coreDataArray)")
        }
        catch {
            fatalError()
        }
        
        while i < coreDataArray.count
        {
            let data  = coreDataArray[i] as! NSManagedObject
            let currentString = data.value(forKey: "exhibitorId") as! String
            let status = data.value(forKey: cat) as! String
            
            if (currentString == exhibitorId) && (status == "true")
            {
                moc.delete(data)
            }
            
            i = i + 1
        }
        do {
            try moc.save()
            //   print(moc)
        }
        catch {
            fatalError("Failure to save context: \(error)")
        }
    }

    func coreDatafetch() {
        var request = NSFetchRequest<NSFetchRequestResult>()
        
        request = NSFetchRequest(entityName: "Exhibitor_StarMarked")
        request.returnsObjectsAsFaults = false
        var i = 0
        do {
            coreDataArray = try self.moc.fetch(request) as NSArray
            coreDataCount = coreDataArray.count
            print("m5 : \(coreDataArray)")
        } catch {
            fatalError()
        }
        
        print(self.exhibitorId)
        while i < coreDataArray.count {
            let data = coreDataArray.object(at: i) as! NSManagedObject
            exhibitorIdForStarMarked = (data.value(forKey:  "exhibitorId") as? String)!
            if self.exhibitorId == exhibitorIdForStarMarked {
                let onBtnTapped = UIImage(named: "bookmark-1.png") as UIImage?
                let visitBtnTapped = UIImage(named: "visited-1.png") as UIImage?
                
                DispatchQueue.main.async {
                    let visitcheck = data.value(forKey: "isVisited") as? String
                    if visitcheck == "true"
                    {
                        self.visitedActionButton.setImage(visitBtnTapped, for: .normal)
                        self.visitedLabel.text = "Visited"
                    }
                    let bookmarkcheck = data.value(forKey: "isBookmarked") as? String
                    
                    if bookmarkcheck == "true"
                    {
                        self.bookmarkActionButton.setImage(onBtnTapped, for: .normal)
                        self.bookmarkLabel.text = "Bookmarked"
                    }
                }
                bookmarkedChecked = true
                isVisitedChecked = true
                
            } else {
                bookmarkedChecked = false
                isVisitedChecked = false
            }
            i = i + 1
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            print(self.exhibitorId)
            print(self.exhibitorCat)
            
            self.coreDatafetch()
            
            self.addActionButton.layer.cornerRadius =  (self.addActionButton.frame.width) / 2
            self.addActionButton.layer.masksToBounds = true
            self.callActionButton.layer.cornerRadius =  (self.callActionButton.frame.width) / 2
            self.callActionButton.layer.masksToBounds = true
            self.emailActionButton.layer.cornerRadius =  (self.emailActionButton.frame.width) / 2
            self.emailActionButton.layer.masksToBounds = true
            self.shareActionButton.layer.cornerRadius =  (self.shareActionButton.frame.width) / 2
            self.shareActionButton.layer.masksToBounds = true
            self.bookmarkActionButton.layer.cornerRadius =  (self.bookmarkActionButton.frame.width) / 2
            self.bookmarkActionButton.layer.masksToBounds = true
            self.visitedActionButton.layer.cornerRadius =  (self.visitedActionButton.frame.width) / 2
            self.visitedActionButton.layer.masksToBounds = true
            self.takeAnoteButton.layer.cornerRadius =  (self.takeAnoteButton.frame.width) / 2
            self.takeAnoteButton.layer.masksToBounds = true
            
            self.callActionButton.center.x -= self.view.bounds.width
            self.emailActionButton.center.x += self.view.bounds.width
            self.takeAnoteButton.center.x -= self.view.bounds.width
            self.shareActionButton.center.x += self.view.bounds.width
            UIView.animate(withDuration: 0.8, delay: 0.3,
                           usingSpringWithDamping: 0.3,
                           initialSpringVelocity: 0.5,
                           options: [], animations: {
                            
                            self.callActionButton.center.x += self.view.bounds.width
                            self.emailActionButton.center.x -= self.view.bounds.width
                            self.takeAnoteButton.center.x += self.view.bounds.width
                            self.shareActionButton.center.x -= self.view.bounds.width
            }, completion: nil)
            
            UIView.animate(withDuration: 0.2) { () -> Void in
                
                self.visitedActionButton.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi))
                self.bookmarkActionButton.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi))
                self.addActionButton.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi))
            }
            UIView.animate(withDuration: 0.5, delay: 0.10, options: UIView.AnimationOptions.curveEaseIn, animations: { () -> Void in
                self.visitedActionButton.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi * 2))
                self.bookmarkActionButton.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi * 2))
                self.addActionButton.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi * 2))
            }, completion: nil)
            
        }
        
    }

}
