//
//  ExhibitorDirectory.swift
//  GJEPC
//
//  Created by Kwebmaker on 28/07/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import  CoreData
import MBProgressHUD
extension String {
   var isAlphabetSet: Bool {
       return !isEmpty && range(of: "[^a-zA-Z]", options: .regularExpression) == nil
   }
}
var myNotificationKey = "ComingFromFilter"

@available(iOS 13.0, *)
class ExhibitorDirectory: UIViewController,UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate,UISearchControllerDelegate, dismissVcDelegate,getDataFromFilter {

    @IBOutlet weak var addActionBtn: UIImageView!
    @IBOutlet weak var searchIcon: UIImageView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var noDataFound: UILabel!
    @IBOutlet weak var retryBtn: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var headingLbl: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var exhibitorDirectoryTableView: UITableView!
    @IBOutlet weak var noInternetView: UIView!
    @IBOutlet weak var headerView: UIView!
    
    
    var exhibitorName = ""
    var exhibitorListFilter_tableView = UITableView()
    var requestToServer = RequestToServer()
    var hallDropDownTableView : UITableView!
    var courtureDropDownTableView : UITableView!
    var callUrls : URL!
    var exhibitorListArray : NSMutableArray = []
    var exhibitorProfileArray : NSMutableArray = []
    var appDel : AppDelegate!
    var context : NSManagedObjectContext!
    var showdropdown = false
    var hallDropDownList : NSMutableArray = []
    var coutureDropDownList : NSMutableArray = []
    var coreDataArray : NSArray = []
    var customArray : NSMutableArray = []
    var customArray1 : NSMutableArray = []
    var coreDataCount = 0
    var tap : UITapGestureRecognizer!
    var customObj = Custom_ObjectsViewController()
    var activityLoader : MBProgressHUD? = nil
    var hallArray  : NSMutableArray = []
    var sectionArray : NSMutableArray = []
    var cityflag = 0
    var hallDropDownFlag : Bool = true
    var sectionDropDownFlag : Bool = true
    var eventCategory = ""
    var floatingView = UIView()
    var floatingBtnTappedFirstTime = true
    var filterString = ""
    var keyString = ""
    var companyName = ""
    var alphabetsArray = ["#","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
    var sectionListWithData : NSMutableDictionary = [:]
    var comingFromfilter = false
    var custom = Custom_ObjectsViewController()
    var getDropDownValueOneFromFilter = ""
    var getDropDownValueTwoFromFilter = ""
    var tempArray : NSMutableArray = []
    var headingTxt = ""
    
   
    @IBAction func backBtn(_ sender: Any) {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    
    @IBAction func searchBtnTapped(_ sender: Any) {
        DispatchQueue.main.async {
            self.searchBar.isHidden = false
            self.headingLbl.isHidden = true
            self.searchBtn.isHidden = true
            self.searchIcon.isHidden = true
            self.cancelButton.isHidden = false
            self.addActionBtn.isHidden = true
        }
    }
    
    
    @IBAction func cancelButtontapped(_ sender: Any) {
        
        
        //         DispatchQueue.main.async {
        //
        //
        //        }
        // customArray = coreDataArray.mutableCopy() as! NSMutableArray
        
        
        //
        //
        //        if filterString == "all" {
        //            comingFromfilter = false
        //        }
        //        else {
        //            comingFromfilter = true
        //        }
        //
        DispatchQueue.main.async {
            self.sortData()
            //     self.viewDidLoad()
            self.contentView.isHidden = false
            self.view.endEditing(true)
            self.searchBar.isHidden = true
            self.headingLbl.isHidden = false
            self.searchBtn.isHidden = false
            self.cancelButton.isHidden = true
            self.searchIcon.isHidden = false
            self.addActionBtn.isHidden = false
            self.exhibitorDirectoryTableView.reloadData()
        }
    }
    
    @IBAction func resetBtn(_ sender: Any) {
         sortData()
    }
    
    @IBAction func floatingButtonTapped(_ sender: Any) {
        
        floatingBtnTappedFirstTime = true
        
        DispatchQueue.main.async {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "FloatingForExhibitorDirectoryViewController") as! FloatingForExhibitorDirectoryViewController
            self.addChild(controller)
            controller.exhibitorName = self.exhibitorName
            controller.eventCategory = self.eventCategory
            controller.hallArray = self.hallArray
            controller.sectionArray = self.sectionArray
            controller.coredataArray = self.tempArray
            controller.view.frame = self.view.bounds
            controller.dismissVCDelegate = self
            controller.companyName = self.companyName
            
            self.view.addSubview(controller.view)
            controller.didMove(toParent: self)
        }
    }
    func dismissVc() {
        removeChildView()
    }
    func removeChildView()
       {
           DispatchQueue.main.async {
               // self.view.alpha = 1
            let vc = self.children.last
            vc?.willMove(toParent: nil)
               vc?.view.removeFromSuperview()
            vc?.removeFromParent()
           }
       }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        removeChildView()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        retryBtn.layer.cornerRadius = 5
        noDataFound.layer.cornerRadius = 5
        searchBar.isHidden = true
        searchBar.returnKeyType = .search
        self.noDataFound.isHidden = true
        cancelButton.layer.cornerRadius = 5
        
        if filterString == "all" {
            headingLbl.text = "All Exhibitors"
        }
        else{
            headingLbl.text = self.headingTxt
        }
        
        //        headingLbl.text = filterString
        appDel = (UIApplication.shared.delegate as! AppDelegate)
        context = appDel.managedObjectContext
        exhibitorDirectoryTableView.tableFooterView = UIView(frame: CGRect.zero)
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: myNotificationKey), object: nil, queue: nil, using :catchNotification)
        
        coreDatafetch()
    }
    
    func catchNotification(notification : Notification) -> Void {
           //        guard getDropDownValueOneFromFilter = notification.userInfo!["dataSentForValueOne"] else {
           //            return
           //        }
           getDropDownValueOneFromFilter = notification.userInfo!["dataSentForValueOne"] as! String
           getDropDownValueTwoFromFilter = notification.userInfo!["dataSentForValueTwo"] as! String
           
           let tempArray = coreDataArray.mutableCopy() as! NSMutableArray
           comingFromfilter = true
           
           customArray = []
           
           for i in 0..<tempArray.count {
               let data = (tempArray.object(at: i) as! NSManagedObject)
               let firstValue = data.value(forKey: "exhibitor_HallNo")   as! String
               let secondValue = data.value(forKey: "exhibitor_Section")   as! String
               
               if getDropDownValueOneFromFilter != "" && getDropDownValueTwoFromFilter != "" {
                   if (firstValue == getDropDownValueOneFromFilter) && (secondValue == getDropDownValueTwoFromFilter) {
                       self.customArray.add(tempArray.object(at: i))
                   }
               }
               else if getDropDownValueOneFromFilter != "" {
                   if firstValue == getDropDownValueOneFromFilter {
                       self.customArray.add(tempArray.object(at: i))
                   }
               }
               else if getDropDownValueTwoFromFilter != "" {
                   if secondValue == getDropDownValueTwoFromFilter {
                       self.customArray.add(tempArray.object(at: i))
                   }
               }
               //        print(getDropDownValueOneFromFilter)
               //        print(getDropDownValueTwoFromFilter)
           }
           DispatchQueue.main.async {
               self.exhibitorDirectoryTableView.reloadData()
               self.exhibitorDirectoryTableView.setContentOffset(CGPoint.zero, animated: true)
               
           }
       }
       
        func coreDatafetch() {
            
            
            var request = NSFetchRequest<NSFetchRequestResult>()
            
            if exhibitorName == "iijs"
            {
                request = NSFetchRequest(entityName: "Exhibitor_IIGS")
                request.returnsObjectsAsFaults = false
            }
            else if exhibitorName == "signature"
            {
                request = NSFetchRequest(entityName: "Exhibitor_SIGNATURE")
                request.returnsObjectsAsFaults = false
            }
                
            else if exhibitorName == "igjme"
            {
                request = NSFetchRequest(entityName: "Exhibitor_GEMS")
                request.returnsObjectsAsFaults = false
            }
            
            let sortDescriptor = NSSortDescriptor(key: "exhibitor_Name", ascending: true)
            let sort = [sortDescriptor]
            request.sortDescriptors = sort
            
            do {
                
                coreDataArray = try self.context.fetch(request) as NSArray
                customArray = coreDataArray.mutableCopy() as! NSMutableArray
                customArray1 = coreDataArray.mutableCopy() as! NSMutableArray
                
                
                coreDataCount = coreDataArray.count
                
    //                        print(coreDataArray)
    //                         print(customArray)
    //                         print(customArray1)
                
                
            } catch {
                fatalError()
            }
            
            //        print(coreDataArray)
            sortData()
        }
        func sortData() {
            print(keyString)
            print(filterString)
            
            if filterString == "all" {
                comingFromfilter = false
                var currentAlphabet = ""
                var membersDetailsArray : NSMutableArray = []
                var dict : NSMutableDictionary =  [:]
                
                for i in 0..<self.alphabetsArray.count
                {
                    currentAlphabet = self.alphabetsArray[i]
                    membersDetailsArray = []
                    for j in 0..<self.coreDataArray.count
                    {
                        dict = [:]
                        let data = (self.coreDataArray.object(at: j) as! NSManagedObject)
                        let companyName = (data.value(forKey: "exhibitor_Name") as! String).uppercased()
                        let index = companyName.index(companyName.startIndex, offsetBy: 0)
                        var firstAlpha = companyName[index]
                        
                        if String(firstAlpha).isAlphabetSet == true
                        {
                            firstAlpha = companyName[index]
                        }
                        else {
                            firstAlpha = "#"
                        }
                        
                        if (String(firstAlpha)  == currentAlphabet){
                            //                        dict.setValue(companyName, forKey: "exhibitor_Name")
                            
                            //                        membersDetailsArray.add(dict)
                            membersDetailsArray.add(data)
                            
                            
                        }
                            
                        else if (String(firstAlpha).isAlphabetSet == false) && (String(firstAlpha) == currentAlphabet)
                        {
                            //                        dict.setValue(companyName, forKey: "exhibitor_Name")
                            
                            //                        membersDetailsArray.add(dict)
                            
                            membersDetailsArray.add(data)
                            
                        }
                    }
                    
                    sectionListWithData.setObject(membersDetailsArray, forKey: currentAlphabet as NSCopying)
                    //                print(sectionListWithData)
                }
            }
            else {
                comingFromfilter = true
                customArray = []
                for i in 0..<coreDataArray.count {
                    
                    print(keyString)
                    let value = (coreDataArray.object(at: i) as AnyObject).value(forKey: keyString) as! String
                    print(filterString)
                    if value.lowercased() == filterString.lowercased() {
                        
                        customArray.add(coreDataArray.object(at: i))
                        // customArray1.add(coreDataArray.object(at: i))
                    }
                }
            }
            
    //        print(self.customArray.object(at: 0))
            print(customArray.count)
           
                if self.customArray.count > 0 {
                    self.exhibitorDirectoryTableView.reloadData()
                }
                else {
                    let alert = UIAlertController(title: "Alert", message: "No Record Found", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: { (UIAlertAction) in
                        self.navigationController?.popViewController(animated: true)
                    }))
                
                        self.present(alert, animated: true, completion: nil)
             
           
            }
            
            
            //        print(coreDataArray)
            //        print(customArray)
            //        print(customArray1)
            
            
        }
    func filteredData(dropDownOneSelectedValue: String, dropDownTwoSelectedValue: String) {
        let tempArray = coreDataArray.mutableCopy() as! NSMutableArray
        comingFromfilter = true
        
        customArray = []
        //        customArray1 = []
        
        //        print(dropDownOneSelectedValue)
        //        print(dropDownTwoSelectedValue)
        
        for i in 0..<tempArray.count {
            let data = (tempArray.object(at: i) as! NSManagedObject)
            let firstValue = data.value(forKey: "exhibitor_HallNo")   as! String
            let secondValue = data.value(forKey: "exhibitor_Section")   as! String
            
            if dropDownOneSelectedValue != "" && dropDownTwoSelectedValue != "" {
                if (firstValue == dropDownOneSelectedValue) && (secondValue == dropDownTwoSelectedValue) {
                    self.customArray.add(tempArray.object(at: i))
                }
            }
            else if dropDownOneSelectedValue != "" {
                if firstValue == dropDownOneSelectedValue {
                    self.customArray.add(tempArray.object(at: i))
                }
            }
            else if dropDownTwoSelectedValue != "" {
                if secondValue == dropDownTwoSelectedValue {
                    self.customArray.add(tempArray.object(at: i))
                }
            }
        }
        
        //        print(customArray)
        
        DispatchQueue.main.async {
            self.exhibitorDirectoryTableView.reloadData()
            self.exhibitorDirectoryTableView.setContentOffset(CGPoint.zero, animated: true)
            
        }
        
    }
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
           self.exhibitorDirectoryTableView.sectionIndexColor = UIColor(red: 131/255, green: 42/255, blue: 106/255, alpha: 1.0)
           
           if tableView == exhibitorDirectoryTableView
               
           {
               if comingFromfilter == false {
                   return alphabetsArray
               }
               else {
                   return nil
               }
           }
           else{
               return nil
           }
       }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if tableView == self.exhibitorDirectoryTableView
        {
            if comingFromfilter == false
            {
                if (tableView.dataSource?.tableView(tableView, numberOfRowsInSection: section) == 0) {
                    return nil
                }
                else {
                    return alphabetsArray[section]
                }
            }
            else {
                return nil
            }
        }
        else{
            return nil
        }    }

    func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        
        let alphaView = UIView(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        
        alphaView.backgroundColor = UIColor.lightGray
        
        let label = UILabel(frame: CGRect(x: 20, y: 20 , width: 20, height: 20))
        label.textAlignment = NSTextAlignment.center
        label.text = title
        label.textColor = UIColor.black
        alphaView.center = CGPoint(x: self.view.frame.size.width / 2, y: self.view.frame.size.height / 2)
        alphaView.layer.cornerRadius = 30
        
        DispatchQueue.main.async {
            
            alphaView.addSubview(label)
            
            self.view.addSubview(alphaView)
            
            alphaView.fadeIn()
            
            alphaView.fadeOut()
        }
        
        return index
    }
    
    func hideKeyboard() {
           searchBar.resignFirstResponder()
           searchBar.isHidden = true
       }
    func exhibitorDirectoryOnLoad() {
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if comingFromfilter == true
        {
            return self.customArray.count
        }
        else
        {
            return (sectionListWithData.object(forKey: self.alphabetsArray[section]) as! NSArray).count
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if comingFromfilter == true {
            return 1
        }
        else {
            return sectionListWithData.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           
           if tableView == exhibitorDirectoryTableView{
               exhibitorDirectoryTableView.isUserInteractionEnabled = true
               let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ExhibitorDirectoryListTableViewCell
               DispatchQueue.main.async {
                   cell.layer.cornerRadius = 3
                   cell.layer.borderColor = UIColor.lightGray.cgColor
                   cell.layer.borderWidth = 0.5
               }
               if comingFromfilter == true
               {
                   cell.exhibitorDirectoryListHeading.text =  (self.customArray.object(at: indexPath.row) as! NSManagedObject).value(forKey: "exhibitor_Name") as! String?
               }
               else {
                   cell.exhibitorDirectoryListHeading.text =  ((self.sectionListWithData.object(forKey: self.alphabetsArray[indexPath.section]) as! NSArray).object(at: indexPath.row) as! NSManagedObject).value(forKey: "exhibitor_Name") as! String?
                   
                   //                print((self.sectionListWithData.object(forKey: self.alphabetsArray[indexPath.section]) as! NSArray).object(at: indexPath.row) as! NSManagedObject).value(forKey: "exhibitor_Name") as! String?)
                   //                cell.exhibitorDirectoryListHeading.text =  ((self.sectionListWithData.object(forKey: self.alphabetsArray[indexPath.section]) as! NSArray)
                   
               }
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
               return cell
           }
           return UITableViewCell()
       }
       
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          
          var data : AnyObject!
          if comingFromfilter == true
          {
              data =  (self.customArray.object(at: indexPath.row) as! NSManagedObject) as AnyObject
          }
          else {
              //            data =  ((self.sectionListWithData.object(forKey: self.alphabetsArray[indexPath.section]) as! NSArray).object(at: indexPath.row) as AnyObject)
              data = ((self.sectionListWithData.object(forKey: self.alphabetsArray[indexPath.section]) as! NSArray).object(at: indexPath.row) as! NSManagedObject)
          }
          
          print(self.sectionListWithData)
          //        let data = customArray.object(at: indexPath.row) as AnyObject
          
          if tableView == exhibitorDirectoryTableView
          {
              let exhibitorProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "ExhibitorProfile") as! ExhibitorProfile
              
              //            print(data)
              
              exhibitorProfileVC.exhibitorCat = exhibitorName
              exhibitorProfileVC.exhibitorId = data.value(forKey: "Exhibitor_ID") as! String
              exhibitorProfileVC.exhibitorname = data.value(forKey: "Exhibitor_Name") as! String
              
              exhibitorProfileVC.getExhibitor_logo =  data.value(forKey: "exhiImage") as! String
              exhibitorProfileVC.getExhibitor_PdtImage = data.value(forKey: "ProductImage") as! String
              exhibitorProfileVC.getExhibitor_Discription = data.value(forKey: "exhibitor_discrption") as! String
              exhibitorProfileVC.getExhibitorCode =  data.value(forKey: "Exhibitor_Code") as! String
              
              let getExhibitorName = data.value(forKey: "Exhibitor_Name")
              exhibitorProfileVC.getExhibitorName = getExhibitorName as! String
              
              let getExhibitorContactNo1 = data.value(forKey: "Exhibitor_Mobile") as! String
              exhibitorProfileVC.getExhibitorContactNo1 = getExhibitorContactNo1
              
              let getExhibitorContactNo2 =  (data.value(forKey: "Exhibitor_Phone") as! String)
              exhibitorProfileVC.getExhibitorContactNo2 = getExhibitorContactNo2
              
              let getExhibitorEmail = (data.value(forKey: "Exhibitor_Email") as! String)
              exhibitorProfileVC.getExhibitorEmail = getExhibitorEmail
              
              exhibitorProfileVC.getExhibitorEmail1 = (data.value(forKey: "Exhibitor_Email1") as! String)
              
              let getExhibitorAddress1 = (data.value(forKey: "Exhibitor_Address1") as! String)
              exhibitorProfileVC.getExhibitorAddress1 = getExhibitorAddress1
              
              let getExhibitorAddress2 = (data.value(forKey: "Exhibitor_Address2") as! String)
              exhibitorProfileVC.getExhibitorAddress2 = getExhibitorAddress2
              
              let getExhibitorAddress3 =  (data.value(forKey: "Exhibitor_Address3") as! String)
              exhibitorProfileVC.getExhibitorAddress3 = getExhibitorAddress3
              
              let getExhibitorHallNo = (data.value(forKey: "Exhibitor_HallNo") as! String)
              exhibitorProfileVC.getExhibitorHallNo = getExhibitorHallNo
              
              exhibitorProfileVC.getExhibitorPincode = (data.value(forKey: "Exhibitor_Pincode") as! String)
              exhibitorProfileVC.getExhibitorCity = (data.value(forKey: "Exhibitor_City") as! String)
              
              exhibitorProfileVC.getBooth = (data.value(forKey: "Exhibitor_StallNo1") as! String)
              exhibitorProfileVC.getBooth1 = (data.value(forKey: "Exhibitor_StallNo2") as! String)
              exhibitorProfileVC.getBooth2 = (data.value(forKey: "Exhibitor_StallNo3") as! String)
              exhibitorProfileVC.getBooth3 = (data.value(forKey: "Exhibitor_StallNo4") as! String)
              exhibitorProfileVC.getBooth4 = (data.value(forKey: "Exhibitor_StallNo5") as! String)
              exhibitorProfileVC.getBooth5 = (data.value(forKey: "Exhibitor_StallNo6") as! String)
              exhibitorProfileVC.getBooth6 = (data.value(forKey: "Exhibitor_StallNo7") as! String)
              exhibitorProfileVC.getBooth7 = (data.value(forKey: "Exhibitor_StallNo8") as! String)
              exhibitorProfileVC.exhiLoginID =  (data.value(forKey: "exhibitor_Code") as! String)
              self.navigationController?.pushViewController(exhibitorProfileVC, animated: true)
          }
          
      }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if((searchBar.text?.count)! < 1)
        {
            self.contentView.isHidden = false
            self.customArray = customArray1.mutableCopy() as! NSMutableArray
            
            print(self.customArray.object(at: 0))
            self.exhibitorDirectoryTableView.reloadData()
        }
    }
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        //        print(searchBar.text!)
        comingFromfilter = true
        if searchBar.text?.count == 1 && text == "" {
            
            //searchCompanyName(resultString: searchBar.text!)
            DispatchQueue.main.async {
                self.contentView.isHidden = false
                self.customArray = self.coreDataArray.mutableCopy() as! NSMutableArray
                self.exhibitorDirectoryTableView.reloadData()
                //self.CustomArray = []
                //self.memberDirectoryTableView.reloadData()
            }
        }
        else {
            if text == "" {
                let truncated = searchBar.text!.substring(to: searchBar.text!.index(before: searchBar.text!.endIndex))
                searchExhibitorName(resultString: truncated)
            }
            else {
                let searchString = searchBar.text! + text
                searchExhibitorName(resultString: searchString)
            }
        }
        return true
    }
    func searchExhibitorName(resultString : String) {
        
        customArray = []
        
        // sortData()
        
        //print(customArray.object(at: 0))
        print(customArray1.object(at: 0))
        print(coreDataArray.object(at: 0))
        
        for i in 0..<customArray1.count {
            
            let result = customArray1[i] as AnyObject
            
            let currentString = (result.value(forKey: "Exhibitor_Name") as! String)
            
            if currentString.localizedCaseInsensitiveContains(resultString) == true
            {
                //                print(currentString)
                self.customArray.add(result)
            }
        }
        
        if customArray.count == 0
        {
            self.noInternetView.isHidden = true
            self.contentView.isHidden = true
            self.noDataFound.isHidden = false
        }
        else {
            self.contentView.isHidden = false
            self.noDataFound.isHidden = true
            self.exhibitorDirectoryTableView.reloadData()
        }
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        // searchBar.isHidden = true
        //  cancelButton.isHidden = true
        // headingLbl.isHidden = false
        //  searchBtn.isHidden = false
        // self.searchIcon.isHidden = false
        
        customArray = []
        
        for i in 0..<coreDataArray.count {
            
            let result = coreDataArray[i] as AnyObject
            
            let currentString = (result.value(forKey: "Exhibitor_Name") as! String)
            
            if currentString.localizedCaseInsensitiveContains(searchBar.text!) == true
            {
                //                print(currentString)
                self.customArray.add(result)
            }
        }
        
        if customArray.count == 0
        {
            self.noInternetView.isHidden = true
            self.contentView.isHidden = true
            self.noDataFound.isHidden = false
        }
        else {
            self.noDataFound.isHidden = true
            self.contentView.isHidden = false
            self.exhibitorDirectoryTableView.reloadData()
        }
        //        exhibitorDirectoryTableView.isHidden = false
        //        exhibitorDirectoryTableView.reloadData()
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}
extension UIView {
    
    func fadeIn(duration: TimeInterval = 0.8, delay: TimeInterval = 3.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
        }, completion: nil)
    }
    
    func fadeOut(duration: TimeInterval = 1.5) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 0.0
        })
    }

}
