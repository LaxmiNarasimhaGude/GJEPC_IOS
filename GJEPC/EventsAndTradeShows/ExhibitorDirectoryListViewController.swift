//
//  ExhibitorDirectoryListViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 28/07/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import CoreData
import MBProgressHUD
@available(iOS 13.0, *)
class ExhibitorDirectoryListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,dataReceivedFromServerDelegate {

    @IBOutlet weak var noInternetLbl: UILabel!
    @IBOutlet weak var exhibitorlistTableView: UITableView!
    var exhibitorCategoryArray : NSMutableArray = []
       var arrayForSignature : NSMutableArray = [[]]
       var customObj = Custom_ObjectsViewController()
       var eventCategory = ""
       var exhibitorSelected = ""
       var requestToServer = RequestToServer()
       var callUrls : URL!
       var exhibitorListArray : NSMutableArray = []
       var titleForSectionArray : NSMutableArray = ["Exhibitors", "Hall", "Section", "Personal Notes"]
       var imageArrayForAllNotes : [UIImage] = [UIImage(named : "all_exhibitors.png")!, UIImage(named : "hall.png")!, UIImage(named : "section.png")!, UIImage(named : "notes.png")!]
       var hallArray : NSMutableArray = []
       var sectionArray : NSMutableArray = []
       var appDel : AppDelegate!
       var context : NSManagedObjectContext!
       var coreDataArray : NSMutableArray = []
       var customArray : NSMutableArray = []
       var coreDataCount = 0
       var activityLoader : MBProgressHUD!
       var coredataCount = 0
       var getImageUrl = ""
       var getListImg = ""
       var tempArray : NSMutableArray = []
       var getImgDictionary : NSDictionary = [:]
       var getTitle = ""
       var getArray : NSArray = []
       var hallDictArray : NSMutableArray = []
       var sectionDictArray : NSMutableArray = []
       var allExhidict : NSMutableArray = []
       var personal : NSMutableArray = []
       var hallStringArray = [String]()
       var sortArray : NSMutableArray = []
       let url = "https://gjepc.org/gjepc_mobile/services/exhibitorMenuList"
    
    
    @IBAction func backBtn(_ sender: Any) {
        if let navController = self.navigationController {
                   navController.popViewController(animated: true)
               }
        
    }
    
    @IBOutlet weak var retryBtn: UIButton!
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(true)
           
       }
    @IBAction func RetryBtn(_ sender: Any) {
        CheckNetwork()
    }
    
    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        
        
        
        retryBtn.layer.cornerRadius = 5
        
        let dict1 : NSMutableDictionary = ["name":"All Exhibitors", "image": "all_exhibitors.png"]
        
        allExhidict.add(dict1)
        
        let dict2 = ["name":"My Bookmarks", "image": "bookmark-1.png"]
        let dict3 = ["name":"Visited Exhibitors", "image": "visited-1.png"]
        let dict4 = ["name":"My Notes", "image": "notes.png"]
        
        personal.add(dict4)
        personal.add(dict2)
        personal.add(dict3)
        
        
        print(allExhidict)
        print(hallArray)
        print(sectionArray)
        print(personal)
        arrayForSignature =  [allExhidict, hallArray,sectionArray,personal]
        
        appDel = (UIApplication.shared.delegate as! AppDelegate)
        context = appDel.managedObjectContext
        
        exhibitorlistTableView.isHidden = true
        exhibitorlistTableView.delegate  = self
        exhibitorlistTableView.dataSource = self
        CheckNetwork()
    }
    func CheckNetwork(){
           if CheckInternet.isConnectedToNetwork() == true{
               coreDatafetch()
               getExhibitorImageList()
               exhibitorlistTableView.isHidden = false
           }else{
               
               exhibitorlistTableView.isHidden = true
           }
           
       }
    func loadActivityIndicator()
       {
           activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true)
           activityLoader?.isUserInteractionEnabled = false
       }
   
    func exhibitorDirectoryOnLoad() {
        
        
        if eventCategory == "national"
        {
            if exhibitorSelected == "signature"
            {
                callUrls = exhibitorDirectoryUrl
                let params = [
                    "year":"2021",
                    "event_name":"signature" ]
                requestToServer.delegate = self
                requestToServer.connectToServer(myUrl: callUrls, params: params as AnyObject)
            }
            else if exhibitorSelected == "iijs"
            {
                callUrls = exhibitorDirectoryUrl
                let params = [
                    "year":"2021",
                    "event_name":"IIJS" ]
                requestToServer.delegate = self
                requestToServer.connectToServer(myUrl: callUrls, params: params as AnyObject)
            }
            else if exhibitorSelected == "igjme"
            {
                callUrls = exhibitorDirectoryUrl
                let params = [
                    "year":"2021",
                    "event_name":"igjme" ]
                requestToServer.delegate = self
                requestToServer.connectToServer(myUrl: callUrls, params: params as AnyObject)
            }
        }
        else if eventCategory == "international"
        {
            callUrls = exhibitorDirectoryUrl
            let params = [
                "year":"2021",
                "event_name":eventCategory ]
            //      print(eventCategory)
            requestToServer.delegate = self
            requestToServer.connectToServer(myUrl: callUrls, params: params as AnyObject)
        }
    }
    func dataReceivedFromServer(data: NSDictionary, url: URL) {
        if ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "true" {
            let temporaryArray = ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "Result") as! NSArray)
            exhibitorListArray  = (temporaryArray.mutableCopy() as! NSMutableArray)
            print(exhibitorListArray)
            
        }
        
        if coredataCount < exhibitorListArray.count {
            DispatchQueue.main.async {
                self.activityLoader.hide(animated: true)
            }
            self.saveCoreData()
        }
        
        //        getExhibitorImageList()
    }
    func getExhibitorImageList()
    {
        DispatchQueue.main.async {
            self.loadActivityIndicator()
        }
        
        let url = exhibitorListUrl
        let session = URLSession.shared
        let parameters : NSMutableDictionary = ["event" : exhibitorSelected]
        
        let request = NSMutableURLRequest(url: url! as URL)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.httpMethod = "POST"
        request.httpBody = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        
        //create dataTask using the session object to send data to the server
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            print(data)
            print(response)
            print(error)
            guard error == nil else {
                return
            }
            guard let data = data else {
                return
            }
            
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as? [String:Any]{
                    self.getImgDictionary = json as NSDictionary
                    
                    
                    print(self.getImgDictionary)
                    
                    
                    if ((self.getImgDictionary.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "true" {
                        let temporaryArray = ((self.getImgDictionary.value(forKey: "Response") as! NSDictionary).value(forKey: "Result") as! NSArray)
                        self.getArray = (temporaryArray.mutableCopy() as! NSArray)
                        print(self.getArray)
                        var hallCtr = 0
                        var sectionCtr = 0
                        //       print(self.getArray)
                        for i in 0..<self.getArray.count {
                            
                            let num = Int((self.getArray.object(at: i) as! NSDictionary)["hall_value"] as! String)
                            if num != nil {
                                self.hallDictArray.add((self.getArray.object(at: i) as! NSDictionary))
                                hallCtr  += 1
                            }
                            else {
                                self.sectionDictArray.add((self.getArray.object(at: i) as! NSDictionary))
                                sectionCtr += 1
                                
                            }
                        }
                        
                        print("hall array is\(self.hallDictArray)")
                        print("section dictionary is \(self.sectionDictArray)")
                        
                        
                        self.arrayForSignature =  [["All Exhibitors"],self.hallDictArray,self.sectionDictArray,[ "My Bookmarks", "Visted Exhibitors", "My Notes"]]
                        
                        // self.saveImageToPhone()
                        if self.coreDataCount < self.exhibitorListArray.count
                        {
                            self.saveCoreData()
                            //self.saveImageToPhone()
                        }
                        DispatchQueue.main.async {
                            self.exhibitorlistTableView.isHidden = false
                            self.activityLoader.hide(animated: true)
                            print(self.hallDictArray)
                            self.exhibitorlistTableView.reloadData()
                        }
                    }
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        task.resume()
    }
    func saveImageToPhone() {
        
        // print(hallDictArray)
        // print(sectionDictArray)
        
        for i in 0..<getArray.count {
            let suffixImageUrl = (getArray.object(at: i) as! NSDictionary)["image"] as! String
            let imageUrl = exhibitorlistImgUrl + suffixImageUrl
            
            URLSession.shared.dataTask(with: NSURL(string: imageUrl)! as URL, completionHandler: { (data, response, error) -> Void in
                if error != nil {
                    return
                }else {
                    DispatchQueue.main.async {
                        let image = UIImage(data: data!)
                        if (image != nil) {
                            do {
                                let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                                let fileURL = documentsURL.appendingPathComponent(suffixImageUrl)
                                let imageData = image!.pngData()
                                try imageData?.write(to: fileURL, options: .atomic)
                                print(error.debugDescription)
                            } catch { }
                        }
                    }
                }
            }).resume()
        }
        
        for i in 0..<getArray.count {
            let suffixImageUrl = (getArray.object(at: i) as! NSDictionary)["image"] as! String
            
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            let filePath = documentsURL.appendingPathComponent(suffixImageUrl).path
            if FileManager.default.fileExists(atPath: filePath) {
                //  print(filePath)
            }
        }
    }
    func coreDatafetch() {
        var request = NSFetchRequest<NSFetchRequestResult>()
        if exhibitorSelected == "iijs"
        {
            request = NSFetchRequest<NSFetchRequestResult>(entityName: "Exhibitor_IIGS")
        }
            
        else if exhibitorSelected == "signature"
        {
            request = NSFetchRequest<NSFetchRequestResult>(entityName: "Exhibitor_SIGNATURE")
        }
            
        else if exhibitorSelected == "igjme"
        {
            request = NSFetchRequest<NSFetchRequestResult>(entityName: "Exhibitor_GEMS")
        }
        
        request.returnsDistinctResults = true
        request.returnsObjectsAsFaults = false
        
        let sortDescriptor = NSSortDescriptor(key: "exhibitor_Name", ascending: true)
        let sort = [sortDescriptor]
        request.sortDescriptors = sort
        
        let mc : AnyObject!
        
        do {
            
            mc = try context.fetch(request) as AnyObject
            
        } catch {
            fatalError()
        }
        
        for i in 0..<mc.count{
            coreDataArray.add(mc.object(at: i))
        }
        
        coredataCount = coreDataArray.count
        print(coredataCount)
        tempArray = coreDataArray.mutableCopy() as! NSMutableArray
        print(tempArray)
        
        
        
        if coredataCount == 0 {
            if CheckInternet.isConnectedToNetwork() == true {
                self.exhibitorDirectoryOnLoad()
            }
            else {
                
            }
        }
        else {
            sectionArray = []
            hallArray = []
            
            for i in 0..<mc.count {
                let hallNo = ((mc.object(at: i) as AnyObject).value(forKey: "exhibitor_HallNo") as! String)
                if !self.hallArray.contains(hallNo) {
                    self.hallArray.add(hallNo)
                }
                
                let section = ((mc.object(at: i) as AnyObject).value(forKey: "exhibitor_Section") as! String)
                if !self.sectionArray.contains(section) {
                    self.sectionArray.add(section)
                }
            }
            customArray = coreDataArray.mutableCopy() as! NSMutableArray
            coreDataCount = coreDataArray.count
        }
        
    }
    func deleteCoreData()
      {
          let request:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Exhibitor_IIGS")
          let request1:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Exhibitor_SIGNATURE")
          let request2:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Exhibitor_GEMS")
          let coord = appDel.persistentStoreCoordinator
          let deleteRequest = NSBatchDeleteRequest(fetchRequest: request)
          let deleteRequest1 = NSBatchDeleteRequest(fetchRequest: request1)
          let deleteRequest2 = NSBatchDeleteRequest(fetchRequest: request2)
          
          do
          {
              if exhibitorSelected == "iijs"
              {
                  try coord.execute(deleteRequest, with: context)
                  
              }
              else if exhibitorSelected == "signature"
              {
                  try coord.execute(deleteRequest1, with: context)
              }
              else if exhibitorSelected == "igjme"
              {
                  try coord.execute(deleteRequest2, with: context)
              }
          }
          catch let error as NSError
          {
              debugPrint(error)
          }
      }
    
    //object(forKey: "Catalog_CompanyLogo") as! String)
    
    func saveCoreData()
    {
        deleteCoreData()
        
        var i = 0
        while ( i < exhibitorListArray.count)
        {
            let getExhibitorLogo = (((self.exhibitorListArray.object(at: i)) as! NSDictionary).object(forKey: "Catalog_CompanyLogo") as! String)
            let getExhibitorProductLogo = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Catalog_ProductLogo") as! String)
            let getExhibitorDiscription = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Catalog_Brief") as! String)
            let getExhibitorCode = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_Code") as! String)
            let getExhibitorId = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_ID") as? String)
            let getExhibitorGid = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_Gid") as! String)
            let getExhibitorCustomerNo = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Customer_No") as! String)
            let getExhibitorName = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_Name") as! String)
            let getExhibitorContactPerson = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_Contact_Person") as! String)
            let getExhibitorDesignation = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_Designation") as! String)
            let getExhibitorLogin = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_Login") as! String)
            let getExhibitorPassword = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_Password") as! String)
            let getExhibitorAddress1 = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_Address1") as! String)
            let getExhibitorAddress2 = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_Address2") as! String)
            let getExhibitorAddress3 =  ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_Address3") as! String)
            let getExhibitorCity = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_City") as! String)
            let getExhibitorState = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_State") as! String)
            let getExhibitorCountryId = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_Country_ID") as! String)
            let getExhibitorPincode = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_Pincode") as! String)
            let getExhibitorContactNo1 = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_Mobile") as! String)
            let getExhibitorContactNo2 =  ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_Phone") as! String)
            let getExhibitorFax = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_Fax") as! String)
            let getExhibitorEmail = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_Email") as! String)
            let getExhibitorEmail1 = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_Email1") as! String)
            let getExhibitorWebsite  = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_Website") as! String)
            let getExhibitorHallNo = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_HallNo") as! String)
            let getExhibitorDivisionNo = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_DivisionNo") as! String)
            let getExhibitorSection = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_Section") as! String)
            let getExhibitorScheme = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_Scheme") as? String)
            let getExhibitorArea = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: " Exhibitor_Area") as? String)
            let getExhibitorStallNo1 = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_StallNo1") as! String)
            let getExhibitorStallNo2 = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_StallNo2") as! String)
            let getExhibitorStallNo3 = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_StallNo3") as! String)
            let getExhibitorStallNo4 = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_StallNo4") as! String)
            let getExhibitorStallNo5 = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_StallNo5") as! String)
            let getExhibitorStallNo6 = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_StallNo6") as! String)
            let getExhibitorStallNo7 = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_StallNo7") as! String)
            let getExhibitorStallNo8 = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_StallNo8") as! String)
            let getExhibitorStallType = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_StallType") as! String)
            let getExhibitorPremium = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_Premium") as! String)
            let getExhibitorRegion = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_Region") as! String)
            let getExhibitorIsActive = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_IsActive") as! String)
            let getExhibitorLayout  = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_Layout") as! String)
            let getExhibitorComments = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "comments") as! String)
            
            
            let getExhibitorSubscribe = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_Subscribe") as! String)
            //            let getExhibitorLastLogin = ((self.exhibitorListArray.object(at: i) as AnyObject).object(forKey: "Exhibitor_LastLogin") as! String)
            let getExhibitorXmlId = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Xml_ID") as! String)
            let getExhibitorRegistrationId = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "Exhibitor_Registration_ID") as! String)
            let getExhibitorEventName = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "event_name") as! String)
            let getExhibitorYear = ((self.exhibitorListArray.object(at: i) as! NSDictionary).object(forKey: "year") as! String)
            
            var getObject : AnyObject!
            
            if exhibitorSelected == "iijs"
            {
                getObject = NSEntityDescription.insertNewObject(forEntityName: "Exhibitor_IIGS", into: context) as! Exhibitor_IIGS
            }
                
            else if exhibitorSelected == "signature"
            {
                getObject = NSEntityDescription.insertNewObject(forEntityName: "Exhibitor_SIGNATURE", into: context) as! Exhibitor_SIGNATURE
            }
                
            else if exhibitorSelected == "igjme"
            {
                getObject = NSEntityDescription.insertNewObject(forEntityName: "Exhibitor_GEMS", into: context) as! Exhibitor_GEMS
                
            }
            
            getObject.setValue(getExhibitorLogo, forKey: "exhiImage")
            getObject.setValue(getExhibitorProductLogo, forKey: "productImage")
            getObject.setValue(getExhibitorDiscription, forKey: "exhibitor_discrption")
            getObject.setValue(getExhibitorId, forKey: "Exhibitor_ID")
            getObject.setValue(getExhibitorGid, forKey: "Exhibitor_Gid")
            getObject.setValue(getExhibitorCustomerNo, forKey: "Customer_No")
            getObject.setValue(getExhibitorName, forKey: "Exhibitor_Name")
            getObject.setValue(getExhibitorContactPerson, forKey: "Exhibitor_Contact_Person")
            getObject.setValue(getExhibitorDesignation, forKey: "Exhibitor_Designation")
            getObject.setValue(getExhibitorCode, forKey: "Exhibitor_Code")
            getObject.setValue(getExhibitorLogin, forKey: "Exhibitor_Login")
            getObject.setValue(getExhibitorPassword, forKey: "Exhibitor_Password")
            getObject.setValue(getExhibitorAddress1, forKey: "Exhibitor_Address1")
            getObject.setValue(getExhibitorAddress2, forKey: "Exhibitor_Address2")
            getObject.setValue(getExhibitorAddress3, forKey: "Exhibitor_Address3")
            getObject.setValue(getExhibitorCity, forKey: "Exhibitor_City")
            getObject.setValue(getExhibitorState, forKey: "Exhibitor_State")
            getObject.setValue(getExhibitorCountryId, forKey: "Exhibitor_Country_ID")
            getObject.setValue(getExhibitorPincode, forKey: "Exhibitor_Pincode")
            getObject.setValue(getExhibitorContactNo1, forKey: "Exhibitor_Mobile")
            getObject.setValue(getExhibitorContactNo2, forKey: "Exhibitor_Phone")
            getObject.setValue(getExhibitorFax, forKey: "Exhibitor_Fax")
            getObject.setValue(getExhibitorEmail1, forKey: "Exhibitor_Email1")
            getObject.setValue(getExhibitorEmail, forKey: "Exhibitor_Email")
            getObject.setValue(getExhibitorWebsite, forKey: "Exhibitor_Website")
            getObject.setValue(getExhibitorHallNo, forKey: "Exhibitor_HallNo")
            getObject.setValue(getExhibitorDivisionNo, forKey: "Exhibitor_DivisionNo")
            getObject.setValue(getExhibitorSection, forKey: "Exhibitor_Section")
            getObject.setValue(getExhibitorScheme, forKey: "Exhibitor_Scheme")
            getObject.setValue(getExhibitorArea, forKey: "Exhibitor_Area")
            getObject.setValue(getExhibitorStallNo1, forKey: "Exhibitor_StallNo1")
            getObject.setValue(getExhibitorStallNo2, forKey: "Exhibitor_StallNo2")
            getObject.setValue(getExhibitorStallNo3, forKey: "Exhibitor_StallNo3")
            getObject.setValue(getExhibitorStallNo4, forKey: "Exhibitor_StallNo4")
            getObject.setValue(getExhibitorStallNo5, forKey: "Exhibitor_StallNo5")
            getObject.setValue(getExhibitorStallNo6, forKey: "Exhibitor_StallNo6")
            getObject.setValue(getExhibitorStallNo7, forKey: "Exhibitor_StallNo7")
            getObject.setValue(getExhibitorStallNo8, forKey: "Exhibitor_StallNo8")
            getObject.setValue(getExhibitorStallType, forKey: "Exhibitor_StallType")
            getObject.setValue(getExhibitorPremium, forKey: "Exhibitor_Premium")
            getObject.setValue(getExhibitorRegion, forKey: "Exhibitor_Region")
            getObject.setValue(getExhibitorIsActive, forKey: "Exhibitor_IsActive")
            getObject.setValue(getExhibitorComments, forKey: "comments")
            getObject.setValue(getExhibitorLayout, forKey: "Exhibitor_Layout")
            getObject.setValue(getExhibitorSubscribe, forKey: "Exhibitor_Subscribe")
            //            getObject.setValue(getExhibitorLastLogin, forKey: "Exhibitor_LastLogin")
            getObject.setValue(getExhibitorXmlId, forKey: "Xml_ID")
            getObject.setValue(getExhibitorRegistrationId, forKey: "Exhibitor_Registration_ID")
            getObject.setValue(getExhibitorEventName, forKey: "event_name")
            getObject.setValue(getExhibitorYear, forKey: "year")
            
            _ = UIImage(named: "\(getExhibitorLogo)")
            
            do {
                try context.save()
            } catch {
                fatalError("Failure to save context: \(error)")
            }
            i = i + 1
        }
        
        var date = Date()
        var todaysDate = ""
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/YYYY"
        todaysDate = dateFormatter.string(from: date)
        UserDefaults.standard.set(todaysDate, forKey: "exhibitorDate")
        UserDefaults.standard.set(true, forKey: "exhibitorIsStoredSuccessful")
        coreDatafetch()
    }
    
    func checkInternetAndLoadData()
       {
           if CheckInternet.isConnectedToNetwork() == true {
               
           }
           else {
               
           }
       }
       
       func numberOfSections(in tableView: UITableView) -> Int {
           
           return titleForSectionArray.count
       }
       
       
       func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
           // return titleForSectionArray[section] as? String
           return nil
       }
       
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return (arrayForSignature.object(at: section) as! NSArray).count
       }
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExhibitorListTableViewCell") as! ExhibitorListTableViewCell
        print("")
        
        
        
        DispatchQueue.main.async {
            cell.selectionStyle = .none
            cell.layer.borderColor = UIColor.lightGray.cgColor
            cell.layer.borderWidth = 0.5
            cell.layer.cornerRadius = 5
            if indexPath.section == 0 {
                cell.exhibitorCategoryLabel.text = (self.allExhidict.object(at: indexPath.row) as! NSDictionary).value(forKey: "name") as? String
                cell.img.image = UIImage(named : (self.allExhidict.object(at: indexPath.row) as! NSDictionary).value(forKey: "image") as! String)
                
            }
            else if indexPath.section == 1
            {
                //print("mm :\(((self.sortArray.object(at: indexPath.row) as! NSDictionary) as! String))")
                cell.exhibitorCategoryLabel.text =   (self.hallDictArray.object(at: indexPath.row) as! NSDictionary).value(forKey: "title") as? String
                let suffixImageString = (self.hallDictArray.object(at: indexPath.row) as! NSDictionary).value(forKey: "image") as? String
                let imageUrl = exhibitorlistImgUrl + suffixImageString!
                URLSession.shared.dataTask(with: NSURL(string: imageUrl)! as URL, completionHandler: { (data, response, error) -> Void in
                    if error != nil {
                        return
                    }else {
                        DispatchQueue.main.async(execute: { () -> Void in
                            let image = UIImage(data: data!)
                            print(data as Any)
                            if (image != nil) {
                                cell.img.image = image
                            }
                        })
                    }
                }).resume()
                
            } else if indexPath.section == 2{
                cell.exhibitorCategoryLabel.text  = (self.sectionDictArray.object(at: indexPath.row) as! NSDictionary).value(forKey: "title") as? String
                let suffixImageString = (self.sectionDictArray.object(at: indexPath.row) as! NSDictionary).value(forKey: "image") as? String
                let imageUrl = exhibitorlistImgUrl + suffixImageString!
                URLSession.shared.dataTask(with: NSURL(string: imageUrl)! as URL, completionHandler: { (data, response, error) -> Void in
                    if error != nil {
                        return
                    }else {
                        DispatchQueue.main.async(execute: { () -> Void in
                            let image = UIImage(data: data!)
                            print(data as Any)
                            if (image != nil) {
                                cell.img.image = image
                            }
                        })
                    }
                }).resume()
            } else {
                cell.exhibitorCategoryLabel.text = (self.personal.object(at: indexPath.row) as! NSDictionary).value(forKey: "name") as? String
                cell.img.image = UIImage(named : (self.personal.object(at: indexPath.row) as! NSDictionary).value(forKey: "image") as! String)
            }
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ExhibitorDirectory") as! ExhibitorDirectory
        vc.eventCategory = self.eventCategory
        vc.exhibitorName = self.exhibitorSelected
        vc.tempArray = self.tempArray.mutableCopy() as! NSMutableArray
        
        if indexPath.section == 0 {
            vc.filterString = "all"
            vc.hallArray  = self.hallArray
            vc.sectionArray = self.sectionArray
            
            DispatchQueue.main.async {
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        if indexPath.section == 1
        {
            let hall = (self.hallDictArray.object(at: indexPath.row) as! NSDictionary).value(forKey: "hall_value") as! String
            
            vc.filterString = hall
           
            if hallDictArray.count > 0 {
                vc.headingTxt = (self.hallDictArray.object(at: indexPath.row) as! NSDictionary).value(forKey: "title") as! String
            }
            print(hallArray)
            print(sectionArray)
            vc.keyString = "exhibitor_HallNo"
            vc.hallArray  = self.hallArray
            vc.sectionArray = self.sectionArray
            //    print(hall)
            DispatchQueue.main.async {
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        if indexPath.section == 2 {
            let section = (self.sectionDictArray.object(at: indexPath.row) as! NSDictionary).value(forKey: "hall_value") as! String
            vc.filterString = section
            
            if ((self.sectionDictArray).count > 0) {
                //        print("hall", hallDictArray)
                //        print(self.hallDictArray.object(at: indexPath.row) as! NSDictionary)
                //        print((self.hallDictArray.object(at: indexPath.row) as! NSDictionary).value(forKey: "title") as! String)
                vc.headingTxt = (self.sectionDictArray.object(at: indexPath.row) as! NSDictionary).value(forKey: "title") as! String
            }
            vc.keyString = "exhibitor_Section"
            if hallArray.count > 0{
                vc.hallArray  = self.hallArray
            }
            if sectionArray.count > 0 {
                vc.sectionArray = self.sectionArray
            }
            //     print(section)
            
            DispatchQueue.main.async {
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        if indexPath.section == 3 {
            let name = UIStoryboard(name: "Main", bundle: nil)
            let vc = name.instantiateViewController(withIdentifier: "starmarked") as! Bookmarks
            
            if indexPath.row == 1{
                vc.eventCat = self.eventCategory
                vc.titleView = "Bookmark"
                vc.exhibitorCat = self.exhibitorSelected
                
                DispatchQueue.main.async {
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else if indexPath.row ==  2{
                vc.eventCat = self.eventCategory
                vc.titleView = "Visited"
                vc.exhibitorCat = self.exhibitorSelected
                DispatchQueue.main.async {
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else if indexPath.row == 0 {
                //let controller = name.instantiateViewController(withIdentifier: "ViewYourNotesViewController") as! ViewYourNotesViewController
                let controller = name.instantiateViewController(withIdentifier: "CompanyNotesListViewController") as! CompanyNotesListViewController
                
                controller.filterRequest = "mainListing"
                controller.exhibitorCategory = self.exhibitorSelected
                DispatchQueue.main.async {
                    self.navigationController?.pushViewController(controller, animated: true)
                }
            }
        }
    }

    
}
