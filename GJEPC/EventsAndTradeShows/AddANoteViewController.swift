//
//  AddANoteViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 29/07/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import CoreData

protocol backButtonClickedDelegate {
    func bckBtnClicked()
}

@available(iOS 13.0, *)
class AddANoteViewController: UIViewController, UITextViewDelegate {

    @IBOutlet weak var notesTextView: UITextView!
    @IBOutlet weak var saveBtn: UIButton!
    
    
       var appDel : AppDelegate!
       var context : NSManagedObjectContext!
       var bckBtnClickedDelegate : backButtonClickedDelegate!
       var exhibitorId = ""
       var exhibitorCategory = ""
       var companyName  = ""
       var currentTime : String  = ""
       var currentDate : String = ""
       var isEdit = false
       var coredataArray : NSMutableArray = []
       var notesFromView = ""
       var savedNotes = ""
       var comingFrom = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDel = (UIApplication.shared.delegate as! AppDelegate)
        context = appDel.managedObjectContext
        
        notesTextView.delegate = self
        if self.isEdit ==  true {
            notesTextView.text =  savedNotes
        }
        //        deleteCoreData()
        //        fetchCoreDataRecords()
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.bckBtnClickedDelegate.bckBtnClicked()
    }
    
    @IBAction func Cancel_btn(_ sender: Any) {
        
        self.bckBtnClickedDelegate.bckBtnClicked()
    }
    
    @IBAction func saveBtnTapped(_ sender: Any) {
        
        let date = Date()
        let calendar = Calendar.current
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        let result = formatter.string(from: date)
        let hour = calendar.component(.hour, from: date as Date)
        let minutes = calendar.component(.minute, from: date as Date)
        let seconds = calendar.component(.second, from: date as Date)
        currentTime = "\(hour):\(minutes):\(seconds)"
        currentDate = "\(result)"
        print(notesTextView.text)
        
        if notesTextView.text != "" {
            notesArray.insert(notesTextView.text, at: 0)
            notesTextView.text = ""
            if self.isEdit == true {
                deleteCoreData()
            }
            else {
                saveCoreData(contents: notesArray.object(at: 0) as! String,eid: self.exhibitorId,eCat: self.exhibitorCategory,date: currentDate , time : currentTime,companyName: self.companyName)
            }
            self.bckBtnClickedDelegate.bckBtnClicked()
        }
    }
    func fetchCoreDataRecords()
        {
            var notesData : AnyObject!
            var request = NSFetchRequest<NSFetchRequestResult>()
            
            request = NSFetchRequest(entityName: "ExhibitorsNotes")
            request.returnsObjectsAsFaults = false
            do {
                notesData = try  self.context.fetch(request) as AnyObject
                
                print(notesData)
                
            } catch {
                fatalError()
            }
        }
        
        func saveCoreData(contents : String,eid : String , eCat : String, date : String,time : String,companyName : String){
            
            do {
                let getObject = NSEntityDescription.insertNewObject(forEntityName:
                    "ExhibitorsNotes", into: context)
                getObject.setValue(eid, forKey: "exhibitorID")
                getObject.setValue(eCat, forKey: "exhibitorCategory")
                getObject.setValue(contents, forKey: "notes")
                getObject.setValue(date, forKey: "date")
                getObject.setValue(time, forKey: "time")
                getObject.setValue(companyName, forKey: "company")
                
                try context.save()
            } catch {
                fatalError("Failure to save context: \(error)")
            }
            
            //fetchCoreDataRecords()
        }
        
        func deleteCoreData()
        {
            let request:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName:"ExhibitorsNotes")
            let cd : AnyObject!
            
            var i = 0
            
            do {
                cd = try self.context.fetch(request) as AnyObject
                //   print("m1 : \(coreDataArray)")
            }
            catch {
                fatalError()
            }
            
            for i in 0..<cd.count {
                coredataArray.add(cd.object(at: i))
            }
            
            //print(coredataArray)
            while i < coredataArray.count
            {
                let data  = coredataArray[i] as! NSManagedObject
                let id = data.value(forKey: "exhibitorID") as! String
                let enotes = data.value(forKey: "notes") as! String
                let cat = data.value(forKey: "exhibitorCategory") as! String
                
                if ( id == self.exhibitorId && enotes == self.savedNotes)
                {
                    context.delete(data)
                }
                
                i = i + 1
                do {
                    try context.save()
                    //   print(moc)
                }
                catch {
                    fatalError("Failure to save context: \(error)")
                }
            }
            
            saveCoreData(contents: notesArray.object(at: 0) as! String,eid: self.exhibitorId,eCat: self.exhibitorCategory,date: currentDate , time : currentTime,companyName: self.companyName)
        }
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(true)
            self.view.alpha = 1
            
            print(self.exhibitorId)
            print(self.exhibitorCategory)
            
            UIView.animate(withDuration: 0.2) { () -> Void in
                self.notesTextView.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi))
            }
            UIView.animate(withDuration: 0.5, delay: 0.10, options: UIView.AnimationOptions.transitionCurlUp, animations: {
                self.notesTextView.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi * 2))
            }, completion: { (true) in
            })
        }
        
        func textViewDidBeginEditing(_ textView: UITextView) {
            if notesTextView.text == "Type Here..."
            {
                notesTextView.text = ""
            }
        }
        
    }
