//
//  EventsAndTradeShowsViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 12/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
@available(iOS 13.0, *)
class EventsAndTradeShowsViewController: UIViewController,dataReceivedFromServerDelegate,UITableViewDelegate,UITableViewDataSource {
   

    @IBOutlet weak var notificationCount: UILabel!
    @IBOutlet weak var EventsTable: UITableView!
    
    @IBOutlet weak var backButton: UIButton!
    
    
    
    
    
    
     var responseNotificationArray:NSMutableArray = []
     var isChange:Bool = false
       var backType = ""
       var activityLoader : MBProgressHUD? = nil
       var requestToServer = RequestToServer()
       var callUrls : URL!
       var Date1:String!
       var replacedImgstring  = ""
       var tableViewMenuArray : NSMutableArray = []
       var isOpen = false
    override func viewDidLoad() {
        super.viewDidLoad()

        if backType == "back"{

            DispatchQueue.main.async {
                self.backButton.setImage(UIImage(named: "507257"), for: UIControl.State.normal)
            }


        }
        else{
            DispatchQueue.main.async {
                self.backButton.setImage(UIImage(named: "menu"), for: UIControl.State.normal)
            }

        }
              let name = Notification.Name("didReceiveData")
              NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: name, object: nil)
                               
              let updateBadgeCount = Notification.Name("updateBadgeCount")
              NotificationCenter.default.addObserver(self, selector: #selector(updateBadgeCount(_:)), name: updateBadgeCount, object: nil)
            
              self.notificationCount.layer.cornerRadius = 20/2
              self.notificationCount.layer.masksToBounds = true
    }
   
    @objc func onDidReceiveData(_ notification:Notification) {
           
           // Do something now
           
           if  UserDefaults.standard.value(forKey: "openNotification") != nil
           {
               if(UserDefaults.standard.value(forKey: "openNotification") as! String == "true")
               {
                   ///NotiifcationViewController
                   
                   
                   
                   
                   let viewControllerArray = self.navigationController?.viewControllers
                   
                   var checkFlag:Bool
                   checkFlag = false
                   for controller in viewControllerArray! {
                       
                       
                       print(controller)
                       if controller.classForCoder == NotificationViewController.self {
                           checkFlag = true
                           print(controller)
                       }
                       
                   }
                   
                   
                   if(checkFlag)
                   {
                       
                       NotificationCenter.default.post(name: Notification.Name("updateTable"), object: nil)
                       
                   }
                   else
                   {
                       let view1 = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController
                       
                       let data:[AnyObject]? = UserDefaults.standard.value(forKey: "NotificationList") as? [AnyObject]
                       
                       view1?.dataArray = (UserDefaults.standard.value(forKey: "NotificationList") as? [AnyObject])!
                       
                       view1?.globalArray = data!.reversed()
                       
                       
                       DispatchQueue.main.async {
                           self.navigationController?.pushViewController(view1!, animated: true)
                       }
                   }
                   
                   
                   
                   
                   
                   
                   
               }
               else
               {
                   // getData()
               }
               
           }
           else
           {
               // getData()
               
           }
           
       }

  func getData()  {
        
        var responseDict : NSMutableDictionary = [:]
        var dataTask: URLSessionDataTask
        
        let url = NSURL(string: "https://gjepc.org/gjepc_mobile/services/firebaseNotificationList")
        
        let request = NSMutableURLRequest(url:url! as URL)
        
        
        
        let session = URLSession.shared
        dataTask = session.dataTask(with: request as URLRequest){(data, response, error) in
            guard error == nil else {
                
                
                return
            }
            guard let data = data else {
                return
            }
            do {
                if let dict = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! NSMutableDictionary{
                    
                    // let dict = json as! NSMutableDictionary
                    //  print(dict)
                    if (dict["Response"] as! NSDictionary)["status"] as! String == "true" {
                        responseDict = (dict["Response"] as! NSMutableDictionary)
                        
                        print(responseDict)
                        
                        
                        
                        self.responseNotificationArray = (dict["Response"] as! NSDictionary)["Result"] as! NSMutableArray
                        
                        
                        
                        
                        
                        let lastNotificationCount:String? = UserDefaults.standard.value(forKey: "lastNotificationCOunt")  as? String
                        
                        let mastercnt:String? = UserDefaults.standard.value(forKey: "MasterNotificationCount")  as? String
                        
                        
                        var master :Int? = 0
                        
                        if(mastercnt != nil)
                        {
                            master = Int(mastercnt!)
                        }
                        else
                            
                        {
                            master = 0
                        }
                        
                        
                        
                        
                        var lastCnt:Int? = 0
                        
                        
                        if(lastNotificationCount != nil)
                        {
                            lastCnt = Int(lastNotificationCount!)
                        }
                        else
                        {
                            lastCnt = 0
                            
                        }
                        
                        
                        
                        
                        
                        
                        let isRead:String? = UserDefaults.standard.value(forKey: "isRead") as? String
                        
                        
                        
                        
                        
                        
                        if(isRead != nil)
                        {
                            if(lastCnt == self.responseNotificationArray.count)
                            {
                                if(isRead == "true")
                                {
                                    
                                    
                                }
                                else
                                {
                                    
                                    UserDefaults.standard.setValue(lastCnt, forKey: "lastNotificationCOunt")
                                    
                                    
                                    UserDefaults.standard.setValue("0", forKey: "MasterNotificationCount")
                                    
                                    UserDefaults.standard.setValue("false", forKey: "isRead")
                                    
                                }
                                
                            }
                            else
                            {
                                
                                if(isRead == "true")
                                {
                                    //when new notification come show new notification count only
                                    
                                    
                                    
                                    let rCount:Int = self.responseNotificationArray.count
                                    
                                   
                                    let newCount:Int = rCount - master!
                                    
                                    //let latestCount = self.responseNotificationArray.count - newCount
                                    
                                    let newCountStr:String = String(newCount)
                                    
                                    UserDefaults.standard.setValue(String(newCountStr), forKey: "lastNotificationCOunt")
                                    
                                    UserDefaults.standard.setValue("false", forKey: "isRead")
                                    //                                    }
                                    
                                    
                                    
                                }
                                else
                                {
                                    
                                    if(master == 0)
                                    {
                                        
                                        let rCount:Int = self.responseNotificationArray.count
                                        
                                        let newCount1:Int = rCount - lastCnt!
                                        
                                        let newCount:Int = newCount1 + lastCnt!
                                        
                                        UserDefaults.standard.setValue(String(newCount), forKey: "lastNotificationCOunt")
                                        
                                        UserDefaults.standard.setValue("false", forKey: "isRead")
                                        
                                    }
                                    else
                                        
                                    {
                                        
                                        
                                        let rCount:Int = self.responseNotificationArray.count
                                        
                                        var newCount1:Int = 0
                                        
                                        
                                        newCount1 = rCount - master!
                                        
                                        UserDefaults.standard.setValue(String(newCount1), forKey: "lastNotificationCOunt")
                                        
                                        UserDefaults.standard.setValue("false", forKey: "isRead")
                                        
                                        //                                        }
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                    }
                                    
                                    
                                    
                                }
                                
                                
                            }
                        }
                        else
                        {
                            
                            
                            
                            let lastNotificationCount1 = self.responseNotificationArray.count
                            
                            UserDefaults.standard.setValue(String(lastNotificationCount1), forKey: "lastNotificationCOunt")
                            
                            
                            UserDefaults.standard.setValue("0", forKey: "MasterNotificationCount")
                            
                            UserDefaults.standard.setValue("false", forKey: "isRead")
                            
                            
                        }
                        
                        
                        
                        
                        
                        
                        var lastNotificationCount1:String? = nil
                        
                        if let lastCount = UserDefaults.standard.value(forKey: "lastNotificationCOunt") as? NSNumber {
                            
                            
                            lastNotificationCount1 = lastCount.stringValue
                            
                            
                            
                        }
                        else if let quantity = UserDefaults.standard.value(forKey: "lastNotificationCOunt") as? String {
                            
                            
                            lastNotificationCount1 = quantity
                            
                            
                        }
                        else
                            
                        {
                            lastNotificationCount1 = "0"
                        }
                        
                        
                        
                        
                        
                        
                       
                        
                        
                        let masterCOunt:String? = (UserDefaults.standard.value(forKey: "MasterNotificationCount") as! String)
                        
                        print("MASTER \(masterCOunt!)")
                        
                        
                        let new = Int(lastNotificationCount1!)
                        
                        if (new! >= 0)
                        {
                            // do positive stuff
                        }
                        else
                            
                        {
                            self.isChange = true
                            
                            UserDefaults.standard.setValue("0", forKey: "lastNotificationCOunt")
                            
                            UserDefaults.standard.setValue("true", forKey: "isRead")
                            
                        }
                        
                        let isRead1:String? = (UserDefaults.standard.value(forKey: "isRead") as! String)
                        
                        
                        if(isRead1 == "true")
                        {
                            //UserDefaults.standard.setValue("0", forKey: "lastNotificationCOunt")
                            
                            
                            DispatchQueue.main.async {
                                
                                
                                self.notificationCount.isHidden = false
                                
                                self.notificationCount.text = "0"
                                
                            }
                        }
                        else
                        {
                            
                            //show count on bell icon
                            
                            DispatchQueue.main.async {
                                
                                self.notificationCount.isHidden = false
                                
                                self.notificationCount.text = lastNotificationCount1
                                
                            }
                            
                            
                            
                            
                            
                            
                            //  print(lastNotificationCount1)
                            
                            
                            
                        }
                        
                        
                        
                        
                        
                        
                    } else {
                        
                        return
                        
                    }
                }
                
            } catch let error {
                print("Json Error",error.localizedDescription)
                
            }
            
            
            
            
        }
        
        dataTask.resume()
        
        
        
        
        
        
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        

            
        
        
                      requestToServer.delegate = self
                      EventsTable.delegate = self
                      EventsTable.dataSource = self
                      EventsTable.separatorStyle = .none
                       checkforInternet()
    }
        func checkforInternet()
        {
            if CheckInternet.isConnectedToNetwork() == true{
                DispatchQueue.main.async {
                    self.loadActivityIndicator()
                    
   
                        self.EventsTable.isHidden = false
                        self.callUrls = AllEventurl
                        let params = [ ""]
                        self.requestToServer.connectToServer(myUrl: self.callUrls as URL, params: params as AnyObject)
                    
                }
                
                
            
            }
            else
            {
                
                self.EventsTable.isHidden = true
            }
            DispatchQueue.main.async {
                self.EventsTable.reloadData()
            }
            
        }

    func loadActivityIndicator()
     {
         activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
         activityLoader?.label.text = "Loading";
         activityLoader?.detailsLabel.text = "Please Wait";
         activityLoader?.isUserInteractionEnabled = false;
     }
    func dataReceivedFromServer(data: NSDictionary, url: URL) {
        
        if ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "true" {
            
            let temporaryArray = ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "Result") as! NSArray)
            let sortedArray = (temporaryArray as NSArray).sortedArray(using: [NSSortDescriptor(key: "event", ascending: true)]) as! [[String:AnyObject]] as NSArray
          

          
            
            
            
            
           tableViewMenuArray = (sortedArray.mutableCopy() as! NSMutableArray)
            
            
            print("tableViewMenuArray.....\(tableViewMenuArray)")
            
            if (tableViewMenuArray == []){
                do {
                    DispatchQueue.main.async {
                        self.activityLoader?.hide(animated: true)
                    }

                    let alert = UIAlertController(title: "", message: "No Events Available", preferredStyle: UIAlertController.Style.alert)
                    let action = UIAlertAction(title: "ok", style: UIAlertAction.Style.default) { (UIAlertAction) in
                        self.navigationController?.popViewController(animated: true)
                    }
                    alert.addAction(action)
                    DispatchQueue.main.async {
                          self.present(alert, animated: true, completion: nil)
                    }
                  
                                            
                                           
                                        }
            }
            else{
                DispatchQueue.main.async {
                    self.activityLoader?.hide(animated: true)
                    self.EventsTable.reloadData()
                }
            }
            
            
           
        }
    }
       
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90.0
    }
    
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return tableViewMenuArray.count
       }
    

       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "AllEventsTableViewCell") as! AllEventsTableViewCell

            cell1.EventsNamelbl.text = ((self.tableViewMenuArray.object(at: indexPath.row) as AnyObject).object(forKey: "event")as! String)

           let eventdate =   JoinString(index1: indexPath.row)
           
           
           cell1.EventDate.text = eventdate

           let imgdata = ((self.tableViewMenuArray.object(at: indexPath.row) as AnyObject).object(forKey: "image")as! String)
           let imgfilterUrl = Eventimgeurl + imgdata
          replacedImgstring = imgfilterUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!
       
//           cell1.EventsImage.sd_setImage(with: URL(string: self.replacedImgstring))
           
            DispatchQueue.main.async {
                 cell1.BagroundView1.backgroundColor = UIColor.white
                cell1.BagroundView1.layer.cornerRadius = 3.0
                cell1.BagroundView1.layer.masksToBounds = false
                           

                cell1.BagroundView1.layer.shadowOffset = CGSize(width: 0, height: 0)
                cell1.BagroundView1.layer.shadowOpacity = 0.5
                

            }
          
           
           return cell1
           
       }
    

    
       func JoinString( index1:Int) -> String {
              
              
              let fromdate1 = ((self.tableViewMenuArray.object(at: index1) as AnyObject).object(forKey: "fromDate")as! String)
              let inputFormatter = DateFormatter()
              inputFormatter.dateFormat = "yyyy-MM-dd"
              let fromDate = inputFormatter.date(from: fromdate1)
              inputFormatter.dateFormat = "dd-MM-yyyy"
              let fromdate = inputFormatter.string(from: fromDate!)
              
              let todate = ((self.tableViewMenuArray.object(at: index1) as AnyObject).object(forKey: "toDate")as! String)
              let inputFormatter1 = DateFormatter()
                    inputFormatter1.dateFormat = "yyyy-MM-dd"
              
              let ToDate = inputFormatter1.date(from: todate)
               inputFormatter1.dateFormat = "dd-MM-yyyy"
              let todate1 = inputFormatter1.string(from: ToDate!)
              Date1 = "\(String(describing: fromdate)) \("To") \(todate1)"
              return Date1
              
          }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        
     
            self.navigationController?.setNavigationBarHidden(true, animated: true)
            
          
                tabBarController?.tabBar.isHidden = false
                self.navigationController?.setNavigationBarHidden(true, animated: true)
                
            
                        
                        
                        
                        let data:[AnyObject]? = UserDefaults.standard.value(forKey: "NotificationList") as? [AnyObject]
                        
                        
                        
                        print("\(UserDefaults.standard.value(forKey: "userSessionBadgeCnt") ?? "0")")
                        
                //        DispatchQueue.main.async {
                        
                         DispatchQueue.main.async {
                        
                            self.notificationCount.isHidden = false
                            
                            let cnt:Int? = UIApplication.shared.applicationIconBadgeNumber
                            if(cnt == 0 || cnt == nil)
                            {
                                DispatchQueue.main.async {
                                    self.notificationCount.text = "0"
                                }
                            }
                            else
                            {   DispatchQueue.main.async {
                                self.notificationCount.text = "\(String(cnt!))"
                                }
                            }
                            
                      
                            
                            // UserDefaults.standard.value(forKey: "userSessionBadgeCnt") as? String
                            
                        //}
                        
                        
                   
                
            }
      
    }
    @objc func updateBadgeCount(_ notification:Notification){
           
           
           print("\(UserDefaults.standard.value(forKey: "userSessionBadgeCnt") ?? "0")")
           
           DispatchQueue.main.async {
               
               self.notificationCount.isHidden = false
               
               
               let cnt:Int? = UIApplication.shared.applicationIconBadgeNumber
           
           
               if(cnt == 0 || cnt == nil)
               {
                      DispatchQueue.main.async {
                   self.notificationCount.text = "0"
                   }
               }
               else
               {   DispatchQueue.main.async {
                   self.notificationCount.text = "\(String(cnt!))"
                   }
               }
           }
          
           
       }

    @IBAction func back(_ sender: Any) {
        
        if backType == "back"{
            
            navigationController?.popViewController(animated: true)

        }
        else{
            let menuVC : SlideViewController = self.storyboard!.instantiateViewController(withIdentifier: "SlideViewController") as! SlideViewController
                       self.view.addSubview(menuVC.view)
                       self.addChild(menuVC)
                       menuVC.view.layoutIfNeeded()

                       menuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width-90, height: UIScreen.main.bounds.size.height);

                       UIView.animate(withDuration: 0.3, animations: { () -> Void in
                           menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width-90, height: UIScreen.main.bounds.size.height);
                   }, completion:nil)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier:"Eventscontent") as! EventContentsViewController
        let title = ((self.tableViewMenuArray.object(at: indexPath.row) as AnyObject).object(forKey: "event") as! String)
       vc.Ltitle = title
        let desc = ((self.tableViewMenuArray.object(at: indexPath.row) as AnyObject).object(forKey: "eventDescription") as! String)
        vc.Ldesc = desc
        let Limg = ((self.tableViewMenuArray.object(at: indexPath.row) as AnyObject).object(forKey: "image") as! String)
        vc.LImage = Limg
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func notification(_ sender: Any) {





        let data:[AnyObject]? = UserDefaults.standard.value(forKey: "NotificationList") as? [AnyObject]
        print(data)

        if  data != nil {



            DispatchQueue.main.async {


                let view1 = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController

                view1?.dataArray = (UserDefaults.standard.value(forKey: "NotificationList") as? [AnyObject])!

                view1?.globalArray = data!.reversed()
                self.navigationController?.pushViewController(view1!, animated: true)
            }


        }
        else
        {




            DispatchQueue.main.async {

                let view1 = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController

                view1?.dataArray = []

                view1?.globalArray = []
                self.navigationController?.pushViewController(view1!, animated: true)
            }
        }






    }
    
    
}

