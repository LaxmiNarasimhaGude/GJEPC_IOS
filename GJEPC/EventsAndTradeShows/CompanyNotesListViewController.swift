//
//  CompanyNotesListViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 29/07/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import CoreData
@available(iOS 13.0, *)
class CompanyNotesListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

       var notesArray : NSMutableArray = []
       var appDel : AppDelegate!
       var context : NSManagedObjectContext!
       var exhibitorCategory = ""
       var filterRequest = ""
       var tempArray : NSMutableArray = []
       var tempComapnayName : NSMutableArray = []
    
    @IBOutlet weak var companyNameTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        companyNameTableView.tableFooterView = UIView(frame: CGRect.zero)
        companyNameTableView.delegate = self
        companyNameTableView.dataSource  = self
        appDel = (UIApplication.shared.delegate as! AppDelegate)
        context = appDel.managedObjectContext
        //coreDatafetch()
    }
    
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(true)
           coreDatafetch()
       }
   
    @IBAction func backBtnClicked(_ sender: Any) {
       _ =  self.navigationController?.popViewController(animated: true)
    }
    func coreDatafetch() {
          tempArray = []
          let notesData : AnyObject!
          var request = NSFetchRequest<NSFetchRequestResult>()
          request = NSFetchRequest(entityName: "ExhibitorsNotes")
          request.returnsObjectsAsFaults = false
          do {
              notesData = try  self.context.fetch(request) as AnyObject
          } catch {
              fatalError()
          }
          
          for i in 0..<notesData.count {
              tempArray.add(notesData.object(at: i))
          }
          
          print(tempArray)
          getSelectedExhibitorName()
          
      }
    func getSelectedExhibitorName() {
        tempComapnayName = []
        notesArray = []

        for i in 0..<tempArray.count {
            let cat = (tempArray.object(at: i ) as AnyObject).value(forKey: "exhibitorCategory") as! String
            let company = (tempArray.object(at: i ) as AnyObject).value(forKey: "company") as! String

            if cat == exhibitorCategory {
                if !tempComapnayName.contains(company) {
                    tempComapnayName.add(company)
                    self.notesArray.add(tempArray.object(at: i ) as AnyObject)
                }
            }
        }
        
        print(notesArray)
        
        DispatchQueue.main.async {
            self.companyNameTableView.reloadData()
        }
        
        if notesArray.count == 0 {
            let alert = UIAlertController(title: "Alert", message: "No Record Found", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            
            DispatchQueue.main.async {
                self.present(alert, animated: true, completion: nil)
            }
        }
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
           return 1
       }
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return notesArray.count
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CompanyNotesListTableViewCell
           cell.companyName.text = (notesArray.object(at: indexPath.row) as AnyObject).value(forKey: "company") as? String
           return cell
       }
       
       func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewYourNotesViewController") as! ViewYourNotesViewController
           vc.exhibitorCategory = self.exhibitorCategory
           vc.exhibitorId = ((notesArray.object(at: indexPath.row) as AnyObject).value(forKey: "exhibitorID") as? String)!
           vc.companyName = ((notesArray.object(at: indexPath.row) as AnyObject).value(forKey: "company") as? String)!
           vc.filterRequest = self.filterRequest
           
           DispatchQueue.main.async {
               self.navigationController?.pushViewController(vc, animated: true)
           }
       }
}
