//
//  ExhibitorProfile.swift
//  GJEPC
//
//  Created by Kwebmaker on 28/07/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import CoreData
import SDWebImage

extension  UIViewController {
    func dismissKeyboard() {
        view.endEditing(true)
    }
}
var notificationKeyForBackBtn = "hideBackBtn"
@available(iOS 13.0, *)
class ExhibitorProfile: UIViewController, backBtnTappedDelegate,UITextViewDelegate {

    @IBOutlet weak var exhibitorProfileName_label: UILabel!
    @IBOutlet weak var boothLbl: UILabel!
    @IBOutlet weak var exhibitorProfileLocation_label: UILabel!
    @IBOutlet weak var exhibitorProfileLocation2_label: UILabel!
    @IBOutlet weak var exhibitorProfileHallNo_label: UILabel!
    @IBOutlet weak var addActionButton: UIButton!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var addView: UILabel!
    @IBOutlet weak var exhibitorProfileImg: UIImageView!
    @IBOutlet weak var DescriptionTxtView: UITextView!
    @IBOutlet weak var exhibitorproductimg: UIImageView!
    
    var callUrls : URL!
       var exhibitorListArray : NSMutableArray = []
       var exhibitorProfileArray : NSMutableArray = []
       var getDataFromList : String = ""
       var getExhibitor_logo = ""
       var getExhibitor_PdtImage = ""
       
       var getExhibitor_Discription = ""
       var getExhibitorCode = ""
       var getExhibitorImage : UIImage? = nil
       var getExhibitorName : String = ""
       var getExhibitorContactNo1 : String = ""
       var getExhibitorContactNo2 : String = ""
       var getExhibitorEmail : String = ""
       var getExhibitorEmail1 : String = ""
       var getExhibitorAddress1 : String = ""
       var getExhibitorAddress2 : String = ""
       var getExhibitorAddress3: String = ""
       var getExhibitorHallNo : String = ""
       var getExhibitorPincode : String = ""
       var getExhibitorCity : String = ""
       var getBooth : String = ""
       var getBooth1 : String = ""
       var getBooth2 : String = ""
       var getBooth3 : String = ""
       var getBooth4 : String = ""
       var getBooth5 : String = ""
       var getBooth6 : String = ""
       var getBooth7 : String = ""
       var profileBtnState = 1
       var bookmarkBtnState = 1
       var exhibitorId = ""
       var exhibitorCat = ""
       var bookmarkedChecked = Bool()
       var bookmarkButtonTapped = Bool()
       var visitedButtonTapped = Bool()
       var isVisitedChecked = Bool()
       var visitedChecked = ""
       var exhibitorname = ""
       var exhibitorIdForStarMarked : String = ""
       var custom  = Custom_ObjectsViewController()
       var addActionVC = AddActionViewController()
       var viewNotesVC = ViewYourNotesViewController()
       var getImageUrl = ""
       var getproductimageUrl = ""
       var replacedImgstring = ""
       var shouldHideFloatBtn = ""
       var exhiImageStr = ""
       var exhibitorSelected = ""
       var exhiLoginID = ""
       var  BOOTSTR: String!
       let Bootarray : NSMutableArray = []
    
    @IBAction func backButtonTapped(_ sender: Any) {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    @IBAction func openMap(_ sender: Any) {
    //        if (UIApplication.shared.canOpenURL(NSURL(string : "comgooglemaps://")! as URL)) {
    //
    //            let location = getExhibitorAddress1 + getExhibitorAddress2 + getExhibitorAddress3
    //            let replacedLocation = location.replacingOccurrences(of: " ", with: "+")
    //
    //            print(replacedLocation)
    //            let locurl = (NSURL(string : "comgooglemaps://?q=\(replacedLocation))") as URL?)!
    //            print(locurl)
    //
    //            UIApplication.shared.openURL(locurl)
    //        }
            
            let vc  = self.storyboard?.instantiateViewController(withIdentifier: "HtmlWebView") as! ServiceVenueWebVIew
            vc.comingFromExhiProfile = true
            print(self.exhibitorId)
            vc.exhibitorId = self.exhiLoginID
            vc.requestName = "floorPlan"
            DispatchQueue.main.async {
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        putData()
         self.addActionButton.isHidden = false
        addActionVC.bckBtnDelegate = self
         NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: notificationKeyForBackBtn), object: nil, queue: nil, using :catchNotificationToHideFloatBtn)
        DescriptionTxtView.delegate = self
        DescriptionTxtView.isEditable = false
        self.DescriptionTxtView.resignFirstResponder()
        
        
        
    }
    
    @IBAction func addActionBtnClicked(_ sender: Any) {
         addChildView()
    }
    func backBtnTapped() {
           removeChildView()
       }
       func addChildView()
          {
              DispatchQueue.main.async {
                  let controller = self.storyboard?.instantiateViewController(withIdentifier: "AddActionViewController") as! AddActionViewController
                self.addChild(controller)
                  self.addActionButton.isHidden = true
                  controller.view.frame = self.mainView.bounds
                  self.mainView.addSubview(controller.view)
                  controller.bckBtnDelegate = self
                  controller.gtExhibitorContactno1 = self.getExhibitorContactNo1
                  controller.gtExhibitorContactno2 = self.getExhibitorContactNo2
                  controller.exhibitorId = self.exhibitorId
                  controller.exhibitorCat = self.exhibitorCat
                  controller.gtExhibitorName = self.exhibitorname
                  controller.gtExhibitorEmail1 = self.getExhibitorEmail
                  controller.gtExhibitorEmail2 = self.getExhibitorEmail1
                  controller.gtExhibitorAddress = "\(self.getExhibitorAddress1) \(self.getExhibitorAddress3)"
                controller.didMove(toParent: self)
              }
          }
    func removeChildView()
      {
          DispatchQueue.main.async {
            let vc = self.children.last
            vc?.willMove(toParent: nil)
              vc?.view.removeFromSuperview()
            vc?.removeFromParent()
              self.addActionButton.isHidden = false
          }
      }
    override func viewDidLayoutSubviews() {
           super.viewDidLayoutSubviews()
           self.DescriptionTxtView.setContentOffset(CGPoint.zero, animated: false)
           
          
       }
    func catchNotificationToHideFloatBtn(notification : Notification) -> Void {
           shouldHideFloatBtn = notification.userInfo!["backBtnHide"] as! String
           if shouldHideFloatBtn == "true" {
               DispatchQueue.main.async {
                   self.addActionButton.isHidden = true
               }
           }
       }
          override func viewWillAppear(_ animated: Bool) {
               super.viewWillAppear(true)
               DispatchQueue.main.async {
                   self.addActionButton.layer.cornerRadius = 0.5 * (self.addActionButton.bounds.size.width)
                   self.addActionButton.clipsToBounds = true
                   self.exhibitorProfileImg.layer.borderColor = UIColor.lightGray.cgColor
                      self.exhibitorProfileImg.layer.borderWidth = 1
       //           self.exhibitorProfileImg.layer.cornerRadius = self.exhibitorProfileImg.layer.frame.width / 2
                   self.exhibitorProfileImg.layer.cornerRadius = 5
                   self.exhibitorProfileImg.layer.masksToBounds = true
                   
                   
                   
                   self.exhibitorproductimg.layer.borderColor = UIColor.lightGray.cgColor
                   self.exhibitorproductimg.layer.borderWidth = 0.5
                   self.exhibitorproductimg.layer.cornerRadius = self.exhibitorproductimg.layer.frame.width / 2
                   self.exhibitorproductimg.layer.masksToBounds = true
               
               }
               print(getExhibitor_logo)
               
               if exhibitorCat == "signature" {
                   
                    let formattedString = getExhibitor_logo.replacingOccurrences(of: " ", with: "%20")
                   getImageUrl = "https://iijs-signature.org/manual/images/catalog/\(getExhibitorCode)/\(formattedString)"
                
                print(getImageUrl)
                   let formattedproductimg = getExhibitor_PdtImage.replacingOccurrences(of: " ", with: "%20")
                   
                   getproductimageUrl = "https://iijs.org/manual/images/catalog/\(getExhibitorCode)/\(formattedproductimg)"
                print(getproductimageUrl)
                   
               } else if exhibitorCat == "iijs" {
                   
                    let formattedString = getExhibitor_logo.replacingOccurrences(of: " ", with: "%20")
                  
                   getImageUrl = "https://iijs.org/manual/images/catalog/\(getExhibitorCode)/\(formattedString)"
                   print("ssssssss===\(getExhibitor_Discription)")
                     let formattedproductimg = getExhibitor_PdtImage.replacingOccurrences(of: " ", with: "%20")
                   
                   getproductimageUrl = "https://iijs.org/manual/images/catalog/\(getExhibitorCode)/\(formattedproductimg)"
                 
                  

                   
               } else if exhibitorCat == "igjme" {
                    let formattedString = getExhibitor_logo.replacingOccurrences(of: " ", with: "%20")
                   getImageUrl = "https://gjepc.org/igjme/manual/images/\(getExhibitorCode)/\(formattedString)"
                print(getImageUrl)
                   let formattedproductimg = getExhibitor_PdtImage.replacingOccurrences(of: " ", with: "%20")
                   
                   getproductimageUrl = "https://iijs.org/manual/images/catalog/\(getExhibitorCode)/\(formattedproductimg)"
                print(getproductimageUrl)
               }
               
               if getExhibitor_logo != "" {
                   DispatchQueue.main.async {
                       self.exhibitorProfileImg.sd_setImage(with: URL(string: self.getImageUrl) , placeholderImage: UIImage(named: "96-x-96"))
                       
                   }        }
               if getExhibitor_PdtImage != "" {

                   DispatchQueue.main.async {
                        self.exhibitorproductimg.sd_setImage(with: URL(string:self.getproductimageUrl) , placeholderImage: UIImage(named: "96-x-96"))

                   }

               }
           }
     func putData() {
         
         
         
         exhibitorProfileName_label.text = getExhibitorName
         exhibitorProfileLocation_label.text = getExhibitorAddress1
         exhibitorProfileLocation2_label.text = getExhibitorAddress2
         exhibitorProfileHallNo_label.text =  "Hall No: " + getExhibitorHallNo
       //  boothLbl.text = "Booth No: " + getBooth + "," + getBooth1 + "," + getBooth2 + "," + getBooth3 + "," + getBooth4 + "," + getBooth5
         if getBooth != "" {
             Bootarray.add(getBooth)
             
         }
         if getBooth1 != "" {
              Bootarray.add(getBooth1)
         }
         if getBooth2 != ""{
             Bootarray.add(getBooth2)
         }
         if getBooth3 != ""{
             Bootarray.add(getBooth3)
         }
         if getBooth4 != ""{
            Bootarray.add(getBooth4)
         }
         if getBooth5 != ""{
               Bootarray.add(getBooth5)
         }
         if getBooth6 != ""{
             Bootarray.add(getBooth6)
         }
         if getBooth7 != ""{
             Bootarray.add(getBooth7)
         }
        
       
         
         
    boothLbl.text = "Booth No: " + Bootarray.componentsJoined(by: ",")
          DescriptionTxtView.text = getExhibitor_Discription
         
     }

}
