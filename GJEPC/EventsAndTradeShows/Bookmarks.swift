//
//  Bookmarks.swift
//  GJEPC
//
//  Created by Kwebmaker on 28/07/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import CoreData


@available(iOS 13.0, *)
class Bookmarks: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var viewTitle: UILabel!
    @IBOutlet weak var loadTableVIew: UITableView!
    
    
      var titleView = ""
       var selectedExhibitorID = ""
       var exhibitorCat = ""
       var bookmarkedChecked = Bool()
       var visitedChecked  = Bool()
       var appdelegate : AppDelegate!
       var moc : NSManagedObjectContext!
       var coreDataCount = 0
       var coreDataArray : NSArray = []
       var newArray : NSMutableArray = []
       var customArray : NSMutableArray = []
       var exP = ExhibitorProfile()
       var exhibitorName = ""
       var exhibitorArray: NSArray = []
       var exhibitorCustomArray : NSMutableArray = []
       var customObj = Custom_ObjectsViewController()
       var eventCat = ""
    
   
    
    @IBAction func backBtn(_ sender: Any) {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
 override func viewWillAppear(_ animated: Bool) {
        viewTitle.text = titleView
        let name = exhibitorCat
        print("exhi \(name)")
        print(titleView)
        
        coreDataFetch()
        loadexhibitorProfileData()
    }
    
    override func viewDidLoad() {
        appdelegate = (UIApplication.shared.delegate as! AppDelegate)
        loadTableVIew.tableFooterView = UIView(frame: CGRect.zero)
        moc = appdelegate.managedObjectContext
        super.viewDidLoad()
    }
       func deleteCoreData()
        {
            
            let request:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName:"Exhibitor_StarMarked")
            
            let coord = appdelegate.persistentStoreCoordinator
            
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: request)
            do
            {
                try coord.execute(deleteRequest, with: moc)
                
            }
            catch let error as NSError
            {
                debugPrint(error)
            }
        }
        
        func coreDataFetch()
        {
            var request = NSFetchRequest<NSFetchRequestResult>()
            
            request = NSFetchRequest(entityName: "Exhibitor_StarMarked")
            request.returnsObjectsAsFaults = false
            
            do {
                coreDataArray = try self.moc.fetch(request) as NSArray
                coreDataCount = coreDataArray.count
                
            } catch {
                fatalError()
            }
            
            filterCoreData()

        }
        
        func filterCoreData()
        {
            customArray = []
            var i = 0
            while i < coreDataArray.count {
                let data = coreDataArray.object(at: i) as AnyObject
                print(data)
                let exhibitorCategory = data.value(forKey: "exhibitorCategory") as? String
                
                if exhibitorCategory == exhibitorCat
                {
                    if titleView == "Bookmark"
                    {
                        let check = data.value(forKey: "isBookmarked") as? String
                        if check == "true"
                        {
                            customArray.add(data)
                        }
                    }
                    else
                    {
                        let check = data.value(forKey: "isVisited") as? String
                        if check == "true"
                        {
                            customArray.add(data)
                        }
                    }
                }
                i = i + 1
            }
            if customArray.count > 0 {
                DispatchQueue.main.async {
                    self.loadTableVIew.isHidden = false
                    self.loadTableVIew.reloadData()
                }
            }
            else {
                let alert = UIAlertController(title: "Alert", message: "No Record Found", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
                DispatchQueue.main.async {
                    self.loadTableVIew.isHidden = true
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           
            return  customArray.count
        }
        
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! tcell
            cell.exhibitorName?.font = UIFont(name: "Arial", size: 14)
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
           // print(customArray[indexPath.row])
            
            let data = customArray[indexPath.row] as AnyObject
            let exhibitorcatCheck = data.value(forKey: "exhibitorName") as? String
            
            cell.exhibitorName.text = exhibitorcatCheck
            
            return cell
        }
        
        
        func loadexhibitorProfileData()
        {
            
            var request = NSFetchRequest<NSFetchRequestResult>()
            
            if exhibitorCat == "iijs"
            {
                request = NSFetchRequest(entityName: "Exhibitor_IIGS")
                request.returnsObjectsAsFaults = false
            }
            else if exhibitorCat == "signature"
            {
                request = NSFetchRequest(entityName: "Exhibitor_SIGNATURE")
                request.returnsObjectsAsFaults = false
            }
                
            else if exhibitorCat == "igjme"
            {
                request = NSFetchRequest(entityName: "Exhibitor_GEMS")
                request.returnsObjectsAsFaults = false
            }
            
            do {
                exhibitorArray = try self.moc.fetch(request) as NSArray
               // print(coreDataArray)
            }
            catch {
                fatalError()
            }
            
        }
        
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            let exhibitorProfileVC = self.storyboard?.instantiateViewController(withIdentifier:  "ExhibitorProfile") as! ExhibitorProfile
            
            let exhibitorIDSelected = (customArray.object(at: indexPath.row) as AnyObject).value(forKey: "exhibitorId") as! String
            
            var i = 0
            while i < exhibitorArray.count {
                let data = exhibitorArray.object(at: i) as AnyObject
                if data.value(forKey: "exhibitor_ID") as! String == exhibitorIDSelected {
                    print("id1 \(exhibitorIDSelected)")
                    
                    exhibitorProfileVC.exhibitorId = data.value(forKey: "exhibitor_ID") as! String
                    let getExhibitorName = data.value(forKey: "Exhibitor_Name")
                    
                    exhibitorProfileVC.getExhibitorName = getExhibitorName as! String
                    
                    let getExhibitorContactNo1 = data.value(forKey: "Exhibitor_Mobile") as! String
                    exhibitorProfileVC.getExhibitorContactNo1 = getExhibitorContactNo1
                    
                    let getExhibitorContactNo2 =  (data.value(forKey: "Exhibitor_Phone") as! String)
                    exhibitorProfileVC.getExhibitorContactNo2 = getExhibitorContactNo2
                    
                    let getExhibitorEmail = (data.value(forKey: "Exhibitor_Email") as! String)
                    exhibitorProfileVC.getExhibitorEmail = getExhibitorEmail
                    
                    exhibitorProfileVC.getExhibitorEmail1 = (data.value(forKey: "Exhibitor_Email1") as! String)
                    
                    let getExhibitorAddress1 = (data.value(forKey: "Exhibitor_Address1") as! String)
                    exhibitorProfileVC.getExhibitorAddress1 = getExhibitorAddress1
                    
                    let getExhibitorAddress2 = (data.value(forKey: "Exhibitor_Address2") as! String)
                    exhibitorProfileVC.getExhibitorAddress2 = getExhibitorAddress2
                    
                    let getExhibitorAddress3 =  (data.value(forKey: "Exhibitor_Address3") as! String)
                    exhibitorProfileVC.getExhibitorAddress3 = getExhibitorAddress3
                    
                    let getExhibitorHallNo = (data.value(forKey: "Exhibitor_HallNo") as! String)
                    exhibitorProfileVC.getExhibitorHallNo = getExhibitorHallNo
                    
                     exhibitorProfileVC.getBooth = (data.value(forKey: "Exhibitor_StallNo1") as! String)
                }
                
                i = i + 1
            }
            self.navigationController?.pushViewController(exhibitorProfileVC, animated: true)
        }
    }
    // End of class Bookmarks

