//
//  FloatingForExhibitorDirectoryViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 28/07/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
protocol dismissVcDelegate {
    func dismissVc()
}
@available(iOS 13.0, *)
class FloatingForExhibitorDirectoryViewController: UIViewController {
    @IBOutlet weak var bookmarkBtn: UIButton!
    @IBOutlet weak var visitedBtn: UIButton!
    @IBOutlet weak var filterBtn: UIButton!
    @IBOutlet weak var bookmarkLabel: UILabel!
    @IBOutlet weak var visitedLabel: UILabel!
    @IBOutlet weak var filterLabel: UILabel!
    
    
       var exhibitorDirectoryVC  = ExhibitorDirectory()
       var dismissVCDelegate : dismissVcDelegate!
       var eventCategory = ""
       var exhibitorName = ""
       var hallArray : NSMutableArray = []
       var sectionArray : NSMutableArray = []
       var coredataArray : NSMutableArray = []
       var companyName  = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    @IBAction func addActionTapped(_ sender: Any) {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.2) { () -> Void in
                self.filterBtn.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi))
                self.filterLabel.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi))
            }
            UIView.animate(withDuration: 0.5, delay: 0.10, options: UIView.AnimationOptions.transitionCurlDown, animations: {
                self.filterBtn.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi * 2))
                self.filterLabel.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi * 2))
            }, completion: { (true) in
                UIView.animate(withDuration: 0.2, animations: {
                    self.filterBtn.isHidden = true
                    self.filterLabel.isHidden = true
                    self.visitedBtn.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi))
                    self.visitedLabel.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi))
                })
                UIView.animate(withDuration: 0.5, delay: 0.10, options: UIView.AnimationOptions.transitionCurlDown, animations: {
                    self.visitedBtn.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi * 2))
                    self.visitedLabel.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi * 2))
                }, completion: { (true) in
                    self.visitedBtn.isHidden = true
                    self.visitedLabel.isHidden = true
                    UIView.animate(withDuration: 0.2, animations: {
                        self.bookmarkBtn.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi))
                        self.bookmarkLabel.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi))
                    })
                    UIView.animate(withDuration: 0.5, delay: 0.10, options: UIView.AnimationOptions.transitionCurlDown, animations: {
                        self.bookmarkBtn.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi * 2))
                        self.bookmarkLabel.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi * 2))
                    }, completion: { (true) in
                        self.bookmarkBtn.isHidden = true
                        self.bookmarkLabel.isHidden = true
                        self.dismissVCDelegate.dismissVc()
                    })
                })
            })
        }
    }
    
    @IBAction func bookmarkBtnTapped(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "starmarked") as! Bookmarks
        vc.titleView = "Bookmark"
        vc.exhibitorCat = self.exhibitorName
        if self.eventCategory == self.eventCategory
        {
            vc.eventCat = self.exhibitorName
        } else
        {
            vc.eventCat = "international"
            
        }
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func visitedBtnTapped(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "starmarked") as! Bookmarks
        vc.titleView = "Visited"
        vc.exhibitorCat = self.exhibitorName
        
        if self.eventCategory == self.eventCategory
        {
            vc.eventCat = self.exhibitorName
            
        } else
        {
            vc.eventCat = "international"
        }
        
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func filterBtnTapped(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ExhibitorFilterViewController") as! ExhibitorFilterViewController
        vc.filterRequest = "exhibitor"
        print(self.hallArray)
        print(self.sectionArray)
        
        vc.dropDownOneArray = self.hallArray
        vc.dropDownTwoArray = self.sectionArray
        vc.coredataArray = self.coredataArray
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.2) { () -> Void in
                self.bookmarkBtn.isHidden = false
                self.bookmarkBtn.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi))
                self.bookmarkLabel.isHidden = false
                self.bookmarkLabel.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi))
            }
            UIView.animate(withDuration: 0.5, delay: 0.10, options: UIView.AnimationOptions.transitionFlipFromTop, animations: {
                self.bookmarkBtn.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi * 2))
                self.bookmarkLabel.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi * 2))
            }, completion: { (true) in
                self.visitedBtn.isHidden = false
                self.visitedLabel.isHidden = false
                UIView.animate(withDuration: 0.2) { () -> Void in
                    self.visitedBtn.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi))
                    self.visitedLabel.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi))
                }
                UIView.animate(withDuration: 0.5, delay: 0.10, options: UIView.AnimationOptions.transitionFlipFromTop, animations: {
                    self.visitedBtn.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi * 2))
                    self.visitedLabel.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi * 2))
                }, completion: { (true) in
                    self.filterBtn.isHidden = false
                    UIView.animate(withDuration: 0.2) { () -> Void in
                        self.filterBtn.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi))
                        self.filterLabel.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi))
                    }
                    UIView.animate(withDuration: 0.5, delay: 0.10, options: UIView.AnimationOptions.transitionFlipFromTop, animations: {
                        self.filterBtn.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi * 2))
                    }, completion: { (true) in
                        self.filterLabel.isHidden = false
                        self.filterLabel.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi * 2))
                    })
                })
            })
        }
    }

}
