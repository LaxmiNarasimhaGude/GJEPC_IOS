//
//  AddANoteTableViewCell.swift
//  GJEPC
//
//  Created by Kwebmaker on 29/07/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit

class AddANoteTableViewCell: UITableViewCell {

    @IBOutlet weak var datelabel: UILabel!
    @IBOutlet weak var timelabel: UILabel!
    @IBOutlet weak var notesTextViewInAddNoteVC: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
