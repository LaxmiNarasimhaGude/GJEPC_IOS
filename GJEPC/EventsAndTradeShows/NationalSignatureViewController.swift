//
//  NationalSignatureViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 13/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
@available(iOS 13.0, *)
class NationalSignatureViewController: UIViewController,dataReceivedFromServerDelegate {
   
    

    @IBOutlet weak var igjmeView: UIView!
    @IBOutlet weak var iijsPremierView: UIView!
    @IBOutlet weak var iijsSignatureView: UIView!
    
        var requestServer = RequestToServer()
        var signaturetapped = false
        var igemstapped = false
        var iijstapped = false
        var activityLoader : MBProgressHUD? = nil
        var sendTitleName = ""
    override func viewDidLoad() {
        super.viewDidLoad()
setview()
        requestServer.delegate = self
    }
    func setview()  {
        igjmeView.backgroundColor = UIColor.white
        igjmeView.layer.shadowColor = UIColor.lightGray.cgColor
        igjmeView.layer.shadowOpacity = 1
        igjmeView.layer.shadowOffset = CGSize.zero
        igjmeView.layer.shadowRadius = 2
        igjmeView.layer.cornerRadius = 5
        igjmeView.layer.borderWidth = 1
        igjmeView.layer.borderColor = UIColor.white.cgColor
        
        
        iijsPremierView.backgroundColor = UIColor.white
        iijsPremierView.layer.shadowColor = UIColor.lightGray.cgColor
        iijsPremierView.layer.shadowOpacity = 1
        iijsPremierView.layer.shadowOffset = CGSize.zero
        iijsPremierView.layer.shadowRadius = 2
        iijsPremierView.layer.cornerRadius = 5
        iijsPremierView.layer.borderWidth = 1
        iijsPremierView.layer.borderColor = UIColor.white.cgColor
        
        iijsSignatureView.backgroundColor = UIColor.white
        iijsSignatureView.layer.shadowColor = UIColor.lightGray.cgColor
        iijsSignatureView.layer.shadowOpacity = 1
        iijsSignatureView.layer.shadowOffset = CGSize.zero
        iijsSignatureView.layer.shadowRadius = 2
        iijsSignatureView.layer.cornerRadius = 5
        iijsSignatureView.layer.borderWidth = 1
        iijsSignatureView.layer.borderColor = UIColor.white.cgColor
        

    }

   
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func signatureBtnTapped(_ sender: Any) {
        if CheckInternet.isConnectedToNetwork() == true {
            DispatchQueue.main.async {
                self.loadActivityIndicator()
            }
            signaturetapped = true
            igemstapped = false
            iijstapped = false
            let params = ["event":"signature"]
            sendTitleName = "IIJS SIGNATURE 2020"
            
            self.requestServer.connectToServer(myUrl: checkEventStatusUrl!, params: params as AnyObject)
        }
        else {
            let alert = UIAlertController(title: "", message: "Please Check Your Internet Connection", preferredStyle: UIAlertController.Style.alert)
            self.present(alert, animated: true, completion: nil)
            let when = DispatchTime.now() + 3
            DispatchQueue.main.asyncAfter(deadline: when){
                alert.dismiss(animated: true, completion: nil)
            }
        }
    }
    @IBAction func premierBtnTapped(_ sender: Any) {
        if CheckInternet.isConnectedToNetwork() == true {
            DispatchQueue.main.async {
                self.loadActivityIndicator()
            }
            
            iijstapped = true
            signaturetapped = false
            igemstapped = false
            let params = ["event":"iijs"]
            sendTitleName = "IIJS PREMIERE 2020"
            requestServer.connectToServer(myUrl: checkEventStatusUrl!, params: params as AnyObject)
        } else {
            let alert = UIAlertController(title: "", message: "Please Check Your Internet Connection", preferredStyle: UIAlertController.Style.alert)
            self.present(alert, animated: true, completion: nil)
            let when = DispatchTime.now() + 3
            DispatchQueue.main.asyncAfter(deadline: when){
                alert.dismiss(animated: true, completion: nil)
            }
        }
    }
    @IBAction func igjmeBtnTapped(_ sender: Any) {
        if CheckInternet.isConnectedToNetwork() == true {
            DispatchQueue.main.async {
                self.loadActivityIndicator()
            }
            igemstapped = true
            signaturetapped = false
            iijstapped = false
            sendTitleName = "IGJME 2020"
            let params = ["event": "igjme"]
            requestServer.connectToServer(myUrl: checkEventStatusUrl!, params: params as AnyObject)
        } else {
            let alert = UIAlertController(title: "", message: "Please Check Your Internet Connection", preferredStyle: UIAlertController.Style.alert)
            self.present(alert, animated: true, completion: nil)
            let when = DispatchTime.now() + 3
            DispatchQueue.main.asyncAfter(deadline: when){
                alert.dismiss(animated: true, completion: nil)
            }
        }
    }
    func loadActivityIndicator()
      {
          activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true)
          activityLoader?.bezelView.color = UIColor.clear

          activityLoader?.bezelView.style = .solidColor
          activityLoader?.isUserInteractionEnabled = false
      }
    func dataReceivedFromServer(data: NSDictionary, url: URL) {
            if ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "true" {
                let temporaryArray = ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "Result") as! NSMutableArray)
                let checkStatus  =  (((temporaryArray.object(at: 0)) as AnyObject).value(forKey: "status") as! String)
                let isTemp = (((temporaryArray.object(at: 0)) as AnyObject).value(forKey: "isTemp") as! String)
                if checkStatus == "0" {
                    
                    
                    DispatchQueue.main.async {
                    self.activityLoader?.hide(animated: true)
                                    }
                    
                    let alert = UIAlertController(title: "", message: "Coming Soon", preferredStyle: UIAlertController.Style.alert)
                    DispatchQueue.main.sync {
                         self.present(alert, animated: true, completion: nil)
                    }
                       
    //
                    
                    let when = DispatchTime.now() + 3
                    DispatchQueue.main.asyncAfter(deadline: when){
                        
                        alert.dismiss(animated: true, completion: nil)
                    }
                } else {
                    
                    if(checkStatus == "1"){
                        
                        if(isTemp == "0"){
                            DispatchQueue.main.async {
                                         self.activityLoader?.hide(animated: true)
                                     }
                                     if signaturetapped == true {
                                         
                                         DispatchQueue.main.async {
                                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignatureViewController") as! SignatureViewController
                                            vc.category = "national"
                                            vc.exhibitorSelected = "signature"
                                            vc.titleName =  self.sendTitleName
                                             self.navigationController?.pushViewController(vc, animated: true)
                                         }
                                         signaturetapped = false
                                     }
                                     else if igemstapped == true {
                                         let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignatureViewController") as! SignatureViewController
                                         vc.category = "national"
                                         vc.exhibitorSelected = "igjme"
                                         vc.titleName = "IGJME"
                                         DispatchQueue.main.async {
                                             self.navigationController?.pushViewController(vc, animated: true)
                                         }
                                         igemstapped = false
                                         
                                     } else {
                                         let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignatureViewController") as! SignatureViewController
                                         vc.category = "national"
                                         vc.exhibitorSelected = "iijs"
                                         vc.titleName = "IIJS Premiere 2019"
                                         DispatchQueue.main.async {
                                             self.navigationController?.pushViewController(vc, animated: true)
                                         }
                                         iijstapped = false
                                     }
                                 }
                        else{
                             let Urldescription = (((temporaryArray.object(at: 0)) as AnyObject).value(forKey: "description") as! String)
                            
                            DispatchQueue.main.async {
                                self.activityLoader?.hide(animated: true)
                                                       
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "SignatureWebViewController") as! SignatureWebViewController
                               
                                vc.des = Urldescription
                                vc.headingDesc = self.sendTitleName
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                            
                           
                          
                            

                            
                        }
                        
                        
                        }
                        
                        
                        
                    }
                    
                    
             
            }
        }
}
