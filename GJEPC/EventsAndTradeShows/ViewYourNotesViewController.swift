//
//  ViewYourNotesViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 29/07/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import CoreData

var notesArray : NSMutableArray = []

@available(iOS 13.0, *)
class ViewYourNotesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, backButtonClickedDelegate {

    @IBOutlet weak var notesTableView: UITableView!
    @IBOutlet weak var heading: UILabel!
    
    
       var currentDate = ""
       var currentTime = ""
       var addnoteVC = AddANoteViewController()
       var appDel : AppDelegate!
       var context : NSManagedObjectContext!
       var coreDataArray : NSArray  = []
       var coreDataCount = 0
       var exhibitorIdForStarMarked : String = ""
       var exhibitorId = ""
       var exhibitorCategory = ""
       var savedNotes = ""
       var exhibitorNotesArray : NSMutableArray = []
       var notesData : AnyObject!
       var filterRequest = ""
       var companyName = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        appDel = (UIApplication.shared.delegate as! AppDelegate)
        context = appDel.managedObjectContext
        notesTableView.delegate = self
        notesTableView.dataSource = self
        addnoteVC.bckBtnClickedDelegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(notificationSucessful), name: NSNotification.Name(rawValue: notificationKeyForBackBtn), object: nil)
        
        heading.text = companyName
        
    }
    @objc func notificationSucessful() {
           //print("hello")
       }
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(true)
           coreDatafetch()
       }

    @IBAction func back(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationKeyForBackBtn), object: nil, userInfo: ["backBtnHide" : "true"])
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    @IBAction func addANoteButtonTapped(_ sender: Any) {
          addChildView()
    }
    func addChildView()
    {
        DispatchQueue.main.async {
            self.notesTableView.isHidden = false
            self.view.alpha = 0.8
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "AddANoteViewController") as! AddANoteViewController
            
            UIView.animate(withDuration: 0.10, delay: 0.20, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
                controller.view.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi ))
                controller.view.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi * 2))
            }, completion: { (true) in
                self.addChild(controller)
                controller.view.alpha = 1
                controller.view.frame = self.view.bounds
                controller.bckBtnClickedDelegate = self
                controller.exhibitorId = self.exhibitorId
                controller.exhibitorCategory = self.exhibitorCategory
                controller.companyName = self.companyName
                
                self.view.addSubview(controller.view)
                controller.didMove(toParent: self)
            })
        }
    }
    func removeChildView()
    {
        DispatchQueue.main.async {
            self.view.alpha = 1
            let vc = self.children.last
            vc?.willMove(toParent: nil)
            vc?.view.removeFromSuperview()
            vc?.removeFromParent()
            self.notesTableView.reloadData()
            if notesArray.count == 0 {
                self.notesTableView.isHidden = true
            } else {
                self.notesTableView.isHidden = false
            }
        }
        coreDatafetch()
    }
    func bckBtnClicked() {
           
           coreDatafetch()
           removeChildView()
       }
     
        func getDateTime() {
            let date = Date()
            let calendar = Calendar.current
            let formatter = DateFormatter()
            formatter.dateFormat = "dd.MM.yyyy"
            let result = formatter.string(from: date)
            let hour = calendar.component(.hour, from: date as Date)
            let minutes = calendar.component(.minute, from: date as Date)
            let seconds = calendar.component(.second, from: date as Date)
            currentTime = "\(hour):\(minutes):\(seconds)"
            currentDate = "\(result)"
        }
        
        
        func deleteCoreData(id : String , category : String, notes : String)
        {
            var request = NSFetchRequest<NSFetchRequestResult>()
            request = NSFetchRequest(entityName: "ExhibitorsNotes")
            
            request.predicate = NSPredicate(format: "exhibitorID = %@",id)
            request.predicate = NSPredicate(format: "exhibitorCategory = %@",category)
            request.predicate = NSPredicate(format: "company = %@",companyName)
            
            request.predicate = NSPredicate(format: "notes = %@",notes)
            
            let result = try? context.fetch(request)
            let resultData = result as! [ExhibitorsNotes]
            
            for object in resultData {
                context.delete(object)
            }
            
            do {
                try context.save()
                print("saved!")
            } catch let error as NSError  {
                print("Could not save \(error), \(error.userInfo)")
            } catch {
                
            }
        }
        
        func coreDatafetch() {
            var request = NSFetchRequest<NSFetchRequestResult>()
            request = NSFetchRequest(entityName: "ExhibitorsNotes")
            request.returnsObjectsAsFaults = false
            do {
                notesData = try  self.context.fetch(request) as AnyObject
                
            } catch {
                fatalError()
            }
            getSelectedExhibitorData()
        }
        
        func getSelectedExhibitorData(){
            
            exhibitorNotesArray = []
            print(notesData)
            
            if (notesData.count != nil)  {
                
                if notesData.count > 0 {
                    for i in 0..<self.notesData.count{
                        let data = self.notesData.object(at: i) as AnyObject
                        let id =  data.value(forKey: "exhibitorID") as! String
                        let category = data.value(forKey: "exhibitorCategory") as! String
                        let company = data.value(forKey: "company") as! String
                        
                        if filterRequest == "mainListing"
                        {
                            if self.companyName == company {
                                self.exhibitorNotesArray.add(data)
                            }
                        }
                        else
                        { if self.exhibitorId == id && self.exhibitorCategory == category{
                            self.exhibitorNotesArray.add(data)
                            }
                        }
                    }
                }
            }
            
            if self.exhibitorNotesArray.count > 0 {
                DispatchQueue.main.async {
                    self.notesTableView.isHidden = false
                    self.notesTableView.reloadData()
                }
            }
            else {
                notesTableView.isHidden = true
            }
        }
        
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddANoteTableViewCell") as! AddANoteTableViewCell
            DispatchQueue.main.async {
                self.getDateTime()
                cell.notesTextViewInAddNoteVC.text = (self.exhibitorNotesArray.object(at: indexPath.row) as AnyObject).value(forKey: "notes") as! String
                cell.timelabel.text = (self.exhibitorNotesArray.object(at: indexPath.row) as AnyObject).value(forKey: "date") as? String
                cell.datelabel.text = (self.exhibitorNotesArray.object(at: indexPath.row) as AnyObject).value(forKey: "time") as? String
            }
            return cell
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.exhibitorNotesArray.count
        }
        
        
        
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
            
        if editingStyle == UITableViewCell.EditingStyle.delete
            {
                
                let alertController = UIAlertController(title: "Warning", message: "Are you sure?", preferredStyle: .alert)
                
                let deleteAction = UIAlertAction(title: "Delete", style: .destructive, handler: { (action) in
                   
              
                DispatchQueue.main.async {
                    let id = (self.exhibitorNotesArray.object(at: indexPath.row) as AnyObject).value(forKey: "exhibitorID") as! String
                    let category = (self.exhibitorNotesArray.object(at: indexPath.row) as AnyObject).value(forKey: "exhibitorCategory") as! String
                    
                    let notes = (self.exhibitorNotesArray.object(at: indexPath.row) as AnyObject).value(forKey: "notes") as! String
                    
                    self.exhibitorNotesArray.removeObject(at: indexPath.row)
                    self.notesTableView.deleteRows(at: [indexPath], with: .fade)
                    self.notesTableView.endUpdates()
                    
                    print(id)
                    print(category)
                    print(notes)
                    self.deleteCoreData(id: id, category: category,notes: notes)
                    
                    if self.exhibitorNotesArray.count == 0 {
                        
                        self.notesTableView.isHidden = true
                    } else {
                        self.notesTableView.isHidden = false
                    }
                }
                    
                })
                alertController.addAction(deleteAction)
                let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
                alertController.addAction(cancelAction)
                
                present(alertController, animated: true, completion: nil)
            }
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            DispatchQueue.main.async {
                self.savedNotes = (self.exhibitorNotesArray.object(at: indexPath.row) as AnyObject).value(forKey: "notes") as! String
                print(self.savedNotes)
                self.notesTableView.isHidden = false
               
                self.view.alpha = 0.8
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "AddANoteViewController") as! AddANoteViewController
                controller.exhibitorId = self.exhibitorId
                controller.exhibitorCategory = self.exhibitorCategory
                controller.isEdit = true
                controller.savedNotes = self.savedNotes
                controller.comingFrom = "mainListing"
                controller.companyName = self.companyName
                
                
                UIView.animate(withDuration: 0.10, delay: 0.20, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
                    controller.view.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi ))
                    controller.view.transform = CGAffineTransform(rotationAngle: CGFloat(Double .pi * 2))
                }, completion: { (true) in
                    self.addChild(controller)
                    controller.view.alpha = 1
                    controller.view.frame = self.notesTableView.bounds
                    controller.bckBtnClickedDelegate = self
                    if notesArray.count != 0 {
                        controller.notesTextView.text = self.savedNotes
                    }
                   // self.notesTableView.addSubview(controller.view)
                  self.view.addSubview(controller.view)
                    controller.didMove(toParent: self)
                })
            }
        }
        
    }
    // end of class
