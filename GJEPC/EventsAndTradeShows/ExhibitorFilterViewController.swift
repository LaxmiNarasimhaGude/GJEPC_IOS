//
//  ExhibitorFilterViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 29/07/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit

protocol getDataFromFilter {
    func filteredData(dropDownOneSelectedValue : String , dropDownTwoSelectedValue : String)
}

@available(iOS 13.0, *)
class ExhibitorFilterViewController: UIViewController,pickerDelegate {
    @IBOutlet weak var selectHallTf: UITextField!
    @IBOutlet weak var selectSectionTf: UITextField!
    
    @IBOutlet weak var resetBtn: UIButton!
    @IBOutlet weak var applyBtn: UIButton!
    
    var hallPickerView : UIPickerView!
    var sectionPickerView : UIPickerView!
    var customArray : NSMutableArray = []
    var pickerObj = PickerViewController()
    var dropDownOneArray : NSMutableArray = []
    var dropDownTwoArray : NSMutableArray = []
    var isDropdownOneClicked : Bool = false
    var isDropdownTwoClicked : Bool = false
    var dropDownOneValue : String = ""
    var dropDownTwoValue : String = ""
    var filterRequest = ""
    var delegate : getDataFromFilter!
    var coredataArray : NSMutableArray = []
    var value1 = ""
    var value2 = ""
    var tempArray1 : NSMutableArray = []
    var tempArray2 : NSMutableArray = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tempArray2 = dropDownTwoArray
        NotificationCenter.default.addObserver(self, selector: #selector(notificationSucessful), name: NSNotification.Name(rawValue: myNotificationKey), object: nil)
        if filterRequest == "member"
        {
            selectHallTf.placeholder = "Select Region"
            selectSectionTf.placeholder = "Select City"
        }
        else {
            selectHallTf.placeholder = "Select Hall"
            selectSectionTf.placeholder = "Select Section"
        }
    }
    
    @IBAction func backBtn(_ sender: Any) {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
   
    @IBAction func resetBtn(_ sender: Any) {
        selectHallTf.text = ""
        selectSectionTf.text = ""
        dropDownOneValue = ""
        dropDownTwoValue = ""
        dropDownTwoArray = tempArray2
        
        if filterRequest == "member"
        {
            selectHallTf.placeholder = "Select Region"
            selectSectionTf.placeholder = "Select City"
            
        }
        else {
            
            selectHallTf.placeholder = "Select Hall"
            selectSectionTf.placeholder = "Select Section"
        }
        
    }
    @objc func notificationSucessful() {
        //print("hello")
    }
    
    @IBAction func applyBtnClicked(_ sender: Any) {
        
        if selectSectionTf.text == "" && selectHallTf.text == "" {
            let alert = UIAlertController(title: "Error", message: "Please Select  One Of These", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler:nil) )
            DispatchQueue.main.async {
                self.present(alert, animated: true, completion: nil)
            }
        }
        else {
            DispatchQueue.main.async {
                if self.filterRequest == "member" {
                    _ = self.navigationController?.popViewController(animated: true)
                    self.delegate?.filteredData(dropDownOneSelectedValue: self.dropDownOneValue, dropDownTwoSelectedValue: self.dropDownTwoValue)
                }
                else
                {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: myNotificationKey), object: nil, userInfo: ["dataSentForValueOne" : "\(self.dropDownOneValue)", "dataSentForValueTwo" : "\(self.dropDownTwoValue)"])
                    
                    let viewControllers: [UIViewController] = self.navigationController!.viewControllers
                    for aViewController in viewControllers {
                        if aViewController is ExhibitorDirectory {
                            DispatchQueue.main.async{
                                _ = self.navigationController?.popToViewController(aViewController, animated: true)
                            }
                        }
                    }
                }
            }
        }
    }
    func sortDropDowntwoData(value : String) {
           
       }
    @IBAction func dropDownOneClicked(_ sender: Any) {
        
        isDropdownOneClicked = true
        let name = UIStoryboard(name: "Main", bundle: nil)
        pickerObj = name.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        pickerObj.delegate = self
        pickerObj.pickerData = dropDownOneArray
        
        DispatchQueue.main.async {
            self.addChild(self.pickerObj)
            self.view.addSubview(self.pickerObj.view)
            self.pickerObj.didMove(toParent: self)
        }
    }
    
    
    @IBAction func dropDownTwoClicked(_ sender: Any) {
        
        print(dropDownTwoArray)
        
        isDropdownTwoClicked = true
        let name = UIStoryboard(name: "Main", bundle: nil)
        pickerObj = name.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        pickerObj.delegate = self
        pickerObj.pickerData = dropDownTwoArray
        DispatchQueue.main.async {
            self.addChild(self.pickerObj)
            self.view.addSubview(self.pickerObj.view)
            self.pickerObj.didMove(toParent: self)
        }
    }
     override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(true)
            DispatchQueue.main.async {
                self.applyBtn.layer.cornerRadius = 5
                self.resetBtn.layer.cornerRadius = 5
                self.selectHallTf.layer.cornerRadius = 5
                self.selectSectionTf.layer.cornerRadius = 5
                self.resetBtn.layer.borderColor = UIColor.purple.cgColor
                self.resetBtn.layer.borderWidth = 0.5
                self.isDropdownTwoClicked = false
                self.isDropdownOneClicked = false
                self.dropDownOneValue = ""
                self.dropDownTwoValue = ""
                self.selectHallTf.layer.borderColor = UIColor.black.cgColor
                self.selectSectionTf.layer.borderColor = UIColor.black.cgColor
                self.selectSectionTf.layer.borderWidth = 0.5
                self.selectHallTf.layer.borderWidth = 0.5
            }
        }
        
        func doneClicked(sender: PickerViewController) {
            print(isDropdownOneClicked)
            print(isDropdownTwoClicked)
            value1 = selectHallTf.text!
            
            if isDropdownOneClicked == true {
                let selected = "\(self.pickerObj.pickerData[self.pickerObj.pickerView.selectedRow(inComponent: (0))] as! String)"
                selectHallTf.text = selected
                isDropdownOneClicked = false
                dropDownOneValue = selected
                
                dropDownTwoArray = []
                
                //print(coredataArray)
                if self.filterRequest == "member"
                {
                    for  i in 0..<coredataArray.count {
                        let city = ((self.coredataArray.object(at: i) as AnyObject).value(forKey: "city") as! String).uppercased()
                        let region = ((self.coredataArray.object(at: i) as AnyObject).value(forKey: "region_id") as! String).uppercased()
                        if selected != ""
                        {
                            if selected == region {
                                if !self.dropDownTwoArray.contains(city) {
                                    self.dropDownTwoArray.add(city)
                                }
                            }
                        }
                    }
                    
                    selectSectionTf.placeholder = "Select City"
                    selectSectionTf.text = ""
                    dropDownTwoValue = ""
                }
                else {
                    for  i in 0..<coredataArray.count {
                        let hall = ((self.coredataArray.object(at: i) as AnyObject).value(forKey: "Exhibitor_HallNo") as! String).uppercased()
                        let section = ((self.coredataArray.object(at: i) as AnyObject).value(forKey: "Exhibitor_Section") as! String).uppercased()
                        if selected != ""
                        {
                            if selected == hall {
                                if !self.dropDownTwoArray.contains(section) {
                                    self.dropDownTwoArray.add(section)
                                }
                            }
                        }
                    }
                    selectSectionTf.placeholder = "Select Section"
                    selectSectionTf.text = ""
                    dropDownTwoValue = ""
                }
            }
                
            else if isDropdownTwoClicked == true{
                let selected = "\(self.pickerObj.pickerData[self.pickerObj.pickerView.selectedRow(inComponent: (0))] as! String)"
                selectSectionTf.text = selected
                isDropdownTwoClicked = false
                dropDownTwoValue = selected
            }
            
            
            DispatchQueue.main.async {
                self.pickerObj.willMove(toParent: nil)
                self.pickerObj.view.removeFromSuperview()
                self.pickerObj.removeFromParent()
                
            }
        }
        
        func cancelClicked(sender: PickerViewController) {
            DispatchQueue.main.async {
                self.pickerObj.willMove(toParent: nil)
                self.pickerObj.view.removeFromSuperview()
                self.pickerObj.removeFromParent()
                
            }
        }
    }

    // end of class
