//
//  SignatureWebViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 13/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
class SignatureWebViewController: UIViewController,UIWebViewDelegate {
    
    @IBOutlet weak var headingTitle: UILabel!
    @IBOutlet weak var signatureWebview: UIWebView!
    var des:String = ""
       var headingDesc = ""
       var activityLoader : MBProgressHUD? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
            self.loadActivityIndicator()
            self.loadHtmlInAWebView()
            self.headingTitle.text = self.headingDesc
            self.signatureWebview.delegate = self
            
        }
        
        
        
    }
    
    func loadHtmlInAWebView() {
        
//        loadActivityIndicator()
           let htmlString : String! = "<html><head><style type='text/css'>span {    text-align: justify;line-height: auto;float: left;font-size:14px;font-family:futura}</style></head><body><font color ='#000000'><span>\(des)<br/><br></span></font></body></html>"
           signatureWebview.backgroundColor = UIColor.white
           signatureWebview.loadHTMLString(htmlString, baseURL: nil)
           DispatchQueue.main.async {
               self.activityLoader?.hide(animated: true)
           }
       }
    
    func loadActivityIndicator()
    {
        activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
        activityLoader?.label.text = "Loading";
        activityLoader?.detailsLabel.text = "Please Wait!!!";
        activityLoader?.isUserInteractionEnabled = false;
    }
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        activityLoader?.hide(animated: true)
    }
}
