//
//  ZoneManager.swift
//  GJEPC
//
//  Created by Kwebmaker on 30/07/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
class ZoneManager: UIViewController,dataReceivedFromServerDelegate,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var retryBtn: UIButton!
    @IBOutlet weak var zoneTableView: UITableView!
    
    var zoneDataArray : NSArray = []
       var requestToServer = RequestToServer()
       var callUrls : URL!
       var activityLoader : MBProgressHUD? = nil
       var eventName : String = ""
       var customArray : NSMutableArray = []
       var mobile : String = ""
       var email : String = ""
       var sectionArray : NSMutableArray = []
       var sectionListData : NSMutableDictionary = [:]
       var callTapped = false
       var emailTaped = true
    
    override func viewDidLoad() {
           super.viewDidLoad()
           checkForInternet()
           retryBtn.layer.cornerRadius = 5
       }
    
    @IBAction func back(_ sender: Any) {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    @IBAction func retryBtnTapped(_ sender: Any) {
        checkForInternet()
    }
    func checkForInternet()
    {
        if CheckInternet.isConnectedToNetwork() == true{
            zoneTableView.isHidden = false
            
            DispatchQueue.main.async {
                self.loadActivityIndicator()
            }
            
            requestToServer.delegate = self
            callUrls = zoneUrl
            
            if eventName == "signature"{
                let params = [ "event_name":"signature"]
                requestToServer.connectToServer(myUrl: callUrls as URL, params: params as AnyObject)
            } else if eventName == "iijs"
            {
                let params = [ "event_name":"iijs"]
                requestToServer.connectToServer(myUrl: callUrls as URL, params: params as AnyObject)
            }
            else if eventName == "igjme"
            {
                let params = [ "event_name":"igjme"]
                requestToServer.connectToServer(myUrl: callUrls as URL, params: params as AnyObject)
            }
        }
        else
        {
            self.zoneTableView.isHidden = true
        }
    }

    func loadActivityIndicator()
    {
        activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
        activityLoader?.label.text = "Loading";
        activityLoader?.detailsLabel.text = "Please Wait";
        activityLoader?.isUserInteractionEnabled = false;
    }
    func dataReceivedFromServer(data: NSDictionary, url: URL) {
           if ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "true" {
               let temporaryArray = ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "Result") as! NSArray)
               zoneDataArray = (temporaryArray.mutableCopy() as! NSMutableArray)
               self.customArray = zoneDataArray.mutableCopy() as! NSMutableArray
               print(zoneDataArray)
               DispatchQueue.main.async {
                   self.activityLoader?.hide(animated: true)
                   //self.zoneTableView.reloadData()
                   self.sortData()
               }
           }
       }
    
    func sortData() {
           for i in 0..<zoneDataArray.count {
               let hall = (zoneDataArray.object(at: i) as! NSDictionary).value(forKey: "hall") as! String
               if !sectionArray.contains(hall) {
                   sectionArray.add(hall)
               }
           }
           
           var tempDict : NSMutableArray = []
           
           for i in 0..<sectionArray.count {
               tempDict = []
               let hallNo  = self.sectionArray[i] as! String
               for j in 0..<self.zoneDataArray.count {
                   let hall = (zoneDataArray.object(at: j) as! NSDictionary).value(forKey: "hall") as! String
                   if hallNo == hall {
                       tempDict.add((zoneDataArray.object(at: j) as! NSDictionary))
                   }
               }
               sectionListData.setObject(tempDict, forKey: hallNo as NSCopying)
           }
           DispatchQueue.main.async {
               //self.activityLoader?.hide(animated: true)
               if self.zoneDataArray.count > 0 {
                   self.zoneTableView.reloadData()
               }
               else {
                   let alert = UIAlertController(title: "", message: "Coming Soon", preferredStyle: .alert)
                let action = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
                       self.navigationController?.popViewController(animated: true)
                   })
                   alert.addAction(action)
                   DispatchQueue.main.async {
                       self.present(alert, animated: true, completion: nil)
                   }
               }
           }
       }
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return (sectionListData.object(forKey: self.sectionArray[section]) as! NSArray).count
        }
        
        func numberOfSections(in tableView: UITableView) -> Int {
            return sectionListData.count
        }
        
        func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
            return sectionArray[section] as? String
        }
        func tableView(_ tableView: UITableView, willDisplayHeaderView view:UIView, forSection: Int) {
            if let tableViewHeaderFooterView = view as? UITableViewHeaderFooterView {
                
                let swiftColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
                
                tableViewHeaderFooterView.textLabel?.textColor = swiftColor
            }
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! TableCell
            cell.zoneNo.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2 )
            cell.name1.text = ((self.sectionListData.object(forKey: self.sectionArray[indexPath.section]) as! NSArray).object(at: indexPath.row) as! NSDictionary).value(forKey: "name") as! String?
            cell.zoneNo.text = ((self.sectionListData.object(forKey: self.sectionArray[indexPath.section]) as! NSArray).object(at: indexPath.row) as! NSDictionary).value(forKey: "zone") as! String?
            cell.contactNo.text = ((self.sectionListData.object(forKey: self.sectionArray[indexPath.section]) as! NSArray).object(at: indexPath.row) as! NSDictionary).value(forKey: "mob") as! String?
                    cell.name2.text = ((self.sectionListData.object(forKey: self.sectionArray[indexPath.section]) as! NSArray).object(at: indexPath.row) as! NSDictionary).value(forKey: "email") as! String?
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
             mobile = (((self.sectionListData.object(forKey: self.sectionArray[indexPath.section]) as! NSArray).object(at: indexPath.row) as! NSDictionary).value(forKey: "mob") as! String?)!
            email = (((self.sectionListData.object(forKey: self.sectionArray[indexPath.section]) as! NSArray).object(at: indexPath.row) as! NSDictionary).value(forKey: "email") as! String?)!
            let optionMenuController = UIAlertController(title: nil, message: "What would you like to do?", preferredStyle: .actionSheet)
            let emailAction = UIAlertAction(title: "Email", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                let stringUrl = NSURL(string : "mailto:\(self.email)")
                if UIApplication.shared.canOpenURL(stringUrl! as URL)
                {
                    UIApplication.shared.openURL(stringUrl! as URL)
                }
            })
            let webAction = UIAlertAction(title: "Call", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                if let phoneCallURL = NSURL(string: "telprompt://\(self.mobile.replacingOccurrences(of: " ", with: ""))"){
                    let application:UIApplication = UIApplication.shared
                    if (application.canOpenURL(phoneCallURL as URL)) {
                        application.openURL(phoneCallURL as URL)
                    }
                }
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
                (alert: UIAlertAction!) -> Void in
                 self.dismiss(animated: true, completion: nil)
            })
            // Add UIAlertAction in UIAlertController
            optionMenuController.addAction(emailAction)
            optionMenuController.addAction(webAction)
             optionMenuController.addAction(cancelAction)
            // Present UIAlertController with Action Sheet
            DispatchQueue.main.async {
                  self.present(optionMenuController, animated: true, completion: nil)
            }
          
        }

    }
    // End of class

