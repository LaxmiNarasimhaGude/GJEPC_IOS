//
//  InternationalEventsViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 12/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
@available(iOS 13.0, *)
class InternationalEventsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,dataReceivedFromServerDelegate {
   
    
    
    @IBOutlet weak var bsmAndDelegationLbl: UILabel!
    @IBOutlet weak var indiaPavilionLbl: UILabel!
    @IBOutlet weak var TradeShowTable: UITableView!
    var activityLoader : MBProgressHUD? = nil
    var requestToServer = RequestToServer()
    var callUrls : URL!
    var Date1:String!
    var replacedImgstring  = ""
    var tableViewMenuArray : NSMutableArray = []
    var type = "IndiaPavilion"
    var titleLabel = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        requestToServer.delegate = self
        TradeShowTable.delegate = self
        TradeShowTable.dataSource = self
        TradeShowTable.separatorStyle = .none
        checkforInternet()    }
    

    func checkforInternet()
        {
            bsmAndDelegationLbl.backgroundColor = UIColor.white
            indiaPavilionLbl.backgroundColor  = UIColor.black
           if CheckInternet.isConnectedToNetwork() == true{
               DispatchQueue.main.async {
                   self.loadActivityIndicator()
                   
                   
                   self.TradeShowTable.isHidden = false
                   self.callUrls = gjepcPrivateLimited
                   let params = [ ""]
                   self.requestToServer.connectToServer(myUrl: self.callUrls as URL, params: params as AnyObject)
                  
                   
               }
               
               
               
           }
           else
           {
               
               self.TradeShowTable.isHidden = true
           }
           
            DispatchQueue.main.async {
                          self.TradeShowTable.reloadData()
                          self.TradeShowTable.contentOffset = .zero

                      }
       }
    @IBAction func indiaPavilion(_ sender: Any) {
        
        self.type = "IndiaPavilion"
        
//        indiaPavilionsView.backgroundColor = UIColor(displayP3Red: 0.094, green: 0.384, blue: 0.686, alpha: 1)
//        indiaPavilionLabel.textColor = UIColor.white
        
        bsmAndDelegationLbl.backgroundColor = UIColor.white
        indiaPavilionLbl.backgroundColor  = UIColor.black
//        bsmAndDelegationLabel.textColor = UIColor(displayP3Red: 0.094, green: 0.384, blue: 0.686, alpha: 1)
        checkforInternet()
        
        
    }
    
    @IBAction func bsmAndDelegation(_ sender: Any) {
        
        self.type = "BSM & Delegation"
        
//        bsmAndDelegationView.backgroundColor = UIColor(displayP3Red: 0.094, green: 0.384, blue: 0.686, alpha: 1)
//        bsmAndDelegationLabel.textColor = UIColor.white
        
//        indiaPavilionsView.backgroundColor = UIColor.white
//        indiaPavilionLabel.textColor = UIColor(displayP3Red: 0.094, green: 0.384, blue: 0.686, alpha: 1)
        bsmAndDelegationLbl.backgroundColor = UIColor.black
        indiaPavilionLbl.backgroundColor  = UIColor.white
        
        if CheckInternet.isConnectedToNetwork() == true{
                  DispatchQueue.main.async {
                      self.loadActivityIndicator()
                      
                      
                      self.TradeShowTable.isHidden = false
                      self.callUrls = bsmEventList
                      let params = [ ""]
                      self.requestToServer.connectToServer(myUrl: self.callUrls as URL, params: params as AnyObject)
                      
                  }
                  
                  
                  
              }
              else
              {
                  
                  self.TradeShowTable.isHidden = true
              }
        DispatchQueue.main.async {
            self.TradeShowTable.reloadData()
            self.TradeShowTable.contentOffset = .zero

        }
              
        
        
    }
    
    func loadActivityIndicator()
           {
               activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
               activityLoader?.label.text = "Loading";
               activityLoader?.detailsLabel.text = "Please Wait";
               activityLoader?.isUserInteractionEnabled = false;
           }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewMenuArray.count
    }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
               
               let cell1 = tableView.dequeueReusableCell(withIdentifier: "AllTradeShowTableViewCell") as! AllTradeShowTableViewCell
               cell1.selectionStyle = .none
               cell1.backgroundColor = UIColor.white
               cell1.layer.backgroundColor = UIColor.lightGray.cgColor
               
               cell1.layer.masksToBounds = true
               cell1.TradeShowNamelbl.text = ((self.tableViewMenuArray.object(at: indexPath.row) as AnyObject).object(forKey: "event")as! String)
        cell1.TradeShowNamelbl.setCharacterSpacing(1)
        cell1.TradeShowNamelbl.setLineSpacing(lineSpacing: 5, lineHeightMultiple: 0)
        cell1.TradeShowDate.setCharacterSpacing(1)
               
               let eventdate =   JoinString(index1: indexPath.row)
               
               
               cell1.TradeShowDate.text = eventdate
               cell1.layer.masksToBounds = true
               cell1.layer.cornerRadius = 5
               cell1.layer.borderColor = UIColor.lightGray.cgColor
               // cell1.layer.borderWidth = 0.5
               let imgdata = ((self.tableViewMenuArray.object(at: indexPath.row) as AnyObject).object(forKey: "image")as! String)
               let imgfilterUrl = Eventimgeurl + imgdata
               replacedImgstring = imgfilterUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!
               
       //        cell1.TradeShowImage.sd_setImage(with: URL(string: self.replacedImgstring))
               
               DispatchQueue.main.async {
                   //  cell1.EventsImage.contentMode = .scaleAspectFill
       //            cell1.TradeShowImage.clipsToBounds = true
                   cell1.BackgroundView1.backgroundColor = UIColor.white
                   cell1.contentView.backgroundColor = UIColor(red: 240/255.0, green:  240/255.0, blue:  240/255.0, alpha: 1.0)
                   cell1.BackgroundView1.layer.cornerRadius = 3.0
                   cell1.BackgroundView1.layer.masksToBounds = false
                   
                   
                   cell1.BackgroundView1.layer.shadowOffset = CGSize(width: 0, height: 0)
                   cell1.BackgroundView1.layer.shadowOpacity = 0.3
               }
               
               
               return cell1
               
           }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//               return 120.0
//           }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let vc = self.storyboard?.instantiateViewController(withIdentifier:"EventContentsViewController") as! EventContentsViewController
            let viewDetails = ((self.tableViewMenuArray.object(at: indexPath.row) as AnyObject).object(forKey: "viewDetails") as! String)
            if(viewDetails == "Y"){
                let title = ((self.tableViewMenuArray.object(at: indexPath.row) as AnyObject).object(forKey: "event") as! String)
//                vc.Ltitle = title
//                vc.header = self.type
                let desc = ((self.tableViewMenuArray.object(at: indexPath.row) as AnyObject).object(forKey: "eventDescription") as! String)
                vc.Ldesc = desc
                let Limg = ((self.tableViewMenuArray.object(at: indexPath.row) as AnyObject).object(forKey: "image") as! String)
                vc.LImage = Limg
                 self.navigationController?.pushViewController(vc, animated: true)
            }
            else{
    //            let alert = UIAlertController(title: "BSM details would be updated shortly.", message: "", preferredStyle: UIAlertControllerStyle.alert)
    //            self.present(alert, animated: true, completion: nil)
                let alert = UIAlertController(title: "", message: "BSM details would be updated shortly.", preferredStyle: .alert)
                self.present(alert, animated: true, completion: nil)

                // change to desired number of seconds (in this case 5 seconds)
                let when = DispatchTime.now() + 2
                DispatchQueue.main.asyncAfter(deadline: when){
                  // your code with delay
                  alert.dismiss(animated: true, completion: nil)
                }
            }
            
            
           
            
        }
       
       func dataReceivedFromServer(data: NSDictionary, url: URL) {
                   
                   if ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "true" {
                       
                       let temporaryArray = ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "Result") as! NSArray)
       //                let sortedArray = (temporaryArray as NSArray).sortedArray(using: [NSSortDescriptor(key: "event", ascending: true)]) as! [[String:AnyObject]] as NSArray
                       
                       
                       
                       
                       
                       
                       
                       tableViewMenuArray = (temporaryArray.mutableCopy() as! NSMutableArray)
                       
                       
                       print("tableViewMenuArray.....\(tableViewMenuArray)")
                       
                       if (tableViewMenuArray == []){
                           do {
                               DispatchQueue.main.async {
                                   self.activityLoader?.hide(animated: true)
                                   self.TradeShowTable.isHidden = true
//                                   self.RetryBtn.isHidden = true
//                                   self.noInternetLabel.isHidden = true

                               }
                                
                            let alert = UIAlertController(title: "", message: "No TradeShows Available", preferredStyle: UIAlertController.Style.alert)
                               self.present(alert, animated: true, completion: nil)
                               let when = DispatchTime.now() + 3
                               DispatchQueue.main.asyncAfter(deadline: when){
                                   alert.dismiss(animated: true, completion: nil)
                                   
                               }
                           }
                       }
                       else{
                           DispatchQueue.main.async {
                               self.activityLoader?.hide(animated: true)
                               self.TradeShowTable.reloadData()
                               self.TradeShowTable.contentOffset = .zero
                           }
                       }
                       
                       
                       
                   }
               }

    func JoinString( index1:Int) -> String {
               
               
               let fromdate1 = ((self.tableViewMenuArray.object(at: index1) as AnyObject).object(forKey: "fromDate")as! String)
               let inputFormatter = DateFormatter()
               inputFormatter.dateFormat = "yyyy-MM-dd"
               let fromDate = inputFormatter.date(from: fromdate1)
               inputFormatter.dateFormat = "dd MMM, yyyy"
               let fromdate = inputFormatter.string(from: fromDate!)
               
               let todate = ((self.tableViewMenuArray.object(at: index1) as AnyObject).object(forKey: "toDate")as! String)
               let inputFormatter1 = DateFormatter()
               inputFormatter1.dateFormat = "yyyy-MM-dd"
               
               let ToDate = inputFormatter1.date(from: todate)
               inputFormatter1.dateFormat = "dd MMM, yyyy"
               let todate1 = inputFormatter1.string(from: ToDate!)
               Date1 = "\(String(describing: fromdate)) \("TO") \(todate1)"
        return Date1.uppercased()
               
           }
    
    @IBAction func back(_ sender: Any) {
//    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
//    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "EventsSignatureViewController") as! EventsSignatureViewController
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//
//    appDelegate.drawerContainer?.setCenterView(redViewController, withCloseAnimation: true, completion: nil)
//        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)

    }
    override func viewWillAppear(_ animated: Bool) {
//        self.tabBarController?.tabBar.isHidden = true
    }
}
