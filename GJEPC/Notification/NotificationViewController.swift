//
//  NotificationViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 24/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD

class notificationListTableViewCell : UITableViewCell {
    
    @IBOutlet weak var msgDate: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var msgDescription: UILabel!
    
    @IBOutlet weak var BackgroudView1: UIView!
    
}


class NotificationViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var responsedata : NSDictionary = [:]
    var notificationDescription = [String]()
    var notificationTitle = [String]()
    
    var dataArray = [AnyObject] ()
    var globalArray = [AnyObject]()
    var dict : NSMutableDictionary = [:]
    var isChange : Bool = false
    var notificationArray : NSMutableArray = []
    var inboxMessages : NSArray = []

    @IBOutlet weak var notificationTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        
//        print(globalArray)
      
        
        
        self.notificationTableView.delegate = self
        self.notificationTableView.dataSource = self
        
        let name1 = Notification.Name("updateTable")
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateTableData(_:)), name: name1, object: nil)
        
    }
    @objc func updateTableData(_ notification:Notification) {
        
        self.globalArray = []
        self.dataArray = []
        let data:[AnyObject]? = UserDefaults.standard.value(forKey: "NotificationList") as? [AnyObject]
        self.dataArray = (UserDefaults.standard.value(forKey: "NotificationList") as? [AnyObject])!
        
        
        self.globalArray = data!.reversed()
        

        
                
                DispatchQueue.main.async {
                    
                    self.notificationTableView.reloadData()
                }
                
     
        
        

        
        DispatchQueue.main.async {
            
            let token:String? = "\(UserDefaults.standard.value(forKey:"iOSToken") ?? "0")"
            
            let callUrls = URL(string: "https://gjepc.org/gjepc_mobile/notification/badgeNull")
            
            
            print(token!)
            
            //            UIApplication.shared.applicationIconBadgeNumber
            
            let params = ["deviceId":token!]
            print("Dictionary \(params)")
            
            
            DispatchQueue.main.async {
                self.connectToServerUpdateCount(myUrl: callUrls!, params: params as AnyObject)
            }
        }
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
            
            
            
          
            
            let cnt:Int? = UIApplication.shared.applicationIconBadgeNumber
            
            if(cnt == 0 || cnt == nil)
            {
              
            }
            else
            {
                
                
                DispatchQueue.main.async {
                    
                    MBProgressHUD.showAdded(to: self.view, animated: true)
                    
                }
                
                
                let token:String? = "\(UserDefaults.standard.value(forKey:"iOSToken") ?? "0")"
                
                let callUrls = URL(string: "https://gjepc.org/gjepc_mobile/notification/badgeNull")
                
                
                print(token!)
                
    //            UIApplication.shared.applicationIconBadgeNumber
                
                let params = ["deviceId":token!]
                
                        print("Dictionary \(params)")
                        DispatchQueue.main.async {
                            self.connectToServerUpdateCount(myUrl: callUrls!, params: params as AnyObject)
                        }
                
                
                
            }
            

            
            
          //  UIApplication.shared.applicationIconBadgeNumber = 0;
           // UserDefaults.standard.removeObject(forKey: "userSessionBadgeCnt")
            
            
            print(self.globalArray)
            
            
            if(self.globalArray.count>0)
            {
                
                

                
            }
            else
            {
                DispatchQueue.main.async {
                    
                    
                    self.getMessages()
                }
                
                
            }
            
            
            
            
            
        }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return globalArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        let cell = self.notificationTableView.dequeueReusableCell(withIdentifier: "cell") as! notificationListTableViewCell
        
        
        
        var dict:[String:Any] = self.globalArray[indexPath.row] as! [String:Any]
        
        
        cell.contentLabel.text = ((dict["alert"] as! [String:Any])["title"] as? String)!
        
        cell.msgDate.text = dict["Date"] as! String
        

        
        cell.msgDescription.text = ((dict["alert"] as! [String:Any])["body"] as? String)!
//        cell.msgDate.text = dict["date
        
       DispatchQueue.main.async {
        
//        let fakedate = dict["Date"] as? String
////        fakedate?.toDate(withFormat: "dd MMM, yyyy")
//        print(fakedate?.toDate(withFormat: "dd MMM, yyyy"))
        
//            let dateFormatterGet = DateFormatter()
//                                dateFormatterGet.dateFormat = "dd.MM.yyy"
//
//                                let dateFormatterPrint = DateFormatter()
//                                dateFormatterPrint.dateFormat = "dd MMM, yyyy"
//
//                                let date: NSDate? = dateFormatterGet.date(from: "\(fakedate)") as NSDate?
//       // print(date)
//                   cell.msgDate.text = "\(dateFormatterPrint.string(from: date! as Date))"

                    cell.BackgroudView1.backgroundColor = UIColor.white
                    cell.contentView.backgroundColor = UIColor(red: 240/255.0, green:  240/255.0, blue:  240/255.0, alpha: 1.0)
                    cell.BackgroudView1.layer.cornerRadius = 3.0
                    cell.BackgroudView1.layer.masksToBounds = false
                    
                    
                    cell.BackgroudView1.layer.shadowOffset = CGSize(width: 0, height: 0)
                    cell.BackgroudView1.layer.shadowOpacity = 0.3
                }
        
        return cell
        
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        var dict:[String:Any] = self.globalArray[indexPath.row] as! [String:Any]
        
        let newsContentvc = self.storyboard?.instantiateViewController(withIdentifier: "ShowRowVC") as! ShowRowVC
        
        
        

        
        newsContentvc.DateString = "\(dict["Date"] as! String)"
        
        newsContentvc.Titlelebel = ((dict["alert"] as! [String:Any])["title"] as? String)!
        
        newsContentvc.newstitle = ((dict["alert"] as! [String:Any])["body"] as? String)!
        
        
        DispatchQueue.main.async {
            self.present(newsContentvc, animated: true, completion: nil)
            
        }
        
        
        
    }
    
    func getMessages() {
        //
        
        let alert = UIAlertController(title: "", message: "No notifictaion available.", preferredStyle: UIAlertController.Style.alert)
        self.present(alert, animated: true, completion: nil)
        let when = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: when){
            alert.dismiss(animated: true, completion: nil)
        }
        
    }
    
    func connectToServerUpdateCount(myUrl : URL, params : AnyObject) {
        
        
        print(params)
        
        let request = NSMutableURLRequest(url:myUrl as URL)
        request.httpMethod = "POST";
        request.httpBody = try! JSONSerialization.data(withJSONObject: params, options: [])
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        //create the session object
        let session = URLSession.shared
        //create dataTask using the session object to send data to the server
        
        
        DispatchQueue.main.async {
            
            
            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                guard error == nil else {
                    return
                }
                guard let data = data else {
                    return
                }
                do {
                    //create json object from data
                    
                    
                    let json:[String:[String:String]] = try (JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:[String:String]])!
                    
                    let response = (json["Response"]!["status"])
                    
                    print("\(response ?? "0")")
                    
                    if(response == "success")
                    {
                        
                        DispatchQueue.main.async {
                            
                            MBProgressHUD.hide(for: self.view, animated: true)
                            
                        }
                        
                        
                        DispatchQueue.main.async {
                            
                            UIApplication.shared.applicationIconBadgeNumber = 0
                        }
                        
                    }
                    else
                    {
                        
                        DispatchQueue.main.async {
                            
                            MBProgressHUD.hide(for: self.view, animated: true)
                            
                        }
                        
                    }
                    
                   
                } catch let error {
                    
                    DispatchQueue.main.async {
                        
                        MBProgressHUD.hide(for: self.view, animated: true)
                        
                    }
                    print(error.localizedDescription)
                }
            })
            task.resume()
        }
        
       
    }
   
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
//extension String {
//  func toDate(withFormat format: String = "dd MMM, yyyy") -> Date {
//    let dateFormatter = DateFormatter()
//    dateFormatter.dateFormat = format
//    guard let date = dateFormatter.date(from: self) else {
//      preconditionFailure("Take a look to your format")
//    }
//    return date
//  }
//}
