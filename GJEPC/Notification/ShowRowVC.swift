//
//  ShowRowVC.swift
//  GJEPC
//
//  Created by Kwebmaker on 24/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit

class ShowRowVC: UIViewController {

       var newstitle : String = ""
       var Titlelebel: String = ""
       var DateString: String = ""
    @IBOutlet weak var TextViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var TitleLbl: UILabel!
    
    @IBOutlet weak var notificationdDetails: UITextView!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
         notificationdDetails.isEditable = false
        self.TitleLbl.text = Titlelebel
        self.notificationdDetails.text = newstitle
        
        let sizeThatFitsTextView = self.notificationdDetails.sizeThatFits(CGSize(width: self.notificationdDetails.frame.size.width, height: CGFloat(MAXFLOAT)))
        
        
        self.TextViewHeightConstraint.constant = sizeThatFitsTextView.height;
        
    
        self.dateLabel.text = DateString
        
    }
    

    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

}
extension ShowRowVC:UITextViewDelegate
{
    
    
    func textViewDidChange(_ textView: UITextView){
        let fixedWidth = textView.frame.size.width
        textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        var newFrame = textView.frame
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        textView.frame = newFrame;
    }
    
    
}
