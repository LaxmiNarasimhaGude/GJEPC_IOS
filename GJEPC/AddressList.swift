//
//  AddressList.swift
//  GJEPC
//
//  Created by Kwebmaker on 21/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import Foundation

var headOfficeAddress = "Office No. AW 1010, Tower A,G Block, Bharat Diamond Bourse,Next to ICICI Bank, Bandra-Kurla Complex, Bandra - East,Mumbai - 400 051, India"
var exhibitionCellAddress = "G2-A, Trade Center,Bandra Kurla Complex,Bandra (E), Mumbai - 400 051,India"
var chennaiAddress = "Ankur Plaza III Floor, 113,G.N. Chetty Road, T. Nagar Chennai - 600017,India"
var jaipurAddress = "3rd Floor, \n" + "Mirza Ismail Road, Jaipur - 302 003"
var kolkataAddress = "Wood Street, Kolkata: 700 016"
var delhiAddress = "F 17-18, Flatted Factories Complex,Jhandewalan, New Delhi - 110 055"
var suratAddress = "401-A, International Commerce Centre,Near Kadiwala School, Ring Road,Surat - 395002"
