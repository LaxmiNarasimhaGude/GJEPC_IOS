//
//  MemberRegistrationViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 17/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit

class MemberRegistrationViewController: UIViewController {

    @IBOutlet weak var memberRegistration: NSLayoutConstraint!
    @IBOutlet weak var membershipFeesimage: UIImageView!
    @IBOutlet weak var membershipProcessAndDocImage: UIImageView!
    
    @IBOutlet weak var memberRenewalImage: UIImageView!
    @IBOutlet weak var memberRegistrationLabel: UILabel!
    @IBOutlet weak var feesLabel: UILabel!
    @IBOutlet weak var membershipRenewLabel: UILabel!
    @IBOutlet weak var membershipAndDocLabel: UILabel!
    @IBOutlet weak var feesView: UIView!
    @IBOutlet weak var membershipRenewalView: UIView!
    @IBOutlet weak var membershipProcessAndDocView: UIView!
    @IBOutlet weak var separatorWidth: NSLayoutConstraint!
    @IBOutlet weak var separatorLabel: UILabel!
    @IBOutlet weak var hindiLabel: UILabel!
    @IBOutlet weak var englishLabel: UILabel!
    @IBOutlet weak var memberRegistrationView: UIView!
    @IBOutlet weak var englishSupporterLabel: UILabel!
    @IBOutlet weak var hindiSupporterLabel: UILabel!
    
     var buttonCat : String = "english"
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        DispatchQueue.main.async {

            
            
//            self.membershipProcessAndDocImage.layer.cornerRadius = self.membershipProcessAndDocImage.frame.size.width/2
//             self.membershipProcessAndDocImage.layer.backgroundColor = UIColor.white.cgColor
//             self.membershipProcessAndDocImage.layer.shadowColor = UIColor.lightGray.cgColor
//             self.membershipProcessAndDocImage.layer.shadowOffset = CGSize(width: 0, height: 2.0)//CGSizeMake(0, 2.0);
//             self.membershipProcessAndDocImage.layer.shadowRadius = 2.0
//             self.membershipProcessAndDocImage.layer.shadowOpacity = 1.0
//             self.membershipProcessAndDocImage.layer.masksToBounds = false
//            self.membershipProcessAndDocImage.layer.shadowPath = UIBezierPath(roundedRect:self.membershipProcessAndDocImage.bounds, cornerRadius:self.membershipProcessAndDocImage.layer.cornerRadius).cgPath

            
            
            
            
//            self.memberRenewalImage.layer.cornerRadius = self.memberRenewalImage.frame.size.width/2
//             self.memberRenewalImage.layer.backgroundColor = UIColor.white.cgColor
//             self.memberRenewalImage.layer.shadowColor = UIColor.lightGray.cgColor
//             self.memberRenewalImage.layer.shadowOffset = CGSize(width: 0, height: 2.0)//CGSizeMake(0, 2.0);
//             self.memberRenewalImage.layer.shadowRadius = 2.0
//             self.memberRenewalImage.layer.shadowOpacity = 1.0
//             self.memberRenewalImage.layer.masksToBounds = false
//            self.memberRenewalImage.layer.shadowPath = UIBezierPath(roundedRect:self.membershipProcessAndDocImage.bounds, cornerRadius:self.memberRenewalImage.layer.cornerRadius).cgPath
            
            
            
//            self.membershipFeesimage.layer.cornerRadius = self.membershipFeesimage.frame.size.width/2
//             self.membershipFeesimage.layer.backgroundColor = UIColor.white.cgColor
//             self.membershipFeesimage.layer.shadowColor = UIColor.lightGray.cgColor
//             self.membershipFeesimage.layer.shadowOffset = CGSize(width: 0, height: 2.0)//CGSizeMake(0, 2.0);
//             self.membershipFeesimage.layer.shadowRadius = 2.0
//             self.membershipFeesimage.layer.shadowOpacity = 1.0
//             self.membershipFeesimage.layer.masksToBounds = false
//            self.membershipFeesimage.layer.shadowPath = UIBezierPath(roundedRect:self.membershipProcessAndDocImage.bounds, cornerRadius:self.membershipFeesimage.layer.cornerRadius).cgPath
           
            
            
            

            
        }

        englishLabel.backgroundColor = UIColor.black
        englishLabel.textColor = UIColor.white
        hindiLabel.textColor = UIColor.black
        
        
//               englishLabel.backgroundColor = UIColor(red: 237/256, green: 237/256, blue: 237/256, alpha: 1.0)
//                      hindiLabel.backgroundColor = UIColor.white
//               separatorLabel.backgroundColor = UIColor(red: 237/256, green: 237/256, blue: 237/256, alpha: 1.0)
//               englishSupporterLabel.backgroundColor = UIColor(red: 237/256, green: 237/256, blue: 237/256, alpha: 1.0)
//               hindiSupporterLabel.backgroundColor = UIColor.white
        
       
        
        

        
//        hindiLabel.layer.cornerRadius = 15
//        hindiLabel.layer.masksToBounds = true
//        hindiLabel.layer.borderColor = UIColor.gray.cgColor
//        hindiLabel.layer.borderWidth = 1.0
//        englishLabel.layer.cornerRadius = 15
//        englishLabel.layer.masksToBounds = true
//        englishLabel.layer.borderColor = UIColor.gray.cgColor
//        englishLabel.layer.borderWidth = 1.0
//
//        separatorLabel.layer.masksToBounds = true
//        separatorLabel.layer.borderColor = UIColor.gray.cgColor
//        separatorLabel.layer.borderWidth = 1.0
        
        memberRegistrationView.layer.cornerRadius = 5
        memberRegistrationView.layer.masksToBounds = true
        memberRegistrationView.layer.borderColor = UIColor.darkGray.cgColor
        memberRegistrationView.layer.borderWidth = 1.0
        
    }
    
    @IBAction func hindi(_ sender: Any) {
hindiLabel.backgroundColor = UIColor.black
hindiLabel.textColor = UIColor.white
englishLabel.textColor = UIColor.black
        englishLabel.backgroundColor = UIColor.white
        memberRegistration.constant = 150
        
        
        buttonCat = "हिन्दी"
        membershipAndDocLabel.text = "ऑनलाइन सदस्यता"
        membershipRenewLabel.text = "ऑनलाइन सदस्यता नवीनीकरण"
        feesLabel.text = "शुल्क विस्तार"
        memberRegistrationLabel.text = "सदस्य पंजीकरण"
        
    }
    
    @IBAction func english(_ sender: Any) {
englishLabel.backgroundColor = UIColor.black
englishLabel.textColor = UIColor.white
hindiLabel.textColor = UIColor.black
        hindiLabel.backgroundColor = UIColor.white
         memberRegistration.constant = 240
        
        
        buttonCat = "english"
        
        membershipAndDocLabel.text = "MEMBERSHIP PROCESS AND DOC"
        membershipRenewLabel.text = "MEMBERSHIP RENEWAL"
        feesLabel.text = "FEES"
        memberRegistrationLabel.text = "MEMBER REGISTRATION"
    }
    
    @IBAction func membershipProcessAndDocs(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MembersStaticContent") as! MembersStaticContent
        vc.buttonSelected = "process"
        vc.languageSelected = buttonCat
        if buttonCat == "english"
        {
            vc.viewTitle = "Membership Process"
        } else
        {
            vc.viewTitle = "ऑनलाइन सदस्यता"
        }
        DispatchQueue.main.async {
             self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func membershipRenewal(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MembersStaticContent") as! MembersStaticContent
        vc.languageSelected = buttonCat
        vc.buttonSelected = "renewal"
        if buttonCat == "english"
        {
            vc.viewTitle = "Membership Renewal"
        } else
        {
            vc.viewTitle = "ऑनलाइन सदस्यता नवीनीकरण"
        }
       // print("selected \(vc.buttonSelected)")
        DispatchQueue.main.async {
             self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func fees(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MembersStaticContent") as! MembersStaticContent
        vc.languageSelected = buttonCat
        vc.buttonSelected = "fees"
        if buttonCat == "english"
        {
            vc.viewTitle = "Fees"
        } else
        {
            vc.viewTitle = "शुल्क विस्तार"
            
        }
    //    print("selected \(vc.buttonSelected)")
        DispatchQueue.main.async {
              self.navigationController?.pushViewController(vc, animated: true)
        }
      
    }
    
    
    @IBAction func membershipRegistration(_ sender: Any) {
        guard let url = URL(string: "https://gjepc.org/login.php") else { return }
               UIApplication.shared.open(url)
    }
    
}
