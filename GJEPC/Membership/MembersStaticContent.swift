//
//  MembersStaticContent.swift
//  GJEPC
//
//  Created by Kwebmaker on 18/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit

extension String {
    var htmlAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data,
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription)
            return nil
        }
        
    }
    var htmlString: String {
        return html2AttributedString?.string ?? ""
    }
}


class MembersStaticContent: UIViewController {

    @IBOutlet weak var pageTitle: UILabel!
    @IBOutlet weak var hindiFeesTextView: UITextView!
    @IBOutlet weak var hindiRenewalTextView: UITextView!
    @IBOutlet weak var hindiProcessTextView: UITextView!
    @IBOutlet weak var engFeesTextView: UITextView!
    @IBOutlet weak var renewalTextView: UITextView!
    @IBOutlet weak var engProcessTextView: UITextView!
    
    
    var buttonSelected : String = ""
    var languageSelected = ""
    var viewTitle  = ""
//    var newsContent = NewsContent()

    override func viewDidLoad() {
          super.viewDidLoad()
          print(languageSelected)
          
          engFeesTextView.isHidden = true
          engProcessTextView.isHidden = true
          renewalTextView.isHidden = true
          hindiFeesTextView.isHidden = true
          hindiProcessTextView.isHidden = true
          hindiRenewalTextView.isHidden = true
         engFeesTextView.setContentOffset(CGPoint.zero, animated: false)
          engProcessTextView.setContentOffset(CGPoint.zero, animated: false)
          renewalTextView.setContentOffset(CGPoint.zero, animated: false)
          hindiFeesTextView.setContentOffset(CGPoint.zero, animated: false)
      hindiProcessTextView.setContentOffset(CGPoint.zero, animated: false)
      hindiRenewalTextView.setContentOffset(CGPoint.zero, animated: false)

          self.engFeesTextView.scrollRangeToVisible(NSMakeRange(0, 0))
          self.engProcessTextView.scrollRangeToVisible(NSMakeRange(0, 0))
          self.renewalTextView.scrollRangeToVisible(NSMakeRange(0, 0))
          self.hindiFeesTextView.scrollRangeToVisible(NSMakeRange(0, 0))
          self.hindiProcessTextView.scrollRangeToVisible(NSMakeRange(0, 0))
          self.hindiRenewalTextView.scrollRangeToVisible(NSMakeRange(0, 0))

          if languageSelected == "english" && buttonSelected == "renewal"
          {
              renewalTextView.isHidden = false
          }
          if languageSelected == "english" && buttonSelected == "process"
          {
              engProcessTextView.isHidden = false
          }
          if languageSelected == "english" && buttonSelected == "fees"
          {
              let htmlString = "<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"5\">\n" +
                  "  <tr>\n" +
                  "    <td align=\"center\">Export Performance of Gem & Jewellery during he year 2015-16</td>\n" +
                  "    <td align=\"center\">Membership fee for 2016-17</td>\n" +
                  "    <td align=\"center\">Service Tax g 14%</td>\n" +
                  "    <td align=\"center\">Swachh Bharat Cess g0.5%</td>\n" +
                  "    <td align=\"center\">Krishi Kalyan ' cess @0.5%</td>\n" +
                  "    <td align=\"center\">Total</td>\n" +
                  "  </tr>\n" +
                  "  <tr>\n" +
                  "    <td></td>\n" +
                  "    <td>Rs</td>\n" +
                  "    <td>Rs.</td>\n" +
                  "    <td>Rs.</td>\n" +
                  "    <td>Rs.</td>\n" +
                  "    <td>Rs</td>\n" +
                  "  </tr> \n" +
                  "  <tr>\n" +
                  "    <td>Export UptoRs. 25 Lakhs</td>\n" +
                  "    <td>6000</td>\n" +
                  "    <td>840</td>\n" +
                  "    <td>30</td>\n" +
                  "    <td>30</td>\n" +
                  "    <td>6900</td>\n" +
                  "  </tr> \n" +
                  "  <tr>\n" +
                  "    <td>Export above Rs. 25 lakhs uptoRs. 1 Crore</td>\n" +
                  "    <td>10000</td>\n" +
                  "    <td>1400</td>\n" +
                  "    <td>50</td>\n" +
                  "    <td>50</td>\n" +
                  "    <td>11500</td>\n" +
                  "  </tr> \n" +
                  "  <tr>\n" +
                  "    <td>Export above Rs.1 Crore uptoRs. 5 Crore</td>\n" +
                  "    <td>15000</td>\n" +
                  "    <td>2100</td>\n" +
                  "    <td>75</td>\n" +
                  "    <td>75</td>\n" +
                  "    <td>17250</td>\n" +
                  "  </tr> \n" +
                  "  <tr>\n" +
                  "    <td>Export above Rs. 5 Crore uptoRs. 15 Crore</td>\n" +
                  "    <td>30000</td>\n" +
                  "    <td>4200</td>\n" +
                  "    <td>150</td>\n" +
                  "    <td>150</td>\n" +
                  "    <td>34500</td>\n" +
                  "  </tr> \n" +
                  "  <tr>\n" +
                  "    <td>Export above Rs.15 Crore uptoRs. 50 Crore</td>\n" +
                  "    <td>40000</td>\n" +
                  "    <td>5600</td>\n" +
                  "    <td>200</td>\n" +
                  "    <td>200</td>\n" +
                  "    <td>46000</td>\n" +
                  "  </tr> \n" +
                  "  <tr>\n" +
                  "    <td>Export above Rs.50 Crore uptoRs. 100 Crore</td>\n" +
                  "    <td>65000</td>\n" +
                  "    <td>9100</td>\n" +
                  "    <td>325</td>\n" +
                  "    <td>325</td>\n" +
                  "    <td>74750</td>\n" +
                  "  </tr> \n" +
                  "  <tr>\n" +
                  "    <td>Export above Rs.100 Crore uptoRs. 500 Crore</td>\n" +
                  "    <td>100000</td>\n" +
                  "    <td>14000</td>\n" +
                  "    <td>500</td>\n" +
                  "    <td>500</td>\n" +
                  "    <td>115000</td>\n" +
                  "  </tr> \n" +
                  "  <tr>\n" +
                  "    <td>Export above Rs.500 Crore upto 1000 Crore</td>\n" +
                  "    <td>150000</td>\n" +
                  "    <td>21000</td>\n" +
                  "    <td>750</td>\n" +
                  "    <td>750</td>\n" +
                  "    <td>172500</td>\n" +
                  "  </tr> \n" +
                  "  <tr>\n" +
                  "    <td>Export above Rs. 1000 Crore</td>\n" +
                  "    <td>200000</td>\n" +
                  "    <td>28000</td>\n" +
                  "    <td>1000</td>\n" +
                  "    <td>1000</td>\n" +
                  "    <td>230000</td>\n" +
                  "  </tr> \n" +
                  "  <tr>\n" +
                  "    <td colspan=\"6\">Admission Fees including ST, SB Cess and KK Cess: 5000+700+25+25 = Rs. 5750/-</td>\n" +
                  "  </tr>\n" +
              "</table>"
              
    
              
              
              
              do{
                  let htmlData = NSString(string: htmlString).data(using: String.Encoding.unicode.rawValue)
                  
                  let attributedString = try NSAttributedString(data: htmlData!,
                                                                options: [.documentType: NSAttributedString.DocumentType.html,
                                                                          .characterEncoding: String.Encoding.utf8.rawValue],
                                                                documentAttributes: nil)
                  engFeesTextView.attributedText = attributedString
                  engFeesTextView.isHidden = false
                  
                  
              }
                  
              catch let error as NSError {
                  print(error.localizedDescription)
                  
              }
              
              
              
              
          }
          if languageSelected == "हिन्दी" && buttonSelected == "process"
          {
              hindiProcessTextView.isHidden = false
          }
          if languageSelected == "हिन्दी" && buttonSelected == "renewal"
          {
              hindiRenewalTextView.isHidden = false
          }
          if languageSelected == "हिन्दी" && buttonSelected == "fees"
          {
              let htmlString1 = "<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"5\">\n" +
                  
                  "  <tr>\n" +
                  
                  "    <td align=\"center\">रत्न तथा आभूषण के निर्यात का वर्ष 2015-16 के दौरान का व्यापार व्यवहार</td>\n" +
                  
                  "    <td align=\"center\">सदस्यता शुल्क 2016-17 का</td>\n" +
                  
                  "    <td align=\"center\">सेवा कर @ 14% </td>\n" +
                  
                  "    <td align=\"center\">स्वच्छ आस्त सेस @ 5% </td>\n" +
                  
                  "    <td align=\"center\">कृषि कल्याण सेस@0.5%</td>\n" +
                  
                  "    <td align=\"center\">कुल</td>\n" +
                  
                  "  </tr>\n" +
                  
                  "  <tr>\n" +
                  
                  "    <td></td>\n" +
                  
                  "    <td>रु</td>\n" +
                  
                  "    <td>रु.</td>\n" +
                  
                  "    <td>रु.</td>\n" +
                  
                  "    <td>रु.</td>\n" +
                  
                  "    <td>रु</td>\n" +
                  
                  "  </tr> \n" +
                  
                  "  <tr>\n" +
                  
                  "    <td>निर्यात रु 25 लाख तक</td>\n" +
                  
                  "    <td>6000</td>\n" +
                  
                  "    <td>840</td>\n" +
                  
                  "    <td>30</td>\n" +
                  
                  "    <td>30</td>\n" +
                  
                  "    <td>6900</td>\n" +
                  
                  "  </tr> \n" +
                  
                  "  <tr>\n" +
                  
                  "    <td>निर्यात रु 25 लाख से 1करोड़ तक </td>\n" +
                  
                  "    <td>10000</td>\n" +
                  
                  "    <td>1400</td>\n" +
                  
                  "    <td>50</td>\n" +
                  
                  "    <td>50</td>\n" +
                  
                  "    <td>11500</td>\n" +
                  
                  "  </tr> \n" +
                  
                  "  <tr>\n" +
                  
                  "    <td>निर्यात रु 1करोड़ से 5 करोड़ तक</td>\n" +
                  
                  "    <td>200000</td>\n" +
                  
                  "    <td>28000</td>\n" +
                  
                  "    <td>1000</td>\n" +
                  
                  "    <td>1000</td>\n" +
                  
                  "    <td>230000</td>\n" +
                  
                  "  </tr> \n" +
                  
                  "  <tr>\n" +
                  
                  "    <td>निर्यात रु 5 करोड़ से 15 करोड़ तक </td>\n" +
                  
                  "    <td>15000</td>\n" +
                  
                  "    <td>2100</td>\n" +
                  
                  "    <td>75</td>\n" +
                  
                  "    <td>75</td>\n" +
                  
                  "    <td>17250</td>\n" +
                  
                  "  </tr> \n" +
                  
                  "  <tr>\n" +
                  
                  "    <td>निर्यात रु 15 करोड़ से 50 करोड़ तक </td>\n" +
                  
                  "    <td>30000</td>\n" +
                  
                  "    <td>4200</td>\n" +
                  
                  "    <td>150</td>\n" +
                  
                  "    <td>150</td>\n" +
                  
                  "    <td>34500</td>\n" +
                  
                  "  </tr> \n" +
                  
                  "  <tr>\n" +
                  
                  "    <td>निर्यात रु 50 करोड़ से 100 करोड़ तक </td>\n" +
                  
                  "    <td>40000</td>\n" +
                  
                  "    <td>5600</td>\n" +
                  
                  "    <td>200</td>\n" +
                  
                  "    <td>200</td>\n" +
                  
                  "    <td>46000</td>\n" +
                  
                  "  </tr> \n" +
                  
                  "  <tr>\n" +
                  
                  "    <td>निर्यात रु 100 करोड़ से 500 करोड़ तक</td>\n" +
                  
                  "    <td>65000</td>\n" +
                  
                  "    <td>9100</td>\n" +
                  
                  "    <td>325</td>\n" +
                  
                  "    <td>325</td>\n" +
                  
                  "    <td>74750</td>\n" +
                  
                  "  </tr> \n" +
                  
                  "  <tr>\n" +
                  
                  "    <td>निर्यात रु 500 करोड़ से 1000 करोड़ तक</td>\n" +
                  
                  "    <td>100000</td>\n" +
                  
                  "    <td>14000</td>\n" +
                  
                  "    <td>500</td>\n" +
                  
                  "    <td>500</td>\n" +
                  
                  "    <td>115000</td>\n" +
                  
                  "  </tr> \n" +
                  
                  "  <tr>\n" +
                  
                  "    <td>निर्यात रु 1करोड़ से 5 करोड़ तक</td>\n" +
                  
                  "    <td>150000</td>\n" +
                  
                  "    <td>21000</td>\n" +
                  
                  "    <td>750</td>\n" +
                  
                  "    <td>750</td>\n" +
                  
                  "    <td>172500</td>\n" +
                  
                  "  </tr> \n" +
                  
                  "  \n" +
                  
                  "  <tr>\n" +
                  
                  "    <td colspan=\"6\">प्रवेश शुल्क  ST, SB सेस और  kk सेस : 5000+700+25+25 = रु. 5750/-</td>\n" +
                  
                  "  </tr>\n" +
                  
              "</table>"
              
              do{
              let htmlData = NSString(string: htmlString1).data(using: String.Encoding.unicode.rawValue)
              
              let attributedString = try NSAttributedString(data: htmlData!,
                                                                options: [.documentType: NSAttributedString.DocumentType.html,
                                                                          .characterEncoding: String.Encoding.utf8.rawValue],
                                                                documentAttributes: nil)
              
              hindiFeesTextView.attributedText = attributedString
              hindiFeesTextView.isHidden = false
              
              }
              
              catch let error as NSError {
                  print(error.localizedDescription)
                  
              }
              
              
             
          }
          
          pageTitle.text = viewTitle
         
      }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        engFeesTextView.setContentOffset(CGPoint.zero, animated: false)
        engProcessTextView.setContentOffset(CGPoint.zero, animated: false)
        renewalTextView.setContentOffset(CGPoint.zero, animated: false)
        hindiFeesTextView.setContentOffset(CGPoint.zero, animated: false)
        hindiProcessTextView.setContentOffset(CGPoint.zero, animated: false)
        hindiRenewalTextView.setContentOffset(CGPoint.zero, animated: false)
    }

   
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
