//
//  MembershipViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 10/02/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
class MembershipViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UISearchBarDelegate,UIPickerViewDelegate,UIPickerViewDataSource,dataReceivedFromServerDelegate {
    
    
   
    

    @IBOutlet weak var notificationCount: UILabel!
    @IBOutlet weak var selectRegionTextField: UITextField!
    @IBOutlet weak var companyNameAndCityView: UIView!
    @IBOutlet weak var filterLabel: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var membershipRegistrationLabel: UIImageView!
    @IBOutlet weak var memberListLabel: UIImageView!
    @IBOutlet weak var memberlistLabel: UIButton!
    @IBOutlet weak var membershipTableview: UITableView!
    
      var tableViewArrayForMemberDirectory : NSMutableArray = []
      var pickerView = UIPickerView()
      var activityLoader : MBProgressHUD? = nil
      var comingFromfilter : Bool = false
      var CustomArray : NSMutableArray! = []
      var coreDataArray : NSArray = []
      var issearching = false
      var isOpen = false
      var regionArray   = [String]()
      var requestToServer = RequestToServer()
    var flag = false
    override func viewDidLoad() {
        super.viewDidLoad()
        regionArray.append("HO-MUM (M)")
        regionArray.append("RO-CHE")
        regionArray.append("RO-DEL")
        regionArray.append("RO-JAI")
        regionArray.append("RO-KOL")
        regionArray.append("RO-SRT")
//        regionArray.append("HO-MUM (M)")
        
        
//        sortRegion()
        membershipTableview.separatorStyle = .none
        
                        if CheckInternet.isConnectedToNetwork() == true{
                             loadActivityIndicator()
                              getdata()
                         }
                        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        
        requestToServer.delegate = self
        
               pickerView = UIPickerView()
               pickerView.delegate = self
               pickerView.dataSource = self
        selectRegionTextField.backgroundColor = UIColor.white
        selectRegionTextField.layer.shadowColor = UIColor.lightGray.cgColor
        selectRegionTextField.layer.shadowOpacity = 1
        selectRegionTextField.layer.shadowOffset = CGSize.zero
        selectRegionTextField.layer.shadowRadius = 2
        selectRegionTextField.layer.cornerRadius = 5
        selectRegionTextField.layer.borderWidth = 1
        selectRegionTextField.layer.borderColor = UIColor.white.cgColor
        
        selectRegionTextField.isHidden = true
        view.isUserInteractionEnabled = false
        searchBar.layer.cornerRadius = 8
        searchBar.clipsToBounds = true
        searchBar.layer.borderWidth = 1
        searchBar.layer.borderColor = UIColor.gray.cgColor
        
        filterLabel.layer.cornerRadius = 8
        filterLabel.clipsToBounds = true
        
        membershipTableview.keyboardDismissMode = .onDrag
        memberListLabel.isHidden = false
        membershipRegistrationLabel.isHidden = true
        
        
        membershipTableview.delegate = self
        membershipTableview.dataSource = self
        self.searchBar.returnKeyType = UIReturnKeyType.search
        searchBar.delegate = self

        manageToolBar()
        
        let name = Notification.Name("didReceiveData")
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: name, object: nil)
                               
        let updateBadgeCount = Notification.Name("updateBadgeCount")
        NotificationCenter.default.addObserver(self, selector: #selector(updateBadgeCount(_:)), name: updateBadgeCount, object: nil)
        
        
        DispatchQueue.main.async {
                            
                            self.notificationCount.layer.cornerRadius = 20/2
                            self.notificationCount.layer.masksToBounds = true
                            
                        }
        
        
        
       

    }
    @objc func updateBadgeCount(_ notification:Notification){
        
        
        print("\(UserDefaults.standard.value(forKey: "userSessionBadgeCnt") ?? "0")")
        
        DispatchQueue.main.async {
            
            self.notificationCount.isHidden = false
            
            
            let cnt:Int? = UIApplication.shared.applicationIconBadgeNumber
        
        
            if(cnt == 0 || cnt == nil)
            {
                   DispatchQueue.main.async {
                self.notificationCount.text = "0"
                }
            }
            else
            {   DispatchQueue.main.async {
                self.notificationCount.text = "\(String(cnt!))"
                }
            }
        }
       
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        tabBarController?.tabBar.isHidden = false
//        self.navigationController?.setNavigationBarHidden(true, animated: true)
     
    
                
                
                
                let data:[AnyObject]? = UserDefaults.standard.value(forKey: "NotificationList") as? [AnyObject]
                
                
                
                print("\(UserDefaults.standard.value(forKey: "userSessionBadgeCnt") ?? "0")")
                
        //        DispatchQueue.main.async {
                
                 DispatchQueue.main.async {
                
                    self.notificationCount.isHidden = false
                    
                    let cnt:Int? = UIApplication.shared.applicationIconBadgeNumber
                    if(cnt == 0 || cnt == nil)
                    {
                        DispatchQueue.main.async {
                            self.notificationCount.text = "0"
                        }
                    }
                    else
                    {   DispatchQueue.main.async {
                        self.notificationCount.text = "\(String(cnt!))"
                        }
                    }
                    
                }
                    
                    // UserDefaults.standard.value(forKey: "userSessionBadgeCnt") as? String
                    
                //}
                
                
           
        
    }

    
    @objc func onDidReceiveData(_ notification:Notification) {
        
        // Do something now
        
        if  UserDefaults.standard.value(forKey: "openNotification") != nil
        {
            if(UserDefaults.standard.value(forKey: "openNotification") as! String == "true")
            {
                ///NotiifcationViewController
                
                
                
                
                let viewControllerArray = self.navigationController?.viewControllers
                
                var checkFlag:Bool
                checkFlag = false
                for controller in viewControllerArray! {
                    
                    
                    print(controller)
                    if controller.classForCoder == NotificationViewController.self {
                        checkFlag = true
                        print(controller)
                    }
                    
                }
                
                
                if(checkFlag)
                {
                    
                    NotificationCenter.default.post(name: Notification.Name("updateTable"), object: nil)
                    
                }
                else
                {
                    let view1 = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController
                    
                    let data:[AnyObject]? = UserDefaults.standard.value(forKey: "NotificationList") as? [AnyObject]
                    
                    view1?.dataArray = (UserDefaults.standard.value(forKey: "NotificationList") as? [AnyObject])!
                    
                    view1?.globalArray = data!.reversed()
                    
                    
                    DispatchQueue.main.async {
                        self.navigationController?.pushViewController(view1!, animated: true)
                    }
                }
                
                
                
                
                
                
                
            }
            else
            {
                // getData()
            }
            
        }
        else
        {
            // getData()
            
        }
        
    }

    
    @available(iOS 13.0, *)
    @IBAction func menu(_ sender: Any) {
        

        
//        if(!isOpen)
//
//        {
            isOpen = true

            let menuVC = self.storyboard!.instantiateViewController(withIdentifier: "SlideViewController") as! SlideViewController
            self.view.addSubview(menuVC.view)
            self.addChild(menuVC)
            menuVC.view.layoutIfNeeded()

            menuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width-90, height: UIScreen.main.bounds.size.height);

            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width-90, height: UIScreen.main.bounds.size.height);
        }, completion:nil)

//        }else if(isOpen)
//        {
//            isOpen = false
//          let viewMenuBack : UIView = view.subviews.last!
//
//            UIView.animate(withDuration: 0.3, animations: { () -> Void in
//                var frameMenu : CGRect = viewMenuBack.frame
//                frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
//                viewMenuBack.frame = frameMenu
//                viewMenuBack.layoutIfNeeded()
//                viewMenuBack.backgroundColor = UIColor.clear
//            }, completion: { (finished) -> Void in
//                viewMenuBack.removeFromSuperview()
//
//            })
//        }

        
        
        
        
        
        
        
        
        
    }


    func getdata(){
        
        URLSession.shared.dataTask(with: memberDirectoryUrl!, completionHandler: { (data, response, error) -> Void in
            
//            print(memberDirectoryUrl)
            if error != nil {
                
                print(error?.localizedDescription as Any)
                return
            }else {
                DispatchQueue.main.async {
                    do {
                        if JSONSerialization.isValidJSONObject(data as Any )
                        {
                            print("Valid JSON Object")
                        }
                        else
                        {
                            do
                            {
//                                self.tableViewArrayForMemberDirectory = []
                                
                                let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as? [String: AnyObject]
//                                print(json)
                                let memberArray = json!["Response"]!["Result"] as! NSArray
                                DispatchQueue.main.async {
                                    self.tableViewArrayForMemberDirectory = memberArray.mutableCopy() as! NSMutableArray
                                    self.coreDataArray = memberArray.mutableCopy() as! NSMutableArray
//                                    self.saveCoreData()
                                    self.membershipTableview.reloadData()
                                    self.activityLoader?.hide(animated:true)
                                    self.view.isUserInteractionEnabled = true
                                }
                            }
                        }
                    }
                    catch
                    {
                        
                    }
                }
            }
        }).resume()
        

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if issearching == true{
            return CustomArray.count
        }else{
             return tableViewArrayForMemberDirectory.count
        }
       
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = membershipTableview.dequeueReusableCell(withIdentifier: "MembershipTableViewCell", for: indexPath) as! MembershipTableViewCell
       
        let index = indexPath.row
        
        
        if(index%2 == 0){
            DispatchQueue.main.async {
                cell.backgroundColor = UIColor(red: 246/256, green: 246/256, blue: 246/256, alpha: 1.0)
            }
            
        }
        else{
            DispatchQueue.main.async {
                           cell.backgroundColor = UIColor.white
                       }
        }
        if issearching == true{
            cell.companyNameLbl.text = ((self.CustomArray.object(at: indexPath.row) as AnyObject).object(forKey: "company_name")as! String)
                              cell.cityNameLbl.text = ((self.CustomArray.object(at: indexPath.row) as AnyObject).object(forKey: "city")as! String)
            cell.companyNameLbl.setLineSpacing(lineSpacing: 5, lineHeightMultiple: 0)
                              return cell
        }else{
            cell.companyNameLbl.text = ((self.tableViewArrayForMemberDirectory.object(at: indexPath.row) as AnyObject).object(forKey: "company_name")as! String)
                   cell.cityNameLbl.text = ((self.tableViewArrayForMemberDirectory.object(at: indexPath.row) as AnyObject).object(forKey: "city")as! String)
                   
            cell.companyNameLbl.setLineSpacing(lineSpacing: 5, lineHeightMultiple: 0)
                   return cell
        }
       
       }
    
    func loadActivityIndicator()
    {
        activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
        activityLoader?.label.text = "Loading";
        activityLoader?.detailsLabel.text = "Please Wait";
        activityLoader?.isUserInteractionEnabled = false;
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
                issearching = true
           if((searchBar.text?.count)! > 0)
           {
             let searchString = searchBar.text!
               searchCompanyName(resultString: searchString)
               
           }
       }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
//        issearching = true
        if(searchBar.text == ""){
             self.searchBar.endEditing(true)
        }
       
        
       
    }
func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//    issearching = true
    comingFromfilter = true
    
    if searchBar.text?.count == 1 && text == "" {
        
        DispatchQueue.main.async {
            self.CustomArray = self.coreDataArray.mutableCopy() as! NSMutableArray
            self.membershipTableview.isHidden = false
            self.membershipTableview.reloadData()
        }
    }
    else {
        if text == "" {
            let truncated = searchBar.text!.substring(to: searchBar.text!.index(before: searchBar.text!.endIndex))
            searchCompanyName(resultString: truncated)
        }
        else {
            
        }
    }
    return true
}
    func searchCompanyName(resultString : String) {
        
        CustomArray = []
        
        for i in 0..<coreDataArray.count {
            
            let result = coreDataArray[i] as AnyObject
            
            let currentString = (result.value(forKey: "company_name") as! String)
            
            if currentString.localizedCaseInsensitiveContains(resultString) == true
            {
                print(currentString)
                CustomArray.add(result)
            }
        }
        
        if CustomArray.count == 0
        {
            membershipTableview.isHidden = true
            
        }
        else
        {
            membershipTableview.isHidden = false
            if self.CustomArray.count > 0 {
                self.membershipTableview.reloadData()
            }
        }
    }

    @IBAction func memberRegistration(_ sender: Any) {
        
            memberListLabel.isHidden = true
            membershipRegistrationLabel.isHidden = false
//        let vc = storyboard?.instantiateViewController(withIdentifier: "MemberRegistrationViewController") as! MemberRegistrationViewController
//        navigationController?.pushViewController(vc, animated: true)
        
        let controller:MemberRegistrationViewController =
               self.storyboard!.instantiateViewController(withIdentifier: "MemberRegistrationViewController") as!
               MemberRegistrationViewController

               let statusbarHeight = UIApplication.shared.statusBarFrame.height

               
               let tabBarHeight = self.tabBarController!.tabBar.frame.size.height+50;

               
               
               controller.view.frame = CGRect(x: 0, y: statusbarHeight+95, width: UIScreen.main.bounds.size.width, height: (UIScreen.main.bounds.size.height-tabBarHeight))
               controller.willMove(toParent: self)
               self.view.addSubview(controller.view)
               self.addChild(controller)
               controller.didMove(toParent: self)
        
        
    }
    
    @IBAction func memberList(_ sender: Any) {
        memberListLabel.isHidden = false
        membershipRegistrationLabel.isHidden = true
        
        if CheckInternet.isConnectedToNetwork() == true{
                                    flag = false
                                               searchBar.isHidden = false
                                               filterLabel.text = "FILTER"
                                               companyNameAndCityView.isHidden = false
                                               membershipTableview.isHidden = false
                                               selectRegionTextField.isHidden = true
                                               if CheckInternet.isConnectedToNetwork() == true{
                                                   loadActivityIndicator()
                                                    getdata()
                                               }
                                }
        
        if self.children.count >= 0{
                  let viewControllers:[UIViewController] = self.children
                  for viewContoller in viewControllers{
                      viewContoller.willMove(toParent: nil)
                      viewContoller.view.removeFromSuperview()
                      viewContoller.removeFromParent()
                  }
              }

        
        
    }
    
    @IBAction func filter(_ sender: Any) {
       issearching = false
    
        if filterLabel.text == "CLEAR"{
//            flag = false
            searchBar.isHidden = false
            filterLabel.text = "FILTER"
            companyNameAndCityView.isHidden = false
            membershipTableview.isHidden = false
            selectRegionTextField.isHidden = true
            DispatchQueue.main.async {
                 if CheckInternet.isConnectedToNetwork() == true{
                    self.loadActivityIndicator()
                    let params = ["region_id" : ""]
                                      print(params)
                    self.requestToServer.connectToServer(myUrl: memberDirectoryUrl!, params: params as AnyObject)
                           }
            }
           
        }
        else if filterLabel.text == "FILTER" {
            selectRegionTextField.text = ""
//            flag = true
            searchBar.isHidden = true
            filterLabel.text = "CLEAR"
                   companyNameAndCityView.isHidden = true
                   membershipTableview.isHidden = true
                   selectRegionTextField.isHidden = false
                   selectRegionTextField.isUserInteractionEnabled = true
//
                   manageToolBar()
                   
//                   self.regionArray = []
//                  sortRegion()
//            print(regionArray)
//                          for i in 0..<self.coreDataArray.count {
//                              let region = ((self.coreDataArray.object(at: i) as AnyObject).value(forKey: "region_id") as! String).uppercased()
//                              if !self.regionArray.contains(region) && region != "" {
//                                  self.regionArray.add(region)
//                                  print("regionn \(region)")
//                              }
//                          }
                   
        }
        
       
       
        
    }
    

    func manageToolBar() {
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        //toolBar.isTranslucent = true
       toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)

        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(cancelPicker))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        selectRegionTextField.inputView = pickerView
        selectRegionTextField.inputAccessoryView = toolBar
//        selectYear.inputView = pickerView
//        selectYear.inputAccessoryView = toolBar
    }

    @objc func donePicker(){
        selectRegionTextField.resignFirstResponder()
    }
    @objc func cancelPicker(){
           
       }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
           return 1
       }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
//           searchBar.isHidden = true
//           cancelButton.isHidden = true
//           searchButton.isHidden = false
//           headingLabel.isHidden = false
           return regionArray.count
       }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return regionArray[row] as? String
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectRegionTextField.text = regionArray[row] as? String
    }
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//          self.view.endEditing(true)
//      }
    
    
    @IBAction func apply(_ sender: Any) {
//        print(selectRegionTextField.text)
        if selectRegionTextField.text == ""{
            let alert = UIAlertController(title: "", message: "select region should not be empty", preferredStyle: UIAlertController.Style.alert)
            self.present(alert, animated: true, completion: nil)
            
            let when = DispatchTime.now() + 2
            DispatchQueue.main.asyncAfter(deadline: when){
              // your code with delay
              alert.dismiss(animated: true, completion: nil)
            }
        }
        else{
            loadActivityIndicator()
            let params = ["region_id" : "\(selectRegionTextField.text!)"]
                   print(params)
                   requestToServer.connectToServer(myUrl: memberDirectoryUrl!, params: params as AnyObject)
                  donePicker()
        }
       
        
    }
    func dataReceivedFromServer(data: NSDictionary, url: URL) {
        
        if ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "true" {
            let temporaryArray = ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "Result") as! NSArray)
                DispatchQueue.main.async {
                        self.tableViewArrayForMemberDirectory = temporaryArray.mutableCopy() as! NSMutableArray
                        self.coreDataArray = temporaryArray.mutableCopy() as! NSMutableArray
            //                                    self.saveCoreData()
                                                self.membershipTableview.reloadData()
                                                self.activityLoader?.hide(animated:true)
                                                self.view.isUserInteractionEnabled = true
                    self.membershipTableview.isHidden = false
                    self.selectRegionTextField.isHidden = true
                    self.companyNameAndCityView.isHidden = false
                                            }
          
        }
    }
    
    
    @IBAction func notification(_ sender: Any) {





        let data:[AnyObject]? = UserDefaults.standard.value(forKey: "NotificationList") as? [AnyObject]
        print(data)

        if  data != nil {



            DispatchQueue.main.async {


                let view1 = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController

                view1?.dataArray = (UserDefaults.standard.value(forKey: "NotificationList") as? [AnyObject])!

                view1?.globalArray = data!.reversed()
                self.navigationController?.pushViewController(view1!, animated: true)
            }


        }
        else
        {




            DispatchQueue.main.async {

                let view1 = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController

                view1?.dataArray = []

                view1?.globalArray = []
                self.navigationController?.pushViewController(view1!, animated: true)
            }
        }






    }
    
}
