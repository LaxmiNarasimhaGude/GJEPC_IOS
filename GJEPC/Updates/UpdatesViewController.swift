//
//  UpdatesViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 15/01/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD

class UpdatesViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UISearchBarDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar!
    var arrayResult : NSMutableArray = []
    var arrayTableView : NSMutableArray = []
    var startIndex = Int()
    var totalIndex = Int()
    var gettingData = false
    var activityLoader : MBProgressHUD? = nil
     var type = "true"
   
    

    @IBOutlet weak var updatesCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
             searchBar.resignFirstResponder()
             searchBar.delegate = self
              searchBar.layer.cornerRadius = 8
               searchBar.clipsToBounds = true
               searchBar.layer.borderWidth = 1
               searchBar.layer.borderColor = UIColor.gray.cgColor
        updatesCollectionView.keyboardDismissMode = .onDrag
        
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        updatesCollectionView.collectionViewLayout = layout
        
        
        updatesCollectionView.delegate = self
        updatesCollectionView.dataSource = self
        
self.checkForInternet()
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
           return arrayTableView.count
       }
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout:

           UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

           let width = UIScreen.main.bounds.size.width
          
           return CGSize(width: ((width / 2) - 15)   , height: 180)



       }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            let dict = self.arrayTableView.object(at: indexPath.row) as? NSDictionary
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewsWebViewController") as! NewsWebViewController
//            vc.web = webType.upload
            vc.dictUpload = dict!
            vc.newstitle = "Updates"
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        print(arrayTableView)
        let cell = updatesCollectionView.dequeueReusableCell(withReuseIdentifier: "UpdatesCollectionViewCell", for: indexPath) as! UpdatesCollectionViewCell
        
                        cell.dateLbl.startShimmeringEffect()
                          cell.updateDescriptionLbl.startShimmeringEffect()
        cell.defaultImage.startShimmeringEffect()
//                          cell.newsImage.startShimmeringEffect()
//                          cell.newsDescriptionLbl.startShimmeringEffect()
        
         cell.contentView.layer.cornerRadius = 2.0
         cell.contentView.layer.borderWidth = 1.0
         cell.contentView.layer.borderColor = UIColor.clear.cgColor
         cell.contentView.layer.masksToBounds = true
        
         cell.layer.backgroundColor = UIColor.white.cgColor
         cell.layer.shadowColor = UIColor.lightGray.cgColor
         cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)//CGSizeMake(0, 2.0);
         cell.layer.shadowRadius = 2.0
         cell.layer.shadowOpacity = 1.0
         cell.layer.masksToBounds = false
         cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
        
        let dateFormatterGet = DateFormatter()
                           dateFormatterGet.dateFormat = "yyyy-MM-dd"

                           let dateFormatterPrint = DateFormatter()
                           dateFormatterPrint.dateFormat = "dd MMM, yyyy"

                           let date: NSDate? = dateFormatterGet.date(from: "\((self.arrayTableView.object(at: indexPath.row) as AnyObject).object(forKey: "post_date")as! String)") as NSDate?
                           print(dateFormatterPrint.string(from: date! as Date))
        
        if type == "false"{
             cell.dateLbl.text = "\(dateFormatterPrint.string(from: date! as Date))"
                    

                    cell.updateDescriptionLbl.text = (((self.arrayTableView.object(at: indexPath.row) as! NSDictionary)["title"]) as! String)
                    cell.updateDescriptionLbl.labelSpacing(label: cell.updateDescriptionLbl, font: "Book")
            cell.dateLbl.layer.sublayers = nil
            cell.updateDescriptionLbl.layer.sublayers = nil
            cell.defaultImage.layer.sublayers = nil
        }
        
       
        return cell
       }
    
    func checkForInternet() {


         if CheckInternet.isConnectedToNetwork(){
             print("Internet Connection Available!")
             
             self.updatesCollectionView.isHidden = false
                         DispatchQueue.main.async {
//                             self.loadActivityIndicator()
                         }
                     self.gettingDatafromServer()
         }
         else{
             print("Internet Connection not Available!")
                         updatesCollectionView.isHidden = true
//                         activityLoader?.hide(animated: true)
         }
         
     }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == ""{
       checkForInternet()
        }
    }
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
               return true;
           }
           func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
               self.searchBar.endEditing(true)
//               searchBar.isHidden = true
//               searchButton.isHidden = false
//               label_Update.isHidden = false
//               cancelButton.isHidden = true
           }
           func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
               searchBar.text = ""
               self.searchBar.endEditing(true)
               self.startIndex = 0
               self.totalIndex = 10
               self.gettingDatafromServer()
           }
           func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
               if text == "\n" {
                   return true
               }
               var params: NSMutableDictionary = ["title" : searchBar.text! + text,
                                                  "start" : "0" ,
                                                  "limit" : "4"]
               if searchBar.text?.count == 1 && text == "" {
                   self.startIndex = 0
                   self.totalIndex = 10
                   self.gettingDatafromServer()
                   return true;
               } else if text == "" {
                   let name: String = searchBar.text!
                   let truncated = name.substring(to: name.index(before: name.endIndex))
                   params = ["title" : truncated,
                             "start" : "0" ,
                             "limit" : "4"]
               }
               DispatchQueue.main.async {
                   guard CheckInternet.isConnectedToNetwork() else
                   {
                       self.updatesCollectionView.isHidden = true
//                       self.activityLoader?.hide(animated: true)
                       return
                   }
                   self.updatesCollectionView.isHidden = false
                   callWebService(url: upload_vc_searchURL, httpMethod: "POST", params: params, completion: { (responseDict) in
                       print(responseDict)
                       let resultArray = responseDict["Result"]
                       self.arrayResult = resultArray as! NSMutableArray
                       guard self.arrayResult.count > 0 else {
                           DispatchQueue.main.async {
                            self.updatesCollectionView.isHidden = true
                            self.updatesCollectionView.backgroundView?.isHidden = true
//                               self.noDataLabel.isHidden = false
                           }
                           return
                       }
                       self.gettingData = true
                       self.arrayTableView = self.arrayResult
                       DispatchQueue.main.async {
//                            self.updatesCollectionView.backgroundView?.isHidden  = false
//                           self.noDataLabel.isHidden = true
//                           self.activityLoader?.hide(animated: true)
                           self.updatesCollectionView.reloadData()
                       }
                   })
               }
               return true
           }
           
    
//    func loadActivityIndicator() {
//        activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
//        activityLoader?.label.text = "Loading";
//        activityLoader?.detailsLabel.text = "Please Wait";
//        activityLoader?.isUserInteractionEnabled = false;
//    }

        func gettingDatafromServer() {
            
            let params: NSMutableDictionary = [ "start":"\(self.startIndex)",
                "limit":"100"]
            callWebService(url: upload_vc_URL, httpMethod: "POST", params: params, completion: ({ (responseDict) in
                //print(responseDict)
                let resultArray = responseDict["Result"] as! NSMutableArray
                self.arrayResult = resultArray
                
                if(resultArray.count == 0)
                {
                    
                    
                    
                    DispatchQueue.main.async {
//                                            self.contentView.isHidden = true
//                                            self.noDataLabel.isHidden = false
                                        }
                                        return
                }
               else
                {
                    
                }
    //            guard resultArray.count > 0 else {
    //                DispatchQueue.main.async {
    //                    self.contentView.isHidden = true
    //                    self.noDataLabel.isHidden = false
    //                }
    //                return
    //            }
                
              
                
                print(self.arrayTableView)
                print(self.arrayResult)
                DispatchQueue.main.async {
//                    self.contentView.isHidden = false
//                    self.noDataLabel.isHidden = true
//                    self.activityLoader?.hide(animated: true)
                    
                    self.gettingData = true
                    self.arrayTableView = self.arrayResult
                    self.updatesCollectionView.reloadData()
                    DispatchQueue.main.asyncAfter(deadline: .now()+1.0) {
                                                       self.changeImage()
                                                   }
                }
                print(self.arrayTableView)
            }))
            }

    @objc func changeImage()  {
           self.type = "false"
           self.updatesCollectionView.reloadData()
       }
}
