//
//  UpdatesCollectionViewCell.swift
//  GJEPC
//
//  Created by Kwebmaker on 15/01/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit

class UpdatesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var updateDescriptionLbl: UILabel!
    @IBOutlet weak var defaultImage: UIImageView!
    
}
