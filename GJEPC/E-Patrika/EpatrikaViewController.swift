//
//  EpatrikaViewController.swift
//  GJEPC
//
//  Created by Kwebmaker on 16/01/2020.
//  Copyright © 2020 Kwebmaker. All rights reserved.
//

import UIKit
import MBProgressHUD
class EpatrikaViewController: UIViewController,dataReceivedFromServerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UISearchBarDelegate {
   
    
    

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var epatrikaCollectionView: UICollectionView!
    
    var callUrls : URL!
    var customArray : NSMutableArray = []
    var loading = false;
    var loadNew = true;
    var requestToServer = RequestToServer()
    var temporaryArray : NSArray = []
    var tableViewArrayForNews : NSMutableArray = []
    var isSearching = false;
    var type = "true"
//    var activityLoader : MBProgressHUD? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
//        searchBar.resignFirstResponder()
        searchBar.text = ""
        searchBar.delegate = self
        searchBar.layer.cornerRadius = 8
        searchBar.clipsToBounds = true
        searchBar.layer.borderWidth = 1
        searchBar.layer.borderColor = UIColor.gray.cgColor

        epatrikaCollectionView.keyboardDismissMode = .onDrag
          
//        searchBar.delegate = self
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        epatrikaCollectionView.collectionViewLayout = layout
        
        
        requestToServer.delegate = self
        
        epatrikaCollectionView.delegate = self
        epatrikaCollectionView.dataSource = self
        onFirstTimeLoad()
    }
//    override func viewDidAppear(_ animated: Bool) {
//         checkForInternet()
//    }
//    func loadActivityIndicator()
//    {
//        activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
//        activityLoader?.label.text = "Loading";
//        activityLoader?.detailsLabel.text = "Please Wait";
//        activityLoader?.isUserInteractionEnabled = false;
//    }
    
    func checkForInternet() {
        if CheckInternet.isConnectedToNetwork() == true
        {
            print("internet available")
            self.epatrikaCollectionView.isHidden = false
            DispatchQueue.main.async {
//                    self.loadActivityIndicator()
            }
            onFirstTimeLoad()
        }
        else
        {
            print("no internet")
            epatrikaCollectionView.isHidden = true
//            activityLoader?.hide(animated: true)
        }
    }

    func onFirstTimeLoad() {
           customArray = [];
           callUrls = epatrikaUrl
           let params = [ "start":"0",
                          "limit":"10"]
           self.loading = true;
              loadNew = true;
           requestToServer.connectToServer(myUrl: callUrls as URL, params: params as AnyObject)
       }
    func dataReceivedFromServer(data: NSDictionary, url: URL) {
    loading = false;
    if ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "true" {
        temporaryArray = ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "Result") as! NSArray)
        print(temporaryArray)
        print(tableViewArrayForNews)
        let arrayCount = self.tableViewArrayForNews.count
        var indexArray : [IndexPath] = [IndexPath]()
        if temporaryArray.count > 0 {
            
            if self.tableViewArrayForNews.count != 0 && loadNew == false  && isSearching == false{
                for i in 0..<temporaryArray.count {
                    indexArray.append(IndexPath(row: arrayCount + i, section: 0));
                    self.tableViewArrayForNews.add(temporaryArray[i]);
                    }
            }
                else {
                    tableViewArrayForNews = (temporaryArray.mutableCopy() as! NSMutableArray)
                    customArray = [];
                    for i in 0..<tableViewArrayForNews.count {
                        
                        let yearString = ((tableViewArrayForNews.object(at: i) as! NSDictionary).value(forKey: "year") as! String)
                        if !customArray.contains(yearString) {
                            customArray.add(yearString)
                        }
                    }
                    
                    for i in 0..<customArray.count {
                        let currentKey = (customArray.object(at: i) as! String)
                        let tempArray : NSMutableArray = []
                        for j in 0..<tableViewArrayForNews.count {
                            if ((tableViewArrayForNews.object(at: j) as! NSDictionary).value(forKey: "year") as! String) == currentKey {
                                let temporaryDict = (self.tableViewArrayForNews.object(at: j) as! NSDictionary)
                                tempArray.add(temporaryDict.mutableCopy())
                            }
                        }
                        customArray.replaceObject(at: i, with: ["sectionName" : currentKey, "Result" : tempArray])
                    }
                    DispatchQueue.main.async {
                        self.epatrikaCollectionView.isHidden = false
//                        self.noDataLabel.isHidden = true
//                        self.activityLoader?.hide(animated: true)
                        
                        self.epatrikaCollectionView.reloadData()
                        DispatchQueue.main.asyncAfter(deadline: .now()+1.0) {
                            self.changeImage()
                        }
                    }
                }
            }
        else
        {
//            if searchBtnTapped != true
//            {
                DispatchQueue.main.async {
//                  self.noDataLabel.isHidden = false
                  self.epatrikaCollectionView.isHidden = true
                    }
//                }
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tableViewArrayForNews.count
       }
       
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let newsContentvc = self.storyboard?.instantiateViewController(withIdentifier: "NewsWebViewController") as! NewsWebViewController

          let dict = self.tableViewArrayForNews.object(at: indexPath.row) as? NSDictionary
                
//                let newsContentvc = self.storyboard?.instantiateViewController(withIdentifier: "NewsWebViewController") as! NewsWebViewController
        
                newsContentvc.newstitle = "epatrika"
                newsContentvc.dictUpload = dict!
                DispatchQueue.main.async {
                    self.navigationController?.pushViewController(newsContentvc, animated: true)
                }
    }
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = epatrikaCollectionView.dequeueReusableCell(withReuseIdentifier: "EpatrikaCollectionViewCell", for: indexPath) as! EpatrikaCollectionViewCell
        
        cell.dateLbl.startShimmeringEffect()
        cell.descriptionLbl.startShimmeringEffect()
        cell.defaultImage.startShimmeringEffect()
        cell.pdfImage.startShimmeringEffect()
        
                cell.contentView.layer.cornerRadius = 2.0
                cell.contentView.layer.borderWidth = 1.0
                cell.contentView.layer.borderColor = UIColor.clear.cgColor
                cell.contentView.layer.masksToBounds = true
               
                cell.layer.backgroundColor = UIColor.white.cgColor
                cell.layer.shadowColor = UIColor.lightGray.cgColor
                cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)//CGSizeMake(0, 2.0);
                cell.layer.shadowRadius = 2.0
                cell.layer.shadowOpacity = 1.0
                cell.layer.masksToBounds = false
                cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
         if type == "false"{
        cell.dateLbl.text = ((self.tableViewArrayForNews.object(at: indexPath.row) as AnyObject).object(forKey: "year")as! String)
        cell.descriptionLbl.text = ((self.tableViewArrayForNews.object(at: indexPath.row) as AnyObject).object(forKey: "name") as! String)
        cell.descriptionLbl.labelSpacing(label: cell.descriptionLbl, font: "Book")
            
            cell.dateLbl.layer.sublayers = nil
                       cell.descriptionLbl.layer.sublayers = nil
                       cell.defaultImage.layer.sublayers = nil
            cell.pdfImage.layer.sublayers = nil
        }
        
        return cell
       }
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout:

                 UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

                 let width = UIScreen.main.bounds.size.width
                
                 return CGSize(width: ((width / 2) - 15)   , height: 180)



             }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
               isSearching = true;
               return true;
           }
           
           func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
//               self.searchBar.endEditing(true)
//               searchBtnTapped = true
//               searchBar.isHidden = true
//               searchButton.isHidden = false
//               newslabel.isHidden = false
//               cancelButton.isHidden = true
           }
           
           func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
               onFirstTimeLoad()
//               inSearchMode = false
               searchBar.text = ""
           }
       
           func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
               print(searchBar.text! + text)
               
               if text == "\n" {
                   return true
               }
               var params = ["name" : searchBar.text! + text,
                             "start" : "0" ,
                             "limit" : "10"]
               if searchBar.text?.count == 1 && text == "" {
                   onFirstTimeLoad()
                   return true;
               }else if text == "" {
                   let name: String = searchBar.text!
                   let truncated = name.substring(to: name.index(before: name.endIndex))
                   params = ["name" : truncated];
               }
               
               DispatchQueue.main.async {
                   self.callUrls = epatrikaSearchUrl
                   self.loading = true
                   self.loadNew = true;
                   self.requestToServer.connectToServer(myUrl: self.callUrls as URL, params: params as AnyObject)
                   }
               return true
           }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == ""{
            onFirstTimeLoad()
        }
    }
    
    @objc func changeImage()  {
        self.type = "false"
        self.epatrikaCollectionView.reloadData()
    }
}
